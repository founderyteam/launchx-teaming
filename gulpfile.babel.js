import gulp from 'gulp'
import bro from 'gulp-bro'
import babelify from 'babelify'
import uglifyify from 'uglifyify'
import sourcemapify from 'sourcemapify'
import babel from 'gulp-babel'
import pump from 'pump'
import uglify from 'gulp-uglify'
import sourcemaps from 'gulp-sourcemaps'
import concat from 'gulp-concat'
import * as path from 'path'

gulp.task('build-browser', () => {
  return gulp.src('./src/*.js')
    .pipe(
      bro({
        transform: [
          babelify.configure({ presets: ['env'] }),
          [
            'uglifyify', { global: true },
            'sourcemapify', {base: 'src'}
          ]
        ]
      })
    )
    .pipe(gulp.dest('dist'))
})

gulp.task('build-node', (cb) => {
  pump([
    gulp.src('./src/*.js'),
    sourcemaps.init(),
    babel({presets: ['env']}),
    sourcemaps.write(null, { sourceRoot: path.join(__dirname, 'src') }),
    gulp.dest('lib')

  ], cb)
})

gulp.task('build', ['build-browser', 'build-node'])
