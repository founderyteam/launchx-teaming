require('./babel-require-hook'); matchFile = require('./src/match'); Match = matchFile.default; generateRandomStudents = matchFile.generateRandomStudents; generateRandomIdeas = matchFile.generateRandomIdeas;
ideas = generateRandomIdeas(290); students = generateRandomStudents(58, ideas);

const Student = require('./src/student').default

ideas = ideas.map(idea => {
  const id = idea
  id.numVotes = students.reduce((old, curr) => old + new Student(curr).ideas.find(ideaToFind => ideaToFind === idea).vote, 0)
  return id
})

matcher = new Match(students)
teams = Array.from(matcher.team(true))
let faults = Match.test(teams)
console.log("Fault Count: ", faults)
console.log(teams.reduce((prev, curr) => prev + curr.score(), 0)/teams.length)
//teams.forEach(team => console.log(team.idea))

// const s = new Student(students[0])
// const s2 = new Student(students[1])

// console.log(s.isEqual(s2))
// console.log(s.isEqual(s))