/* eslint-disable no-console */
// require('./babel-require-hook');
const fs = require('fs');
const matchFile = require('./lib/match');
const DPMatch = require('./lib/dp-matcher').default;
const OPTMatch = require('./lib/opt-matcher').default;
const KKMatchReverse = require('./lib/kk-reverse-matcher').default;
const GreedyMatch = require('./lib/greedy-matcher').default;
const Student = require('./lib/student').default;
const Team = require('./lib/team').default;
const { defaultTeamingConfig } = require('./lib/student');

const Match = matchFile.default;
const { generateRandomStudents, generateRandomIdeas } = matchFile;
const names = [];
for (let i = 0; i < 59; i += 1) {
  names[i] = Math.random().toString(36).substr(2, 7);
}
const ideas = generateRandomIdeas(291, names);
const students = generateRandomStudents(93, ideas, names);
// const { students, ideas } = JSON.parse(fs.readFileSync('./test-data.json'));
// console.log(JSON.stringify(students[0]))
//
fs.writeFileSync('./test-data.json', JSON.stringify({ students, ideas }));


// const Student = require('./src/student').default;

// ideas = ideas.map((idea) => {
//   const id = idea;
//   id.numVotes = students.reduce(
//     (old, curr) => old + new Student(curr).ideas.find(ideaToFind => ideaToFind === idea).vote,
//     0,
//   );
//   return id;
// });

// randomly match into lowest amount of teams
const lowestNumberOfTeams = 89 / 4;
let randomTeamsArr = [];
for (let i = 0; i < lowestNumberOfTeams; i += 1) { randomTeamsArr.push([]); }
students
  .map(student => new Student(student, defaultTeamingConfig))
  .forEach((student) => {
    let randomTeam = Math.floor(Math.random() * randomTeamsArr.length);
    while (randomTeamsArr[randomTeam].length >= 4) {
      randomTeam = Math.floor(Math.random() * randomTeamsArr.length);
    }

    randomTeamsArr[randomTeam].push(student);
  });

randomTeamsArr = randomTeamsArr.map(team => new Team(team));

const matcher = new KKMatchReverse(students, defaultTeamingConfig, true);
const teams = Array.from(matcher.team({}));

console.log(`num teams ${teams.length}`);
const num3Teams = teams.reduce((count, team) => {
  if (team.students.length === 3) { return count + 1; } return count;
}, 0);
console.log(`num 3 teams ${num3Teams}`);
const num4Teams = teams.reduce((count, team) => {
  if (team.students.length === 4) { return count + 1; } return count;
}, 0);
console.log(`num 4 teams ${num4Teams}`);

const teamSizeArr = Object.keys(teams.reduce((mult, team) => {
  mult[team.students.length] = true;
  return mult;
}, {}));
console.log(`teams: ${teamSizeArr.join(' ')}`);
// const faults = Match.test(teams);
// console.log('Fault Count: ', faults);
console.log(teams.reduce((prev, curr) => prev + curr.score(), 0) / teams.length);
console.log(randomTeamsArr.reduce((prev, curr) => prev + curr.score(), 0) / randomTeamsArr.length);
console.log('best ideas for first team:');
for (let i = 0; i < teams[0].getTopIdeas(ideas).length; i += 1) {
  process.stdout.write(`${teams[0].getTopIdeas(ideas)[i]} `);
}
process.stdout.write('\n');
console.log('ideas for first team member:');
for (let i = 0; i < teams[0].getStudentOwnIdeas(ideas)[1].length; i += 1) {
  process.stdout.write(`${teams[0].getStudentOwnIdeas(ideas)[1][i]} `);
}
process.stdout.write('\n');
// teams.forEach(team => console.log(team.idea))

// const s = new Student(students[0])
// const s2 = new Student(students[1])

// console.log(s.isEqual(s2))
// console.log(s.isEqual(s))
