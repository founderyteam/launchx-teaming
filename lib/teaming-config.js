'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
// const config = {
//   studentsPerTeam: {
//     name: 'Students Per Team',
//     default: 4.0,
//   },
//   minimumStudentsPerTeam: {
//     name: 'Minimum Students Per Team',
//     default: 3.0,
//   },
//   maximumStudentsPerTeam: {
//     name: 'Maximum Students Per Team',
//     default: 4.0,
//   },
//   learningStyle: {
//     name: 'Learning Style',
//     default: 5.0,
//   },
//   skillsAbilityPoints: {
//     name: 'Skill Ability Points (Programming, Marketing, etc)',
//     default: 3.0,
//   },
//   interestPoints: {
//     name: 'Interest Overlap Points',
//     default: 5.0,
//   },
//   interestPointsMax: {
//     name: 'Interest Overlap Points Limit',
//     default: 15.0,
//   },
//   commonIdeaPoints: {
//     name: 'Common Idea Points',
//     default: 1.0,
//   },
//   workStyle: {
//     name: 'Work Style Complement Points',
//     default: 5.0,
//   },
//   EP10Type: {
//     name: 'EP10 Complement Points',
//     default: 5.0,
//   },
//   personalityType: {
//     name: 'Personality Complement Points',
//     default: 5.0,
//   },
//   entrepreneurPoints: {
//     name: 'Entrepreneurial Skills Points Per Complement',
//     default: 5.0,
//   },
//   skillsPoints: {
//     name: 'Skills Points Per Complement',
//     default: 5.0,
//   },
// };

var config = {
  studentsPerTeam: {
    name: 'Maximum Students Per Team',
    default: 4.0
  },
  minimumStudentsPerTeam: {
    name: 'Minimum Students Per Team',
    default: 3.0
  },
  balanceFactor: {
    name: 'Balance (Between 0 and 10) 0 means bigger teams',
    default: 3
  },
  rolePoints: {
    name: 'Points For Role Complement',
    default: 15.0
  },
  entrepreneurTypePoints: {
    name: 'Points for Entrepreneur Type Complement',
    default: 5.0
  },
  skillsPoints: {
    name: 'Points for Skills Complement',
    default: 10.0
  },
  learningStylePoints: {
    name: 'Learning Style Overlap',
    default: 5.0
  },
  workStylePoints: {
    name: 'Points for Work Style Complement',
    default: 5.0
  },
  personalityPoints: {
    name: 'Points for Personality Complement',
    default: 8.0
  },
  genderPoints: {
    name: 'Points for Gender Complement',
    default: 10.0
  },
  ethnicityPoints: {
    name: 'Points for Ethnicity Complement',
    default: 10.0
  },
  interestPoints: {
    name: 'Points for Interests Overlap',
    default: 12.0
  },
  ideaPoints: {
    name: 'Points for Idea Overlap',
    default: 40.0
  }
};

exports.default = config;
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRlYW1pbmctY29uZmlnLmpzIl0sIm5hbWVzIjpbImNvbmZpZyIsInN0dWRlbnRzUGVyVGVhbSIsIm5hbWUiLCJkZWZhdWx0IiwibWluaW11bVN0dWRlbnRzUGVyVGVhbSIsImJhbGFuY2VGYWN0b3IiLCJyb2xlUG9pbnRzIiwiZW50cmVwcmVuZXVyVHlwZVBvaW50cyIsInNraWxsc1BvaW50cyIsImxlYXJuaW5nU3R5bGVQb2ludHMiLCJ3b3JrU3R5bGVQb2ludHMiLCJwZXJzb25hbGl0eVBvaW50cyIsImdlbmRlclBvaW50cyIsImV0aG5pY2l0eVBvaW50cyIsImludGVyZXN0UG9pbnRzIiwiaWRlYVBvaW50cyJdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUEsSUFBTUEsU0FBUztBQUNiQyxtQkFBaUI7QUFDZkMsVUFBTSwyQkFEUztBQUVmQyxhQUFTO0FBRk0sR0FESjtBQUtiQywwQkFBd0I7QUFDdEJGLFVBQU0sMkJBRGdCO0FBRXRCQyxhQUFTO0FBRmEsR0FMWDtBQVNiRSxpQkFBZTtBQUNiSCxVQUFNLGlEQURPO0FBRWJDLGFBQVM7QUFGSSxHQVRGO0FBYWJHLGNBQVk7QUFDVkosVUFBTSw0QkFESTtBQUVWQyxhQUFTO0FBRkMsR0FiQztBQWlCYkksMEJBQXdCO0FBQ3RCTCxVQUFNLHlDQURnQjtBQUV0QkMsYUFBUztBQUZhLEdBakJYO0FBcUJiSyxnQkFBYztBQUNaTixVQUFNLDhCQURNO0FBRVpDLGFBQVM7QUFGRyxHQXJCRDtBQXlCYk0sdUJBQXFCO0FBQ25CUCxVQUFNLHdCQURhO0FBRW5CQyxhQUFTO0FBRlUsR0F6QlI7QUE2QmJPLG1CQUFpQjtBQUNmUixVQUFNLGtDQURTO0FBRWZDLGFBQVM7QUFGTSxHQTdCSjtBQWlDYlEscUJBQW1CO0FBQ2pCVCxVQUFNLG1DQURXO0FBRWpCQyxhQUFTO0FBRlEsR0FqQ047QUFxQ2JTLGdCQUFjO0FBQ1pWLFVBQU0sOEJBRE07QUFFWkMsYUFBUztBQUZHLEdBckNEO0FBeUNiVSxtQkFBaUI7QUFDZlgsVUFBTSxpQ0FEUztBQUVmQyxhQUFTO0FBRk0sR0F6Q0o7QUE2Q2JXLGtCQUFnQjtBQUNkWixVQUFNLDhCQURRO0FBRWRDLGFBQVM7QUFGSyxHQTdDSDtBQWlEYlksY0FBWTtBQUNWYixVQUFNLHlCQURJO0FBRVZDLGFBQVM7QUFGQztBQWpEQyxDQUFmOztrQkF1RGVILE0iLCJmaWxlIjoidGVhbWluZy1jb25maWcuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBjb25zdCBjb25maWcgPSB7XG4vLyAgIHN0dWRlbnRzUGVyVGVhbToge1xuLy8gICAgIG5hbWU6ICdTdHVkZW50cyBQZXIgVGVhbScsXG4vLyAgICAgZGVmYXVsdDogNC4wLFxuLy8gICB9LFxuLy8gICBtaW5pbXVtU3R1ZGVudHNQZXJUZWFtOiB7XG4vLyAgICAgbmFtZTogJ01pbmltdW0gU3R1ZGVudHMgUGVyIFRlYW0nLFxuLy8gICAgIGRlZmF1bHQ6IDMuMCxcbi8vICAgfSxcbi8vICAgbWF4aW11bVN0dWRlbnRzUGVyVGVhbToge1xuLy8gICAgIG5hbWU6ICdNYXhpbXVtIFN0dWRlbnRzIFBlciBUZWFtJyxcbi8vICAgICBkZWZhdWx0OiA0LjAsXG4vLyAgIH0sXG4vLyAgIGxlYXJuaW5nU3R5bGU6IHtcbi8vICAgICBuYW1lOiAnTGVhcm5pbmcgU3R5bGUnLFxuLy8gICAgIGRlZmF1bHQ6IDUuMCxcbi8vICAgfSxcbi8vICAgc2tpbGxzQWJpbGl0eVBvaW50czoge1xuLy8gICAgIG5hbWU6ICdTa2lsbCBBYmlsaXR5IFBvaW50cyAoUHJvZ3JhbW1pbmcsIE1hcmtldGluZywgZXRjKScsXG4vLyAgICAgZGVmYXVsdDogMy4wLFxuLy8gICB9LFxuLy8gICBpbnRlcmVzdFBvaW50czoge1xuLy8gICAgIG5hbWU6ICdJbnRlcmVzdCBPdmVybGFwIFBvaW50cycsXG4vLyAgICAgZGVmYXVsdDogNS4wLFxuLy8gICB9LFxuLy8gICBpbnRlcmVzdFBvaW50c01heDoge1xuLy8gICAgIG5hbWU6ICdJbnRlcmVzdCBPdmVybGFwIFBvaW50cyBMaW1pdCcsXG4vLyAgICAgZGVmYXVsdDogMTUuMCxcbi8vICAgfSxcbi8vICAgY29tbW9uSWRlYVBvaW50czoge1xuLy8gICAgIG5hbWU6ICdDb21tb24gSWRlYSBQb2ludHMnLFxuLy8gICAgIGRlZmF1bHQ6IDEuMCxcbi8vICAgfSxcbi8vICAgd29ya1N0eWxlOiB7XG4vLyAgICAgbmFtZTogJ1dvcmsgU3R5bGUgQ29tcGxlbWVudCBQb2ludHMnLFxuLy8gICAgIGRlZmF1bHQ6IDUuMCxcbi8vICAgfSxcbi8vICAgRVAxMFR5cGU6IHtcbi8vICAgICBuYW1lOiAnRVAxMCBDb21wbGVtZW50IFBvaW50cycsXG4vLyAgICAgZGVmYXVsdDogNS4wLFxuLy8gICB9LFxuLy8gICBwZXJzb25hbGl0eVR5cGU6IHtcbi8vICAgICBuYW1lOiAnUGVyc29uYWxpdHkgQ29tcGxlbWVudCBQb2ludHMnLFxuLy8gICAgIGRlZmF1bHQ6IDUuMCxcbi8vICAgfSxcbi8vICAgZW50cmVwcmVuZXVyUG9pbnRzOiB7XG4vLyAgICAgbmFtZTogJ0VudHJlcHJlbmV1cmlhbCBTa2lsbHMgUG9pbnRzIFBlciBDb21wbGVtZW50Jyxcbi8vICAgICBkZWZhdWx0OiA1LjAsXG4vLyAgIH0sXG4vLyAgIHNraWxsc1BvaW50czoge1xuLy8gICAgIG5hbWU6ICdTa2lsbHMgUG9pbnRzIFBlciBDb21wbGVtZW50Jyxcbi8vICAgICBkZWZhdWx0OiA1LjAsXG4vLyAgIH0sXG4vLyB9O1xuXG5jb25zdCBjb25maWcgPSB7XG4gIHN0dWRlbnRzUGVyVGVhbToge1xuICAgIG5hbWU6ICdNYXhpbXVtIFN0dWRlbnRzIFBlciBUZWFtJyxcbiAgICBkZWZhdWx0OiA0LjAsXG4gIH0sXG4gIG1pbmltdW1TdHVkZW50c1BlclRlYW06IHtcbiAgICBuYW1lOiAnTWluaW11bSBTdHVkZW50cyBQZXIgVGVhbScsXG4gICAgZGVmYXVsdDogMy4wLFxuICB9LFxuICBiYWxhbmNlRmFjdG9yOiB7XG4gICAgbmFtZTogJ0JhbGFuY2UgKEJldHdlZW4gMCBhbmQgMTApIDAgbWVhbnMgYmlnZ2VyIHRlYW1zJyxcbiAgICBkZWZhdWx0OiAzLFxuICB9LFxuICByb2xlUG9pbnRzOiB7XG4gICAgbmFtZTogJ1BvaW50cyBGb3IgUm9sZSBDb21wbGVtZW50JyxcbiAgICBkZWZhdWx0OiAxNS4wLFxuICB9LFxuICBlbnRyZXByZW5ldXJUeXBlUG9pbnRzOiB7XG4gICAgbmFtZTogJ1BvaW50cyBmb3IgRW50cmVwcmVuZXVyIFR5cGUgQ29tcGxlbWVudCcsXG4gICAgZGVmYXVsdDogNS4wLFxuICB9LFxuICBza2lsbHNQb2ludHM6IHtcbiAgICBuYW1lOiAnUG9pbnRzIGZvciBTa2lsbHMgQ29tcGxlbWVudCcsXG4gICAgZGVmYXVsdDogMTAuMCxcbiAgfSxcbiAgbGVhcm5pbmdTdHlsZVBvaW50czoge1xuICAgIG5hbWU6ICdMZWFybmluZyBTdHlsZSBPdmVybGFwJyxcbiAgICBkZWZhdWx0OiA1LjAsXG4gIH0sXG4gIHdvcmtTdHlsZVBvaW50czoge1xuICAgIG5hbWU6ICdQb2ludHMgZm9yIFdvcmsgU3R5bGUgQ29tcGxlbWVudCcsXG4gICAgZGVmYXVsdDogNS4wLFxuICB9LFxuICBwZXJzb25hbGl0eVBvaW50czoge1xuICAgIG5hbWU6ICdQb2ludHMgZm9yIFBlcnNvbmFsaXR5IENvbXBsZW1lbnQnLFxuICAgIGRlZmF1bHQ6IDguMCxcbiAgfSxcbiAgZ2VuZGVyUG9pbnRzOiB7XG4gICAgbmFtZTogJ1BvaW50cyBmb3IgR2VuZGVyIENvbXBsZW1lbnQnLFxuICAgIGRlZmF1bHQ6IDEwLjAsXG4gIH0sXG4gIGV0aG5pY2l0eVBvaW50czoge1xuICAgIG5hbWU6ICdQb2ludHMgZm9yIEV0aG5pY2l0eSBDb21wbGVtZW50JyxcbiAgICBkZWZhdWx0OiAxMC4wLFxuICB9LFxuICBpbnRlcmVzdFBvaW50czoge1xuICAgIG5hbWU6ICdQb2ludHMgZm9yIEludGVyZXN0cyBPdmVybGFwJyxcbiAgICBkZWZhdWx0OiAxMi4wLFxuICB9LFxuICBpZGVhUG9pbnRzOiB7XG4gICAgbmFtZTogJ1BvaW50cyBmb3IgSWRlYSBPdmVybGFwJyxcbiAgICBkZWZhdWx0OiA0MC4wLFxuICB9LFxufTtcblxuZXhwb3J0IGRlZmF1bHQgY29uZmlnO1xuIl0sInNvdXJjZVJvb3QiOiIvbW50L2MvVXNlcnMvcHVyZXUvRG9jdW1lbnRzL3Byb2plY3RzL2xhdW5jaHgtdGVhbWluZy9zcmMifQ==
