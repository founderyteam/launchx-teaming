'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DefaultConfig = exports.Config = exports.Student = exports.Match = exports.Team = undefined;

var _team = require('./team');

var _team2 = _interopRequireDefault(_team);

var _match = require('./match');

var _match2 = _interopRequireDefault(_match);

var _student = require('./student');

var _student2 = _interopRequireDefault(_student);

var _teamingConfig = require('./teaming-config');

var _teamingConfig2 = _interopRequireDefault(_teamingConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.Team = _team2.default;
exports.Match = _match2.default;
exports.Student = _student2.default;
exports.Config = _teamingConfig2.default;
exports.DefaultConfig = _student.defaultTeamingConfig;
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImluZGV4LmpzIl0sIm5hbWVzIjpbIlRlYW0iLCJNYXRjaCIsIlN0dWRlbnQiLCJDb25maWciLCJEZWZhdWx0Q29uZmlnIiwiZGVmYXVsdFRlYW1pbmdDb25maWciXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFBQTs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7Ozs7O1FBQ1FBLEksR0FBQUEsYztRQUFNQyxLLEdBQUFBLGU7UUFBT0MsTyxHQUFBQSxpQjtRQUFTQyxNLEdBQUFBLHVCO1FBQWdDQyxhLEdBQXhCQyw2QiIsImZpbGUiOiJpbmRleC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBUZWFtIGZyb20gJy4vdGVhbSdcbmltcG9ydCBNYXRjaCBmcm9tICcuL21hdGNoJ1xuaW1wb3J0IFN0dWRlbnQsIHtkZWZhdWx0VGVhbWluZ0NvbmZpZ30gZnJvbSAnLi9zdHVkZW50J1xuaW1wb3J0IENvbmZpZyBmcm9tICcuL3RlYW1pbmctY29uZmlnJ1xuZXhwb3J0IHtUZWFtLCBNYXRjaCwgU3R1ZGVudCwgQ29uZmlnLCBkZWZhdWx0VGVhbWluZ0NvbmZpZyBhcyBEZWZhdWx0Q29uZmlnfSJdLCJzb3VyY2VSb290IjoiL21udC9jL1VzZXJzL3B1cmV1L0RvY3VtZW50cy9wcm9qZWN0cy9sYXVuY2h4LXRlYW1pbmcvc3JjIn0=
