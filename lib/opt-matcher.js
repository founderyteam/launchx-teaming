'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); /* eslint-disable no-console */
/* eslint-disable no-continue */


var _student = require('./student');

var _student2 = _interopRequireDefault(_student);

var _team = require('./team');

var _team2 = _interopRequireDefault(_team);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// basically stars and bars
// we have students and some seperators for them
// we need an inital estimate tho

var OPTMatch = function () {
  // public
  function OPTMatch(students, teamingConfig) {
    var debug = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

    _classCallCheck(this, OPTMatch);

    this.students = students.map(function (student) {
      return new _student2.default(student, teamingConfig);
    });
    this.debug = debug;
    this.numVisited = 0;

    this.initializeAdjacencyMatrix();

    if (this.debug) {
      console.log('DPMatch: Removing duplicates...');
    }

    this.removeDuplicatesStudents();
  }

  _createClass(OPTMatch, [{
    key: 'getStudents',
    value: function getStudents() {
      return this.students;
    }
  }, {
    key: 'initializeAdjacencyMatrix',
    value: function initializeAdjacencyMatrix() {
      this.scoredAgainst = [];
      for (var i = 0; i < this.students.length; i += 1) {
        this.scoredAgainst.push([]);
        for (var j = 0; j < this.students.length; j += 1) {
          if (i != j) {
            this.scoredAgainst[i].push(this.students[i].scoreAgainst(this.students[j]));
          }
        }
      }
    }
  }, {
    key: 'team',
    value: function team(_ref) {
      var _ref$minimumStudentsP = _ref.minimumStudentsPerTeam,
          minimumStudentsPerTeam = _ref$minimumStudentsP === undefined ? 3 : _ref$minimumStudentsP,
          _ref$maximumStudentsP = _ref.maximumStudentsPerTeam,
          maximumStudentsPerTeam = _ref$maximumStudentsP === undefined ? 4 : _ref$maximumStudentsP;

      // gen perms
      // dp this shiz
      this.minimumStudentsPerTeam = minimumStudentsPerTeam;
      this.maximumStudentsPerTeam = maximumStudentsPerTeam;

      if (this.debug) {
        console.log('DPMatch: Generating initial path...');
      }
      // this.generateInitialSolutionPath();
      this.initializeTour();
      this.solutionPath = this.bestSolutionPath;

      if (this.debug) {
        console.log('DPMatch: teaming...');console.time('DPMatcher');
      }
      while (this.numVisited < this.students.length) {
        this.insertSelection(this.selectTour());
      }
      if (this.debug) {
        console.log('DPMatch: done...');console.timeEnd('DPMatcher');
      }

      if (this.debug) {
        console.log('DPMatch: generating teams from match...');
      }
      return this.pathToTeams(this.bestSolutionPath);
    }

    // private

  }, {
    key: 'removeDuplicatesStudents',
    value: function removeDuplicatesStudents() {
      var dupArray = {};
      this.getStudents().forEach(function (student) {
        dupArray[student.hash] = student;
      });

      this.students = Object.values(dupArray);
    }

    // generateInitialSolutionPath() {
    //   const initialNumberOfBars = Math.floor(this.getStudents().length / this.maximumStudentsPerTeam);
    //   this.bestSolutionPath = [];
    //   this.getStudents().forEach((student, index) => {
    //     this.bestSolutionPath.push({ type: 'student', student: index });
    //   });

    //   // shuffle
    //   for (let i = 0; i < 50; i += 1) {
    //     // swap two random indexes
    //     const i1 = Math.floor(Math.random() * this.getStudents().length) % this.getStudents().length;
    //     const i2 = Math.floor(Math.random() * this.getStudents().length) % this.getStudents().length;
    //     const temp = this.bestSolutionPath[i1];
    //     this.bestSolutionPath[i1] = this.bestSolutionPath[i2];
    //     this.bestSolutionPath[i2] = temp;
    //   }

    //   for (let i = 0; i < initialNumberOfBars; i += 1) {
    //     const index = ((i + 1) * this.maximumStudentsPerTeam) + i;
    //     const swapVal = Object.assign({}, this.bestSolutionPath[index]);
    //     this.bestSolutionPath[index] = { type: 'bar' };
    //     this.bestSolutionPath.push(swapVal);
    //   }

    //   // find out remainder
    //   const remainderStudents = this.getStudents().length % initialNumberOfBars;
    //   if (remainderStudents < this.minimumStudentsPerTeam) {
    //     let lastIndex = this.bestSolutionPath.length;
    //     for (let i = 0; i < remainderStudents; i += 1) {
    //       // move last bar back by remained students
    //       // find last bar index
    //       for (let j = lastIndex - 1; j > 0; j -= 1) {
    //         if (this.bestSolutionPath[j].type === 'bar') {
    //           lastIndex = j;
    //           break;
    //         }
    //       }

    //       for (let k = 0; k < this.minimumStudentsPerTeam - remainderStudents - i; k += 1) {
    //         this.bestSolutionPath = DPMatch.swap(this.bestSolutionPath, lastIndex, lastIndex - k - 1);
    //       }
    //     }
    //   }
    //   // this.bestSolutionPath.push({ type: 'bar' });
    // }

  }, {
    key: 'initializeTour',
    value: function initializeTour() {
      // start with a team of one
      this.visited = [];
      for (var i = 0; i < this.students.length; i += 1) {
        this.visited[i] = false;
      }

      this.bestSolutionPath = [[0]];
      this.numTeams = 1;
      this.visited[0] = true;
      // find optimal pairing

      var bestMatch = this.bestMatch(0);
      this.bestSolutionPath[0].push(bestMatch);
      this.numVisited = 2;
      this.visited[bestMatch] = true;
    }
  }, {
    key: 'insertSelection',
    value: function insertSelection(sel) {
      var _this = this;

      // find optimal team to insert
      var insertionPoint = -1;
      var replacementPoint = -1;
      var currentCost = 0;

      var testTeam = function testTeam(currentTeam, i, r) {
        var cost = _this.scoreTeam(currentTeam);
        if (cost > currentCost) {
          currentCost = cost;
          insertionPoint = i;
          replacementPoint = r;
        }
      };

      for (var i = 0; i < this.numTeams; i += 1) {
        var currentTeam = Array.from(this.bestSolutionPath[i]);

        if (currentTeam.length < this.maximumStudentsPerTeam) {
          currentTeam.push(sel);
          testTeam(currentTeam, i, -1);
        } else {
          // replace each and try
          for (var j = 0; j < currentTeam.length; j += 1) {
            var oldElement = currentTeam[j];
            currentTeam[j] = sel;
            testTeam(currentTeam, i, j);
            currentTeam[j] = oldElement;
          }

          // but if main team is better, push to new
          var cost = this.scoreTeam(currentTeam);
          if (cost >= currentCost && insertionPoint === i) {
            // skip
            insertionPoint = -1;
            replacementPoint = -1;
          }
        }
      }

      if (insertionPoint === -1) {
        if (this.numTeams >= Math.floor(this.students.length / this.minimumStudentsPerTeam)) {
          // add to random insert
          insertionPoint = 0;
          do {
            insertionPoint += 1;
          } while (this.bestSolutionPath[insertionPoint].length >= this.maximumStudentsPerTeam);
        } else {
          this.bestSolutionPath.push([sel]);
          this.numTeams += 1;
        }
      } else if (replacementPoint > -1) {
        var replacedElement = this.bestSolutionPath[insertionPoint][replacementPoint];
        this.bestSolutionPath[insertionPoint][replacementPoint] = sel;

        this.visited[replacedElement] = false;
        this.numVisited -= 1;
      } else {
        this.bestSolutionPath[insertionPoint].push(sel);
      }

      this.visited[sel] = true;
      this.numVisited += 1;
    }
  }, {
    key: 'selectTour',
    value: function selectTour() {
      var randomSelect = 0;
      do {
        randomSelect = Math.floor(Math.random() * this.students.length) % this.students.length;
      } while (this.visited[randomSelect]);

      return randomSelect;
    }
  }, {
    key: 'pathToTeams',
    value: function pathToTeams(solutionPath) {
      var _this2 = this;

      var teams = [];
      for (var i = 0; i < solutionPath.length; i += 1) {
        var team = solutionPath[i].map(function (j) {
          return _this2.students[j];
        });
        teams.push(new _team2.default(team));
      }

      return teams;
    }
  }, {
    key: 'calculateScore',
    value: function calculateScore(solutionPath, permLength) {
      var firstBarIndex = -1;
      var nextBarIndex = 0;

      var score = 0;
      var findFunc = function findFunc(v, i) {
        return i > nextBarIndex && v.type === 'bar';
      };
      while (nextBarIndex !== permLength) {
        firstBarIndex = nextBarIndex;
        nextBarIndex = solutionPath.findIndex(findFunc);
        if (nextBarIndex === -1 || nextBarIndex > permLength) {
          nextBarIndex = permLength;
        }

        var team = [];
        for (var i = firstBarIndex + 1; i < nextBarIndex; i += 1) {
          team.push(solutionPath[i].student);
        }

        score += this.scoreTeam(team);
      }

      return score;
    }
  }, {
    key: 'scoreTeam',
    value: function scoreTeam(team) {
      // pairwise team all members against each other
      if (team.length <= 1) {
        return 0;
      }

      var score = 0;
      var numPerms = 0;
      for (var i = 0; i < team.length; i += 1) {
        for (var j = i + 1; j < team.length; j += 1) {
          score += this.scoreStudents(team[i], team[j]);
          numPerms += 1;
        }
      }

      return score / numPerms;
    }
  }, {
    key: 'scoreStudents',
    value: function scoreStudents(a, b) {
      return this.scoredAgainst[a][b];
    }
  }, {
    key: 'bestMatch',
    value: function bestMatch(studentIndex) {
      var bestElement = -1;
      var bestScore = 0;
      for (var i = 0; i < this.students.length; i += 1) {
        if (i !== studentIndex) {
          var score = this.scoredAgainst[studentIndex][i];
          if (score > bestScore) {
            bestElement = i;
            bestScore = score;
          }
        }
      }

      return bestElement;
    }
  }]);

  return OPTMatch;
}();

exports.default = OPTMatch;
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm9wdC1tYXRjaGVyLmpzIl0sIm5hbWVzIjpbIk9QVE1hdGNoIiwic3R1ZGVudHMiLCJ0ZWFtaW5nQ29uZmlnIiwiZGVidWciLCJtYXAiLCJTdHVkZW50Iiwic3R1ZGVudCIsIm51bVZpc2l0ZWQiLCJpbml0aWFsaXplQWRqYWNlbmN5TWF0cml4IiwiY29uc29sZSIsImxvZyIsInJlbW92ZUR1cGxpY2F0ZXNTdHVkZW50cyIsInNjb3JlZEFnYWluc3QiLCJpIiwibGVuZ3RoIiwicHVzaCIsImoiLCJzY29yZUFnYWluc3QiLCJtaW5pbXVtU3R1ZGVudHNQZXJUZWFtIiwibWF4aW11bVN0dWRlbnRzUGVyVGVhbSIsImluaXRpYWxpemVUb3VyIiwic29sdXRpb25QYXRoIiwiYmVzdFNvbHV0aW9uUGF0aCIsInRpbWUiLCJpbnNlcnRTZWxlY3Rpb24iLCJzZWxlY3RUb3VyIiwidGltZUVuZCIsInBhdGhUb1RlYW1zIiwiZHVwQXJyYXkiLCJnZXRTdHVkZW50cyIsImZvckVhY2giLCJoYXNoIiwiT2JqZWN0IiwidmFsdWVzIiwidmlzaXRlZCIsIm51bVRlYW1zIiwiYmVzdE1hdGNoIiwic2VsIiwiaW5zZXJ0aW9uUG9pbnQiLCJyZXBsYWNlbWVudFBvaW50IiwiY3VycmVudENvc3QiLCJ0ZXN0VGVhbSIsImN1cnJlbnRUZWFtIiwiciIsImNvc3QiLCJzY29yZVRlYW0iLCJBcnJheSIsImZyb20iLCJvbGRFbGVtZW50IiwiTWF0aCIsImZsb29yIiwicmVwbGFjZWRFbGVtZW50IiwicmFuZG9tU2VsZWN0IiwicmFuZG9tIiwidGVhbXMiLCJ0ZWFtIiwiVGVhbSIsInBlcm1MZW5ndGgiLCJmaXJzdEJhckluZGV4IiwibmV4dEJhckluZGV4Iiwic2NvcmUiLCJmaW5kRnVuYyIsInYiLCJ0eXBlIiwiZmluZEluZGV4IiwibnVtUGVybXMiLCJzY29yZVN0dWRlbnRzIiwiYSIsImIiLCJzdHVkZW50SW5kZXgiLCJiZXN0RWxlbWVudCIsImJlc3RTY29yZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7O3FqQkFBQTtBQUNBOzs7QUFDQTs7OztBQUNBOzs7Ozs7OztBQUVBO0FBQ0E7QUFDQTs7SUFFcUJBLFE7QUFDbkI7QUFDQSxvQkFBWUMsUUFBWixFQUFzQkMsYUFBdEIsRUFBb0Q7QUFBQSxRQUFmQyxLQUFlLHVFQUFQLEtBQU87O0FBQUE7O0FBQ2xELFNBQUtGLFFBQUwsR0FBZ0JBLFNBQVNHLEdBQVQsQ0FBYTtBQUFBLGFBQVcsSUFBSUMsaUJBQUosQ0FBWUMsT0FBWixFQUFxQkosYUFBckIsQ0FBWDtBQUFBLEtBQWIsQ0FBaEI7QUFDQSxTQUFLQyxLQUFMLEdBQWFBLEtBQWI7QUFDQSxTQUFLSSxVQUFMLEdBQWtCLENBQWxCOztBQUVBLFNBQUtDLHlCQUFMOztBQUVBLFFBQUksS0FBS0wsS0FBVCxFQUFnQjtBQUFFTSxjQUFRQyxHQUFSLENBQVksaUNBQVo7QUFBaUQ7O0FBRW5FLFNBQUtDLHdCQUFMO0FBQ0Q7Ozs7a0NBRWE7QUFDWixhQUFPLEtBQUtWLFFBQVo7QUFDRDs7O2dEQUUyQjtBQUMxQixXQUFLVyxhQUFMLEdBQXFCLEVBQXJCO0FBQ0EsV0FBSyxJQUFJQyxJQUFJLENBQWIsRUFBZ0JBLElBQUksS0FBS1osUUFBTCxDQUFjYSxNQUFsQyxFQUEwQ0QsS0FBSyxDQUEvQyxFQUFrRDtBQUNoRCxhQUFLRCxhQUFMLENBQW1CRyxJQUFuQixDQUF3QixFQUF4QjtBQUNBLGFBQUssSUFBSUMsSUFBSSxDQUFiLEVBQWdCQSxJQUFJLEtBQUtmLFFBQUwsQ0FBY2EsTUFBbEMsRUFBMENFLEtBQUssQ0FBL0MsRUFBa0Q7QUFDaEQsY0FBSUgsS0FBS0csQ0FBVCxFQUFZO0FBQ1YsaUJBQUtKLGFBQUwsQ0FBbUJDLENBQW5CLEVBQXNCRSxJQUF0QixDQUEyQixLQUFLZCxRQUFMLENBQWNZLENBQWQsRUFBaUJJLFlBQWpCLENBQThCLEtBQUtoQixRQUFMLENBQWNlLENBQWQsQ0FBOUIsQ0FBM0I7QUFDRDtBQUNGO0FBQ0Y7QUFDRjs7OytCQUVnRTtBQUFBLHVDQUExREUsc0JBQTBEO0FBQUEsVUFBMURBLHNCQUEwRCx5Q0FBakMsQ0FBaUM7QUFBQSx1Q0FBOUJDLHNCQUE4QjtBQUFBLFVBQTlCQSxzQkFBOEIseUNBQUwsQ0FBSzs7QUFDL0Q7QUFDQTtBQUNBLFdBQUtELHNCQUFMLEdBQThCQSxzQkFBOUI7QUFDQSxXQUFLQyxzQkFBTCxHQUE4QkEsc0JBQTlCOztBQUVBLFVBQUksS0FBS2hCLEtBQVQsRUFBZ0I7QUFBRU0sZ0JBQVFDLEdBQVIsQ0FBWSxxQ0FBWjtBQUFxRDtBQUN2RTtBQUNBLFdBQUtVLGNBQUw7QUFDQSxXQUFLQyxZQUFMLEdBQW9CLEtBQUtDLGdCQUF6Qjs7QUFFQSxVQUFJLEtBQUtuQixLQUFULEVBQWdCO0FBQUVNLGdCQUFRQyxHQUFSLENBQVkscUJBQVosRUFBb0NELFFBQVFjLElBQVIsQ0FBYSxXQUFiO0FBQTRCO0FBQ2xGLGFBQU8sS0FBS2hCLFVBQUwsR0FBa0IsS0FBS04sUUFBTCxDQUFjYSxNQUF2QyxFQUErQztBQUM3QyxhQUFLVSxlQUFMLENBQXFCLEtBQUtDLFVBQUwsRUFBckI7QUFDRDtBQUNELFVBQUksS0FBS3RCLEtBQVQsRUFBZ0I7QUFBRU0sZ0JBQVFDLEdBQVIsQ0FBWSxrQkFBWixFQUFpQ0QsUUFBUWlCLE9BQVIsQ0FBZ0IsV0FBaEI7QUFBK0I7O0FBRWxGLFVBQUksS0FBS3ZCLEtBQVQsRUFBZ0I7QUFBRU0sZ0JBQVFDLEdBQVIsQ0FBWSx5Q0FBWjtBQUF5RDtBQUMzRSxhQUFPLEtBQUtpQixXQUFMLENBQWlCLEtBQUtMLGdCQUF0QixDQUFQO0FBQ0Q7O0FBRUQ7Ozs7K0NBQzJCO0FBQ3pCLFVBQU1NLFdBQVcsRUFBakI7QUFDQSxXQUFLQyxXQUFMLEdBQW1CQyxPQUFuQixDQUEyQixVQUFDeEIsT0FBRCxFQUFhO0FBQ3RDc0IsaUJBQVN0QixRQUFReUIsSUFBakIsSUFBeUJ6QixPQUF6QjtBQUNELE9BRkQ7O0FBSUEsV0FBS0wsUUFBTCxHQUFnQitCLE9BQU9DLE1BQVAsQ0FBY0wsUUFBZCxDQUFoQjtBQUNEOztBQUVEO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O3FDQUVpQjtBQUNmO0FBQ0EsV0FBS00sT0FBTCxHQUFlLEVBQWY7QUFDQSxXQUFLLElBQUlyQixJQUFJLENBQWIsRUFBZ0JBLElBQUksS0FBS1osUUFBTCxDQUFjYSxNQUFsQyxFQUEwQ0QsS0FBSyxDQUEvQyxFQUFrRDtBQUNoRCxhQUFLcUIsT0FBTCxDQUFhckIsQ0FBYixJQUFrQixLQUFsQjtBQUNEOztBQUVELFdBQUtTLGdCQUFMLEdBQXdCLENBQUMsQ0FBQyxDQUFELENBQUQsQ0FBeEI7QUFDQSxXQUFLYSxRQUFMLEdBQWdCLENBQWhCO0FBQ0EsV0FBS0QsT0FBTCxDQUFhLENBQWIsSUFBa0IsSUFBbEI7QUFDQTs7QUFFQSxVQUFNRSxZQUFZLEtBQUtBLFNBQUwsQ0FBZSxDQUFmLENBQWxCO0FBQ0EsV0FBS2QsZ0JBQUwsQ0FBc0IsQ0FBdEIsRUFBeUJQLElBQXpCLENBQThCcUIsU0FBOUI7QUFDQSxXQUFLN0IsVUFBTCxHQUFrQixDQUFsQjtBQUNBLFdBQUsyQixPQUFMLENBQWFFLFNBQWIsSUFBMEIsSUFBMUI7QUFDRDs7O29DQUVlQyxHLEVBQUs7QUFBQTs7QUFDbkI7QUFDQSxVQUFJQyxpQkFBaUIsQ0FBQyxDQUF0QjtBQUNBLFVBQUlDLG1CQUFtQixDQUFDLENBQXhCO0FBQ0EsVUFBSUMsY0FBYyxDQUFsQjs7QUFFQSxVQUFNQyxXQUFXLFNBQVhBLFFBQVcsQ0FBQ0MsV0FBRCxFQUFjN0IsQ0FBZCxFQUFpQjhCLENBQWpCLEVBQXVCO0FBQ3RDLFlBQU1DLE9BQU8sTUFBS0MsU0FBTCxDQUFlSCxXQUFmLENBQWI7QUFDQSxZQUFJRSxPQUFPSixXQUFYLEVBQXdCO0FBQ3RCQSx3QkFBY0ksSUFBZDtBQUNBTiwyQkFBaUJ6QixDQUFqQjtBQUNBMEIsNkJBQW1CSSxDQUFuQjtBQUNEO0FBQ0YsT0FQRDs7QUFTQSxXQUFLLElBQUk5QixJQUFJLENBQWIsRUFBZ0JBLElBQUksS0FBS3NCLFFBQXpCLEVBQW1DdEIsS0FBSyxDQUF4QyxFQUEyQztBQUN6QyxZQUFNNkIsY0FBY0ksTUFBTUMsSUFBTixDQUFXLEtBQUt6QixnQkFBTCxDQUFzQlQsQ0FBdEIsQ0FBWCxDQUFwQjs7QUFFQSxZQUFJNkIsWUFBWTVCLE1BQVosR0FBcUIsS0FBS0ssc0JBQTlCLEVBQXNEO0FBQ3BEdUIsc0JBQVkzQixJQUFaLENBQWlCc0IsR0FBakI7QUFDQUksbUJBQVNDLFdBQVQsRUFBc0I3QixDQUF0QixFQUF5QixDQUFDLENBQTFCO0FBQ0QsU0FIRCxNQUdPO0FBQ0w7QUFDQSxlQUFLLElBQUlHLElBQUksQ0FBYixFQUFnQkEsSUFBSTBCLFlBQVk1QixNQUFoQyxFQUF3Q0UsS0FBSyxDQUE3QyxFQUFnRDtBQUM5QyxnQkFBTWdDLGFBQWFOLFlBQVkxQixDQUFaLENBQW5CO0FBQ0EwQix3QkFBWTFCLENBQVosSUFBaUJxQixHQUFqQjtBQUNBSSxxQkFBU0MsV0FBVCxFQUFzQjdCLENBQXRCLEVBQXlCRyxDQUF6QjtBQUNBMEIsd0JBQVkxQixDQUFaLElBQWlCZ0MsVUFBakI7QUFDRDs7QUFFRDtBQUNBLGNBQU1KLE9BQU8sS0FBS0MsU0FBTCxDQUFlSCxXQUFmLENBQWI7QUFDQSxjQUFJRSxRQUFRSixXQUFSLElBQXVCRixtQkFBbUJ6QixDQUE5QyxFQUFpRDtBQUMvQztBQUNBeUIsNkJBQWlCLENBQUMsQ0FBbEI7QUFDQUMsK0JBQW1CLENBQUMsQ0FBcEI7QUFDRDtBQUNGO0FBQ0Y7O0FBRUQsVUFBSUQsbUJBQW1CLENBQUMsQ0FBeEIsRUFBMkI7QUFDekIsWUFBSSxLQUFLSCxRQUFMLElBQWlCYyxLQUFLQyxLQUFMLENBQVcsS0FBS2pELFFBQUwsQ0FBY2EsTUFBZCxHQUF1QixLQUFLSSxzQkFBdkMsQ0FBckIsRUFBcUY7QUFDbkY7QUFDQW9CLDJCQUFpQixDQUFqQjtBQUNBLGFBQUc7QUFDREEsOEJBQWtCLENBQWxCO0FBQ0QsV0FGRCxRQUVTLEtBQUtoQixnQkFBTCxDQUFzQmdCLGNBQXRCLEVBQXNDeEIsTUFBdEMsSUFBZ0QsS0FBS0ssc0JBRjlEO0FBR0QsU0FORCxNQU1PO0FBQ0wsZUFBS0csZ0JBQUwsQ0FBc0JQLElBQXRCLENBQTJCLENBQUNzQixHQUFELENBQTNCO0FBQ0EsZUFBS0YsUUFBTCxJQUFpQixDQUFqQjtBQUNEO0FBQ0YsT0FYRCxNQVdPLElBQUlJLG1CQUFtQixDQUFDLENBQXhCLEVBQTJCO0FBQ2hDLFlBQU1ZLGtCQUFrQixLQUFLN0IsZ0JBQUwsQ0FBc0JnQixjQUF0QixFQUFzQ0MsZ0JBQXRDLENBQXhCO0FBQ0EsYUFBS2pCLGdCQUFMLENBQXNCZ0IsY0FBdEIsRUFBc0NDLGdCQUF0QyxJQUEwREYsR0FBMUQ7O0FBRUEsYUFBS0gsT0FBTCxDQUFhaUIsZUFBYixJQUFnQyxLQUFoQztBQUNBLGFBQUs1QyxVQUFMLElBQW1CLENBQW5CO0FBQ0QsT0FOTSxNQU1BO0FBQ0wsYUFBS2UsZ0JBQUwsQ0FBc0JnQixjQUF0QixFQUFzQ3ZCLElBQXRDLENBQTJDc0IsR0FBM0M7QUFDRDs7QUFFRCxXQUFLSCxPQUFMLENBQWFHLEdBQWIsSUFBb0IsSUFBcEI7QUFDQSxXQUFLOUIsVUFBTCxJQUFtQixDQUFuQjtBQUNEOzs7aUNBRVk7QUFDWCxVQUFJNkMsZUFBZSxDQUFuQjtBQUNBLFNBQUc7QUFDREEsdUJBQWVILEtBQUtDLEtBQUwsQ0FBV0QsS0FBS0ksTUFBTCxLQUFnQixLQUFLcEQsUUFBTCxDQUFjYSxNQUF6QyxJQUFtRCxLQUFLYixRQUFMLENBQWNhLE1BQWhGO0FBQ0QsT0FGRCxRQUVTLEtBQUtvQixPQUFMLENBQWFrQixZQUFiLENBRlQ7O0FBSUEsYUFBT0EsWUFBUDtBQUNEOzs7Z0NBRVcvQixZLEVBQWM7QUFBQTs7QUFDeEIsVUFBTWlDLFFBQVEsRUFBZDtBQUNBLFdBQUssSUFBSXpDLElBQUksQ0FBYixFQUFnQkEsSUFBSVEsYUFBYVAsTUFBakMsRUFBeUNELEtBQUssQ0FBOUMsRUFBaUQ7QUFDL0MsWUFBTTBDLE9BQU9sQyxhQUFhUixDQUFiLEVBQWdCVCxHQUFoQixDQUFvQjtBQUFBLGlCQUFLLE9BQUtILFFBQUwsQ0FBY2UsQ0FBZCxDQUFMO0FBQUEsU0FBcEIsQ0FBYjtBQUNBc0MsY0FBTXZDLElBQU4sQ0FBVyxJQUFJeUMsY0FBSixDQUFTRCxJQUFULENBQVg7QUFDRDs7QUFFRCxhQUFPRCxLQUFQO0FBQ0Q7OzttQ0FFY2pDLFksRUFBY29DLFUsRUFBWTtBQUN2QyxVQUFJQyxnQkFBZ0IsQ0FBQyxDQUFyQjtBQUNBLFVBQUlDLGVBQWUsQ0FBbkI7O0FBRUEsVUFBSUMsUUFBUSxDQUFaO0FBQ0EsVUFBTUMsV0FBVyxTQUFYQSxRQUFXLENBQUNDLENBQUQsRUFBSWpELENBQUo7QUFBQSxlQUFVQSxJQUFLOEMsWUFBTCxJQUFzQkcsRUFBRUMsSUFBRixLQUFXLEtBQTNDO0FBQUEsT0FBakI7QUFDQSxhQUFPSixpQkFBaUJGLFVBQXhCLEVBQW9DO0FBQ2xDQyx3QkFBZ0JDLFlBQWhCO0FBQ0FBLHVCQUFldEMsYUFBYTJDLFNBQWIsQ0FBdUJILFFBQXZCLENBQWY7QUFDQSxZQUFJRixpQkFBaUIsQ0FBQyxDQUFsQixJQUF1QkEsZUFBZUYsVUFBMUMsRUFBc0Q7QUFBRUUseUJBQWVGLFVBQWY7QUFBNEI7O0FBRXBGLFlBQU1GLE9BQU8sRUFBYjtBQUNBLGFBQUssSUFBSTFDLElBQUk2QyxnQkFBZ0IsQ0FBN0IsRUFBZ0M3QyxJQUFJOEMsWUFBcEMsRUFBa0Q5QyxLQUFLLENBQXZELEVBQTBEO0FBQ3hEMEMsZUFBS3hDLElBQUwsQ0FBVU0sYUFBYVIsQ0FBYixFQUFnQlAsT0FBMUI7QUFDRDs7QUFFRHNELGlCQUFTLEtBQUtmLFNBQUwsQ0FBZVUsSUFBZixDQUFUO0FBQ0Q7O0FBRUQsYUFBT0ssS0FBUDtBQUNEOzs7OEJBRVNMLEksRUFBTTtBQUNkO0FBQ0EsVUFBSUEsS0FBS3pDLE1BQUwsSUFBZSxDQUFuQixFQUFzQjtBQUNwQixlQUFPLENBQVA7QUFDRDs7QUFFRCxVQUFJOEMsUUFBUSxDQUFaO0FBQ0EsVUFBSUssV0FBVyxDQUFmO0FBQ0EsV0FBSyxJQUFJcEQsSUFBSSxDQUFiLEVBQWdCQSxJQUFJMEMsS0FBS3pDLE1BQXpCLEVBQWlDRCxLQUFLLENBQXRDLEVBQXlDO0FBQ3ZDLGFBQUssSUFBSUcsSUFBSUgsSUFBSSxDQUFqQixFQUFvQkcsSUFBSXVDLEtBQUt6QyxNQUE3QixFQUFxQ0UsS0FBSyxDQUExQyxFQUE2QztBQUMzQzRDLG1CQUFTLEtBQUtNLGFBQUwsQ0FBbUJYLEtBQUsxQyxDQUFMLENBQW5CLEVBQTRCMEMsS0FBS3ZDLENBQUwsQ0FBNUIsQ0FBVDtBQUNBaUQsc0JBQVksQ0FBWjtBQUNEO0FBQ0Y7O0FBRUQsYUFBT0wsUUFBUUssUUFBZjtBQUNEOzs7a0NBRWFFLEMsRUFBR0MsQyxFQUFHO0FBQ2xCLGFBQU8sS0FBS3hELGFBQUwsQ0FBbUJ1RCxDQUFuQixFQUFzQkMsQ0FBdEIsQ0FBUDtBQUNEOzs7OEJBRVNDLFksRUFBYztBQUN0QixVQUFJQyxjQUFjLENBQUMsQ0FBbkI7QUFDQSxVQUFJQyxZQUFZLENBQWhCO0FBQ0EsV0FBSyxJQUFJMUQsSUFBSSxDQUFiLEVBQWdCQSxJQUFJLEtBQUtaLFFBQUwsQ0FBY2EsTUFBbEMsRUFBMENELEtBQUssQ0FBL0MsRUFBa0Q7QUFDaEQsWUFBSUEsTUFBTXdELFlBQVYsRUFBd0I7QUFDdEIsY0FBTVQsUUFBUSxLQUFLaEQsYUFBTCxDQUFtQnlELFlBQW5CLEVBQWlDeEQsQ0FBakMsQ0FBZDtBQUNBLGNBQUkrQyxRQUFRVyxTQUFaLEVBQXVCO0FBQ3JCRCwwQkFBY3pELENBQWQ7QUFDQTBELHdCQUFZWCxLQUFaO0FBQ0Q7QUFDRjtBQUNGOztBQUVELGFBQU9VLFdBQVA7QUFDRDs7Ozs7O2tCQTNRa0J0RSxRIiwiZmlsZSI6Im9wdC1tYXRjaGVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogZXNsaW50LWRpc2FibGUgbm8tY29uc29sZSAqL1xuLyogZXNsaW50LWRpc2FibGUgbm8tY29udGludWUgKi9cbmltcG9ydCBTdHVkZW50IGZyb20gJy4vc3R1ZGVudCc7XG5pbXBvcnQgVGVhbSBmcm9tICcuL3RlYW0nO1xuXG4vLyBiYXNpY2FsbHkgc3RhcnMgYW5kIGJhcnNcbi8vIHdlIGhhdmUgc3R1ZGVudHMgYW5kIHNvbWUgc2VwZXJhdG9ycyBmb3IgdGhlbVxuLy8gd2UgbmVlZCBhbiBpbml0YWwgZXN0aW1hdGUgdGhvXG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIE9QVE1hdGNoIHtcbiAgLy8gcHVibGljXG4gIGNvbnN0cnVjdG9yKHN0dWRlbnRzLCB0ZWFtaW5nQ29uZmlnLCBkZWJ1ZyA9IGZhbHNlKSB7XG4gICAgdGhpcy5zdHVkZW50cyA9IHN0dWRlbnRzLm1hcChzdHVkZW50ID0+IG5ldyBTdHVkZW50KHN0dWRlbnQsIHRlYW1pbmdDb25maWcpKTtcbiAgICB0aGlzLmRlYnVnID0gZGVidWc7XG4gICAgdGhpcy5udW1WaXNpdGVkID0gMDtcblxuICAgIHRoaXMuaW5pdGlhbGl6ZUFkamFjZW5jeU1hdHJpeCgpO1xuXG4gICAgaWYgKHRoaXMuZGVidWcpIHsgY29uc29sZS5sb2coJ0RQTWF0Y2g6IFJlbW92aW5nIGR1cGxpY2F0ZXMuLi4nKTsgfVxuXG4gICAgdGhpcy5yZW1vdmVEdXBsaWNhdGVzU3R1ZGVudHMoKTtcbiAgfVxuXG4gIGdldFN0dWRlbnRzKCkge1xuICAgIHJldHVybiB0aGlzLnN0dWRlbnRzO1xuICB9XG5cbiAgaW5pdGlhbGl6ZUFkamFjZW5jeU1hdHJpeCgpIHtcbiAgICB0aGlzLnNjb3JlZEFnYWluc3QgPSBbXTtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuc3R1ZGVudHMubGVuZ3RoOyBpICs9IDEpIHtcbiAgICAgIHRoaXMuc2NvcmVkQWdhaW5zdC5wdXNoKFtdKTtcbiAgICAgIGZvciAobGV0IGogPSAwOyBqIDwgdGhpcy5zdHVkZW50cy5sZW5ndGg7IGogKz0gMSkge1xuICAgICAgICBpZiAoaSAhPSBqKSB7XG4gICAgICAgICAgdGhpcy5zY29yZWRBZ2FpbnN0W2ldLnB1c2godGhpcy5zdHVkZW50c1tpXS5zY29yZUFnYWluc3QodGhpcy5zdHVkZW50c1tqXSkpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgdGVhbSh7IG1pbmltdW1TdHVkZW50c1BlclRlYW0gPSAzLCBtYXhpbXVtU3R1ZGVudHNQZXJUZWFtID0gNCB9KSB7XG4gICAgLy8gZ2VuIHBlcm1zXG4gICAgLy8gZHAgdGhpcyBzaGl6XG4gICAgdGhpcy5taW5pbXVtU3R1ZGVudHNQZXJUZWFtID0gbWluaW11bVN0dWRlbnRzUGVyVGVhbTtcbiAgICB0aGlzLm1heGltdW1TdHVkZW50c1BlclRlYW0gPSBtYXhpbXVtU3R1ZGVudHNQZXJUZWFtO1xuXG4gICAgaWYgKHRoaXMuZGVidWcpIHsgY29uc29sZS5sb2coJ0RQTWF0Y2g6IEdlbmVyYXRpbmcgaW5pdGlhbCBwYXRoLi4uJyk7IH1cbiAgICAvLyB0aGlzLmdlbmVyYXRlSW5pdGlhbFNvbHV0aW9uUGF0aCgpO1xuICAgIHRoaXMuaW5pdGlhbGl6ZVRvdXIoKTtcbiAgICB0aGlzLnNvbHV0aW9uUGF0aCA9IHRoaXMuYmVzdFNvbHV0aW9uUGF0aDtcblxuICAgIGlmICh0aGlzLmRlYnVnKSB7IGNvbnNvbGUubG9nKCdEUE1hdGNoOiB0ZWFtaW5nLi4uJyk7IGNvbnNvbGUudGltZSgnRFBNYXRjaGVyJyk7IH1cbiAgICB3aGlsZSAodGhpcy5udW1WaXNpdGVkIDwgdGhpcy5zdHVkZW50cy5sZW5ndGgpIHtcbiAgICAgIHRoaXMuaW5zZXJ0U2VsZWN0aW9uKHRoaXMuc2VsZWN0VG91cigpKTtcbiAgICB9XG4gICAgaWYgKHRoaXMuZGVidWcpIHsgY29uc29sZS5sb2coJ0RQTWF0Y2g6IGRvbmUuLi4nKTsgY29uc29sZS50aW1lRW5kKCdEUE1hdGNoZXInKTsgfVxuXG4gICAgaWYgKHRoaXMuZGVidWcpIHsgY29uc29sZS5sb2coJ0RQTWF0Y2g6IGdlbmVyYXRpbmcgdGVhbXMgZnJvbSBtYXRjaC4uLicpOyB9XG4gICAgcmV0dXJuIHRoaXMucGF0aFRvVGVhbXModGhpcy5iZXN0U29sdXRpb25QYXRoKTtcbiAgfVxuXG4gIC8vIHByaXZhdGVcbiAgcmVtb3ZlRHVwbGljYXRlc1N0dWRlbnRzKCkge1xuICAgIGNvbnN0IGR1cEFycmF5ID0ge307XG4gICAgdGhpcy5nZXRTdHVkZW50cygpLmZvckVhY2goKHN0dWRlbnQpID0+IHtcbiAgICAgIGR1cEFycmF5W3N0dWRlbnQuaGFzaF0gPSBzdHVkZW50O1xuICAgIH0pO1xuXG4gICAgdGhpcy5zdHVkZW50cyA9IE9iamVjdC52YWx1ZXMoZHVwQXJyYXkpO1xuICB9XG5cbiAgLy8gZ2VuZXJhdGVJbml0aWFsU29sdXRpb25QYXRoKCkge1xuICAvLyAgIGNvbnN0IGluaXRpYWxOdW1iZXJPZkJhcnMgPSBNYXRoLmZsb29yKHRoaXMuZ2V0U3R1ZGVudHMoKS5sZW5ndGggLyB0aGlzLm1heGltdW1TdHVkZW50c1BlclRlYW0pO1xuICAvLyAgIHRoaXMuYmVzdFNvbHV0aW9uUGF0aCA9IFtdO1xuICAvLyAgIHRoaXMuZ2V0U3R1ZGVudHMoKS5mb3JFYWNoKChzdHVkZW50LCBpbmRleCkgPT4ge1xuICAvLyAgICAgdGhpcy5iZXN0U29sdXRpb25QYXRoLnB1c2goeyB0eXBlOiAnc3R1ZGVudCcsIHN0dWRlbnQ6IGluZGV4IH0pO1xuICAvLyAgIH0pO1xuXG4gIC8vICAgLy8gc2h1ZmZsZVxuICAvLyAgIGZvciAobGV0IGkgPSAwOyBpIDwgNTA7IGkgKz0gMSkge1xuICAvLyAgICAgLy8gc3dhcCB0d28gcmFuZG9tIGluZGV4ZXNcbiAgLy8gICAgIGNvbnN0IGkxID0gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogdGhpcy5nZXRTdHVkZW50cygpLmxlbmd0aCkgJSB0aGlzLmdldFN0dWRlbnRzKCkubGVuZ3RoO1xuICAvLyAgICAgY29uc3QgaTIgPSBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiB0aGlzLmdldFN0dWRlbnRzKCkubGVuZ3RoKSAlIHRoaXMuZ2V0U3R1ZGVudHMoKS5sZW5ndGg7XG4gIC8vICAgICBjb25zdCB0ZW1wID0gdGhpcy5iZXN0U29sdXRpb25QYXRoW2kxXTtcbiAgLy8gICAgIHRoaXMuYmVzdFNvbHV0aW9uUGF0aFtpMV0gPSB0aGlzLmJlc3RTb2x1dGlvblBhdGhbaTJdO1xuICAvLyAgICAgdGhpcy5iZXN0U29sdXRpb25QYXRoW2kyXSA9IHRlbXA7XG4gIC8vICAgfVxuXG4gIC8vICAgZm9yIChsZXQgaSA9IDA7IGkgPCBpbml0aWFsTnVtYmVyT2ZCYXJzOyBpICs9IDEpIHtcbiAgLy8gICAgIGNvbnN0IGluZGV4ID0gKChpICsgMSkgKiB0aGlzLm1heGltdW1TdHVkZW50c1BlclRlYW0pICsgaTtcbiAgLy8gICAgIGNvbnN0IHN3YXBWYWwgPSBPYmplY3QuYXNzaWduKHt9LCB0aGlzLmJlc3RTb2x1dGlvblBhdGhbaW5kZXhdKTtcbiAgLy8gICAgIHRoaXMuYmVzdFNvbHV0aW9uUGF0aFtpbmRleF0gPSB7IHR5cGU6ICdiYXInIH07XG4gIC8vICAgICB0aGlzLmJlc3RTb2x1dGlvblBhdGgucHVzaChzd2FwVmFsKTtcbiAgLy8gICB9XG5cbiAgLy8gICAvLyBmaW5kIG91dCByZW1haW5kZXJcbiAgLy8gICBjb25zdCByZW1haW5kZXJTdHVkZW50cyA9IHRoaXMuZ2V0U3R1ZGVudHMoKS5sZW5ndGggJSBpbml0aWFsTnVtYmVyT2ZCYXJzO1xuICAvLyAgIGlmIChyZW1haW5kZXJTdHVkZW50cyA8IHRoaXMubWluaW11bVN0dWRlbnRzUGVyVGVhbSkge1xuICAvLyAgICAgbGV0IGxhc3RJbmRleCA9IHRoaXMuYmVzdFNvbHV0aW9uUGF0aC5sZW5ndGg7XG4gIC8vICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHJlbWFpbmRlclN0dWRlbnRzOyBpICs9IDEpIHtcbiAgLy8gICAgICAgLy8gbW92ZSBsYXN0IGJhciBiYWNrIGJ5IHJlbWFpbmVkIHN0dWRlbnRzXG4gIC8vICAgICAgIC8vIGZpbmQgbGFzdCBiYXIgaW5kZXhcbiAgLy8gICAgICAgZm9yIChsZXQgaiA9IGxhc3RJbmRleCAtIDE7IGogPiAwOyBqIC09IDEpIHtcbiAgLy8gICAgICAgICBpZiAodGhpcy5iZXN0U29sdXRpb25QYXRoW2pdLnR5cGUgPT09ICdiYXInKSB7XG4gIC8vICAgICAgICAgICBsYXN0SW5kZXggPSBqO1xuICAvLyAgICAgICAgICAgYnJlYWs7XG4gIC8vICAgICAgICAgfVxuICAvLyAgICAgICB9XG5cbiAgLy8gICAgICAgZm9yIChsZXQgayA9IDA7IGsgPCB0aGlzLm1pbmltdW1TdHVkZW50c1BlclRlYW0gLSByZW1haW5kZXJTdHVkZW50cyAtIGk7IGsgKz0gMSkge1xuICAvLyAgICAgICAgIHRoaXMuYmVzdFNvbHV0aW9uUGF0aCA9IERQTWF0Y2guc3dhcCh0aGlzLmJlc3RTb2x1dGlvblBhdGgsIGxhc3RJbmRleCwgbGFzdEluZGV4IC0gayAtIDEpO1xuICAvLyAgICAgICB9XG4gIC8vICAgICB9XG4gIC8vICAgfVxuICAvLyAgIC8vIHRoaXMuYmVzdFNvbHV0aW9uUGF0aC5wdXNoKHsgdHlwZTogJ2JhcicgfSk7XG4gIC8vIH1cblxuICBpbml0aWFsaXplVG91cigpIHtcbiAgICAvLyBzdGFydCB3aXRoIGEgdGVhbSBvZiBvbmVcbiAgICB0aGlzLnZpc2l0ZWQgPSBbXTtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuc3R1ZGVudHMubGVuZ3RoOyBpICs9IDEpIHtcbiAgICAgIHRoaXMudmlzaXRlZFtpXSA9IGZhbHNlO1xuICAgIH1cblxuICAgIHRoaXMuYmVzdFNvbHV0aW9uUGF0aCA9IFtbMF1dO1xuICAgIHRoaXMubnVtVGVhbXMgPSAxO1xuICAgIHRoaXMudmlzaXRlZFswXSA9IHRydWU7XG4gICAgLy8gZmluZCBvcHRpbWFsIHBhaXJpbmdcblxuICAgIGNvbnN0IGJlc3RNYXRjaCA9IHRoaXMuYmVzdE1hdGNoKDApO1xuICAgIHRoaXMuYmVzdFNvbHV0aW9uUGF0aFswXS5wdXNoKGJlc3RNYXRjaCk7XG4gICAgdGhpcy5udW1WaXNpdGVkID0gMjtcbiAgICB0aGlzLnZpc2l0ZWRbYmVzdE1hdGNoXSA9IHRydWU7XG4gIH1cblxuICBpbnNlcnRTZWxlY3Rpb24oc2VsKSB7XG4gICAgLy8gZmluZCBvcHRpbWFsIHRlYW0gdG8gaW5zZXJ0XG4gICAgbGV0IGluc2VydGlvblBvaW50ID0gLTE7XG4gICAgbGV0IHJlcGxhY2VtZW50UG9pbnQgPSAtMTtcbiAgICBsZXQgY3VycmVudENvc3QgPSAwO1xuXG4gICAgY29uc3QgdGVzdFRlYW0gPSAoY3VycmVudFRlYW0sIGksIHIpID0+IHtcbiAgICAgIGNvbnN0IGNvc3QgPSB0aGlzLnNjb3JlVGVhbShjdXJyZW50VGVhbSk7XG4gICAgICBpZiAoY29zdCA+IGN1cnJlbnRDb3N0KSB7XG4gICAgICAgIGN1cnJlbnRDb3N0ID0gY29zdDtcbiAgICAgICAgaW5zZXJ0aW9uUG9pbnQgPSBpO1xuICAgICAgICByZXBsYWNlbWVudFBvaW50ID0gcjtcbiAgICAgIH1cbiAgICB9O1xuXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLm51bVRlYW1zOyBpICs9IDEpIHtcbiAgICAgIGNvbnN0IGN1cnJlbnRUZWFtID0gQXJyYXkuZnJvbSh0aGlzLmJlc3RTb2x1dGlvblBhdGhbaV0pO1xuXG4gICAgICBpZiAoY3VycmVudFRlYW0ubGVuZ3RoIDwgdGhpcy5tYXhpbXVtU3R1ZGVudHNQZXJUZWFtKSB7XG4gICAgICAgIGN1cnJlbnRUZWFtLnB1c2goc2VsKTtcbiAgICAgICAgdGVzdFRlYW0oY3VycmVudFRlYW0sIGksIC0xKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIC8vIHJlcGxhY2UgZWFjaCBhbmQgdHJ5XG4gICAgICAgIGZvciAobGV0IGogPSAwOyBqIDwgY3VycmVudFRlYW0ubGVuZ3RoOyBqICs9IDEpIHtcbiAgICAgICAgICBjb25zdCBvbGRFbGVtZW50ID0gY3VycmVudFRlYW1bal07XG4gICAgICAgICAgY3VycmVudFRlYW1bal0gPSBzZWw7XG4gICAgICAgICAgdGVzdFRlYW0oY3VycmVudFRlYW0sIGksIGopO1xuICAgICAgICAgIGN1cnJlbnRUZWFtW2pdID0gb2xkRWxlbWVudDtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIGJ1dCBpZiBtYWluIHRlYW0gaXMgYmV0dGVyLCBwdXNoIHRvIG5ld1xuICAgICAgICBjb25zdCBjb3N0ID0gdGhpcy5zY29yZVRlYW0oY3VycmVudFRlYW0pO1xuICAgICAgICBpZiAoY29zdCA+PSBjdXJyZW50Q29zdCAmJiBpbnNlcnRpb25Qb2ludCA9PT0gaSkge1xuICAgICAgICAgIC8vIHNraXBcbiAgICAgICAgICBpbnNlcnRpb25Qb2ludCA9IC0xO1xuICAgICAgICAgIHJlcGxhY2VtZW50UG9pbnQgPSAtMTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIGlmIChpbnNlcnRpb25Qb2ludCA9PT0gLTEpIHtcbiAgICAgIGlmICh0aGlzLm51bVRlYW1zID49IE1hdGguZmxvb3IodGhpcy5zdHVkZW50cy5sZW5ndGggLyB0aGlzLm1pbmltdW1TdHVkZW50c1BlclRlYW0pKSB7XG4gICAgICAgIC8vIGFkZCB0byByYW5kb20gaW5zZXJ0XG4gICAgICAgIGluc2VydGlvblBvaW50ID0gMDtcbiAgICAgICAgZG8ge1xuICAgICAgICAgIGluc2VydGlvblBvaW50ICs9IDE7XG4gICAgICAgIH0gd2hpbGUgKHRoaXMuYmVzdFNvbHV0aW9uUGF0aFtpbnNlcnRpb25Qb2ludF0ubGVuZ3RoID49IHRoaXMubWF4aW11bVN0dWRlbnRzUGVyVGVhbSk7XG4gICAgICB9IGVsc2Uge1xuICAgICAgICB0aGlzLmJlc3RTb2x1dGlvblBhdGgucHVzaChbc2VsXSk7XG4gICAgICAgIHRoaXMubnVtVGVhbXMgKz0gMTtcbiAgICAgIH1cbiAgICB9IGVsc2UgaWYgKHJlcGxhY2VtZW50UG9pbnQgPiAtMSkge1xuICAgICAgY29uc3QgcmVwbGFjZWRFbGVtZW50ID0gdGhpcy5iZXN0U29sdXRpb25QYXRoW2luc2VydGlvblBvaW50XVtyZXBsYWNlbWVudFBvaW50XTtcbiAgICAgIHRoaXMuYmVzdFNvbHV0aW9uUGF0aFtpbnNlcnRpb25Qb2ludF1bcmVwbGFjZW1lbnRQb2ludF0gPSBzZWw7XG5cbiAgICAgIHRoaXMudmlzaXRlZFtyZXBsYWNlZEVsZW1lbnRdID0gZmFsc2U7XG4gICAgICB0aGlzLm51bVZpc2l0ZWQgLT0gMTtcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5iZXN0U29sdXRpb25QYXRoW2luc2VydGlvblBvaW50XS5wdXNoKHNlbCk7XG4gICAgfVxuXG4gICAgdGhpcy52aXNpdGVkW3NlbF0gPSB0cnVlO1xuICAgIHRoaXMubnVtVmlzaXRlZCArPSAxO1xuICB9XG5cbiAgc2VsZWN0VG91cigpIHtcbiAgICBsZXQgcmFuZG9tU2VsZWN0ID0gMDtcbiAgICBkbyB7XG4gICAgICByYW5kb21TZWxlY3QgPSBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiB0aGlzLnN0dWRlbnRzLmxlbmd0aCkgJSB0aGlzLnN0dWRlbnRzLmxlbmd0aDtcbiAgICB9IHdoaWxlICh0aGlzLnZpc2l0ZWRbcmFuZG9tU2VsZWN0XSk7XG5cbiAgICByZXR1cm4gcmFuZG9tU2VsZWN0O1xuICB9XG5cbiAgcGF0aFRvVGVhbXMoc29sdXRpb25QYXRoKSB7XG4gICAgY29uc3QgdGVhbXMgPSBbXTtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHNvbHV0aW9uUGF0aC5sZW5ndGg7IGkgKz0gMSkge1xuICAgICAgY29uc3QgdGVhbSA9IHNvbHV0aW9uUGF0aFtpXS5tYXAoaiA9PiB0aGlzLnN0dWRlbnRzW2pdKTtcbiAgICAgIHRlYW1zLnB1c2gobmV3IFRlYW0odGVhbSkpO1xuICAgIH1cblxuICAgIHJldHVybiB0ZWFtcztcbiAgfVxuXG4gIGNhbGN1bGF0ZVNjb3JlKHNvbHV0aW9uUGF0aCwgcGVybUxlbmd0aCkge1xuICAgIGxldCBmaXJzdEJhckluZGV4ID0gLTE7XG4gICAgbGV0IG5leHRCYXJJbmRleCA9IDA7XG5cbiAgICBsZXQgc2NvcmUgPSAwO1xuICAgIGNvbnN0IGZpbmRGdW5jID0gKHYsIGkpID0+IGkgPiAobmV4dEJhckluZGV4KSAmJiB2LnR5cGUgPT09ICdiYXInO1xuICAgIHdoaWxlIChuZXh0QmFySW5kZXggIT09IHBlcm1MZW5ndGgpIHtcbiAgICAgIGZpcnN0QmFySW5kZXggPSBuZXh0QmFySW5kZXg7XG4gICAgICBuZXh0QmFySW5kZXggPSBzb2x1dGlvblBhdGguZmluZEluZGV4KGZpbmRGdW5jKTtcbiAgICAgIGlmIChuZXh0QmFySW5kZXggPT09IC0xIHx8IG5leHRCYXJJbmRleCA+IHBlcm1MZW5ndGgpIHsgbmV4dEJhckluZGV4ID0gcGVybUxlbmd0aDsgfVxuXG4gICAgICBjb25zdCB0ZWFtID0gW107XG4gICAgICBmb3IgKGxldCBpID0gZmlyc3RCYXJJbmRleCArIDE7IGkgPCBuZXh0QmFySW5kZXg7IGkgKz0gMSkge1xuICAgICAgICB0ZWFtLnB1c2goc29sdXRpb25QYXRoW2ldLnN0dWRlbnQpO1xuICAgICAgfVxuXG4gICAgICBzY29yZSArPSB0aGlzLnNjb3JlVGVhbSh0ZWFtKTtcbiAgICB9XG5cbiAgICByZXR1cm4gc2NvcmU7XG4gIH1cblxuICBzY29yZVRlYW0odGVhbSkge1xuICAgIC8vIHBhaXJ3aXNlIHRlYW0gYWxsIG1lbWJlcnMgYWdhaW5zdCBlYWNoIG90aGVyXG4gICAgaWYgKHRlYW0ubGVuZ3RoIDw9IDEpIHtcbiAgICAgIHJldHVybiAwO1xuICAgIH1cblxuICAgIGxldCBzY29yZSA9IDA7XG4gICAgbGV0IG51bVBlcm1zID0gMDtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRlYW0ubGVuZ3RoOyBpICs9IDEpIHtcbiAgICAgIGZvciAobGV0IGogPSBpICsgMTsgaiA8IHRlYW0ubGVuZ3RoOyBqICs9IDEpIHtcbiAgICAgICAgc2NvcmUgKz0gdGhpcy5zY29yZVN0dWRlbnRzKHRlYW1baV0sIHRlYW1bal0pO1xuICAgICAgICBudW1QZXJtcyArPSAxO1xuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiBzY29yZSAvIG51bVBlcm1zO1xuICB9XG5cbiAgc2NvcmVTdHVkZW50cyhhLCBiKSB7XG4gICAgcmV0dXJuIHRoaXMuc2NvcmVkQWdhaW5zdFthXVtiXTtcbiAgfVxuXG4gIGJlc3RNYXRjaChzdHVkZW50SW5kZXgpIHtcbiAgICBsZXQgYmVzdEVsZW1lbnQgPSAtMTtcbiAgICBsZXQgYmVzdFNjb3JlID0gMDtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuc3R1ZGVudHMubGVuZ3RoOyBpICs9IDEpIHtcbiAgICAgIGlmIChpICE9PSBzdHVkZW50SW5kZXgpIHtcbiAgICAgICAgY29uc3Qgc2NvcmUgPSB0aGlzLnNjb3JlZEFnYWluc3Rbc3R1ZGVudEluZGV4XVtpXTtcbiAgICAgICAgaWYgKHNjb3JlID4gYmVzdFNjb3JlKSB7XG4gICAgICAgICAgYmVzdEVsZW1lbnQgPSBpO1xuICAgICAgICAgIGJlc3RTY29yZSA9IHNjb3JlO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIGJlc3RFbGVtZW50O1xuICB9XG59XG4iXSwic291cmNlUm9vdCI6Ii9tbnQvYy9Vc2Vycy9wdXJldS9Eb2N1bWVudHMvcHJvamVjdHMvbGF1bmNoeC10ZWFtaW5nL3NyYyJ9
