'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); /* eslint-disable no-console */
/* eslint-disable no-continue */


var _student = require('./student');

var _student2 = _interopRequireDefault(_student);

var _team = require('./team');

var _team2 = _interopRequireDefault(_team);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// basically stars and bars
// we have students and some seperators for them
// we need an inital estimate tho

var DPMatch = function () {
  // public
  function DPMatch(students, teamingConfig) {
    var debug = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

    _classCallCheck(this, DPMatch);

    this.students = students.map(function (student) {
      return new _student2.default(student, teamingConfig);
    });
    this.debug = debug;

    this.initializeAdjacencyMatrix();

    if (this.debug) {
      console.log('DPMatch: Removing duplicates...');
    }

    this.removeDuplicatesStudents();
  }

  _createClass(DPMatch, [{
    key: 'getStudents',
    value: function getStudents() {
      return this.students;
    }
  }, {
    key: 'initializeAdjacencyMatrix',
    value: function initializeAdjacencyMatrix() {
      this.scoredAgainst = [];
      for (var i = 0; i < this.students.length; i += 1) {
        this.scoredAgainst.push([]);
        for (var j = 0; j < this.students.length; j += 1) {
          if (i != j) {
            this.scoredAgainst[i].push(this.students[i].scoreAgainst(this.students[j]));
          }
        }
      }
    }
  }, {
    key: 'team',
    value: function team(_ref) {
      var _ref$minimumStudentsP = _ref.minimumStudentsPerTeam,
          minimumStudentsPerTeam = _ref$minimumStudentsP === undefined ? 3 : _ref$minimumStudentsP,
          _ref$maximumStudentsP = _ref.maximumStudentsPerTeam,
          maximumStudentsPerTeam = _ref$maximumStudentsP === undefined ? 4 : _ref$maximumStudentsP;

      // gen perms
      // dp this shiz
      this.minimumStudentsPerTeam = minimumStudentsPerTeam;
      this.maximumStudentsPerTeam = maximumStudentsPerTeam;

      this.lowerBound = 0;
      if (this.debug) {
        console.log('DPMatch: Generating initial path...');
      }
      this.generateInitialSolutionPath();
      this.solutionPath = this.bestSolutionPath;

      if (this.debug) {
        console.log('DPMatch: calculating initial upper bound...');
      }
      this.upperBound = this.calculateUpperBound(this.bestSolutionPath, 0);

      if (this.debug) {
        console.log('DPMatch: teaming...');console.time('DPMatcher');
      }
      this.genPerms(0);
      if (this.debug) {
        console.log('DPMatch: done...');console.timeEnd('DPMatcher');
      }

      if (this.debug) {
        console.log('DPMatch: generating teams from match...');
      }
      return this.pathToTeams(this.bestSolutionPath);
    }

    // private

  }, {
    key: 'removeDuplicatesStudents',
    value: function removeDuplicatesStudents() {
      var dupArray = {};
      this.getStudents().forEach(function (student) {
        dupArray[student.hash] = student;
      });

      this.students = Object.values(dupArray);
    }
  }, {
    key: 'generateInitialSolutionPath',
    value: function generateInitialSolutionPath() {
      var _this = this;

      var initialNumberOfBars = Math.floor(this.getStudents().length / this.maximumStudentsPerTeam);
      this.bestSolutionPath = [];
      this.getStudents().forEach(function (student, index) {
        _this.bestSolutionPath.push({ type: 'student', student: index });
      });

      // shuffle
      for (var i = 0; i < 50; i += 1) {
        // swap two random indexes
        var i1 = Math.floor(Math.random() * this.getStudents().length) % this.getStudents().length;
        var i2 = Math.floor(Math.random() * this.getStudents().length) % this.getStudents().length;
        var temp = this.bestSolutionPath[i1];
        this.bestSolutionPath[i1] = this.bestSolutionPath[i2];
        this.bestSolutionPath[i2] = temp;
      }

      for (var _i = 0; _i < initialNumberOfBars; _i += 1) {
        var index = (_i + 1) * this.maximumStudentsPerTeam + _i;
        var swapVal = Object.assign({}, this.bestSolutionPath[index]);
        this.bestSolutionPath[index] = { type: 'bar' };
        this.bestSolutionPath.push(swapVal);
      }

      // find out remainder
      var remainderStudents = this.getStudents().length % initialNumberOfBars;
      if (remainderStudents < this.minimumStudentsPerTeam) {
        var lastIndex = this.bestSolutionPath.length;
        for (var _i2 = 0; _i2 < remainderStudents; _i2 += 1) {
          // move last bar back by remained students
          // find last bar index
          for (var j = lastIndex - 1; j > 0; j -= 1) {
            if (this.bestSolutionPath[j].type === 'bar') {
              lastIndex = j;
              break;
            }
          }

          for (var k = 0; k < this.minimumStudentsPerTeam - remainderStudents - _i2; k += 1) {
            this.bestSolutionPath = DPMatch.swap(this.bestSolutionPath, lastIndex, lastIndex - k - 1);
          }
        }
      }
      // this.bestSolutionPath.push({ type: 'bar' });
    }
  }, {
    key: 'pathToTeams',
    value: function pathToTeams(solutionPath) {
      var firstBarIndex = 0;
      var nextBarIndex = 0;
      var teams = [];
      var findFunc = function findFunc(v, i) {
        return i > nextBarIndex && v.type === 'bar';
      };
      while (nextBarIndex !== solutionPath.length) {
        firstBarIndex = nextBarIndex;
        nextBarIndex = solutionPath.findIndex(findFunc);
        if (nextBarIndex === -1 || nextBarIndex > solutionPath.length) {
          nextBarIndex = solutionPath.length;
        }

        var team = [];
        for (var i = firstBarIndex + 1; i < nextBarIndex; i += 1) {
          team.push(this.students[solutionPath[i].student]);
        }

        teams.push(new _team2.default(team));
      }

      return teams;
    }
  }, {
    key: 'isSolution',
    value: function isSolution(solutionPath) {
      // check for minimum and maximum
      for (var i = 0; i < solutionPath.length; i += 1) {
        if (solutionPath[i].type === 'bar') {
          // count til next bar
          var count = 1;
          while (i + count < solutionPath.length && solutionPath[i + count].type !== 'bar') {
            count += 1;
          }
          count -= 1;

          if (count > this.maximumStudentsPerTeam || count < this.minimumStudentsPerTeam) {
            return false;
          }
        }
      }

      return true;
    }
  }, {
    key: 'isValid',
    value: function isValid(solutionPath, permLength) {
      // check for minimum and maximum

      if (permLength === 0) {
        return true;
      }

      for (var i = 0; i < Math.min(this.minimumStudentsPerTeam, permLength); i += 1) {
        if (solutionPath[i].type === 'bar') {
          return false;
        }
      }

      if (permLength <= this.maximumStudentsPerTeam) {
        return true;
      }

      for (var _i3 = 0; _i3 < permLength; _i3 += 1) {
        if (solutionPath[_i3].type === 'bar' || _i3 === 0) {
          // count til next bar
          var count = 1;
          while (_i3 + count < permLength && solutionPath[_i3 + count].type !== 'bar') {
            count += 1;
          }
          count -= 1;

          if ((count > this.maximumStudentsPerTeam || count < this.minimumStudentsPerTeam) && _i3 <= permLength - this.maximumStudentsPerTeam) {
            return false;
          }
        }
      }

      return true;
    }
  }, {
    key: 'calculateScore',
    value: function calculateScore(solutionPath, permLength) {
      var firstBarIndex = -1;
      var nextBarIndex = 0;

      var score = 0;
      var findFunc = function findFunc(v, i) {
        return i > nextBarIndex && v.type === 'bar';
      };
      while (nextBarIndex !== permLength) {
        firstBarIndex = nextBarIndex;
        nextBarIndex = solutionPath.findIndex(findFunc);
        if (nextBarIndex === -1 || nextBarIndex > permLength) {
          nextBarIndex = permLength;
        }

        var team = [];
        for (var i = firstBarIndex + 1; i < nextBarIndex; i += 1) {
          team.push(solutionPath[i].student);
        }

        score += this.scoreTeam(team);
      }

      return score;
    }
  }, {
    key: 'scoreTeam',
    value: function scoreTeam(team) {
      // pairwise team all members against each other
      if (team.length <= 1) {
        return 0;
      }

      var score = 0;
      var numPerms = 0;
      for (var i = 0; i < team.length; i += 1) {
        for (var j = i + 1; j < team.length; j += 1) {
          score += this.scoreStudents(i, j);
          numPerms += 1;
        }
      }

      return score / numPerms;
    }
  }, {
    key: 'isPromising',
    value: function isPromising(solutionPath, permLength) {
      // calculate upper bound

      if (!this.isValid(solutionPath, permLength)) {
        return false;
      }

      // estimate solutionPath score;
      var currentScore = this.calculateScore(solutionPath, permLength);

      // calculate maximum estimate for rest of thing
      // find best pairwise compatibility, then multiply by number of teams remaining?
      var estimate = this.calculateEstimate(solutionPath, permLength);

      var currentEstimate = currentScore + estimate;

      if (currentEstimate < this.lowerBound || currentEstimate > this.upperBound) {
        return false;
      }

      return true;
    }
  }, {
    key: 'calculateUpperBound',
    value: function calculateUpperBound(solutionPath) {
      var permLength = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

      return this.calculateEstimate(solutionPath, permLength);
    }
  }, {
    key: 'calculateEstimate',
    value: function calculateEstimate(solutionPath) {
      var permLength = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;

      // find best pairwise compatibility in remaining set
      var bestScore = 0;
      var maxRemainingTeams = Math.ceil((solutionPath.length - permLength) / this.minimumStudentsPerTeam);

      for (var i = permLength; i < solutionPath.length; i += 1) {
        if (solutionPath[i].type === 'bar') {
          continue;
        }

        var studentOne = solutionPath[i].student;
        for (var j = i + 1; j < solutionPath.length; j += 1) {
          if (solutionPath[j].type === 'bar') {
            continue;
          }

          var score = this.scoreStudents(solutionPath[j].student, studentOne);
          if (score > bestScore) {
            bestScore = score;
          }
        }
      }

      return maxRemainingTeams * bestScore;
    }
  }, {
    key: 'scoreStudents',
    value: function scoreStudents(a, b) {
      return this.scoredAgainst[a][b];
    }
  }, {
    key: 'genPerms',
    value: function genPerms(permLength) {
      if (permLength === this.solutionPath.length) {
        if (this.isSolution(this.solutionPath)) {
          var score = this.calculateScore(this.solutionPath);
          if (score > this.lowerBound) {
            this.lowerBound = score;
            this.bestSolutionPath = this.solutionPath;
            if (this.debug) {
              console.log('DPMatch: found a solution (' + score + ')');
            }
          }
        }
      }

      if (!this.isPromising(this.solutionPath, permLength)) {
        return;
      }

      for (var i = permLength; i < this.solutionPath.length; i += 1) {
        this.solutionPath = DPMatch.swap(this.solutionPath, permLength, i);
        this.genPerms(permLength + 1);
        this.solutionPath = DPMatch.swap(this.solutionPath, permLength, i);
      }
    }
  }], [{
    key: 'swap',
    value: function swap(solutionPath, a, b) {
      var newSolPath = solutionPath;
      var temp = Object.assign({}, newSolPath[a]);
      newSolPath[a] = solutionPath[b];
      newSolPath[b] = temp;

      return newSolPath;
    }
  }]);

  return DPMatch;
}();

exports.default = DPMatch;
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImRwLW1hdGNoZXIuanMiXSwibmFtZXMiOlsiRFBNYXRjaCIsInN0dWRlbnRzIiwidGVhbWluZ0NvbmZpZyIsImRlYnVnIiwibWFwIiwiU3R1ZGVudCIsInN0dWRlbnQiLCJpbml0aWFsaXplQWRqYWNlbmN5TWF0cml4IiwiY29uc29sZSIsImxvZyIsInJlbW92ZUR1cGxpY2F0ZXNTdHVkZW50cyIsInNjb3JlZEFnYWluc3QiLCJpIiwibGVuZ3RoIiwicHVzaCIsImoiLCJzY29yZUFnYWluc3QiLCJtaW5pbXVtU3R1ZGVudHNQZXJUZWFtIiwibWF4aW11bVN0dWRlbnRzUGVyVGVhbSIsImxvd2VyQm91bmQiLCJnZW5lcmF0ZUluaXRpYWxTb2x1dGlvblBhdGgiLCJzb2x1dGlvblBhdGgiLCJiZXN0U29sdXRpb25QYXRoIiwidXBwZXJCb3VuZCIsImNhbGN1bGF0ZVVwcGVyQm91bmQiLCJ0aW1lIiwiZ2VuUGVybXMiLCJ0aW1lRW5kIiwicGF0aFRvVGVhbXMiLCJkdXBBcnJheSIsImdldFN0dWRlbnRzIiwiZm9yRWFjaCIsImhhc2giLCJPYmplY3QiLCJ2YWx1ZXMiLCJpbml0aWFsTnVtYmVyT2ZCYXJzIiwiTWF0aCIsImZsb29yIiwiaW5kZXgiLCJ0eXBlIiwiaTEiLCJyYW5kb20iLCJpMiIsInRlbXAiLCJzd2FwVmFsIiwiYXNzaWduIiwicmVtYWluZGVyU3R1ZGVudHMiLCJsYXN0SW5kZXgiLCJrIiwic3dhcCIsImZpcnN0QmFySW5kZXgiLCJuZXh0QmFySW5kZXgiLCJ0ZWFtcyIsImZpbmRGdW5jIiwidiIsImZpbmRJbmRleCIsInRlYW0iLCJUZWFtIiwiY291bnQiLCJwZXJtTGVuZ3RoIiwibWluIiwic2NvcmUiLCJzY29yZVRlYW0iLCJudW1QZXJtcyIsInNjb3JlU3R1ZGVudHMiLCJpc1ZhbGlkIiwiY3VycmVudFNjb3JlIiwiY2FsY3VsYXRlU2NvcmUiLCJlc3RpbWF0ZSIsImNhbGN1bGF0ZUVzdGltYXRlIiwiY3VycmVudEVzdGltYXRlIiwiYmVzdFNjb3JlIiwibWF4UmVtYWluaW5nVGVhbXMiLCJjZWlsIiwic3R1ZGVudE9uZSIsImEiLCJiIiwiaXNTb2x1dGlvbiIsImlzUHJvbWlzaW5nIiwibmV3U29sUGF0aCJdLCJtYXBwaW5ncyI6Ijs7Ozs7O3FqQkFBQTtBQUNBOzs7QUFDQTs7OztBQUNBOzs7Ozs7OztBQUVBO0FBQ0E7QUFDQTs7SUFFcUJBLE87QUFDbkI7QUFDQSxtQkFBWUMsUUFBWixFQUFzQkMsYUFBdEIsRUFBb0Q7QUFBQSxRQUFmQyxLQUFlLHVFQUFQLEtBQU87O0FBQUE7O0FBQ2xELFNBQUtGLFFBQUwsR0FBZ0JBLFNBQVNHLEdBQVQsQ0FBYTtBQUFBLGFBQVcsSUFBSUMsaUJBQUosQ0FBWUMsT0FBWixFQUFxQkosYUFBckIsQ0FBWDtBQUFBLEtBQWIsQ0FBaEI7QUFDQSxTQUFLQyxLQUFMLEdBQWFBLEtBQWI7O0FBRUEsU0FBS0kseUJBQUw7O0FBRUEsUUFBSSxLQUFLSixLQUFULEVBQWdCO0FBQUVLLGNBQVFDLEdBQVIsQ0FBWSxpQ0FBWjtBQUFpRDs7QUFFbkUsU0FBS0Msd0JBQUw7QUFDRDs7OztrQ0FFYTtBQUNaLGFBQU8sS0FBS1QsUUFBWjtBQUNEOzs7Z0RBRTJCO0FBQzFCLFdBQUtVLGFBQUwsR0FBcUIsRUFBckI7QUFDQSxXQUFLLElBQUlDLElBQUksQ0FBYixFQUFnQkEsSUFBSSxLQUFLWCxRQUFMLENBQWNZLE1BQWxDLEVBQTBDRCxLQUFLLENBQS9DLEVBQWtEO0FBQ2hELGFBQUtELGFBQUwsQ0FBbUJHLElBQW5CLENBQXdCLEVBQXhCO0FBQ0EsYUFBSyxJQUFJQyxJQUFJLENBQWIsRUFBZ0JBLElBQUksS0FBS2QsUUFBTCxDQUFjWSxNQUFsQyxFQUEwQ0UsS0FBSyxDQUEvQyxFQUFrRDtBQUNoRCxjQUFJSCxLQUFLRyxDQUFULEVBQVk7QUFDVixpQkFBS0osYUFBTCxDQUFtQkMsQ0FBbkIsRUFBc0JFLElBQXRCLENBQTJCLEtBQUtiLFFBQUwsQ0FBY1csQ0FBZCxFQUFpQkksWUFBakIsQ0FBOEIsS0FBS2YsUUFBTCxDQUFjYyxDQUFkLENBQTlCLENBQTNCO0FBQ0Q7QUFDRjtBQUNGO0FBQ0Y7OzsrQkFFZ0U7QUFBQSx1Q0FBMURFLHNCQUEwRDtBQUFBLFVBQTFEQSxzQkFBMEQseUNBQWpDLENBQWlDO0FBQUEsdUNBQTlCQyxzQkFBOEI7QUFBQSxVQUE5QkEsc0JBQThCLHlDQUFMLENBQUs7O0FBQy9EO0FBQ0E7QUFDQSxXQUFLRCxzQkFBTCxHQUE4QkEsc0JBQTlCO0FBQ0EsV0FBS0Msc0JBQUwsR0FBOEJBLHNCQUE5Qjs7QUFFQSxXQUFLQyxVQUFMLEdBQWtCLENBQWxCO0FBQ0EsVUFBSSxLQUFLaEIsS0FBVCxFQUFnQjtBQUFFSyxnQkFBUUMsR0FBUixDQUFZLHFDQUFaO0FBQXFEO0FBQ3ZFLFdBQUtXLDJCQUFMO0FBQ0EsV0FBS0MsWUFBTCxHQUFvQixLQUFLQyxnQkFBekI7O0FBRUEsVUFBSSxLQUFLbkIsS0FBVCxFQUFnQjtBQUFFSyxnQkFBUUMsR0FBUixDQUFZLDZDQUFaO0FBQTZEO0FBQy9FLFdBQUtjLFVBQUwsR0FBa0IsS0FBS0MsbUJBQUwsQ0FBeUIsS0FBS0YsZ0JBQTlCLEVBQWdELENBQWhELENBQWxCOztBQUVBLFVBQUksS0FBS25CLEtBQVQsRUFBZ0I7QUFBRUssZ0JBQVFDLEdBQVIsQ0FBWSxxQkFBWixFQUFvQ0QsUUFBUWlCLElBQVIsQ0FBYSxXQUFiO0FBQTRCO0FBQ2xGLFdBQUtDLFFBQUwsQ0FBYyxDQUFkO0FBQ0EsVUFBSSxLQUFLdkIsS0FBVCxFQUFnQjtBQUFFSyxnQkFBUUMsR0FBUixDQUFZLGtCQUFaLEVBQWlDRCxRQUFRbUIsT0FBUixDQUFnQixXQUFoQjtBQUErQjs7QUFFbEYsVUFBSSxLQUFLeEIsS0FBVCxFQUFnQjtBQUFFSyxnQkFBUUMsR0FBUixDQUFZLHlDQUFaO0FBQXlEO0FBQzNFLGFBQU8sS0FBS21CLFdBQUwsQ0FBaUIsS0FBS04sZ0JBQXRCLENBQVA7QUFDRDs7QUFFRDs7OzsrQ0FDMkI7QUFDekIsVUFBTU8sV0FBVyxFQUFqQjtBQUNBLFdBQUtDLFdBQUwsR0FBbUJDLE9BQW5CLENBQTJCLFVBQUN6QixPQUFELEVBQWE7QUFDdEN1QixpQkFBU3ZCLFFBQVEwQixJQUFqQixJQUF5QjFCLE9BQXpCO0FBQ0QsT0FGRDs7QUFJQSxXQUFLTCxRQUFMLEdBQWdCZ0MsT0FBT0MsTUFBUCxDQUFjTCxRQUFkLENBQWhCO0FBQ0Q7OztrREFFNkI7QUFBQTs7QUFDNUIsVUFBTU0sc0JBQXNCQyxLQUFLQyxLQUFMLENBQVcsS0FBS1AsV0FBTCxHQUFtQmpCLE1BQW5CLEdBQTRCLEtBQUtLLHNCQUE1QyxDQUE1QjtBQUNBLFdBQUtJLGdCQUFMLEdBQXdCLEVBQXhCO0FBQ0EsV0FBS1EsV0FBTCxHQUFtQkMsT0FBbkIsQ0FBMkIsVUFBQ3pCLE9BQUQsRUFBVWdDLEtBQVYsRUFBb0I7QUFDN0MsY0FBS2hCLGdCQUFMLENBQXNCUixJQUF0QixDQUEyQixFQUFFeUIsTUFBTSxTQUFSLEVBQW1CakMsU0FBU2dDLEtBQTVCLEVBQTNCO0FBQ0QsT0FGRDs7QUFJQTtBQUNBLFdBQUssSUFBSTFCLElBQUksQ0FBYixFQUFnQkEsSUFBSSxFQUFwQixFQUF3QkEsS0FBSyxDQUE3QixFQUFnQztBQUM5QjtBQUNBLFlBQU00QixLQUFLSixLQUFLQyxLQUFMLENBQVdELEtBQUtLLE1BQUwsS0FBZ0IsS0FBS1gsV0FBTCxHQUFtQmpCLE1BQTlDLElBQXdELEtBQUtpQixXQUFMLEdBQW1CakIsTUFBdEY7QUFDQSxZQUFNNkIsS0FBS04sS0FBS0MsS0FBTCxDQUFXRCxLQUFLSyxNQUFMLEtBQWdCLEtBQUtYLFdBQUwsR0FBbUJqQixNQUE5QyxJQUF3RCxLQUFLaUIsV0FBTCxHQUFtQmpCLE1BQXRGO0FBQ0EsWUFBTThCLE9BQU8sS0FBS3JCLGdCQUFMLENBQXNCa0IsRUFBdEIsQ0FBYjtBQUNBLGFBQUtsQixnQkFBTCxDQUFzQmtCLEVBQXRCLElBQTRCLEtBQUtsQixnQkFBTCxDQUFzQm9CLEVBQXRCLENBQTVCO0FBQ0EsYUFBS3BCLGdCQUFMLENBQXNCb0IsRUFBdEIsSUFBNEJDLElBQTVCO0FBQ0Q7O0FBRUQsV0FBSyxJQUFJL0IsS0FBSSxDQUFiLEVBQWdCQSxLQUFJdUIsbUJBQXBCLEVBQXlDdkIsTUFBSyxDQUE5QyxFQUFpRDtBQUMvQyxZQUFNMEIsUUFBUyxDQUFDMUIsS0FBSSxDQUFMLElBQVUsS0FBS00sc0JBQWhCLEdBQTBDTixFQUF4RDtBQUNBLFlBQU1nQyxVQUFVWCxPQUFPWSxNQUFQLENBQWMsRUFBZCxFQUFrQixLQUFLdkIsZ0JBQUwsQ0FBc0JnQixLQUF0QixDQUFsQixDQUFoQjtBQUNBLGFBQUtoQixnQkFBTCxDQUFzQmdCLEtBQXRCLElBQStCLEVBQUVDLE1BQU0sS0FBUixFQUEvQjtBQUNBLGFBQUtqQixnQkFBTCxDQUFzQlIsSUFBdEIsQ0FBMkI4QixPQUEzQjtBQUNEOztBQUVEO0FBQ0EsVUFBTUUsb0JBQW9CLEtBQUtoQixXQUFMLEdBQW1CakIsTUFBbkIsR0FBNEJzQixtQkFBdEQ7QUFDQSxVQUFJVyxvQkFBb0IsS0FBSzdCLHNCQUE3QixFQUFxRDtBQUNuRCxZQUFJOEIsWUFBWSxLQUFLekIsZ0JBQUwsQ0FBc0JULE1BQXRDO0FBQ0EsYUFBSyxJQUFJRCxNQUFJLENBQWIsRUFBZ0JBLE1BQUlrQyxpQkFBcEIsRUFBdUNsQyxPQUFLLENBQTVDLEVBQStDO0FBQzdDO0FBQ0E7QUFDQSxlQUFLLElBQUlHLElBQUlnQyxZQUFZLENBQXpCLEVBQTRCaEMsSUFBSSxDQUFoQyxFQUFtQ0EsS0FBSyxDQUF4QyxFQUEyQztBQUN6QyxnQkFBSSxLQUFLTyxnQkFBTCxDQUFzQlAsQ0FBdEIsRUFBeUJ3QixJQUF6QixLQUFrQyxLQUF0QyxFQUE2QztBQUMzQ1EsMEJBQVloQyxDQUFaO0FBQ0E7QUFDRDtBQUNGOztBQUVELGVBQUssSUFBSWlDLElBQUksQ0FBYixFQUFnQkEsSUFBSSxLQUFLL0Isc0JBQUwsR0FBOEI2QixpQkFBOUIsR0FBa0RsQyxHQUF0RSxFQUF5RW9DLEtBQUssQ0FBOUUsRUFBaUY7QUFDL0UsaUJBQUsxQixnQkFBTCxHQUF3QnRCLFFBQVFpRCxJQUFSLENBQWEsS0FBSzNCLGdCQUFsQixFQUFvQ3lCLFNBQXBDLEVBQStDQSxZQUFZQyxDQUFaLEdBQWdCLENBQS9ELENBQXhCO0FBQ0Q7QUFDRjtBQUNGO0FBQ0Q7QUFDRDs7O2dDQUVXM0IsWSxFQUFjO0FBQ3hCLFVBQUk2QixnQkFBZ0IsQ0FBcEI7QUFDQSxVQUFJQyxlQUFlLENBQW5CO0FBQ0EsVUFBTUMsUUFBUSxFQUFkO0FBQ0EsVUFBTUMsV0FBVyxTQUFYQSxRQUFXLENBQUNDLENBQUQsRUFBSTFDLENBQUo7QUFBQSxlQUFVQSxJQUFLdUMsWUFBTCxJQUFzQkcsRUFBRWYsSUFBRixLQUFXLEtBQTNDO0FBQUEsT0FBakI7QUFDQSxhQUFPWSxpQkFBaUI5QixhQUFhUixNQUFyQyxFQUE2QztBQUMzQ3FDLHdCQUFnQkMsWUFBaEI7QUFDQUEsdUJBQWU5QixhQUFha0MsU0FBYixDQUF1QkYsUUFBdkIsQ0FBZjtBQUNBLFlBQUlGLGlCQUFpQixDQUFDLENBQWxCLElBQXVCQSxlQUFlOUIsYUFBYVIsTUFBdkQsRUFBK0Q7QUFBRXNDLHlCQUFlOUIsYUFBYVIsTUFBNUI7QUFBcUM7O0FBRXRHLFlBQU0yQyxPQUFPLEVBQWI7QUFDQSxhQUFLLElBQUk1QyxJQUFJc0MsZ0JBQWdCLENBQTdCLEVBQWdDdEMsSUFBSXVDLFlBQXBDLEVBQWtEdkMsS0FBSyxDQUF2RCxFQUEwRDtBQUN4RDRDLGVBQUsxQyxJQUFMLENBQVUsS0FBS2IsUUFBTCxDQUFjb0IsYUFBYVQsQ0FBYixFQUFnQk4sT0FBOUIsQ0FBVjtBQUNEOztBQUVEOEMsY0FBTXRDLElBQU4sQ0FBVyxJQUFJMkMsY0FBSixDQUFTRCxJQUFULENBQVg7QUFDRDs7QUFFRCxhQUFPSixLQUFQO0FBQ0Q7OzsrQkFFVS9CLFksRUFBYztBQUN2QjtBQUNBLFdBQUssSUFBSVQsSUFBSSxDQUFiLEVBQWdCQSxJQUFJUyxhQUFhUixNQUFqQyxFQUF5Q0QsS0FBSyxDQUE5QyxFQUFpRDtBQUMvQyxZQUFJUyxhQUFhVCxDQUFiLEVBQWdCMkIsSUFBaEIsS0FBeUIsS0FBN0IsRUFBb0M7QUFDbEM7QUFDQSxjQUFJbUIsUUFBUSxDQUFaO0FBQ0EsaUJBQU85QyxJQUFJOEMsS0FBSixHQUFZckMsYUFBYVIsTUFBekIsSUFDRVEsYUFBYVQsSUFBSThDLEtBQWpCLEVBQXdCbkIsSUFBeEIsS0FBaUMsS0FEMUMsRUFDaUQ7QUFDL0NtQixxQkFBUyxDQUFUO0FBQ0Q7QUFDREEsbUJBQVMsQ0FBVDs7QUFFQSxjQUFJQSxRQUFRLEtBQUt4QyxzQkFBYixJQUF1Q3dDLFFBQVEsS0FBS3pDLHNCQUF4RCxFQUFnRjtBQUM5RSxtQkFBTyxLQUFQO0FBQ0Q7QUFDRjtBQUNGOztBQUVELGFBQU8sSUFBUDtBQUNEOzs7NEJBRU9JLFksRUFBY3NDLFUsRUFBWTtBQUNoQzs7QUFFQSxVQUFJQSxlQUFlLENBQW5CLEVBQXNCO0FBQ3BCLGVBQU8sSUFBUDtBQUNEOztBQUVELFdBQUssSUFBSS9DLElBQUksQ0FBYixFQUFnQkEsSUFBSXdCLEtBQUt3QixHQUFMLENBQVMsS0FBSzNDLHNCQUFkLEVBQXNDMEMsVUFBdEMsQ0FBcEIsRUFBdUUvQyxLQUFLLENBQTVFLEVBQStFO0FBQzdFLFlBQUlTLGFBQWFULENBQWIsRUFBZ0IyQixJQUFoQixLQUF5QixLQUE3QixFQUFvQztBQUNsQyxpQkFBTyxLQUFQO0FBQ0Q7QUFDRjs7QUFFRCxVQUFJb0IsY0FBYyxLQUFLekMsc0JBQXZCLEVBQStDO0FBQzdDLGVBQU8sSUFBUDtBQUNEOztBQUVELFdBQUssSUFBSU4sTUFBSSxDQUFiLEVBQWdCQSxNQUFJK0MsVUFBcEIsRUFBZ0MvQyxPQUFLLENBQXJDLEVBQXdDO0FBQ3RDLFlBQUlTLGFBQWFULEdBQWIsRUFBZ0IyQixJQUFoQixLQUF5QixLQUF6QixJQUFrQzNCLFFBQU0sQ0FBNUMsRUFBK0M7QUFDN0M7QUFDQSxjQUFJOEMsUUFBUSxDQUFaO0FBQ0EsaUJBQU85QyxNQUFJOEMsS0FBSixHQUFZQyxVQUFaLElBQ0V0QyxhQUFhVCxNQUFJOEMsS0FBakIsRUFBd0JuQixJQUF4QixLQUFpQyxLQUQxQyxFQUNpRDtBQUMvQ21CLHFCQUFTLENBQVQ7QUFDRDtBQUNEQSxtQkFBUyxDQUFUOztBQUVBLGNBQUksQ0FBQ0EsUUFBUSxLQUFLeEMsc0JBQWIsSUFDRXdDLFFBQVEsS0FBS3pDLHNCQURoQixLQUVHTCxPQUFLK0MsYUFBYSxLQUFLekMsc0JBRjlCLEVBRXNEO0FBQ3BELG1CQUFPLEtBQVA7QUFDRDtBQUNGO0FBQ0Y7O0FBRUQsYUFBTyxJQUFQO0FBQ0Q7OzttQ0FFY0csWSxFQUFjc0MsVSxFQUFZO0FBQ3ZDLFVBQUlULGdCQUFnQixDQUFDLENBQXJCO0FBQ0EsVUFBSUMsZUFBZSxDQUFuQjs7QUFFQSxVQUFJVSxRQUFRLENBQVo7QUFDQSxVQUFNUixXQUFXLFNBQVhBLFFBQVcsQ0FBQ0MsQ0FBRCxFQUFJMUMsQ0FBSjtBQUFBLGVBQVVBLElBQUt1QyxZQUFMLElBQXNCRyxFQUFFZixJQUFGLEtBQVcsS0FBM0M7QUFBQSxPQUFqQjtBQUNBLGFBQU9ZLGlCQUFpQlEsVUFBeEIsRUFBb0M7QUFDbENULHdCQUFnQkMsWUFBaEI7QUFDQUEsdUJBQWU5QixhQUFha0MsU0FBYixDQUF1QkYsUUFBdkIsQ0FBZjtBQUNBLFlBQUlGLGlCQUFpQixDQUFDLENBQWxCLElBQXVCQSxlQUFlUSxVQUExQyxFQUFzRDtBQUFFUix5QkFBZVEsVUFBZjtBQUE0Qjs7QUFFcEYsWUFBTUgsT0FBTyxFQUFiO0FBQ0EsYUFBSyxJQUFJNUMsSUFBSXNDLGdCQUFnQixDQUE3QixFQUFnQ3RDLElBQUl1QyxZQUFwQyxFQUFrRHZDLEtBQUssQ0FBdkQsRUFBMEQ7QUFDeEQ0QyxlQUFLMUMsSUFBTCxDQUFVTyxhQUFhVCxDQUFiLEVBQWdCTixPQUExQjtBQUNEOztBQUVEdUQsaUJBQVMsS0FBS0MsU0FBTCxDQUFlTixJQUFmLENBQVQ7QUFDRDs7QUFFRCxhQUFPSyxLQUFQO0FBQ0Q7Ozs4QkFFU0wsSSxFQUFNO0FBQ2Q7QUFDQSxVQUFJQSxLQUFLM0MsTUFBTCxJQUFlLENBQW5CLEVBQXNCO0FBQ3BCLGVBQU8sQ0FBUDtBQUNEOztBQUVELFVBQUlnRCxRQUFRLENBQVo7QUFDQSxVQUFJRSxXQUFXLENBQWY7QUFDQSxXQUFLLElBQUluRCxJQUFJLENBQWIsRUFBZ0JBLElBQUk0QyxLQUFLM0MsTUFBekIsRUFBaUNELEtBQUssQ0FBdEMsRUFBeUM7QUFDdkMsYUFBSyxJQUFJRyxJQUFJSCxJQUFJLENBQWpCLEVBQW9CRyxJQUFJeUMsS0FBSzNDLE1BQTdCLEVBQXFDRSxLQUFLLENBQTFDLEVBQTZDO0FBQzNDOEMsbUJBQVMsS0FBS0csYUFBTCxDQUFtQnBELENBQW5CLEVBQXNCRyxDQUF0QixDQUFUO0FBQ0FnRCxzQkFBWSxDQUFaO0FBQ0Q7QUFDRjs7QUFFRCxhQUFPRixRQUFRRSxRQUFmO0FBQ0Q7OztnQ0FFVzFDLFksRUFBY3NDLFUsRUFBWTtBQUNwQzs7QUFFQSxVQUFJLENBQUMsS0FBS00sT0FBTCxDQUFhNUMsWUFBYixFQUEyQnNDLFVBQTNCLENBQUwsRUFBNkM7QUFDM0MsZUFBTyxLQUFQO0FBQ0Q7O0FBRUQ7QUFDQSxVQUFNTyxlQUFlLEtBQUtDLGNBQUwsQ0FBb0I5QyxZQUFwQixFQUFrQ3NDLFVBQWxDLENBQXJCOztBQUVBO0FBQ0E7QUFDQSxVQUFNUyxXQUFXLEtBQUtDLGlCQUFMLENBQXVCaEQsWUFBdkIsRUFBcUNzQyxVQUFyQyxDQUFqQjs7QUFFQSxVQUFNVyxrQkFBa0JKLGVBQWVFLFFBQXZDOztBQUVBLFVBQUlFLGtCQUFrQixLQUFLbkQsVUFBdkIsSUFBcUNtRCxrQkFBa0IsS0FBSy9DLFVBQWhFLEVBQTRFO0FBQzFFLGVBQU8sS0FBUDtBQUNEOztBQUVELGFBQU8sSUFBUDtBQUNEOzs7d0NBV21CRixZLEVBQThCO0FBQUEsVUFBaEJzQyxVQUFnQix1RUFBSCxDQUFHOztBQUNoRCxhQUFPLEtBQUtVLGlCQUFMLENBQXVCaEQsWUFBdkIsRUFBcUNzQyxVQUFyQyxDQUFQO0FBQ0Q7OztzQ0FFaUJ0QyxZLEVBQThCO0FBQUEsVUFBaEJzQyxVQUFnQix1RUFBSCxDQUFHOztBQUM5QztBQUNBLFVBQUlZLFlBQVksQ0FBaEI7QUFDQSxVQUFNQyxvQkFBb0JwQyxLQUFLcUMsSUFBTCxDQUFVLENBQUNwRCxhQUFhUixNQUFiLEdBQXNCOEMsVUFBdkIsSUFBcUMsS0FBSzFDLHNCQUFwRCxDQUExQjs7QUFFQSxXQUFLLElBQUlMLElBQUkrQyxVQUFiLEVBQXlCL0MsSUFBSVMsYUFBYVIsTUFBMUMsRUFBa0RELEtBQUssQ0FBdkQsRUFBMEQ7QUFDeEQsWUFBSVMsYUFBYVQsQ0FBYixFQUFnQjJCLElBQWhCLEtBQXlCLEtBQTdCLEVBQW9DO0FBQ2xDO0FBQ0Q7O0FBRUQsWUFBTW1DLGFBQWFyRCxhQUFhVCxDQUFiLEVBQWdCTixPQUFuQztBQUNBLGFBQUssSUFBSVMsSUFBSUgsSUFBSSxDQUFqQixFQUFvQkcsSUFBSU0sYUFBYVIsTUFBckMsRUFBNkNFLEtBQUssQ0FBbEQsRUFBcUQ7QUFDbkQsY0FBSU0sYUFBYU4sQ0FBYixFQUFnQndCLElBQWhCLEtBQXlCLEtBQTdCLEVBQW9DO0FBQ2xDO0FBQ0Q7O0FBRUQsY0FBTXNCLFFBQVEsS0FBS0csYUFBTCxDQUFtQjNDLGFBQWFOLENBQWIsRUFBZ0JULE9BQW5DLEVBQTRDb0UsVUFBNUMsQ0FBZDtBQUNBLGNBQUliLFFBQVFVLFNBQVosRUFBdUI7QUFDckJBLHdCQUFZVixLQUFaO0FBQ0Q7QUFDRjtBQUNGOztBQUVELGFBQU9XLG9CQUFvQkQsU0FBM0I7QUFDRDs7O2tDQUVhSSxDLEVBQUdDLEMsRUFBRztBQUNsQixhQUFPLEtBQUtqRSxhQUFMLENBQW1CZ0UsQ0FBbkIsRUFBc0JDLENBQXRCLENBQVA7QUFDRDs7OzZCQUVRakIsVSxFQUFZO0FBQ25CLFVBQUlBLGVBQWUsS0FBS3RDLFlBQUwsQ0FBa0JSLE1BQXJDLEVBQTZDO0FBQzNDLFlBQUksS0FBS2dFLFVBQUwsQ0FBZ0IsS0FBS3hELFlBQXJCLENBQUosRUFBd0M7QUFDdEMsY0FBTXdDLFFBQVEsS0FBS00sY0FBTCxDQUFvQixLQUFLOUMsWUFBekIsQ0FBZDtBQUNBLGNBQUl3QyxRQUFRLEtBQUsxQyxVQUFqQixFQUE2QjtBQUMzQixpQkFBS0EsVUFBTCxHQUFrQjBDLEtBQWxCO0FBQ0EsaUJBQUt2QyxnQkFBTCxHQUF3QixLQUFLRCxZQUE3QjtBQUNBLGdCQUFJLEtBQUtsQixLQUFULEVBQWdCO0FBQUVLLHNCQUFRQyxHQUFSLGlDQUEwQ29ELEtBQTFDO0FBQXNEO0FBQ3pFO0FBQ0Y7QUFDRjs7QUFFRCxVQUFJLENBQUMsS0FBS2lCLFdBQUwsQ0FBaUIsS0FBS3pELFlBQXRCLEVBQW9Dc0MsVUFBcEMsQ0FBTCxFQUFzRDtBQUNwRDtBQUNEOztBQUVELFdBQUssSUFBSS9DLElBQUkrQyxVQUFiLEVBQXlCL0MsSUFBSSxLQUFLUyxZQUFMLENBQWtCUixNQUEvQyxFQUF1REQsS0FBSyxDQUE1RCxFQUErRDtBQUM3RCxhQUFLUyxZQUFMLEdBQW9CckIsUUFBUWlELElBQVIsQ0FBYSxLQUFLNUIsWUFBbEIsRUFBZ0NzQyxVQUFoQyxFQUE0Qy9DLENBQTVDLENBQXBCO0FBQ0EsYUFBS2MsUUFBTCxDQUFjaUMsYUFBYSxDQUEzQjtBQUNBLGFBQUt0QyxZQUFMLEdBQW9CckIsUUFBUWlELElBQVIsQ0FBYSxLQUFLNUIsWUFBbEIsRUFBZ0NzQyxVQUFoQyxFQUE0Qy9DLENBQTVDLENBQXBCO0FBQ0Q7QUFDRjs7O3lCQWhFV1MsWSxFQUFjc0QsQyxFQUFHQyxDLEVBQUc7QUFDOUIsVUFBTUcsYUFBYTFELFlBQW5CO0FBQ0EsVUFBTXNCLE9BQU9WLE9BQU9ZLE1BQVAsQ0FBYyxFQUFkLEVBQWtCa0MsV0FBV0osQ0FBWCxDQUFsQixDQUFiO0FBQ0FJLGlCQUFXSixDQUFYLElBQWdCdEQsYUFBYXVELENBQWIsQ0FBaEI7QUFDQUcsaUJBQVdILENBQVgsSUFBZ0JqQyxJQUFoQjs7QUFFQSxhQUFPb0MsVUFBUDtBQUNEOzs7Ozs7a0JBalFrQi9FLE8iLCJmaWxlIjoiZHAtbWF0Y2hlci5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIGVzbGludC1kaXNhYmxlIG5vLWNvbnNvbGUgKi9cbi8qIGVzbGludC1kaXNhYmxlIG5vLWNvbnRpbnVlICovXG5pbXBvcnQgU3R1ZGVudCBmcm9tICcuL3N0dWRlbnQnO1xuaW1wb3J0IFRlYW0gZnJvbSAnLi90ZWFtJztcblxuLy8gYmFzaWNhbGx5IHN0YXJzIGFuZCBiYXJzXG4vLyB3ZSBoYXZlIHN0dWRlbnRzIGFuZCBzb21lIHNlcGVyYXRvcnMgZm9yIHRoZW1cbi8vIHdlIG5lZWQgYW4gaW5pdGFsIGVzdGltYXRlIHRob1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBEUE1hdGNoIHtcbiAgLy8gcHVibGljXG4gIGNvbnN0cnVjdG9yKHN0dWRlbnRzLCB0ZWFtaW5nQ29uZmlnLCBkZWJ1ZyA9IGZhbHNlKSB7XG4gICAgdGhpcy5zdHVkZW50cyA9IHN0dWRlbnRzLm1hcChzdHVkZW50ID0+IG5ldyBTdHVkZW50KHN0dWRlbnQsIHRlYW1pbmdDb25maWcpKTtcbiAgICB0aGlzLmRlYnVnID0gZGVidWc7XG5cbiAgICB0aGlzLmluaXRpYWxpemVBZGphY2VuY3lNYXRyaXgoKTtcblxuICAgIGlmICh0aGlzLmRlYnVnKSB7IGNvbnNvbGUubG9nKCdEUE1hdGNoOiBSZW1vdmluZyBkdXBsaWNhdGVzLi4uJyk7IH1cblxuICAgIHRoaXMucmVtb3ZlRHVwbGljYXRlc1N0dWRlbnRzKCk7XG4gIH1cblxuICBnZXRTdHVkZW50cygpIHtcbiAgICByZXR1cm4gdGhpcy5zdHVkZW50cztcbiAgfVxuXG4gIGluaXRpYWxpemVBZGphY2VuY3lNYXRyaXgoKSB7XG4gICAgdGhpcy5zY29yZWRBZ2FpbnN0ID0gW107XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLnN0dWRlbnRzLmxlbmd0aDsgaSArPSAxKSB7XG4gICAgICB0aGlzLnNjb3JlZEFnYWluc3QucHVzaChbXSk7XG4gICAgICBmb3IgKGxldCBqID0gMDsgaiA8IHRoaXMuc3R1ZGVudHMubGVuZ3RoOyBqICs9IDEpIHtcbiAgICAgICAgaWYgKGkgIT0gaikge1xuICAgICAgICAgIHRoaXMuc2NvcmVkQWdhaW5zdFtpXS5wdXNoKHRoaXMuc3R1ZGVudHNbaV0uc2NvcmVBZ2FpbnN0KHRoaXMuc3R1ZGVudHNbal0pKTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHRlYW0oeyBtaW5pbXVtU3R1ZGVudHNQZXJUZWFtID0gMywgbWF4aW11bVN0dWRlbnRzUGVyVGVhbSA9IDQgfSkge1xuICAgIC8vIGdlbiBwZXJtc1xuICAgIC8vIGRwIHRoaXMgc2hpelxuICAgIHRoaXMubWluaW11bVN0dWRlbnRzUGVyVGVhbSA9IG1pbmltdW1TdHVkZW50c1BlclRlYW07XG4gICAgdGhpcy5tYXhpbXVtU3R1ZGVudHNQZXJUZWFtID0gbWF4aW11bVN0dWRlbnRzUGVyVGVhbTtcblxuICAgIHRoaXMubG93ZXJCb3VuZCA9IDA7XG4gICAgaWYgKHRoaXMuZGVidWcpIHsgY29uc29sZS5sb2coJ0RQTWF0Y2g6IEdlbmVyYXRpbmcgaW5pdGlhbCBwYXRoLi4uJyk7IH1cbiAgICB0aGlzLmdlbmVyYXRlSW5pdGlhbFNvbHV0aW9uUGF0aCgpO1xuICAgIHRoaXMuc29sdXRpb25QYXRoID0gdGhpcy5iZXN0U29sdXRpb25QYXRoO1xuXG4gICAgaWYgKHRoaXMuZGVidWcpIHsgY29uc29sZS5sb2coJ0RQTWF0Y2g6IGNhbGN1bGF0aW5nIGluaXRpYWwgdXBwZXIgYm91bmQuLi4nKTsgfVxuICAgIHRoaXMudXBwZXJCb3VuZCA9IHRoaXMuY2FsY3VsYXRlVXBwZXJCb3VuZCh0aGlzLmJlc3RTb2x1dGlvblBhdGgsIDApO1xuXG4gICAgaWYgKHRoaXMuZGVidWcpIHsgY29uc29sZS5sb2coJ0RQTWF0Y2g6IHRlYW1pbmcuLi4nKTsgY29uc29sZS50aW1lKCdEUE1hdGNoZXInKTsgfVxuICAgIHRoaXMuZ2VuUGVybXMoMCk7XG4gICAgaWYgKHRoaXMuZGVidWcpIHsgY29uc29sZS5sb2coJ0RQTWF0Y2g6IGRvbmUuLi4nKTsgY29uc29sZS50aW1lRW5kKCdEUE1hdGNoZXInKTsgfVxuXG4gICAgaWYgKHRoaXMuZGVidWcpIHsgY29uc29sZS5sb2coJ0RQTWF0Y2g6IGdlbmVyYXRpbmcgdGVhbXMgZnJvbSBtYXRjaC4uLicpOyB9XG4gICAgcmV0dXJuIHRoaXMucGF0aFRvVGVhbXModGhpcy5iZXN0U29sdXRpb25QYXRoKTtcbiAgfVxuXG4gIC8vIHByaXZhdGVcbiAgcmVtb3ZlRHVwbGljYXRlc1N0dWRlbnRzKCkge1xuICAgIGNvbnN0IGR1cEFycmF5ID0ge307XG4gICAgdGhpcy5nZXRTdHVkZW50cygpLmZvckVhY2goKHN0dWRlbnQpID0+IHtcbiAgICAgIGR1cEFycmF5W3N0dWRlbnQuaGFzaF0gPSBzdHVkZW50O1xuICAgIH0pO1xuXG4gICAgdGhpcy5zdHVkZW50cyA9IE9iamVjdC52YWx1ZXMoZHVwQXJyYXkpO1xuICB9XG5cbiAgZ2VuZXJhdGVJbml0aWFsU29sdXRpb25QYXRoKCkge1xuICAgIGNvbnN0IGluaXRpYWxOdW1iZXJPZkJhcnMgPSBNYXRoLmZsb29yKHRoaXMuZ2V0U3R1ZGVudHMoKS5sZW5ndGggLyB0aGlzLm1heGltdW1TdHVkZW50c1BlclRlYW0pO1xuICAgIHRoaXMuYmVzdFNvbHV0aW9uUGF0aCA9IFtdO1xuICAgIHRoaXMuZ2V0U3R1ZGVudHMoKS5mb3JFYWNoKChzdHVkZW50LCBpbmRleCkgPT4ge1xuICAgICAgdGhpcy5iZXN0U29sdXRpb25QYXRoLnB1c2goeyB0eXBlOiAnc3R1ZGVudCcsIHN0dWRlbnQ6IGluZGV4IH0pO1xuICAgIH0pO1xuXG4gICAgLy8gc2h1ZmZsZVxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgNTA7IGkgKz0gMSkge1xuICAgICAgLy8gc3dhcCB0d28gcmFuZG9tIGluZGV4ZXNcbiAgICAgIGNvbnN0IGkxID0gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogdGhpcy5nZXRTdHVkZW50cygpLmxlbmd0aCkgJSB0aGlzLmdldFN0dWRlbnRzKCkubGVuZ3RoO1xuICAgICAgY29uc3QgaTIgPSBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiB0aGlzLmdldFN0dWRlbnRzKCkubGVuZ3RoKSAlIHRoaXMuZ2V0U3R1ZGVudHMoKS5sZW5ndGg7XG4gICAgICBjb25zdCB0ZW1wID0gdGhpcy5iZXN0U29sdXRpb25QYXRoW2kxXTtcbiAgICAgIHRoaXMuYmVzdFNvbHV0aW9uUGF0aFtpMV0gPSB0aGlzLmJlc3RTb2x1dGlvblBhdGhbaTJdO1xuICAgICAgdGhpcy5iZXN0U29sdXRpb25QYXRoW2kyXSA9IHRlbXA7XG4gICAgfVxuXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBpbml0aWFsTnVtYmVyT2ZCYXJzOyBpICs9IDEpIHtcbiAgICAgIGNvbnN0IGluZGV4ID0gKChpICsgMSkgKiB0aGlzLm1heGltdW1TdHVkZW50c1BlclRlYW0pICsgaTtcbiAgICAgIGNvbnN0IHN3YXBWYWwgPSBPYmplY3QuYXNzaWduKHt9LCB0aGlzLmJlc3RTb2x1dGlvblBhdGhbaW5kZXhdKTtcbiAgICAgIHRoaXMuYmVzdFNvbHV0aW9uUGF0aFtpbmRleF0gPSB7IHR5cGU6ICdiYXInIH07XG4gICAgICB0aGlzLmJlc3RTb2x1dGlvblBhdGgucHVzaChzd2FwVmFsKTtcbiAgICB9XG5cbiAgICAvLyBmaW5kIG91dCByZW1haW5kZXJcbiAgICBjb25zdCByZW1haW5kZXJTdHVkZW50cyA9IHRoaXMuZ2V0U3R1ZGVudHMoKS5sZW5ndGggJSBpbml0aWFsTnVtYmVyT2ZCYXJzO1xuICAgIGlmIChyZW1haW5kZXJTdHVkZW50cyA8IHRoaXMubWluaW11bVN0dWRlbnRzUGVyVGVhbSkge1xuICAgICAgbGV0IGxhc3RJbmRleCA9IHRoaXMuYmVzdFNvbHV0aW9uUGF0aC5sZW5ndGg7XG4gICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHJlbWFpbmRlclN0dWRlbnRzOyBpICs9IDEpIHtcbiAgICAgICAgLy8gbW92ZSBsYXN0IGJhciBiYWNrIGJ5IHJlbWFpbmVkIHN0dWRlbnRzXG4gICAgICAgIC8vIGZpbmQgbGFzdCBiYXIgaW5kZXhcbiAgICAgICAgZm9yIChsZXQgaiA9IGxhc3RJbmRleCAtIDE7IGogPiAwOyBqIC09IDEpIHtcbiAgICAgICAgICBpZiAodGhpcy5iZXN0U29sdXRpb25QYXRoW2pdLnR5cGUgPT09ICdiYXInKSB7XG4gICAgICAgICAgICBsYXN0SW5kZXggPSBqO1xuICAgICAgICAgICAgYnJlYWs7XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgZm9yIChsZXQgayA9IDA7IGsgPCB0aGlzLm1pbmltdW1TdHVkZW50c1BlclRlYW0gLSByZW1haW5kZXJTdHVkZW50cyAtIGk7IGsgKz0gMSkge1xuICAgICAgICAgIHRoaXMuYmVzdFNvbHV0aW9uUGF0aCA9IERQTWF0Y2guc3dhcCh0aGlzLmJlc3RTb2x1dGlvblBhdGgsIGxhc3RJbmRleCwgbGFzdEluZGV4IC0gayAtIDEpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICAgIC8vIHRoaXMuYmVzdFNvbHV0aW9uUGF0aC5wdXNoKHsgdHlwZTogJ2JhcicgfSk7XG4gIH1cblxuICBwYXRoVG9UZWFtcyhzb2x1dGlvblBhdGgpIHtcbiAgICBsZXQgZmlyc3RCYXJJbmRleCA9IDA7XG4gICAgbGV0IG5leHRCYXJJbmRleCA9IDA7XG4gICAgY29uc3QgdGVhbXMgPSBbXTtcbiAgICBjb25zdCBmaW5kRnVuYyA9ICh2LCBpKSA9PiBpID4gKG5leHRCYXJJbmRleCkgJiYgdi50eXBlID09PSAnYmFyJztcbiAgICB3aGlsZSAobmV4dEJhckluZGV4ICE9PSBzb2x1dGlvblBhdGgubGVuZ3RoKSB7XG4gICAgICBmaXJzdEJhckluZGV4ID0gbmV4dEJhckluZGV4O1xuICAgICAgbmV4dEJhckluZGV4ID0gc29sdXRpb25QYXRoLmZpbmRJbmRleChmaW5kRnVuYyk7XG4gICAgICBpZiAobmV4dEJhckluZGV4ID09PSAtMSB8fCBuZXh0QmFySW5kZXggPiBzb2x1dGlvblBhdGgubGVuZ3RoKSB7IG5leHRCYXJJbmRleCA9IHNvbHV0aW9uUGF0aC5sZW5ndGg7IH1cblxuICAgICAgY29uc3QgdGVhbSA9IFtdO1xuICAgICAgZm9yIChsZXQgaSA9IGZpcnN0QmFySW5kZXggKyAxOyBpIDwgbmV4dEJhckluZGV4OyBpICs9IDEpIHtcbiAgICAgICAgdGVhbS5wdXNoKHRoaXMuc3R1ZGVudHNbc29sdXRpb25QYXRoW2ldLnN0dWRlbnRdKTtcbiAgICAgIH1cblxuICAgICAgdGVhbXMucHVzaChuZXcgVGVhbSh0ZWFtKSk7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRlYW1zO1xuICB9XG5cbiAgaXNTb2x1dGlvbihzb2x1dGlvblBhdGgpIHtcbiAgICAvLyBjaGVjayBmb3IgbWluaW11bSBhbmQgbWF4aW11bVxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgc29sdXRpb25QYXRoLmxlbmd0aDsgaSArPSAxKSB7XG4gICAgICBpZiAoc29sdXRpb25QYXRoW2ldLnR5cGUgPT09ICdiYXInKSB7XG4gICAgICAgIC8vIGNvdW50IHRpbCBuZXh0IGJhclxuICAgICAgICBsZXQgY291bnQgPSAxO1xuICAgICAgICB3aGlsZSAoaSArIGNvdW50IDwgc29sdXRpb25QYXRoLmxlbmd0aFxuICAgICAgICAgICAgICAmJiBzb2x1dGlvblBhdGhbaSArIGNvdW50XS50eXBlICE9PSAnYmFyJykge1xuICAgICAgICAgIGNvdW50ICs9IDE7XG4gICAgICAgIH1cbiAgICAgICAgY291bnQgLT0gMTtcblxuICAgICAgICBpZiAoY291bnQgPiB0aGlzLm1heGltdW1TdHVkZW50c1BlclRlYW0gfHwgY291bnQgPCB0aGlzLm1pbmltdW1TdHVkZW50c1BlclRlYW0pIHtcbiAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gdHJ1ZTtcbiAgfVxuXG4gIGlzVmFsaWQoc29sdXRpb25QYXRoLCBwZXJtTGVuZ3RoKSB7XG4gICAgLy8gY2hlY2sgZm9yIG1pbmltdW0gYW5kIG1heGltdW1cblxuICAgIGlmIChwZXJtTGVuZ3RoID09PSAwKSB7XG4gICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG5cbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IE1hdGgubWluKHRoaXMubWluaW11bVN0dWRlbnRzUGVyVGVhbSwgcGVybUxlbmd0aCk7IGkgKz0gMSkge1xuICAgICAgaWYgKHNvbHV0aW9uUGF0aFtpXS50eXBlID09PSAnYmFyJykge1xuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKHBlcm1MZW5ndGggPD0gdGhpcy5tYXhpbXVtU3R1ZGVudHNQZXJUZWFtKSB7XG4gICAgICByZXR1cm4gdHJ1ZTtcbiAgICB9XG5cbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHBlcm1MZW5ndGg7IGkgKz0gMSkge1xuICAgICAgaWYgKHNvbHV0aW9uUGF0aFtpXS50eXBlID09PSAnYmFyJyB8fCBpID09PSAwKSB7XG4gICAgICAgIC8vIGNvdW50IHRpbCBuZXh0IGJhclxuICAgICAgICBsZXQgY291bnQgPSAxO1xuICAgICAgICB3aGlsZSAoaSArIGNvdW50IDwgcGVybUxlbmd0aFxuICAgICAgICAgICAgICAmJiBzb2x1dGlvblBhdGhbaSArIGNvdW50XS50eXBlICE9PSAnYmFyJykge1xuICAgICAgICAgIGNvdW50ICs9IDE7XG4gICAgICAgIH1cbiAgICAgICAgY291bnQgLT0gMTtcblxuICAgICAgICBpZiAoKGNvdW50ID4gdGhpcy5tYXhpbXVtU3R1ZGVudHNQZXJUZWFtXG4gICAgICAgICAgICB8fCBjb3VudCA8IHRoaXMubWluaW11bVN0dWRlbnRzUGVyVGVhbSlcbiAgICAgICAgICAgICYmIGkgPD0gcGVybUxlbmd0aCAtIHRoaXMubWF4aW11bVN0dWRlbnRzUGVyVGVhbSkge1xuICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiB0cnVlO1xuICB9XG5cbiAgY2FsY3VsYXRlU2NvcmUoc29sdXRpb25QYXRoLCBwZXJtTGVuZ3RoKSB7XG4gICAgbGV0IGZpcnN0QmFySW5kZXggPSAtMTtcbiAgICBsZXQgbmV4dEJhckluZGV4ID0gMDtcblxuICAgIGxldCBzY29yZSA9IDA7XG4gICAgY29uc3QgZmluZEZ1bmMgPSAodiwgaSkgPT4gaSA+IChuZXh0QmFySW5kZXgpICYmIHYudHlwZSA9PT0gJ2Jhcic7XG4gICAgd2hpbGUgKG5leHRCYXJJbmRleCAhPT0gcGVybUxlbmd0aCkge1xuICAgICAgZmlyc3RCYXJJbmRleCA9IG5leHRCYXJJbmRleDtcbiAgICAgIG5leHRCYXJJbmRleCA9IHNvbHV0aW9uUGF0aC5maW5kSW5kZXgoZmluZEZ1bmMpO1xuICAgICAgaWYgKG5leHRCYXJJbmRleCA9PT0gLTEgfHwgbmV4dEJhckluZGV4ID4gcGVybUxlbmd0aCkgeyBuZXh0QmFySW5kZXggPSBwZXJtTGVuZ3RoOyB9XG5cbiAgICAgIGNvbnN0IHRlYW0gPSBbXTtcbiAgICAgIGZvciAobGV0IGkgPSBmaXJzdEJhckluZGV4ICsgMTsgaSA8IG5leHRCYXJJbmRleDsgaSArPSAxKSB7XG4gICAgICAgIHRlYW0ucHVzaChzb2x1dGlvblBhdGhbaV0uc3R1ZGVudCk7XG4gICAgICB9XG5cbiAgICAgIHNjb3JlICs9IHRoaXMuc2NvcmVUZWFtKHRlYW0pO1xuICAgIH1cblxuICAgIHJldHVybiBzY29yZTtcbiAgfVxuXG4gIHNjb3JlVGVhbSh0ZWFtKSB7XG4gICAgLy8gcGFpcndpc2UgdGVhbSBhbGwgbWVtYmVycyBhZ2FpbnN0IGVhY2ggb3RoZXJcbiAgICBpZiAodGVhbS5sZW5ndGggPD0gMSkge1xuICAgICAgcmV0dXJuIDA7XG4gICAgfVxuXG4gICAgbGV0IHNjb3JlID0gMDtcbiAgICBsZXQgbnVtUGVybXMgPSAwO1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGVhbS5sZW5ndGg7IGkgKz0gMSkge1xuICAgICAgZm9yIChsZXQgaiA9IGkgKyAxOyBqIDwgdGVhbS5sZW5ndGg7IGogKz0gMSkge1xuICAgICAgICBzY29yZSArPSB0aGlzLnNjb3JlU3R1ZGVudHMoaSwgaik7XG4gICAgICAgIG51bVBlcm1zICs9IDE7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIHNjb3JlIC8gbnVtUGVybXM7XG4gIH1cblxuICBpc1Byb21pc2luZyhzb2x1dGlvblBhdGgsIHBlcm1MZW5ndGgpIHtcbiAgICAvLyBjYWxjdWxhdGUgdXBwZXIgYm91bmRcblxuICAgIGlmICghdGhpcy5pc1ZhbGlkKHNvbHV0aW9uUGF0aCwgcGVybUxlbmd0aCkpIHtcbiAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICAvLyBlc3RpbWF0ZSBzb2x1dGlvblBhdGggc2NvcmU7XG4gICAgY29uc3QgY3VycmVudFNjb3JlID0gdGhpcy5jYWxjdWxhdGVTY29yZShzb2x1dGlvblBhdGgsIHBlcm1MZW5ndGgpO1xuXG4gICAgLy8gY2FsY3VsYXRlIG1heGltdW0gZXN0aW1hdGUgZm9yIHJlc3Qgb2YgdGhpbmdcbiAgICAvLyBmaW5kIGJlc3QgcGFpcndpc2UgY29tcGF0aWJpbGl0eSwgdGhlbiBtdWx0aXBseSBieSBudW1iZXIgb2YgdGVhbXMgcmVtYWluaW5nP1xuICAgIGNvbnN0IGVzdGltYXRlID0gdGhpcy5jYWxjdWxhdGVFc3RpbWF0ZShzb2x1dGlvblBhdGgsIHBlcm1MZW5ndGgpO1xuXG4gICAgY29uc3QgY3VycmVudEVzdGltYXRlID0gY3VycmVudFNjb3JlICsgZXN0aW1hdGU7XG5cbiAgICBpZiAoY3VycmVudEVzdGltYXRlIDwgdGhpcy5sb3dlckJvdW5kIHx8IGN1cnJlbnRFc3RpbWF0ZSA+IHRoaXMudXBwZXJCb3VuZCkge1xuICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIHJldHVybiB0cnVlO1xuICB9XG5cbiAgc3RhdGljIHN3YXAoc29sdXRpb25QYXRoLCBhLCBiKSB7XG4gICAgY29uc3QgbmV3U29sUGF0aCA9IHNvbHV0aW9uUGF0aDtcbiAgICBjb25zdCB0ZW1wID0gT2JqZWN0LmFzc2lnbih7fSwgbmV3U29sUGF0aFthXSk7XG4gICAgbmV3U29sUGF0aFthXSA9IHNvbHV0aW9uUGF0aFtiXTtcbiAgICBuZXdTb2xQYXRoW2JdID0gdGVtcDtcblxuICAgIHJldHVybiBuZXdTb2xQYXRoO1xuICB9XG5cbiAgY2FsY3VsYXRlVXBwZXJCb3VuZChzb2x1dGlvblBhdGgsIHBlcm1MZW5ndGggPSAwKSB7XG4gICAgcmV0dXJuIHRoaXMuY2FsY3VsYXRlRXN0aW1hdGUoc29sdXRpb25QYXRoLCBwZXJtTGVuZ3RoKTtcbiAgfVxuXG4gIGNhbGN1bGF0ZUVzdGltYXRlKHNvbHV0aW9uUGF0aCwgcGVybUxlbmd0aCA9IDApIHtcbiAgICAvLyBmaW5kIGJlc3QgcGFpcndpc2UgY29tcGF0aWJpbGl0eSBpbiByZW1haW5pbmcgc2V0XG4gICAgbGV0IGJlc3RTY29yZSA9IDA7XG4gICAgY29uc3QgbWF4UmVtYWluaW5nVGVhbXMgPSBNYXRoLmNlaWwoKHNvbHV0aW9uUGF0aC5sZW5ndGggLSBwZXJtTGVuZ3RoKSAvIHRoaXMubWluaW11bVN0dWRlbnRzUGVyVGVhbSk7XG5cbiAgICBmb3IgKGxldCBpID0gcGVybUxlbmd0aDsgaSA8IHNvbHV0aW9uUGF0aC5sZW5ndGg7IGkgKz0gMSkge1xuICAgICAgaWYgKHNvbHV0aW9uUGF0aFtpXS50eXBlID09PSAnYmFyJykge1xuICAgICAgICBjb250aW51ZTtcbiAgICAgIH1cblxuICAgICAgY29uc3Qgc3R1ZGVudE9uZSA9IHNvbHV0aW9uUGF0aFtpXS5zdHVkZW50O1xuICAgICAgZm9yIChsZXQgaiA9IGkgKyAxOyBqIDwgc29sdXRpb25QYXRoLmxlbmd0aDsgaiArPSAxKSB7XG4gICAgICAgIGlmIChzb2x1dGlvblBhdGhbal0udHlwZSA9PT0gJ2JhcicpIHtcbiAgICAgICAgICBjb250aW51ZTtcbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnN0IHNjb3JlID0gdGhpcy5zY29yZVN0dWRlbnRzKHNvbHV0aW9uUGF0aFtqXS5zdHVkZW50LCBzdHVkZW50T25lKTtcbiAgICAgICAgaWYgKHNjb3JlID4gYmVzdFNjb3JlKSB7XG4gICAgICAgICAgYmVzdFNjb3JlID0gc2NvcmU7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gbWF4UmVtYWluaW5nVGVhbXMgKiBiZXN0U2NvcmU7XG4gIH1cblxuICBzY29yZVN0dWRlbnRzKGEsIGIpIHtcbiAgICByZXR1cm4gdGhpcy5zY29yZWRBZ2FpbnN0W2FdW2JdO1xuICB9XG5cbiAgZ2VuUGVybXMocGVybUxlbmd0aCkge1xuICAgIGlmIChwZXJtTGVuZ3RoID09PSB0aGlzLnNvbHV0aW9uUGF0aC5sZW5ndGgpIHtcbiAgICAgIGlmICh0aGlzLmlzU29sdXRpb24odGhpcy5zb2x1dGlvblBhdGgpKSB7XG4gICAgICAgIGNvbnN0IHNjb3JlID0gdGhpcy5jYWxjdWxhdGVTY29yZSh0aGlzLnNvbHV0aW9uUGF0aCk7XG4gICAgICAgIGlmIChzY29yZSA+IHRoaXMubG93ZXJCb3VuZCkge1xuICAgICAgICAgIHRoaXMubG93ZXJCb3VuZCA9IHNjb3JlO1xuICAgICAgICAgIHRoaXMuYmVzdFNvbHV0aW9uUGF0aCA9IHRoaXMuc29sdXRpb25QYXRoO1xuICAgICAgICAgIGlmICh0aGlzLmRlYnVnKSB7IGNvbnNvbGUubG9nKGBEUE1hdGNoOiBmb3VuZCBhIHNvbHV0aW9uICgke3Njb3JlfSlgKTsgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKCF0aGlzLmlzUHJvbWlzaW5nKHRoaXMuc29sdXRpb25QYXRoLCBwZXJtTGVuZ3RoKSkge1xuICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIGZvciAobGV0IGkgPSBwZXJtTGVuZ3RoOyBpIDwgdGhpcy5zb2x1dGlvblBhdGgubGVuZ3RoOyBpICs9IDEpIHtcbiAgICAgIHRoaXMuc29sdXRpb25QYXRoID0gRFBNYXRjaC5zd2FwKHRoaXMuc29sdXRpb25QYXRoLCBwZXJtTGVuZ3RoLCBpKTtcbiAgICAgIHRoaXMuZ2VuUGVybXMocGVybUxlbmd0aCArIDEpO1xuICAgICAgdGhpcy5zb2x1dGlvblBhdGggPSBEUE1hdGNoLnN3YXAodGhpcy5zb2x1dGlvblBhdGgsIHBlcm1MZW5ndGgsIGkpO1xuICAgIH1cbiAgfVxufVxuIl0sInNvdXJjZVJvb3QiOiIvbW50L2MvVXNlcnMvcHVyZXUvRG9jdW1lbnRzL3Byb2plY3RzL2xhdW5jaHgtdGVhbWluZy9zcmMifQ==
