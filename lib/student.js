'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.StudentPairSet = exports.StudentSet = exports.SortedStudentSet = exports.defaultTeamingConfig = undefined;

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _get = function get(object, property, receiver) { if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { return get(parent, property, receiver); } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } };

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _btoaLite = require('btoa-lite');

var _btoaLite2 = _interopRequireDefault(_btoaLite);

var _binarySearch = require('binary-search');

var _binarySearch2 = _interopRequireDefault(_binarySearch);

var _teamingConfig = require('./teaming-config');

var _teamingConfig2 = _interopRequireDefault(_teamingConfig);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var defaultTeamingConfig = exports.defaultTeamingConfig = Object.keys(_teamingConfig2.default).reduce(function (old, next) {
  old[next] = _teamingConfig2.default[next].default;
  return old;
}, {});

var mapSentiment = function mapSentiment(sentiment) {
  switch (sentiment) {
    case 'strong_yes':
      return 4.0;
    case 'yes':
      return 3.0;
    case 'no':
      return 2.0;
    case 'strong_no':
      return 1.0;

    default:
      return null;
  }
};

var FalseSet = function () {
  function FalseSet() {
    _classCallCheck(this, FalseSet);

    for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
      args[_key] = arguments[_key];
    }

    this.p_set = new Set(args);
  }

  _createClass(FalseSet, [{
    key: 'forEach',
    value: function forEach(callbackFn) {
      return this.p_set.forEach(callbackFn);
    }
  }, {
    key: 'add',
    value: function add(value) {
      return this.p_set.add(value);
    }
  }, {
    key: 'has',
    value: function has(value) {
      return this.p_set.has(value);
    }
  }, {
    key: 'clear',
    value: function clear() {
      return this.p_set.clear();
    }
  }, {
    key: 'delete',
    value: function _delete(value) {
      return this.p_set.delete(value);
    }
  }, {
    key: Symbol.iterator,
    value: function value() {
      return this.p_set[Symbol.iterator]();
    }
  }]);

  return FalseSet;
}();

var SortedStudentSet = exports.SortedStudentSet = function (_FalseSet) {
  _inherits(SortedStudentSet, _FalseSet);

  function SortedStudentSet() {
    _classCallCheck(this, SortedStudentSet);

    for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
      args[_key2] = arguments[_key2];
    }

    var _this = _possibleConstructorReturn(this, (SortedStudentSet.__proto__ || Object.getPrototypeOf(SortedStudentSet)).call(this, args));

    _this.p_hashSet = new Set();
    return _this;
  }

  _createClass(SortedStudentSet, [{
    key: 'add',
    value: function add(value) {
      var first = value[0].p_hash > value[1].p_hash ? value[0] : value[1];
      var second = value[0].p_hash > value[1].p_hash ? value[1] : value[0];
      _get(SortedStudentSet.prototype.__proto__ || Object.getPrototypeOf(SortedStudentSet.prototype), 'add', this).call(this, [first, second]);
      return this.p_hashSet.add(first.p_hash + second.p_hash);
    }
  }, {
    key: 'has',
    value: function has(value) {
      var first = value[0].p_hash > value[1].p_hash ? value[0] : value[1];
      var second = value[0].p_hash > value[1].p_hash ? value[1] : value[0];
      return this.p_hashSet.has(first.p_hash + second.p_hash);
    }
  }]);

  return SortedStudentSet;
}(FalseSet);

var StudentSet = exports.StudentSet = function (_FalseSet2) {
  _inherits(StudentSet, _FalseSet2);

  function StudentSet() {
    _classCallCheck(this, StudentSet);

    for (var _len3 = arguments.length, args = Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
      args[_key3] = arguments[_key3];
    }

    var _this2 = _possibleConstructorReturn(this, (StudentSet.__proto__ || Object.getPrototypeOf(StudentSet)).call(this, args));

    _this2.p_actualSet = new Set(args);
    return _this2;
  }

  _createClass(StudentSet, [{
    key: 'has',
    value: function has(value) {
      return _get(StudentSet.prototype.__proto__ || Object.getPrototypeOf(StudentSet.prototype), 'has', this).call(this, value.p_hash);
    }
  }, {
    key: 'add',
    value: function add(value) {
      this.p_actualSet.add(value);
      return _get(StudentSet.prototype.__proto__ || Object.getPrototypeOf(StudentSet.prototype), 'add', this).call(this, value.p_hash);
    }
  }, {
    key: 'forEach',
    value: function forEach(callbackFn) {
      return this.p_actualSet.forEach(callbackFn);
    }
  }]);

  return StudentSet;
}(FalseSet);

var StudentPairSet = exports.StudentPairSet = function (_FalseSet3) {
  _inherits(StudentPairSet, _FalseSet3);

  function StudentPairSet() {
    _classCallCheck(this, StudentPairSet);

    for (var _len4 = arguments.length, args = Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
      args[_key4] = arguments[_key4];
    }

    var _this3 = _possibleConstructorReturn(this, (StudentPairSet.__proto__ || Object.getPrototypeOf(StudentPairSet)).call(this, args));

    _this3.p_actualSet = new Set(args);
    return _this3;
  }

  _createClass(StudentPairSet, [{
    key: 'hasStudentPair',
    value: function hasStudentPair(_ref) {
      var _ref2 = _slicedToArray(_ref, 2),
          student = _ref2[0],
          otherStudent = _ref2[1];

      var possiblity1 = student.p_hash + otherStudent.p_hash;
      var possiblity2 = otherStudent.p_hash + student.p_hash;
      return this.has(possiblity1) || this.has(possiblity2);
    }
  }, {
    key: 'addStudentPair',
    value: function addStudentPair(_ref3) {
      var _ref4 = _slicedToArray(_ref3, 2),
          student = _ref4[0],
          otherStudent = _ref4[1];

      this.p_actualSet.add([student, otherStudent]);
      return this.add(student.p_hash + otherStudent.p_hash);
    }
  }]);

  return StudentPairSet;
}(FalseSet);

var Student = function () {
  function Student(json) {
    var teamingConfig = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : defaultTeamingConfig;

    _classCallCheck(this, Student);

    this.p_json = json;
    this.p_hash = (0, _btoaLite2.default)(unescape(encodeURIComponent(JSON.stringify(json))));
    this.p_learningStyle = json.learningStyle || 'default';
    this.p_workStyle = json.workStyle || 'default';
    this.p_EP10Type = json.EP10Type || 'default';
    this.p_personalityType = json.personalityType || 'default';
    this.p_entrepreneurType = json.entrepreneurType || 'default';
    this.p_launchSkills = json.launchSkills || 'default';
    this.p_interests = json.interests || [];
    this.p_ideas = json.ideas || [];
    this.p_teamingConfig = teamingConfig;
    this.p_role = json.role || json.metadata.role;
    this.p_skills = json.skills;
    this.p_gender = json.gender || json.metadata.gender;
    this.p_ethnicity = json.ethnicity || json.metadata.ethnicity;
  }

  _createClass(Student, [{
    key: 'isEqual',
    value: function isEqual(otherStudent) {
      return this.p_hash === otherStudent.p_hash;
    }
  }, {
    key: 'learningStyleOverlap',
    value: function learningStyleOverlap(learningStyle) {
      if (this.learningStyle === learningStyle) {
        return 1;
      }

      return 0;
    }
  }, {
    key: 'workStyleCompliment',
    value: function workStyleCompliment(workStyle) {
      var comparator = function comparator(a, b) {
        return a.localeCompare(b);
      };
      var workStylePoints = 0;

      var style = this.workStyle.split('/').map(function (s) {
        return s.trim();
      }); // ["Amicable", "Analytical"]
      style.sort(comparator);
      var otherStyle = workStyle.split('/').map(function (s) {
        return s.trim();
      });
      otherStyle.sort(comparator);

      var smaller = style.length < otherStyle.length ? style : otherStyle; // get smaller
      var larger = style.length < otherStyle.length ? otherStyle : style;

      // set difference
      for (var i = 0; i < smaller.length; i += 1) {
        if ((0, _binarySearch2.default)(larger, smaller[i], comparator) < 0) {
          workStylePoints += 1;
        }
      }

      return workStylePoints / smaller.length; // perfectly disjoint is 1
    }
  }, {
    key: 'ep10Compliment',
    value: function ep10Compliment(otherStudent) {
      // ep 10 complementary
      if (this.EP10Type !== otherStudent.EP10Type) {
        return this.p_teamingConfig.EP10Type || defaultTeamingConfig.EP10Type;
      }

      return 0;
    }
  }, {
    key: 'personalityComplement',
    value: function personalityComplement(otherPersonalityType) {
      var personalityScore = 0;
      for (var i = 0; i < 4; i += 1) {
        if (this.personalityType[i] !== otherPersonalityType[i]) {
          personalityScore += 1;
        }
      }

      return personalityScore / 4;
    }
  }, {
    key: 'skillsComplement',
    value: function skillsComplement(otherSkills) {
      var normalSkillsPoints = 0.0;
      var normalSkills = this.skills;
      var otherNormalSkills = otherSkills;
      var skillsKeys = Object.keys(normalSkills || {});

      for (var i = 0, r = skillsKeys.length; i < r; i += 1) {
        var skillKey = skillsKeys[i];
        var skill = normalSkills[skillKey];
        var otherSkill = otherNormalSkills[skillKey];
        var skillDifference = Math.abs(skill - otherSkill);

        if (skillDifference > 3) {
          normalSkillsPoints += 1;
        }
      }

      return normalSkillsPoints / skillsKeys.length; // perfectly disjoint is 1
    }
  }, {
    key: 'launchSkillsComplement',
    value: function launchSkillsComplement() {
      var otherLaunchSkills = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : [];

      var skillsPoints = 0.0;
      var _launchSkills = this.launchSkills,
          launchSkills = _launchSkills === undefined ? [] : _launchSkills;


      var comparator = function comparator(a, b) {
        return a.localeCompare(b);
      };
      launchSkills.sort(comparator);
      otherLaunchSkills.sort(comparator);

      var smaller = launchSkills.length < otherLaunchSkills.length ? launchSkills : otherLaunchSkills;
      var larger = launchSkills.length < otherLaunchSkills.length ? otherLaunchSkills : launchSkills;

      for (var i = 0, r = smaller.length; i < r; i += 1) {
        if ((0, _binarySearch2.default)(larger, smaller[i], comparator) < 0) {
          skillsPoints += 1;
        }
      }

      return skillsPoints / larger.length;
    }
  }, {
    key: 'interestOverlap',
    value: function interestOverlap(otherInterests) {
      var interestsPoints = 0.0;
      var comparator = function comparator(a, b) {
        return a.localeCompare(b);
      };

      var interests = this.interests;


      if (interests.length === 0 || otherInterests.length === 0) {
        return 0;
      }

      interests.sort(comparator);

      otherInterests.sort(comparator);

      var smaller = interests.length < otherInterests.length ? interests : otherInterests;
      var larger = interests.length < otherInterests.length ? otherInterests : interests;

      for (var i = 0, r = smaller.length; i < r; i += 1) {
        if ((0, _binarySearch2.default)(larger, smaller[i], comparator) > -1) {
          interestsPoints += 1;
        }
      }

      return interestsPoints / larger.length;
    }
  }, {
    key: 'ideaOverlap',
    value: function ideaOverlap(otherIdeas) {
      var problemInterestOverlap = 0.0;

      var comparator = function comparator(a, b) {
        return a.id - b.id;
      };
      var ideas = this.ideas;

      ideas.sort(comparator);

      otherIdeas.sort(comparator);

      // should be same size, but if not do smaller
      var smaller = ideas.length < otherIdeas.length ? ideas : otherIdeas;
      var larger = ideas.length < otherIdeas.length ? otherIdeas : ideas;

      for (var i = 0; i < smaller.length; i += 1) {
        var myIdea = smaller[i];
        var otherIdeaPos = (0, _binarySearch2.default)(larger, myIdea, comparator);

        if (otherIdeaPos > -1) {
          var otherIdea = larger[otherIdeaPos];
          var sentiment = myIdea.strength;
          var otherSentiment = otherIdea.strength;

          problemInterestOverlap += 3.0 - Math.abs(sentiment - otherSentiment);
        }
      }

      if (this.ideas.length > 0) {
        return problemInterestOverlap / (3 * larger.length); // max of larger length
      }

      return 0;
    }

    // generate a percentage out of 1 that we scale

  }, {
    key: 'roleComplement',
    value: function roleComplement(role) {
      // return 1 if complement and 0 if not ?
      if (this.role !== role) {
        return 1;
      }

      return 0;
    }
  }, {
    key: 'genderComplement',
    value: function genderComplement(gender) {
      // return 1 if complement and 0 if not ?
      if (this.gender !== gender) {
        return 1;
      }

      return 0;
    }
  }, {
    key: 'ethnicityComplement',
    value: function ethnicityComplement(ethnicity) {
      // return 1 if complement and 0 if not ?
      if (this.ethnicity !== ethnicity) {
        return 1;
      }

      return 0;
    }
  }, {
    key: 'entrepreneurTypeComplement',
    value: function entrepreneurTypeComplement(otherETIn) {
      var comparator = function comparator(a, b) {
        return a.localeCompare(b);
      };
      var etPoints = 0;

      var ET = this.entrepreneurType.split(', ');
      ET.sort(comparator);

      var otherET = otherETIn.split(', ');
      otherET.sort(comparator);

      var smaller = ET.length < otherET.length ? ET : otherET;
      var larger = ET.length < otherET.length ? otherET : ET;

      for (var i = 0; i < smaller.length; i += 1) {
        if ((0, _binarySearch2.default)(larger, smaller[i], comparator) < 0) {
          // if et type not found in other, add a point
          etPoints += 1;
        }
      }

      // max et points is the smaller size

      return etPoints / smaller.length; // how perfectly disjoint
    }
  }, {
    key: 'scoreAgainst',
    value: function scoreAgainst(otherStudent) {
      var points = 0.0;

      var teamingConfig = Object.assign({}, defaultTeamingConfig, this.p_teamingConfig);

      points += teamingConfig.rolePoints * this.roleComplement(otherStudent.role);
      // points += teamingConfig.entrepreneurTypePoints * this.entrepreneurTypeComplement(otherStudent.entrepreneurType);
      points += teamingConfig.entrepreneurTypePoints * this.launchSkillsComplement(otherStudent.launchSkills);
      points += teamingConfig.skillsPoints * this.skillsComplement(otherStudent.skills);
      points += teamingConfig.learningStylePoints * this.learningStyleOverlap(otherStudent.learningStyle);
      points += teamingConfig.workStylePoints * this.workStyleCompliment(otherStudent.workStyle);
      points += teamingConfig.personalityPoints * this.personalityComplement(otherStudent.personalityType);
      points += teamingConfig.genderPoints * this.genderComplement(otherStudent.gender);
      points += teamingConfig.ethnicityPoints * this.genderComplement(otherStudent.ethnicity);

      // interest overlap
      points += teamingConfig.interestPoints * this.interestOverlap(otherStudent.interests);

      // Idea overlap
      points += teamingConfig.ideaPoints * this.ideaOverlap(otherStudent.ideas);

      return points;
    }
  }, {
    key: 'hash',
    get: function get() {
      return this.p_hash;
    }
  }, {
    key: 'role',
    get: function get() {
      return this.p_role;
    }
  }, {
    key: 'gender',
    get: function get() {
      return this.p_gender;
    }
  }, {
    key: 'ethnicity',
    get: function get() {
      return this.p_ethnicity;
    }
  }, {
    key: 'ideas',
    get: function get() {
      return this.p_ideas;
    }
  }, {
    key: 'ownIdeas',
    get: function get() {
      return (this.p_json.metadata || {}).ideas || [];
    }
  }, {
    key: 'interests',
    get: function get() {
      return this.p_interests;
    }
  }, {
    key: 'learningStyle',
    get: function get() {
      return this.p_learningStyle;
    }
  }, {
    key: 'personalityType',
    get: function get() {
      return this.p_personalityType;
    }
  }, {
    key: 'EP10Type',
    get: function get() {
      return this.p_EP10Type;
    }
  }, {
    key: 'workStyle',
    get: function get() {
      return this.p_workStyle;
    }
  }, {
    key: 'entrepreneurType',
    get: function get() {
      return this.p_entrepreneurType;
    }
  }, {
    key: 'launchSkills',
    get: function get() {
      return this.p_launchSkills;
    }
  }, {
    key: 'skills',
    get: function get() {
      return this.p_skills;
    }
  }]);

  return Student;
}();

exports.default = Student;
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInN0dWRlbnQuanMiXSwibmFtZXMiOlsiZGVmYXVsdFRlYW1pbmdDb25maWciLCJPYmplY3QiLCJrZXlzIiwiVGVhbWluZ0NvbmZpZyIsInJlZHVjZSIsIm9sZCIsIm5leHQiLCJkZWZhdWx0IiwibWFwU2VudGltZW50Iiwic2VudGltZW50IiwiRmFsc2VTZXQiLCJhcmdzIiwicF9zZXQiLCJTZXQiLCJjYWxsYmFja0ZuIiwiZm9yRWFjaCIsInZhbHVlIiwiYWRkIiwiaGFzIiwiY2xlYXIiLCJkZWxldGUiLCJTeW1ib2wiLCJpdGVyYXRvciIsIlNvcnRlZFN0dWRlbnRTZXQiLCJwX2hhc2hTZXQiLCJmaXJzdCIsInBfaGFzaCIsInNlY29uZCIsIlN0dWRlbnRTZXQiLCJwX2FjdHVhbFNldCIsIlN0dWRlbnRQYWlyU2V0Iiwic3R1ZGVudCIsIm90aGVyU3R1ZGVudCIsInBvc3NpYmxpdHkxIiwicG9zc2libGl0eTIiLCJTdHVkZW50IiwianNvbiIsInRlYW1pbmdDb25maWciLCJwX2pzb24iLCJ1bmVzY2FwZSIsImVuY29kZVVSSUNvbXBvbmVudCIsIkpTT04iLCJzdHJpbmdpZnkiLCJwX2xlYXJuaW5nU3R5bGUiLCJsZWFybmluZ1N0eWxlIiwicF93b3JrU3R5bGUiLCJ3b3JrU3R5bGUiLCJwX0VQMTBUeXBlIiwiRVAxMFR5cGUiLCJwX3BlcnNvbmFsaXR5VHlwZSIsInBlcnNvbmFsaXR5VHlwZSIsInBfZW50cmVwcmVuZXVyVHlwZSIsImVudHJlcHJlbmV1clR5cGUiLCJwX2xhdW5jaFNraWxscyIsImxhdW5jaFNraWxscyIsInBfaW50ZXJlc3RzIiwiaW50ZXJlc3RzIiwicF9pZGVhcyIsImlkZWFzIiwicF90ZWFtaW5nQ29uZmlnIiwicF9yb2xlIiwicm9sZSIsIm1ldGFkYXRhIiwicF9za2lsbHMiLCJza2lsbHMiLCJwX2dlbmRlciIsImdlbmRlciIsInBfZXRobmljaXR5IiwiZXRobmljaXR5IiwiY29tcGFyYXRvciIsImEiLCJiIiwibG9jYWxlQ29tcGFyZSIsIndvcmtTdHlsZVBvaW50cyIsInN0eWxlIiwic3BsaXQiLCJtYXAiLCJzIiwidHJpbSIsInNvcnQiLCJvdGhlclN0eWxlIiwic21hbGxlciIsImxlbmd0aCIsImxhcmdlciIsImkiLCJvdGhlclBlcnNvbmFsaXR5VHlwZSIsInBlcnNvbmFsaXR5U2NvcmUiLCJvdGhlclNraWxscyIsIm5vcm1hbFNraWxsc1BvaW50cyIsIm5vcm1hbFNraWxscyIsIm90aGVyTm9ybWFsU2tpbGxzIiwic2tpbGxzS2V5cyIsInIiLCJza2lsbEtleSIsInNraWxsIiwib3RoZXJTa2lsbCIsInNraWxsRGlmZmVyZW5jZSIsIk1hdGgiLCJhYnMiLCJvdGhlckxhdW5jaFNraWxscyIsInNraWxsc1BvaW50cyIsIm90aGVySW50ZXJlc3RzIiwiaW50ZXJlc3RzUG9pbnRzIiwib3RoZXJJZGVhcyIsInByb2JsZW1JbnRlcmVzdE92ZXJsYXAiLCJpZCIsIm15SWRlYSIsIm90aGVySWRlYVBvcyIsIm90aGVySWRlYSIsInN0cmVuZ3RoIiwib3RoZXJTZW50aW1lbnQiLCJvdGhlckVUSW4iLCJldFBvaW50cyIsIkVUIiwib3RoZXJFVCIsInBvaW50cyIsImFzc2lnbiIsInJvbGVQb2ludHMiLCJyb2xlQ29tcGxlbWVudCIsImVudHJlcHJlbmV1clR5cGVQb2ludHMiLCJsYXVuY2hTa2lsbHNDb21wbGVtZW50Iiwic2tpbGxzQ29tcGxlbWVudCIsImxlYXJuaW5nU3R5bGVQb2ludHMiLCJsZWFybmluZ1N0eWxlT3ZlcmxhcCIsIndvcmtTdHlsZUNvbXBsaW1lbnQiLCJwZXJzb25hbGl0eVBvaW50cyIsInBlcnNvbmFsaXR5Q29tcGxlbWVudCIsImdlbmRlclBvaW50cyIsImdlbmRlckNvbXBsZW1lbnQiLCJldGhuaWNpdHlQb2ludHMiLCJpbnRlcmVzdFBvaW50cyIsImludGVyZXN0T3ZlcmxhcCIsImlkZWFQb2ludHMiLCJpZGVhT3ZlcmxhcCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7OztBQUNBOzs7O0FBQ0E7Ozs7QUFDQTs7Ozs7Ozs7Ozs7O0FBRU8sSUFBTUEsc0RBQXVCQyxPQUFPQyxJQUFQLENBQVlDLHVCQUFaLEVBQTJCQyxNQUEzQixDQUFrQyxVQUFDQyxHQUFELEVBQU1DLElBQU4sRUFBZTtBQUNuRkQsTUFBSUMsSUFBSixJQUFZSCx3QkFBY0csSUFBZCxFQUFvQkMsT0FBaEM7QUFDQSxTQUFPRixHQUFQO0FBQ0QsQ0FIbUMsRUFHakMsRUFIaUMsQ0FBN0I7O0FBS1AsSUFBTUcsZUFBZSxTQUFmQSxZQUFlLENBQUNDLFNBQUQsRUFBZTtBQUNsQyxVQUFRQSxTQUFSO0FBQ0UsU0FBSyxZQUFMO0FBQ0UsYUFBTyxHQUFQO0FBQ0YsU0FBSyxLQUFMO0FBQ0UsYUFBTyxHQUFQO0FBQ0YsU0FBSyxJQUFMO0FBQ0UsYUFBTyxHQUFQO0FBQ0YsU0FBSyxXQUFMO0FBQ0UsYUFBTyxHQUFQOztBQUVGO0FBQ0UsYUFBTyxJQUFQO0FBWEo7QUFhRCxDQWREOztJQWdCTUMsUTtBQUNKLHNCQUFxQjtBQUFBOztBQUFBLHNDQUFOQyxJQUFNO0FBQU5BLFVBQU07QUFBQTs7QUFDbkIsU0FBS0MsS0FBTCxHQUFhLElBQUlDLEdBQUosQ0FBUUYsSUFBUixDQUFiO0FBQ0Q7Ozs7NEJBRU9HLFUsRUFBWTtBQUNsQixhQUFPLEtBQUtGLEtBQUwsQ0FBV0csT0FBWCxDQUFtQkQsVUFBbkIsQ0FBUDtBQUNEOzs7d0JBRUdFLEssRUFBTztBQUNULGFBQU8sS0FBS0osS0FBTCxDQUFXSyxHQUFYLENBQWVELEtBQWYsQ0FBUDtBQUNEOzs7d0JBRUdBLEssRUFBTztBQUNULGFBQU8sS0FBS0osS0FBTCxDQUFXTSxHQUFYLENBQWVGLEtBQWYsQ0FBUDtBQUNEOzs7NEJBRU87QUFDTixhQUFPLEtBQUtKLEtBQUwsQ0FBV08sS0FBWCxFQUFQO0FBQ0Q7Ozs0QkFFTUgsSyxFQUFPO0FBQ1osYUFBTyxLQUFLSixLQUFMLENBQVdRLE1BQVgsQ0FBa0JKLEtBQWxCLENBQVA7QUFDRDs7U0FFQUssT0FBT0MsUTs0QkFBWTtBQUNsQixhQUFPLEtBQUtWLEtBQUwsQ0FBV1MsT0FBT0MsUUFBbEIsR0FBUDtBQUNEOzs7Ozs7SUFHVUMsZ0IsV0FBQUEsZ0I7OztBQUNYLDhCQUFxQjtBQUFBOztBQUFBLHVDQUFOWixJQUFNO0FBQU5BLFVBQU07QUFBQTs7QUFBQSxvSUFDYkEsSUFEYTs7QUFFbkIsVUFBS2EsU0FBTCxHQUFpQixJQUFJWCxHQUFKLEVBQWpCO0FBRm1CO0FBR3BCOzs7O3dCQUVHRyxLLEVBQU87QUFDVCxVQUFNUyxRQUFRVCxNQUFNLENBQU4sRUFBU1UsTUFBVCxHQUFrQlYsTUFBTSxDQUFOLEVBQVNVLE1BQTNCLEdBQW9DVixNQUFNLENBQU4sQ0FBcEMsR0FBK0NBLE1BQU0sQ0FBTixDQUE3RDtBQUNBLFVBQU1XLFNBQVNYLE1BQU0sQ0FBTixFQUFTVSxNQUFULEdBQWtCVixNQUFNLENBQU4sRUFBU1UsTUFBM0IsR0FBb0NWLE1BQU0sQ0FBTixDQUFwQyxHQUErQ0EsTUFBTSxDQUFOLENBQTlEO0FBQ0EsOEhBQVUsQ0FBQ1MsS0FBRCxFQUFRRSxNQUFSLENBQVY7QUFDQSxhQUFPLEtBQUtILFNBQUwsQ0FBZVAsR0FBZixDQUFtQlEsTUFBTUMsTUFBTixHQUFlQyxPQUFPRCxNQUF6QyxDQUFQO0FBQ0Q7Ozt3QkFFR1YsSyxFQUFPO0FBQ1QsVUFBTVMsUUFBUVQsTUFBTSxDQUFOLEVBQVNVLE1BQVQsR0FBa0JWLE1BQU0sQ0FBTixFQUFTVSxNQUEzQixHQUFvQ1YsTUFBTSxDQUFOLENBQXBDLEdBQStDQSxNQUFNLENBQU4sQ0FBN0Q7QUFDQSxVQUFNVyxTQUFTWCxNQUFNLENBQU4sRUFBU1UsTUFBVCxHQUFrQlYsTUFBTSxDQUFOLEVBQVNVLE1BQTNCLEdBQW9DVixNQUFNLENBQU4sQ0FBcEMsR0FBK0NBLE1BQU0sQ0FBTixDQUE5RDtBQUNBLGFBQU8sS0FBS1EsU0FBTCxDQUFlTixHQUFmLENBQW1CTyxNQUFNQyxNQUFOLEdBQWVDLE9BQU9ELE1BQXpDLENBQVA7QUFDRDs7OztFQWpCbUNoQixROztJQW9CekJrQixVLFdBQUFBLFU7OztBQUNYLHdCQUFxQjtBQUFBOztBQUFBLHVDQUFOakIsSUFBTTtBQUFOQSxVQUFNO0FBQUE7O0FBQUEseUhBQ2JBLElBRGE7O0FBRW5CLFdBQUtrQixXQUFMLEdBQW1CLElBQUloQixHQUFKLENBQVFGLElBQVIsQ0FBbkI7QUFGbUI7QUFHcEI7Ozs7d0JBRUdLLEssRUFBTztBQUNULHlIQUFpQkEsTUFBTVUsTUFBdkI7QUFDRDs7O3dCQUVHVixLLEVBQU87QUFDVCxXQUFLYSxXQUFMLENBQWlCWixHQUFqQixDQUFxQkQsS0FBckI7QUFDQSx5SEFBaUJBLE1BQU1VLE1BQXZCO0FBQ0Q7Ozs0QkFFT1osVSxFQUFZO0FBQ2xCLGFBQU8sS0FBS2UsV0FBTCxDQUFpQmQsT0FBakIsQ0FBeUJELFVBQXpCLENBQVA7QUFDRDs7OztFQWpCNkJKLFE7O0lBb0JuQm9CLGMsV0FBQUEsYzs7O0FBQ1gsNEJBQXFCO0FBQUE7O0FBQUEsdUNBQU5uQixJQUFNO0FBQU5BLFVBQU07QUFBQTs7QUFBQSxpSUFDYkEsSUFEYTs7QUFFbkIsV0FBS2tCLFdBQUwsR0FBbUIsSUFBSWhCLEdBQUosQ0FBUUYsSUFBUixDQUFuQjtBQUZtQjtBQUdwQjs7Ozt5Q0FFdUM7QUFBQTtBQUFBLFVBQXhCb0IsT0FBd0I7QUFBQSxVQUFmQyxZQUFlOztBQUN0QyxVQUFNQyxjQUFjRixRQUFRTCxNQUFSLEdBQWlCTSxhQUFhTixNQUFsRDtBQUNBLFVBQU1RLGNBQWNGLGFBQWFOLE1BQWIsR0FBc0JLLFFBQVFMLE1BQWxEO0FBQ0EsYUFBTyxLQUFLUixHQUFMLENBQVNlLFdBQVQsS0FBeUIsS0FBS2YsR0FBTCxDQUFTZ0IsV0FBVCxDQUFoQztBQUNEOzs7MENBRXVDO0FBQUE7QUFBQSxVQUF4QkgsT0FBd0I7QUFBQSxVQUFmQyxZQUFlOztBQUN0QyxXQUFLSCxXQUFMLENBQWlCWixHQUFqQixDQUFxQixDQUFDYyxPQUFELEVBQVVDLFlBQVYsQ0FBckI7QUFDQSxhQUFPLEtBQUtmLEdBQUwsQ0FBU2MsUUFBUUwsTUFBUixHQUFpQk0sYUFBYU4sTUFBdkMsQ0FBUDtBQUNEOzs7O0VBZmlDaEIsUTs7SUFrQmZ5QixPO0FBQ25CLG1CQUFZQyxJQUFaLEVBQXdEO0FBQUEsUUFBdENDLGFBQXNDLHVFQUF0QnJDLG9CQUFzQjs7QUFBQTs7QUFDdEQsU0FBS3NDLE1BQUwsR0FBY0YsSUFBZDtBQUNBLFNBQUtWLE1BQUwsR0FBYyx3QkFBS2EsU0FBU0MsbUJBQW1CQyxLQUFLQyxTQUFMLENBQWVOLElBQWYsQ0FBbkIsQ0FBVCxDQUFMLENBQWQ7QUFDQSxTQUFLTyxlQUFMLEdBQXVCUCxLQUFLUSxhQUFMLElBQXNCLFNBQTdDO0FBQ0EsU0FBS0MsV0FBTCxHQUFtQlQsS0FBS1UsU0FBTCxJQUFrQixTQUFyQztBQUNBLFNBQUtDLFVBQUwsR0FBa0JYLEtBQUtZLFFBQUwsSUFBaUIsU0FBbkM7QUFDQSxTQUFLQyxpQkFBTCxHQUF5QmIsS0FBS2MsZUFBTCxJQUF3QixTQUFqRDtBQUNBLFNBQUtDLGtCQUFMLEdBQTBCZixLQUFLZ0IsZ0JBQUwsSUFBeUIsU0FBbkQ7QUFDQSxTQUFLQyxjQUFMLEdBQXNCakIsS0FBS2tCLFlBQUwsSUFBcUIsU0FBM0M7QUFDQSxTQUFLQyxXQUFMLEdBQW1CbkIsS0FBS29CLFNBQUwsSUFBa0IsRUFBckM7QUFDQSxTQUFLQyxPQUFMLEdBQWVyQixLQUFLc0IsS0FBTCxJQUFjLEVBQTdCO0FBQ0EsU0FBS0MsZUFBTCxHQUF1QnRCLGFBQXZCO0FBQ0EsU0FBS3VCLE1BQUwsR0FBY3hCLEtBQUt5QixJQUFMLElBQWF6QixLQUFLMEIsUUFBTCxDQUFjRCxJQUF6QztBQUNBLFNBQUtFLFFBQUwsR0FBZ0IzQixLQUFLNEIsTUFBckI7QUFDQSxTQUFLQyxRQUFMLEdBQWdCN0IsS0FBSzhCLE1BQUwsSUFBZTlCLEtBQUswQixRQUFMLENBQWNJLE1BQTdDO0FBQ0EsU0FBS0MsV0FBTCxHQUFtQi9CLEtBQUtnQyxTQUFMLElBQWtCaEMsS0FBSzBCLFFBQUwsQ0FBY00sU0FBbkQ7QUFDRDs7Ozs0QkEwRE9wQyxZLEVBQWM7QUFDcEIsYUFBTyxLQUFLTixNQUFMLEtBQWdCTSxhQUFhTixNQUFwQztBQUNEOzs7eUNBRW9Ca0IsYSxFQUFlO0FBQ2xDLFVBQUksS0FBS0EsYUFBTCxLQUF1QkEsYUFBM0IsRUFBMEM7QUFDeEMsZUFBTyxDQUFQO0FBQ0Q7O0FBRUQsYUFBTyxDQUFQO0FBQ0Q7Ozt3Q0FFbUJFLFMsRUFBVztBQUM3QixVQUFNdUIsYUFBYSxTQUFiQSxVQUFhLENBQUNDLENBQUQsRUFBSUMsQ0FBSjtBQUFBLGVBQVVELEVBQUVFLGFBQUYsQ0FBZ0JELENBQWhCLENBQVY7QUFBQSxPQUFuQjtBQUNBLFVBQUlFLGtCQUFrQixDQUF0Qjs7QUFFQSxVQUFNQyxRQUFRLEtBQUs1QixTQUFMLENBQWU2QixLQUFmLENBQXFCLEdBQXJCLEVBQTBCQyxHQUExQixDQUE4QjtBQUFBLGVBQUtDLEVBQUVDLElBQUYsRUFBTDtBQUFBLE9BQTlCLENBQWQsQ0FKNkIsQ0FJK0I7QUFDNURKLFlBQU1LLElBQU4sQ0FBV1YsVUFBWDtBQUNBLFVBQU1XLGFBQWFsQyxVQUFVNkIsS0FBVixDQUFnQixHQUFoQixFQUFxQkMsR0FBckIsQ0FBeUI7QUFBQSxlQUFLQyxFQUFFQyxJQUFGLEVBQUw7QUFBQSxPQUF6QixDQUFuQjtBQUNBRSxpQkFBV0QsSUFBWCxDQUFnQlYsVUFBaEI7O0FBRUEsVUFBTVksVUFBVVAsTUFBTVEsTUFBTixHQUFlRixXQUFXRSxNQUExQixHQUFtQ1IsS0FBbkMsR0FBMkNNLFVBQTNELENBVDZCLENBUzBDO0FBQ3ZFLFVBQU1HLFNBQVNULE1BQU1RLE1BQU4sR0FBZUYsV0FBV0UsTUFBMUIsR0FBbUNGLFVBQW5DLEdBQWdETixLQUEvRDs7QUFFQTtBQUNBLFdBQUssSUFBSVUsSUFBSSxDQUFiLEVBQWdCQSxJQUFJSCxRQUFRQyxNQUE1QixFQUFvQ0UsS0FBSyxDQUF6QyxFQUE0QztBQUMxQyxZQUFJLDRCQUFHRCxNQUFILEVBQVdGLFFBQVFHLENBQVIsQ0FBWCxFQUF1QmYsVUFBdkIsSUFBcUMsQ0FBekMsRUFBNEM7QUFDMUNJLDZCQUFtQixDQUFuQjtBQUNEO0FBQ0Y7O0FBRUQsYUFBT0Esa0JBQWtCUSxRQUFRQyxNQUFqQyxDQW5CNkIsQ0FtQlk7QUFDMUM7OzttQ0FFY2xELFksRUFBYztBQUMzQjtBQUNBLFVBQUksS0FBS2dCLFFBQUwsS0FBa0JoQixhQUFhZ0IsUUFBbkMsRUFBNkM7QUFDM0MsZUFBTyxLQUFLVyxlQUFMLENBQXFCWCxRQUFyQixJQUFpQ2hELHFCQUFxQmdELFFBQTdEO0FBQ0Q7O0FBRUQsYUFBTyxDQUFQO0FBQ0Q7OzswQ0FFcUJxQyxvQixFQUFzQjtBQUMxQyxVQUFJQyxtQkFBbUIsQ0FBdkI7QUFDQSxXQUFLLElBQUlGLElBQUksQ0FBYixFQUFnQkEsSUFBSSxDQUFwQixFQUF1QkEsS0FBSyxDQUE1QixFQUErQjtBQUM3QixZQUFJLEtBQUtsQyxlQUFMLENBQXFCa0MsQ0FBckIsTUFBNEJDLHFCQUFxQkQsQ0FBckIsQ0FBaEMsRUFBeUQ7QUFDdkRFLDhCQUFvQixDQUFwQjtBQUNEO0FBQ0Y7O0FBRUQsYUFBT0EsbUJBQW1CLENBQTFCO0FBQ0Q7OztxQ0FFZ0JDLFcsRUFBYTtBQUM1QixVQUFJQyxxQkFBcUIsR0FBekI7QUFDQSxVQUFNQyxlQUFlLEtBQUt6QixNQUExQjtBQUNBLFVBQU0wQixvQkFBb0JILFdBQTFCO0FBQ0EsVUFBTUksYUFBYTFGLE9BQU9DLElBQVAsQ0FBWXVGLGdCQUFnQixFQUE1QixDQUFuQjs7QUFFQSxXQUFLLElBQUlMLElBQUksQ0FBUixFQUFXUSxJQUFJRCxXQUFXVCxNQUEvQixFQUF1Q0UsSUFBSVEsQ0FBM0MsRUFBOENSLEtBQUssQ0FBbkQsRUFBc0Q7QUFDcEQsWUFBTVMsV0FBV0YsV0FBV1AsQ0FBWCxDQUFqQjtBQUNBLFlBQU1VLFFBQVFMLGFBQWFJLFFBQWIsQ0FBZDtBQUNBLFlBQU1FLGFBQWFMLGtCQUFrQkcsUUFBbEIsQ0FBbkI7QUFDQSxZQUFNRyxrQkFBa0JDLEtBQUtDLEdBQUwsQ0FBU0osUUFBUUMsVUFBakIsQ0FBeEI7O0FBRUEsWUFBSUMsa0JBQWtCLENBQXRCLEVBQXlCO0FBQ3ZCUixnQ0FBc0IsQ0FBdEI7QUFDRDtBQUNGOztBQUVELGFBQU9BLHFCQUFxQkcsV0FBV1QsTUFBdkMsQ0FqQjRCLENBaUJtQjtBQUNoRDs7OzZDQUU4QztBQUFBLFVBQXhCaUIsaUJBQXdCLHVFQUFKLEVBQUk7O0FBQzdDLFVBQUlDLGVBQWUsR0FBbkI7QUFENkMsMEJBRWYsSUFGZSxDQUVyQzlDLFlBRnFDO0FBQUEsVUFFckNBLFlBRnFDLGlDQUV0QixFQUZzQjs7O0FBSTdDLFVBQU1lLGFBQWEsU0FBYkEsVUFBYSxDQUFDQyxDQUFELEVBQUlDLENBQUo7QUFBQSxlQUFVRCxFQUFFRSxhQUFGLENBQWdCRCxDQUFoQixDQUFWO0FBQUEsT0FBbkI7QUFDQWpCLG1CQUFheUIsSUFBYixDQUFrQlYsVUFBbEI7QUFDQThCLHdCQUFrQnBCLElBQWxCLENBQXVCVixVQUF2Qjs7QUFFQSxVQUFNWSxVQUFVM0IsYUFBYTRCLE1BQWIsR0FBc0JpQixrQkFBa0JqQixNQUF4QyxHQUFpRDVCLFlBQWpELEdBQWdFNkMsaUJBQWhGO0FBQ0EsVUFBTWhCLFNBQVM3QixhQUFhNEIsTUFBYixHQUFzQmlCLGtCQUFrQmpCLE1BQXhDLEdBQWlEaUIsaUJBQWpELEdBQXFFN0MsWUFBcEY7O0FBRUEsV0FBSyxJQUFJOEIsSUFBSSxDQUFSLEVBQVdRLElBQUlYLFFBQVFDLE1BQTVCLEVBQW9DRSxJQUFJUSxDQUF4QyxFQUEyQ1IsS0FBSyxDQUFoRCxFQUFtRDtBQUNqRCxZQUFJLDRCQUFHRCxNQUFILEVBQVdGLFFBQVFHLENBQVIsQ0FBWCxFQUF1QmYsVUFBdkIsSUFBcUMsQ0FBekMsRUFBNEM7QUFDMUMrQiwwQkFBZ0IsQ0FBaEI7QUFDRDtBQUNGOztBQUVELGFBQU9BLGVBQWVqQixPQUFPRCxNQUE3QjtBQUNEOzs7b0NBRWVtQixjLEVBQWdCO0FBQzlCLFVBQUlDLGtCQUFrQixHQUF0QjtBQUNBLFVBQU1qQyxhQUFhLFNBQWJBLFVBQWEsQ0FBQ0MsQ0FBRCxFQUFJQyxDQUFKO0FBQUEsZUFBVUQsRUFBRUUsYUFBRixDQUFnQkQsQ0FBaEIsQ0FBVjtBQUFBLE9BQW5COztBQUY4QixVQUl0QmYsU0FKc0IsR0FJUixJQUpRLENBSXRCQSxTQUpzQjs7O0FBTTlCLFVBQUlBLFVBQVUwQixNQUFWLEtBQXFCLENBQXJCLElBQTBCbUIsZUFBZW5CLE1BQWYsS0FBMEIsQ0FBeEQsRUFBMkQ7QUFDekQsZUFBTyxDQUFQO0FBQ0Q7O0FBRUQxQixnQkFBVXVCLElBQVYsQ0FBZVYsVUFBZjs7QUFFQWdDLHFCQUFldEIsSUFBZixDQUFvQlYsVUFBcEI7O0FBRUEsVUFBTVksVUFBVXpCLFVBQVUwQixNQUFWLEdBQW1CbUIsZUFBZW5CLE1BQWxDLEdBQTJDMUIsU0FBM0MsR0FBdUQ2QyxjQUF2RTtBQUNBLFVBQU1sQixTQUFTM0IsVUFBVTBCLE1BQVYsR0FBbUJtQixlQUFlbkIsTUFBbEMsR0FBMkNtQixjQUEzQyxHQUE0RDdDLFNBQTNFOztBQUVBLFdBQUssSUFBSTRCLElBQUksQ0FBUixFQUFXUSxJQUFJWCxRQUFRQyxNQUE1QixFQUFvQ0UsSUFBSVEsQ0FBeEMsRUFBMkNSLEtBQUssQ0FBaEQsRUFBbUQ7QUFDakQsWUFBSSw0QkFBR0QsTUFBSCxFQUFXRixRQUFRRyxDQUFSLENBQVgsRUFBdUJmLFVBQXZCLElBQXFDLENBQUMsQ0FBMUMsRUFBNkM7QUFDM0NpQyw2QkFBbUIsQ0FBbkI7QUFDRDtBQUNGOztBQUVELGFBQU9BLGtCQUFrQm5CLE9BQU9ELE1BQWhDO0FBQ0Q7OztnQ0FFV3FCLFUsRUFBWTtBQUN0QixVQUFJQyx5QkFBeUIsR0FBN0I7O0FBRUEsVUFBTW5DLGFBQWEsU0FBYkEsVUFBYSxDQUFDQyxDQUFELEVBQUlDLENBQUo7QUFBQSxlQUFVRCxFQUFFbUMsRUFBRixHQUFPbEMsRUFBRWtDLEVBQW5CO0FBQUEsT0FBbkI7QUFIc0IsVUFJZC9DLEtBSmMsR0FJSixJQUpJLENBSWRBLEtBSmM7O0FBS3RCQSxZQUFNcUIsSUFBTixDQUFXVixVQUFYOztBQUVBa0MsaUJBQVd4QixJQUFYLENBQWdCVixVQUFoQjs7QUFFQTtBQUNBLFVBQU1ZLFVBQVV2QixNQUFNd0IsTUFBTixHQUFlcUIsV0FBV3JCLE1BQTFCLEdBQW1DeEIsS0FBbkMsR0FBMkM2QyxVQUEzRDtBQUNBLFVBQU1wQixTQUFTekIsTUFBTXdCLE1BQU4sR0FBZXFCLFdBQVdyQixNQUExQixHQUFtQ3FCLFVBQW5DLEdBQWdEN0MsS0FBL0Q7O0FBRUEsV0FBSyxJQUFJMEIsSUFBSSxDQUFiLEVBQWdCQSxJQUFJSCxRQUFRQyxNQUE1QixFQUFvQ0UsS0FBSyxDQUF6QyxFQUE0QztBQUMxQyxZQUFNc0IsU0FBU3pCLFFBQVFHLENBQVIsQ0FBZjtBQUNBLFlBQU11QixlQUFlLDRCQUFHeEIsTUFBSCxFQUFXdUIsTUFBWCxFQUFtQnJDLFVBQW5CLENBQXJCOztBQUVBLFlBQUlzQyxlQUFlLENBQUMsQ0FBcEIsRUFBdUI7QUFDckIsY0FBTUMsWUFBWXpCLE9BQU93QixZQUFQLENBQWxCO0FBQ0EsY0FBTWxHLFlBQVlpRyxPQUFPRyxRQUF6QjtBQUNBLGNBQU1DLGlCQUFpQkYsVUFBVUMsUUFBakM7O0FBRUFMLG9DQUEwQixNQUFNUCxLQUFLQyxHQUFMLENBQVN6RixZQUFZcUcsY0FBckIsQ0FBaEM7QUFDRDtBQUNGOztBQUVELFVBQUksS0FBS3BELEtBQUwsQ0FBV3dCLE1BQVgsR0FBb0IsQ0FBeEIsRUFBMkI7QUFDekIsZUFBT3NCLDBCQUEwQixJQUFJckIsT0FBT0QsTUFBckMsQ0FBUCxDQUR5QixDQUM0QjtBQUN0RDs7QUFFRCxhQUFPLENBQVA7QUFDRDs7QUFFRDs7OzttQ0FDZXJCLEksRUFBTTtBQUNuQjtBQUNBLFVBQUksS0FBS0EsSUFBTCxLQUFjQSxJQUFsQixFQUF3QjtBQUN0QixlQUFPLENBQVA7QUFDRDs7QUFFRCxhQUFPLENBQVA7QUFDRDs7O3FDQUVnQkssTSxFQUFRO0FBQ3ZCO0FBQ0EsVUFBSSxLQUFLQSxNQUFMLEtBQWdCQSxNQUFwQixFQUE0QjtBQUMxQixlQUFPLENBQVA7QUFDRDs7QUFFRCxhQUFPLENBQVA7QUFDRDs7O3dDQUVtQkUsUyxFQUFXO0FBQzdCO0FBQ0EsVUFBSSxLQUFLQSxTQUFMLEtBQW1CQSxTQUF2QixFQUFrQztBQUNoQyxlQUFPLENBQVA7QUFDRDs7QUFFRCxhQUFPLENBQVA7QUFDRDs7OytDQUUwQjJDLFMsRUFBVztBQUNwQyxVQUFNMUMsYUFBYSxTQUFiQSxVQUFhLENBQUNDLENBQUQsRUFBSUMsQ0FBSjtBQUFBLGVBQVVELEVBQUVFLGFBQUYsQ0FBZ0JELENBQWhCLENBQVY7QUFBQSxPQUFuQjtBQUNBLFVBQUl5QyxXQUFXLENBQWY7O0FBRUEsVUFBTUMsS0FBSyxLQUFLN0QsZ0JBQUwsQ0FBc0J1QixLQUF0QixDQUE0QixJQUE1QixDQUFYO0FBQ0FzQyxTQUFHbEMsSUFBSCxDQUFRVixVQUFSOztBQUVBLFVBQU02QyxVQUFVSCxVQUFVcEMsS0FBVixDQUFnQixJQUFoQixDQUFoQjtBQUNBdUMsY0FBUW5DLElBQVIsQ0FBYVYsVUFBYjs7QUFFQSxVQUFNWSxVQUFVZ0MsR0FBRy9CLE1BQUgsR0FBWWdDLFFBQVFoQyxNQUFwQixHQUE2QitCLEVBQTdCLEdBQWtDQyxPQUFsRDtBQUNBLFVBQU0vQixTQUFTOEIsR0FBRy9CLE1BQUgsR0FBWWdDLFFBQVFoQyxNQUFwQixHQUE2QmdDLE9BQTdCLEdBQXVDRCxFQUF0RDs7QUFFQSxXQUFLLElBQUk3QixJQUFJLENBQWIsRUFBZ0JBLElBQUlILFFBQVFDLE1BQTVCLEVBQW9DRSxLQUFLLENBQXpDLEVBQTRDO0FBQzFDLFlBQUksNEJBQUdELE1BQUgsRUFBV0YsUUFBUUcsQ0FBUixDQUFYLEVBQXVCZixVQUF2QixJQUFxQyxDQUF6QyxFQUE0QztBQUMxQztBQUNBMkMsc0JBQVksQ0FBWjtBQUNEO0FBQ0Y7O0FBRUQ7O0FBRUEsYUFBT0EsV0FBVy9CLFFBQVFDLE1BQTFCLENBdEJvQyxDQXNCRjtBQUNuQzs7O2lDQUVZbEQsWSxFQUFjO0FBQ3pCLFVBQUltRixTQUFTLEdBQWI7O0FBRUEsVUFBTTlFLGdCQUFnQnBDLE9BQU9tSCxNQUFQLENBQWMsRUFBZCxFQUFrQnBILG9CQUFsQixFQUF3QyxLQUFLMkQsZUFBN0MsQ0FBdEI7O0FBRUF3RCxnQkFBVTlFLGNBQWNnRixVQUFkLEdBQTJCLEtBQUtDLGNBQUwsQ0FBb0J0RixhQUFhNkIsSUFBakMsQ0FBckM7QUFDQTtBQUNBc0QsZ0JBQVU5RSxjQUFja0Ysc0JBQWQsR0FBdUMsS0FBS0Msc0JBQUwsQ0FBNEJ4RixhQUFhc0IsWUFBekMsQ0FBakQ7QUFDQTZELGdCQUFVOUUsY0FBYytELFlBQWQsR0FBNkIsS0FBS3FCLGdCQUFMLENBQXNCekYsYUFBYWdDLE1BQW5DLENBQXZDO0FBQ0FtRCxnQkFBVTlFLGNBQWNxRixtQkFBZCxHQUFvQyxLQUFLQyxvQkFBTCxDQUEwQjNGLGFBQWFZLGFBQXZDLENBQTlDO0FBQ0F1RSxnQkFBVTlFLGNBQWNvQyxlQUFkLEdBQWdDLEtBQUttRCxtQkFBTCxDQUF5QjVGLGFBQWFjLFNBQXRDLENBQTFDO0FBQ0FxRSxnQkFBVTlFLGNBQWN3RixpQkFBZCxHQUFrQyxLQUFLQyxxQkFBTCxDQUEyQjlGLGFBQWFrQixlQUF4QyxDQUE1QztBQUNBaUUsZ0JBQVU5RSxjQUFjMEYsWUFBZCxHQUE2QixLQUFLQyxnQkFBTCxDQUFzQmhHLGFBQWFrQyxNQUFuQyxDQUF2QztBQUNBaUQsZ0JBQVU5RSxjQUFjNEYsZUFBZCxHQUFnQyxLQUFLRCxnQkFBTCxDQUFzQmhHLGFBQWFvQyxTQUFuQyxDQUExQzs7QUFFQTtBQUNBK0MsZ0JBQVU5RSxjQUFjNkYsY0FBZCxHQUErQixLQUFLQyxlQUFMLENBQXFCbkcsYUFBYXdCLFNBQWxDLENBQXpDOztBQUVBO0FBQ0EyRCxnQkFBVTlFLGNBQWMrRixVQUFkLEdBQTJCLEtBQUtDLFdBQUwsQ0FBaUJyRyxhQUFhMEIsS0FBOUIsQ0FBckM7O0FBR0EsYUFBT3lELE1BQVA7QUFDRDs7O3dCQTdSVTtBQUNULGFBQU8sS0FBS3pGLE1BQVo7QUFDRDs7O3dCQUVVO0FBQ1QsYUFBTyxLQUFLa0MsTUFBWjtBQUNEOzs7d0JBRVk7QUFDWCxhQUFPLEtBQUtLLFFBQVo7QUFDRDs7O3dCQUVlO0FBQ2QsYUFBTyxLQUFLRSxXQUFaO0FBQ0Q7Ozt3QkFFVztBQUNWLGFBQU8sS0FBS1YsT0FBWjtBQUNEOzs7d0JBRWM7QUFDYixhQUFPLENBQUMsS0FBS25CLE1BQUwsQ0FBWXdCLFFBQVosSUFBd0IsRUFBekIsRUFBNkJKLEtBQTdCLElBQXNDLEVBQTdDO0FBQ0Q7Ozt3QkFFZTtBQUNkLGFBQU8sS0FBS0gsV0FBWjtBQUNEOzs7d0JBRW1CO0FBQ2xCLGFBQU8sS0FBS1osZUFBWjtBQUNEOzs7d0JBRXFCO0FBQ3BCLGFBQU8sS0FBS00saUJBQVo7QUFDRDs7O3dCQUVjO0FBQ2IsYUFBTyxLQUFLRixVQUFaO0FBQ0Q7Ozt3QkFFZTtBQUNkLGFBQU8sS0FBS0YsV0FBWjtBQUNEOzs7d0JBRXNCO0FBQ3JCLGFBQU8sS0FBS00sa0JBQVo7QUFDRDs7O3dCQUVrQjtBQUNqQixhQUFPLEtBQUtFLGNBQVo7QUFDRDs7O3dCQUVZO0FBQ1gsYUFBTyxLQUFLVSxRQUFaO0FBQ0Q7Ozs7OztrQkF6RWtCNUIsTyIsImZpbGUiOiJzdHVkZW50LmpzIiwic291cmNlc0NvbnRlbnQiOlsiXG5pbXBvcnQgYnRvYSBmcm9tICdidG9hLWxpdGUnO1xuaW1wb3J0IGJzIGZyb20gJ2JpbmFyeS1zZWFyY2gnO1xuaW1wb3J0IFRlYW1pbmdDb25maWcgZnJvbSAnLi90ZWFtaW5nLWNvbmZpZyc7XG5cbmV4cG9ydCBjb25zdCBkZWZhdWx0VGVhbWluZ0NvbmZpZyA9IE9iamVjdC5rZXlzKFRlYW1pbmdDb25maWcpLnJlZHVjZSgob2xkLCBuZXh0KSA9PiB7XG4gIG9sZFtuZXh0XSA9IFRlYW1pbmdDb25maWdbbmV4dF0uZGVmYXVsdDtcbiAgcmV0dXJuIG9sZDtcbn0sIHt9KTtcblxuY29uc3QgbWFwU2VudGltZW50ID0gKHNlbnRpbWVudCkgPT4ge1xuICBzd2l0Y2ggKHNlbnRpbWVudCkge1xuICAgIGNhc2UgJ3N0cm9uZ195ZXMnOlxuICAgICAgcmV0dXJuIDQuMDtcbiAgICBjYXNlICd5ZXMnOlxuICAgICAgcmV0dXJuIDMuMDtcbiAgICBjYXNlICdubyc6XG4gICAgICByZXR1cm4gMi4wO1xuICAgIGNhc2UgJ3N0cm9uZ19ubyc6XG4gICAgICByZXR1cm4gMS4wO1xuXG4gICAgZGVmYXVsdDpcbiAgICAgIHJldHVybiBudWxsO1xuICB9XG59O1xuXG5jbGFzcyBGYWxzZVNldCB7XG4gIGNvbnN0cnVjdG9yKC4uLmFyZ3MpIHtcbiAgICB0aGlzLnBfc2V0ID0gbmV3IFNldChhcmdzKTtcbiAgfVxuXG4gIGZvckVhY2goY2FsbGJhY2tGbikge1xuICAgIHJldHVybiB0aGlzLnBfc2V0LmZvckVhY2goY2FsbGJhY2tGbik7XG4gIH1cblxuICBhZGQodmFsdWUpIHtcbiAgICByZXR1cm4gdGhpcy5wX3NldC5hZGQodmFsdWUpO1xuICB9XG5cbiAgaGFzKHZhbHVlKSB7XG4gICAgcmV0dXJuIHRoaXMucF9zZXQuaGFzKHZhbHVlKTtcbiAgfVxuXG4gIGNsZWFyKCkge1xuICAgIHJldHVybiB0aGlzLnBfc2V0LmNsZWFyKCk7XG4gIH1cblxuICBkZWxldGUodmFsdWUpIHtcbiAgICByZXR1cm4gdGhpcy5wX3NldC5kZWxldGUodmFsdWUpO1xuICB9XG5cbiAgW1N5bWJvbC5pdGVyYXRvcl0oKSB7XG4gICAgcmV0dXJuIHRoaXMucF9zZXRbU3ltYm9sLml0ZXJhdG9yXSgpO1xuICB9XG59XG5cbmV4cG9ydCBjbGFzcyBTb3J0ZWRTdHVkZW50U2V0IGV4dGVuZHMgRmFsc2VTZXQge1xuICBjb25zdHJ1Y3RvciguLi5hcmdzKSB7XG4gICAgc3VwZXIoYXJncyk7XG4gICAgdGhpcy5wX2hhc2hTZXQgPSBuZXcgU2V0KCk7XG4gIH1cblxuICBhZGQodmFsdWUpIHtcbiAgICBjb25zdCBmaXJzdCA9IHZhbHVlWzBdLnBfaGFzaCA+IHZhbHVlWzFdLnBfaGFzaCA/IHZhbHVlWzBdIDogdmFsdWVbMV07XG4gICAgY29uc3Qgc2Vjb25kID0gdmFsdWVbMF0ucF9oYXNoID4gdmFsdWVbMV0ucF9oYXNoID8gdmFsdWVbMV0gOiB2YWx1ZVswXTtcbiAgICBzdXBlci5hZGQoW2ZpcnN0LCBzZWNvbmRdKTtcbiAgICByZXR1cm4gdGhpcy5wX2hhc2hTZXQuYWRkKGZpcnN0LnBfaGFzaCArIHNlY29uZC5wX2hhc2gpO1xuICB9XG5cbiAgaGFzKHZhbHVlKSB7XG4gICAgY29uc3QgZmlyc3QgPSB2YWx1ZVswXS5wX2hhc2ggPiB2YWx1ZVsxXS5wX2hhc2ggPyB2YWx1ZVswXSA6IHZhbHVlWzFdO1xuICAgIGNvbnN0IHNlY29uZCA9IHZhbHVlWzBdLnBfaGFzaCA+IHZhbHVlWzFdLnBfaGFzaCA/IHZhbHVlWzFdIDogdmFsdWVbMF07XG4gICAgcmV0dXJuIHRoaXMucF9oYXNoU2V0LmhhcyhmaXJzdC5wX2hhc2ggKyBzZWNvbmQucF9oYXNoKTtcbiAgfVxufVxuXG5leHBvcnQgY2xhc3MgU3R1ZGVudFNldCBleHRlbmRzIEZhbHNlU2V0IHtcbiAgY29uc3RydWN0b3IoLi4uYXJncykge1xuICAgIHN1cGVyKGFyZ3MpO1xuICAgIHRoaXMucF9hY3R1YWxTZXQgPSBuZXcgU2V0KGFyZ3MpO1xuICB9XG5cbiAgaGFzKHZhbHVlKSB7XG4gICAgcmV0dXJuIHN1cGVyLmhhcyh2YWx1ZS5wX2hhc2gpO1xuICB9XG5cbiAgYWRkKHZhbHVlKSB7XG4gICAgdGhpcy5wX2FjdHVhbFNldC5hZGQodmFsdWUpO1xuICAgIHJldHVybiBzdXBlci5hZGQodmFsdWUucF9oYXNoKTtcbiAgfVxuXG4gIGZvckVhY2goY2FsbGJhY2tGbikge1xuICAgIHJldHVybiB0aGlzLnBfYWN0dWFsU2V0LmZvckVhY2goY2FsbGJhY2tGbik7XG4gIH1cbn1cblxuZXhwb3J0IGNsYXNzIFN0dWRlbnRQYWlyU2V0IGV4dGVuZHMgRmFsc2VTZXQge1xuICBjb25zdHJ1Y3RvciguLi5hcmdzKSB7XG4gICAgc3VwZXIoYXJncyk7XG4gICAgdGhpcy5wX2FjdHVhbFNldCA9IG5ldyBTZXQoYXJncyk7XG4gIH1cblxuICBoYXNTdHVkZW50UGFpcihbc3R1ZGVudCwgb3RoZXJTdHVkZW50XSkge1xuICAgIGNvbnN0IHBvc3NpYmxpdHkxID0gc3R1ZGVudC5wX2hhc2ggKyBvdGhlclN0dWRlbnQucF9oYXNoO1xuICAgIGNvbnN0IHBvc3NpYmxpdHkyID0gb3RoZXJTdHVkZW50LnBfaGFzaCArIHN0dWRlbnQucF9oYXNoO1xuICAgIHJldHVybiB0aGlzLmhhcyhwb3NzaWJsaXR5MSkgfHwgdGhpcy5oYXMocG9zc2libGl0eTIpO1xuICB9XG5cbiAgYWRkU3R1ZGVudFBhaXIoW3N0dWRlbnQsIG90aGVyU3R1ZGVudF0pIHtcbiAgICB0aGlzLnBfYWN0dWFsU2V0LmFkZChbc3R1ZGVudCwgb3RoZXJTdHVkZW50XSk7XG4gICAgcmV0dXJuIHRoaXMuYWRkKHN0dWRlbnQucF9oYXNoICsgb3RoZXJTdHVkZW50LnBfaGFzaCk7XG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgU3R1ZGVudCB7XG4gIGNvbnN0cnVjdG9yKGpzb24sIHRlYW1pbmdDb25maWcgPSBkZWZhdWx0VGVhbWluZ0NvbmZpZykge1xuICAgIHRoaXMucF9qc29uID0ganNvbjtcbiAgICB0aGlzLnBfaGFzaCA9IGJ0b2EodW5lc2NhcGUoZW5jb2RlVVJJQ29tcG9uZW50KEpTT04uc3RyaW5naWZ5KGpzb24pKSkpO1xuICAgIHRoaXMucF9sZWFybmluZ1N0eWxlID0ganNvbi5sZWFybmluZ1N0eWxlIHx8ICdkZWZhdWx0JztcbiAgICB0aGlzLnBfd29ya1N0eWxlID0ganNvbi53b3JrU3R5bGUgfHwgJ2RlZmF1bHQnO1xuICAgIHRoaXMucF9FUDEwVHlwZSA9IGpzb24uRVAxMFR5cGUgfHwgJ2RlZmF1bHQnO1xuICAgIHRoaXMucF9wZXJzb25hbGl0eVR5cGUgPSBqc29uLnBlcnNvbmFsaXR5VHlwZSB8fCAnZGVmYXVsdCc7XG4gICAgdGhpcy5wX2VudHJlcHJlbmV1clR5cGUgPSBqc29uLmVudHJlcHJlbmV1clR5cGUgfHwgJ2RlZmF1bHQnO1xuICAgIHRoaXMucF9sYXVuY2hTa2lsbHMgPSBqc29uLmxhdW5jaFNraWxscyB8fCAnZGVmYXVsdCc7XG4gICAgdGhpcy5wX2ludGVyZXN0cyA9IGpzb24uaW50ZXJlc3RzIHx8IFtdO1xuICAgIHRoaXMucF9pZGVhcyA9IGpzb24uaWRlYXMgfHwgW107XG4gICAgdGhpcy5wX3RlYW1pbmdDb25maWcgPSB0ZWFtaW5nQ29uZmlnO1xuICAgIHRoaXMucF9yb2xlID0ganNvbi5yb2xlIHx8IGpzb24ubWV0YWRhdGEucm9sZTtcbiAgICB0aGlzLnBfc2tpbGxzID0ganNvbi5za2lsbHM7XG4gICAgdGhpcy5wX2dlbmRlciA9IGpzb24uZ2VuZGVyIHx8IGpzb24ubWV0YWRhdGEuZ2VuZGVyO1xuICAgIHRoaXMucF9ldGhuaWNpdHkgPSBqc29uLmV0aG5pY2l0eSB8fCBqc29uLm1ldGFkYXRhLmV0aG5pY2l0eTtcbiAgfVxuXG4gIGdldCBoYXNoKCkge1xuICAgIHJldHVybiB0aGlzLnBfaGFzaDtcbiAgfVxuXG4gIGdldCByb2xlKCkge1xuICAgIHJldHVybiB0aGlzLnBfcm9sZTtcbiAgfVxuXG4gIGdldCBnZW5kZXIoKSB7XG4gICAgcmV0dXJuIHRoaXMucF9nZW5kZXI7XG4gIH1cblxuICBnZXQgZXRobmljaXR5KCkge1xuICAgIHJldHVybiB0aGlzLnBfZXRobmljaXR5O1xuICB9XG5cbiAgZ2V0IGlkZWFzKCkge1xuICAgIHJldHVybiB0aGlzLnBfaWRlYXM7XG4gIH1cblxuICBnZXQgb3duSWRlYXMoKSB7XG4gICAgcmV0dXJuICh0aGlzLnBfanNvbi5tZXRhZGF0YSB8fCB7fSkuaWRlYXMgfHwgW107XG4gIH1cblxuICBnZXQgaW50ZXJlc3RzKCkge1xuICAgIHJldHVybiB0aGlzLnBfaW50ZXJlc3RzO1xuICB9XG5cbiAgZ2V0IGxlYXJuaW5nU3R5bGUoKSB7XG4gICAgcmV0dXJuIHRoaXMucF9sZWFybmluZ1N0eWxlO1xuICB9XG5cbiAgZ2V0IHBlcnNvbmFsaXR5VHlwZSgpIHtcbiAgICByZXR1cm4gdGhpcy5wX3BlcnNvbmFsaXR5VHlwZTtcbiAgfVxuXG4gIGdldCBFUDEwVHlwZSgpIHtcbiAgICByZXR1cm4gdGhpcy5wX0VQMTBUeXBlO1xuICB9XG5cbiAgZ2V0IHdvcmtTdHlsZSgpIHtcbiAgICByZXR1cm4gdGhpcy5wX3dvcmtTdHlsZTtcbiAgfVxuXG4gIGdldCBlbnRyZXByZW5ldXJUeXBlKCkge1xuICAgIHJldHVybiB0aGlzLnBfZW50cmVwcmVuZXVyVHlwZTtcbiAgfVxuXG4gIGdldCBsYXVuY2hTa2lsbHMoKSB7XG4gICAgcmV0dXJuIHRoaXMucF9sYXVuY2hTa2lsbHM7XG4gIH1cblxuICBnZXQgc2tpbGxzKCkge1xuICAgIHJldHVybiB0aGlzLnBfc2tpbGxzO1xuICB9XG5cbiAgaXNFcXVhbChvdGhlclN0dWRlbnQpIHtcbiAgICByZXR1cm4gdGhpcy5wX2hhc2ggPT09IG90aGVyU3R1ZGVudC5wX2hhc2g7XG4gIH1cblxuICBsZWFybmluZ1N0eWxlT3ZlcmxhcChsZWFybmluZ1N0eWxlKSB7XG4gICAgaWYgKHRoaXMubGVhcm5pbmdTdHlsZSA9PT0gbGVhcm5pbmdTdHlsZSkge1xuICAgICAgcmV0dXJuIDE7XG4gICAgfVxuXG4gICAgcmV0dXJuIDA7XG4gIH1cblxuICB3b3JrU3R5bGVDb21wbGltZW50KHdvcmtTdHlsZSkge1xuICAgIGNvbnN0IGNvbXBhcmF0b3IgPSAoYSwgYikgPT4gYS5sb2NhbGVDb21wYXJlKGIpO1xuICAgIGxldCB3b3JrU3R5bGVQb2ludHMgPSAwO1xuXG4gICAgY29uc3Qgc3R5bGUgPSB0aGlzLndvcmtTdHlsZS5zcGxpdCgnLycpLm1hcChzID0+IHMudHJpbSgpKTsgLy8gW1wiQW1pY2FibGVcIiwgXCJBbmFseXRpY2FsXCJdXG4gICAgc3R5bGUuc29ydChjb21wYXJhdG9yKTtcbiAgICBjb25zdCBvdGhlclN0eWxlID0gd29ya1N0eWxlLnNwbGl0KCcvJykubWFwKHMgPT4gcy50cmltKCkpO1xuICAgIG90aGVyU3R5bGUuc29ydChjb21wYXJhdG9yKTtcblxuICAgIGNvbnN0IHNtYWxsZXIgPSBzdHlsZS5sZW5ndGggPCBvdGhlclN0eWxlLmxlbmd0aCA/IHN0eWxlIDogb3RoZXJTdHlsZTsgLy8gZ2V0IHNtYWxsZXJcbiAgICBjb25zdCBsYXJnZXIgPSBzdHlsZS5sZW5ndGggPCBvdGhlclN0eWxlLmxlbmd0aCA/IG90aGVyU3R5bGUgOiBzdHlsZTtcblxuICAgIC8vIHNldCBkaWZmZXJlbmNlXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBzbWFsbGVyLmxlbmd0aDsgaSArPSAxKSB7XG4gICAgICBpZiAoYnMobGFyZ2VyLCBzbWFsbGVyW2ldLCBjb21wYXJhdG9yKSA8IDApIHtcbiAgICAgICAgd29ya1N0eWxlUG9pbnRzICs9IDE7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIHdvcmtTdHlsZVBvaW50cyAvIHNtYWxsZXIubGVuZ3RoOyAvLyBwZXJmZWN0bHkgZGlzam9pbnQgaXMgMVxuICB9XG5cbiAgZXAxMENvbXBsaW1lbnQob3RoZXJTdHVkZW50KSB7XG4gICAgLy8gZXAgMTAgY29tcGxlbWVudGFyeVxuICAgIGlmICh0aGlzLkVQMTBUeXBlICE9PSBvdGhlclN0dWRlbnQuRVAxMFR5cGUpIHtcbiAgICAgIHJldHVybiB0aGlzLnBfdGVhbWluZ0NvbmZpZy5FUDEwVHlwZSB8fCBkZWZhdWx0VGVhbWluZ0NvbmZpZy5FUDEwVHlwZTtcbiAgICB9XG5cbiAgICByZXR1cm4gMDtcbiAgfVxuXG4gIHBlcnNvbmFsaXR5Q29tcGxlbWVudChvdGhlclBlcnNvbmFsaXR5VHlwZSkge1xuICAgIGxldCBwZXJzb25hbGl0eVNjb3JlID0gMDtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IDQ7IGkgKz0gMSkge1xuICAgICAgaWYgKHRoaXMucGVyc29uYWxpdHlUeXBlW2ldICE9PSBvdGhlclBlcnNvbmFsaXR5VHlwZVtpXSkge1xuICAgICAgICBwZXJzb25hbGl0eVNjb3JlICs9IDE7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIHBlcnNvbmFsaXR5U2NvcmUgLyA0O1xuICB9XG5cbiAgc2tpbGxzQ29tcGxlbWVudChvdGhlclNraWxscykge1xuICAgIGxldCBub3JtYWxTa2lsbHNQb2ludHMgPSAwLjA7XG4gICAgY29uc3Qgbm9ybWFsU2tpbGxzID0gdGhpcy5za2lsbHM7XG4gICAgY29uc3Qgb3RoZXJOb3JtYWxTa2lsbHMgPSBvdGhlclNraWxscztcbiAgICBjb25zdCBza2lsbHNLZXlzID0gT2JqZWN0LmtleXMobm9ybWFsU2tpbGxzIHx8IHt9KTtcblxuICAgIGZvciAobGV0IGkgPSAwLCByID0gc2tpbGxzS2V5cy5sZW5ndGg7IGkgPCByOyBpICs9IDEpIHtcbiAgICAgIGNvbnN0IHNraWxsS2V5ID0gc2tpbGxzS2V5c1tpXTtcbiAgICAgIGNvbnN0IHNraWxsID0gbm9ybWFsU2tpbGxzW3NraWxsS2V5XTtcbiAgICAgIGNvbnN0IG90aGVyU2tpbGwgPSBvdGhlck5vcm1hbFNraWxsc1tza2lsbEtleV07XG4gICAgICBjb25zdCBza2lsbERpZmZlcmVuY2UgPSBNYXRoLmFicyhza2lsbCAtIG90aGVyU2tpbGwpO1xuXG4gICAgICBpZiAoc2tpbGxEaWZmZXJlbmNlID4gMykge1xuICAgICAgICBub3JtYWxTa2lsbHNQb2ludHMgKz0gMTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gbm9ybWFsU2tpbGxzUG9pbnRzIC8gc2tpbGxzS2V5cy5sZW5ndGg7IC8vIHBlcmZlY3RseSBkaXNqb2ludCBpcyAxXG4gIH1cblxuICBsYXVuY2hTa2lsbHNDb21wbGVtZW50KG90aGVyTGF1bmNoU2tpbGxzID0gW10pIHtcbiAgICBsZXQgc2tpbGxzUG9pbnRzID0gMC4wO1xuICAgIGNvbnN0IHsgbGF1bmNoU2tpbGxzID0gW10gfSA9IHRoaXM7XG5cbiAgICBjb25zdCBjb21wYXJhdG9yID0gKGEsIGIpID0+IGEubG9jYWxlQ29tcGFyZShiKTtcbiAgICBsYXVuY2hTa2lsbHMuc29ydChjb21wYXJhdG9yKTtcbiAgICBvdGhlckxhdW5jaFNraWxscy5zb3J0KGNvbXBhcmF0b3IpO1xuXG4gICAgY29uc3Qgc21hbGxlciA9IGxhdW5jaFNraWxscy5sZW5ndGggPCBvdGhlckxhdW5jaFNraWxscy5sZW5ndGggPyBsYXVuY2hTa2lsbHMgOiBvdGhlckxhdW5jaFNraWxscztcbiAgICBjb25zdCBsYXJnZXIgPSBsYXVuY2hTa2lsbHMubGVuZ3RoIDwgb3RoZXJMYXVuY2hTa2lsbHMubGVuZ3RoID8gb3RoZXJMYXVuY2hTa2lsbHMgOiBsYXVuY2hTa2lsbHM7XG5cbiAgICBmb3IgKGxldCBpID0gMCwgciA9IHNtYWxsZXIubGVuZ3RoOyBpIDwgcjsgaSArPSAxKSB7XG4gICAgICBpZiAoYnMobGFyZ2VyLCBzbWFsbGVyW2ldLCBjb21wYXJhdG9yKSA8IDApIHtcbiAgICAgICAgc2tpbGxzUG9pbnRzICs9IDE7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIHNraWxsc1BvaW50cyAvIGxhcmdlci5sZW5ndGg7XG4gIH1cblxuICBpbnRlcmVzdE92ZXJsYXAob3RoZXJJbnRlcmVzdHMpIHtcbiAgICBsZXQgaW50ZXJlc3RzUG9pbnRzID0gMC4wO1xuICAgIGNvbnN0IGNvbXBhcmF0b3IgPSAoYSwgYikgPT4gYS5sb2NhbGVDb21wYXJlKGIpO1xuXG4gICAgY29uc3QgeyBpbnRlcmVzdHMgfSA9IHRoaXM7XG5cbiAgICBpZiAoaW50ZXJlc3RzLmxlbmd0aCA9PT0gMCB8fCBvdGhlckludGVyZXN0cy5sZW5ndGggPT09IDApIHtcbiAgICAgIHJldHVybiAwO1xuICAgIH1cblxuICAgIGludGVyZXN0cy5zb3J0KGNvbXBhcmF0b3IpO1xuXG4gICAgb3RoZXJJbnRlcmVzdHMuc29ydChjb21wYXJhdG9yKTtcblxuICAgIGNvbnN0IHNtYWxsZXIgPSBpbnRlcmVzdHMubGVuZ3RoIDwgb3RoZXJJbnRlcmVzdHMubGVuZ3RoID8gaW50ZXJlc3RzIDogb3RoZXJJbnRlcmVzdHM7XG4gICAgY29uc3QgbGFyZ2VyID0gaW50ZXJlc3RzLmxlbmd0aCA8IG90aGVySW50ZXJlc3RzLmxlbmd0aCA/IG90aGVySW50ZXJlc3RzIDogaW50ZXJlc3RzO1xuXG4gICAgZm9yIChsZXQgaSA9IDAsIHIgPSBzbWFsbGVyLmxlbmd0aDsgaSA8IHI7IGkgKz0gMSkge1xuICAgICAgaWYgKGJzKGxhcmdlciwgc21hbGxlcltpXSwgY29tcGFyYXRvcikgPiAtMSkge1xuICAgICAgICBpbnRlcmVzdHNQb2ludHMgKz0gMTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gaW50ZXJlc3RzUG9pbnRzIC8gbGFyZ2VyLmxlbmd0aDtcbiAgfVxuXG4gIGlkZWFPdmVybGFwKG90aGVySWRlYXMpIHtcbiAgICBsZXQgcHJvYmxlbUludGVyZXN0T3ZlcmxhcCA9IDAuMDtcblxuICAgIGNvbnN0IGNvbXBhcmF0b3IgPSAoYSwgYikgPT4gYS5pZCAtIGIuaWQ7XG4gICAgY29uc3QgeyBpZGVhcyB9ID0gdGhpcztcbiAgICBpZGVhcy5zb3J0KGNvbXBhcmF0b3IpO1xuXG4gICAgb3RoZXJJZGVhcy5zb3J0KGNvbXBhcmF0b3IpO1xuXG4gICAgLy8gc2hvdWxkIGJlIHNhbWUgc2l6ZSwgYnV0IGlmIG5vdCBkbyBzbWFsbGVyXG4gICAgY29uc3Qgc21hbGxlciA9IGlkZWFzLmxlbmd0aCA8IG90aGVySWRlYXMubGVuZ3RoID8gaWRlYXMgOiBvdGhlcklkZWFzO1xuICAgIGNvbnN0IGxhcmdlciA9IGlkZWFzLmxlbmd0aCA8IG90aGVySWRlYXMubGVuZ3RoID8gb3RoZXJJZGVhcyA6IGlkZWFzO1xuXG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCBzbWFsbGVyLmxlbmd0aDsgaSArPSAxKSB7XG4gICAgICBjb25zdCBteUlkZWEgPSBzbWFsbGVyW2ldO1xuICAgICAgY29uc3Qgb3RoZXJJZGVhUG9zID0gYnMobGFyZ2VyLCBteUlkZWEsIGNvbXBhcmF0b3IpO1xuXG4gICAgICBpZiAob3RoZXJJZGVhUG9zID4gLTEpIHtcbiAgICAgICAgY29uc3Qgb3RoZXJJZGVhID0gbGFyZ2VyW290aGVySWRlYVBvc107XG4gICAgICAgIGNvbnN0IHNlbnRpbWVudCA9IG15SWRlYS5zdHJlbmd0aDtcbiAgICAgICAgY29uc3Qgb3RoZXJTZW50aW1lbnQgPSBvdGhlcklkZWEuc3RyZW5ndGg7XG5cbiAgICAgICAgcHJvYmxlbUludGVyZXN0T3ZlcmxhcCArPSAzLjAgLSBNYXRoLmFicyhzZW50aW1lbnQgLSBvdGhlclNlbnRpbWVudCk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKHRoaXMuaWRlYXMubGVuZ3RoID4gMCkge1xuICAgICAgcmV0dXJuIHByb2JsZW1JbnRlcmVzdE92ZXJsYXAgLyAoMyAqIGxhcmdlci5sZW5ndGgpOyAvLyBtYXggb2YgbGFyZ2VyIGxlbmd0aFxuICAgIH1cblxuICAgIHJldHVybiAwO1xuICB9XG5cbiAgLy8gZ2VuZXJhdGUgYSBwZXJjZW50YWdlIG91dCBvZiAxIHRoYXQgd2Ugc2NhbGVcbiAgcm9sZUNvbXBsZW1lbnQocm9sZSkge1xuICAgIC8vIHJldHVybiAxIGlmIGNvbXBsZW1lbnQgYW5kIDAgaWYgbm90ID9cbiAgICBpZiAodGhpcy5yb2xlICE9PSByb2xlKSB7XG4gICAgICByZXR1cm4gMTtcbiAgICB9XG5cbiAgICByZXR1cm4gMDtcbiAgfVxuXG4gIGdlbmRlckNvbXBsZW1lbnQoZ2VuZGVyKSB7XG4gICAgLy8gcmV0dXJuIDEgaWYgY29tcGxlbWVudCBhbmQgMCBpZiBub3QgP1xuICAgIGlmICh0aGlzLmdlbmRlciAhPT0gZ2VuZGVyKSB7XG4gICAgICByZXR1cm4gMTtcbiAgICB9XG5cbiAgICByZXR1cm4gMDtcbiAgfVxuXG4gIGV0aG5pY2l0eUNvbXBsZW1lbnQoZXRobmljaXR5KSB7XG4gICAgLy8gcmV0dXJuIDEgaWYgY29tcGxlbWVudCBhbmQgMCBpZiBub3QgP1xuICAgIGlmICh0aGlzLmV0aG5pY2l0eSAhPT0gZXRobmljaXR5KSB7XG4gICAgICByZXR1cm4gMTtcbiAgICB9XG5cbiAgICByZXR1cm4gMDtcbiAgfVxuXG4gIGVudHJlcHJlbmV1clR5cGVDb21wbGVtZW50KG90aGVyRVRJbikge1xuICAgIGNvbnN0IGNvbXBhcmF0b3IgPSAoYSwgYikgPT4gYS5sb2NhbGVDb21wYXJlKGIpO1xuICAgIGxldCBldFBvaW50cyA9IDA7XG5cbiAgICBjb25zdCBFVCA9IHRoaXMuZW50cmVwcmVuZXVyVHlwZS5zcGxpdCgnLCAnKTtcbiAgICBFVC5zb3J0KGNvbXBhcmF0b3IpO1xuXG4gICAgY29uc3Qgb3RoZXJFVCA9IG90aGVyRVRJbi5zcGxpdCgnLCAnKTtcbiAgICBvdGhlckVULnNvcnQoY29tcGFyYXRvcik7XG5cbiAgICBjb25zdCBzbWFsbGVyID0gRVQubGVuZ3RoIDwgb3RoZXJFVC5sZW5ndGggPyBFVCA6IG90aGVyRVQ7XG4gICAgY29uc3QgbGFyZ2VyID0gRVQubGVuZ3RoIDwgb3RoZXJFVC5sZW5ndGggPyBvdGhlckVUIDogRVQ7XG5cbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHNtYWxsZXIubGVuZ3RoOyBpICs9IDEpIHtcbiAgICAgIGlmIChicyhsYXJnZXIsIHNtYWxsZXJbaV0sIGNvbXBhcmF0b3IpIDwgMCkge1xuICAgICAgICAvLyBpZiBldCB0eXBlIG5vdCBmb3VuZCBpbiBvdGhlciwgYWRkIGEgcG9pbnRcbiAgICAgICAgZXRQb2ludHMgKz0gMTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICAvLyBtYXggZXQgcG9pbnRzIGlzIHRoZSBzbWFsbGVyIHNpemVcblxuICAgIHJldHVybiBldFBvaW50cyAvIHNtYWxsZXIubGVuZ3RoOyAvLyBob3cgcGVyZmVjdGx5IGRpc2pvaW50XG4gIH1cblxuICBzY29yZUFnYWluc3Qob3RoZXJTdHVkZW50KSB7XG4gICAgbGV0IHBvaW50cyA9IDAuMDtcblxuICAgIGNvbnN0IHRlYW1pbmdDb25maWcgPSBPYmplY3QuYXNzaWduKHt9LCBkZWZhdWx0VGVhbWluZ0NvbmZpZywgdGhpcy5wX3RlYW1pbmdDb25maWcpO1xuXG4gICAgcG9pbnRzICs9IHRlYW1pbmdDb25maWcucm9sZVBvaW50cyAqIHRoaXMucm9sZUNvbXBsZW1lbnQob3RoZXJTdHVkZW50LnJvbGUpO1xuICAgIC8vIHBvaW50cyArPSB0ZWFtaW5nQ29uZmlnLmVudHJlcHJlbmV1clR5cGVQb2ludHMgKiB0aGlzLmVudHJlcHJlbmV1clR5cGVDb21wbGVtZW50KG90aGVyU3R1ZGVudC5lbnRyZXByZW5ldXJUeXBlKTtcbiAgICBwb2ludHMgKz0gdGVhbWluZ0NvbmZpZy5lbnRyZXByZW5ldXJUeXBlUG9pbnRzICogdGhpcy5sYXVuY2hTa2lsbHNDb21wbGVtZW50KG90aGVyU3R1ZGVudC5sYXVuY2hTa2lsbHMpO1xuICAgIHBvaW50cyArPSB0ZWFtaW5nQ29uZmlnLnNraWxsc1BvaW50cyAqIHRoaXMuc2tpbGxzQ29tcGxlbWVudChvdGhlclN0dWRlbnQuc2tpbGxzKTtcbiAgICBwb2ludHMgKz0gdGVhbWluZ0NvbmZpZy5sZWFybmluZ1N0eWxlUG9pbnRzICogdGhpcy5sZWFybmluZ1N0eWxlT3ZlcmxhcChvdGhlclN0dWRlbnQubGVhcm5pbmdTdHlsZSk7XG4gICAgcG9pbnRzICs9IHRlYW1pbmdDb25maWcud29ya1N0eWxlUG9pbnRzICogdGhpcy53b3JrU3R5bGVDb21wbGltZW50KG90aGVyU3R1ZGVudC53b3JrU3R5bGUpO1xuICAgIHBvaW50cyArPSB0ZWFtaW5nQ29uZmlnLnBlcnNvbmFsaXR5UG9pbnRzICogdGhpcy5wZXJzb25hbGl0eUNvbXBsZW1lbnQob3RoZXJTdHVkZW50LnBlcnNvbmFsaXR5VHlwZSk7XG4gICAgcG9pbnRzICs9IHRlYW1pbmdDb25maWcuZ2VuZGVyUG9pbnRzICogdGhpcy5nZW5kZXJDb21wbGVtZW50KG90aGVyU3R1ZGVudC5nZW5kZXIpO1xuICAgIHBvaW50cyArPSB0ZWFtaW5nQ29uZmlnLmV0aG5pY2l0eVBvaW50cyAqIHRoaXMuZ2VuZGVyQ29tcGxlbWVudChvdGhlclN0dWRlbnQuZXRobmljaXR5KTtcblxuICAgIC8vIGludGVyZXN0IG92ZXJsYXBcbiAgICBwb2ludHMgKz0gdGVhbWluZ0NvbmZpZy5pbnRlcmVzdFBvaW50cyAqIHRoaXMuaW50ZXJlc3RPdmVybGFwKG90aGVyU3R1ZGVudC5pbnRlcmVzdHMpO1xuXG4gICAgLy8gSWRlYSBvdmVybGFwXG4gICAgcG9pbnRzICs9IHRlYW1pbmdDb25maWcuaWRlYVBvaW50cyAqIHRoaXMuaWRlYU92ZXJsYXAob3RoZXJTdHVkZW50LmlkZWFzKTtcblxuXG4gICAgcmV0dXJuIHBvaW50cztcbiAgfVxufVxuIl0sInNvdXJjZVJvb3QiOiIvbW50L2MvVXNlcnMvcHVyZXUvRG9jdW1lbnRzL3Byb2plY3RzL2xhdW5jaHgtdGVhbWluZy9zcmMifQ==
