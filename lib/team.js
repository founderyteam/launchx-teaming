'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); /* eslint-disable no-param-reassign */
/* eslint-disable no-underscore-dangle */


var _student = require('./student');

var _student2 = _interopRequireDefault(_student);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Team = function () {
  function Team(students) {
    _classCallCheck(this, Team);

    this._students = students;
  }

  _createClass(Team, [{
    key: 'names',
    value: function names() {
      return this._students.map(function (student) {
        return student._json.name;
      });
    }
  }, {
    key: 'getTopIdeas',
    value: function getTopIdeas(studentIdeas) {
      // get all the ideas of all the students
      // figure out the ids that the students do not share.
      // yay a set difference

      // const masterSet = this.determineMasterSet();

      // // baked student ideas into [] of hashMaps for O(1)
      // let studentIdeas = this.students.map(student => student.ideas);
      // studentIdeas = studentIdeas.map(ideas => ideas.reduce((hashMap, idea) => {
      //   hashMap[idea.id] = idea.strength;
      //   return hashMap;
      // }, {}));

      // for each master set key, see if student doesnt have the key
      // let ownIdeas = {};
      // for (let i = 0; i < masterSet.length; i += 1) {
      //   const key = masterSet[i];
      //   for (let j = 0; j < studentIdeas.length; j += 1) { // should be team size
      //     if (!studentIdeas[j][key]) {
      //       ownIdeas[key] = true;
      //     }
      //   }
      // }

      var ownIdeas = this.students.map(function (student) {
        return studentIdeas.map(function (i, k) {
          return k;
        }).filter(function (ideaId) {
          return studentIdeas[ideaId].thinker.email === (student.p_json.email || student.p_json.name);
        }).map(function (k) {
          return studentIdeas[k].id;
        });
      });

      ownIdeas = Object.keys(ownIdeas).reduce(function (prev, next) {
        return prev.concat(ownIdeas[next]);
      }, []);

      // ownIdeas = Object.keys(ownIdeas);
      // rank by compatibility
      var ownIdeaTeamRanks = {};
      for (var i = 0; i < ownIdeas.length; i += 1) {
        var id = ownIdeas[i];
        var score = 4; // someone on the team made it
        for (var k = 0; k < this.students.length; k += 1) {
          for (var g = 0; g < this.students[k].ideas.length; g += 1) {
            if (this.students[k].ideas[g].id === id) {
              score += this.students[k].ideas[g].strength;
            }
          }
        }

        score /= this.students.length;
        ownIdeaTeamRanks[id] = score;
      }

      // sort by rank
      var ownIdeasArr = ownIdeas;
      var shuffle = function shuffle(array) {
        var currentIndex = array.length;
        var temporaryValue = void 0;
        var randomIndex = void 0;

        // While there remain elements to shuffle...
        while (currentIndex !== 0) {
          // Pick a remaining element...
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;

          // And swap it with the current element.
          temporaryValue = array[currentIndex];
          array[currentIndex] = array[randomIndex];
          array[randomIndex] = temporaryValue;
        }

        return array;
      };

      ownIdeasArr = shuffle(ownIdeasArr);
      ownIdeasArr.sort(function (a, b) {
        return ownIdeaTeamRanks[b] - ownIdeaTeamRanks[a];
      });

      this.best5 = ownIdeasArr.slice(0, 5);
      return this.best5;
    }
  }, {
    key: 'getStudentOwnIdeas',
    value: function getStudentOwnIdeas(studentIdeas) {
      if (!this.p_studentOwnIdeas) {
        // const masterSet = this.determineMasterSet();

        // // baked student ideas into [] of hashMaps for O(1)
        // let studentIdeas = this.students.map(student => student.ideas);
        // studentIdeas = studentIdeas.map(ideas => ideas.reduce((hashMap, idea) => {
        //   hashMap[idea.id] = idea.strength;
        //   return hashMap;
        // }, {}));

        // // for each master set key, see if student doesnt have the key
        // let ownIdeas = this.students.map(() => ({}));
        // for (let i = 0; i < masterSet.length; i += 1) {
        //   const key = masterSet[i];
        //   for (let j = 0; j < studentIdeas.length; j += 1) { // should be team size
        //     if (!studentIdeas[j][key]) {
        //       ownIdeas[j][key] = true;
        //     }
        //   }
        // }

        var ownIdeas = this.students.map(function (student) {
          return studentIdeas.map(function (i, k) {
            return Number(k);
          }).filter(function (ideaId) {
            return studentIdeas[ideaId].thinker.email === (student.p_json.email || student.p_json.name);
          }).map(function (k) {
            return studentIdeas[k].id;
          });
        });

        this.p_studentOwnIdeas = ownIdeas;
      }

      return this.p_studentOwnIdeas;
    }
  }, {
    key: 'determineMasterSet',
    value: function determineMasterSet() {
      var masterSet = {};
      for (var i = 0; i < this.students.length; i += 1) {
        for (var j = 0; j < this.students[i].ideas.length; j += 1) {
          var id = this.students[i].ideas[j].id;

          masterSet[id] = true;
        }
      }

      return Object.keys(masterSet);
    }
  }, {
    key: 'score',
    value: function score() {
      // pairwise team all members against each other
      if (this.students.length <= 1) {
        return 0;
      }

      var score = 0;
      var numPerms = 0;
      for (var i = 0; i < this.students.length; i += 1) {
        for (var j = i + 1; j < this.students.length; j += 1) {
          score += this.students[i].scoreAgainst(this.students[j]);
          numPerms += 1;
        }
      }

      return score / numPerms;
    }
  }, {
    key: 'students',
    get: function get() {
      return this._students;
    }
  }, {
    key: 'idea',
    set: function set(newValue) {
      this._idea = newValue;
    },
    get: function get() {
      return this._idea;
    }
  }, {
    key: 'bestFiveIdeas',
    get: function get() {
      if (!this.best5) {
        this.getTopIdeas(this.p_ideas);
      }

      return this.best5;
    }
  }, {
    key: 'studentOwnIdeas',
    get: function get() {
      return this.getStudentOwnIdeas(this.p_ideas);
    }
  }], [{
    key: 'score',
    value: function score(students) {
      return new Team(students).score();
    }
  }]);

  return Team;
}();

exports.default = Team;
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInRlYW0uanMiXSwibmFtZXMiOlsiVGVhbSIsInN0dWRlbnRzIiwiX3N0dWRlbnRzIiwibWFwIiwic3R1ZGVudCIsIl9qc29uIiwibmFtZSIsInN0dWRlbnRJZGVhcyIsIm93bklkZWFzIiwiaSIsImsiLCJmaWx0ZXIiLCJpZGVhSWQiLCJ0aGlua2VyIiwiZW1haWwiLCJwX2pzb24iLCJpZCIsIk9iamVjdCIsImtleXMiLCJyZWR1Y2UiLCJwcmV2IiwibmV4dCIsImNvbmNhdCIsIm93bklkZWFUZWFtUmFua3MiLCJsZW5ndGgiLCJzY29yZSIsImciLCJpZGVhcyIsInN0cmVuZ3RoIiwib3duSWRlYXNBcnIiLCJzaHVmZmxlIiwiYXJyYXkiLCJjdXJyZW50SW5kZXgiLCJ0ZW1wb3JhcnlWYWx1ZSIsInJhbmRvbUluZGV4IiwiTWF0aCIsImZsb29yIiwicmFuZG9tIiwic29ydCIsImEiLCJiIiwiYmVzdDUiLCJzbGljZSIsInBfc3R1ZGVudE93bklkZWFzIiwiTnVtYmVyIiwibWFzdGVyU2V0IiwiaiIsIm51bVBlcm1zIiwic2NvcmVBZ2FpbnN0IiwibmV3VmFsdWUiLCJfaWRlYSIsImdldFRvcElkZWFzIiwicF9pZGVhcyIsImdldFN0dWRlbnRPd25JZGVhcyJdLCJtYXBwaW5ncyI6Ijs7Ozs7O3FqQkFBQTtBQUNBOzs7QUFDQTs7Ozs7Ozs7SUFFcUJBLEk7QUFDbkIsZ0JBQVlDLFFBQVosRUFBc0I7QUFBQTs7QUFDcEIsU0FBS0MsU0FBTCxHQUFpQkQsUUFBakI7QUFDRDs7Ozs0QkFNTztBQUNOLGFBQU8sS0FBS0MsU0FBTCxDQUFlQyxHQUFmLENBQW1CO0FBQUEsZUFBV0MsUUFBUUMsS0FBUixDQUFjQyxJQUF6QjtBQUFBLE9BQW5CLENBQVA7QUFDRDs7O2dDQVVXQyxZLEVBQWM7QUFDeEI7QUFDQTtBQUNBOztBQUVBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxVQUFJQyxXQUFXLEtBQUtQLFFBQUwsQ0FBY0UsR0FBZCxDQUFrQjtBQUFBLGVBQVdJLGFBQ3pDSixHQUR5QyxDQUNyQyxVQUFDTSxDQUFELEVBQUlDLENBQUo7QUFBQSxpQkFBVUEsQ0FBVjtBQUFBLFNBRHFDLEVBRXpDQyxNQUZ5QyxDQUd4QztBQUFBLGlCQUFVSixhQUFhSyxNQUFiLEVBQXFCQyxPQUFyQixDQUE2QkMsS0FBN0IsTUFBd0NWLFFBQVFXLE1BQVIsQ0FBZUQsS0FBZixJQUF3QlYsUUFBUVcsTUFBUixDQUFlVCxJQUEvRSxDQUFWO0FBQUEsU0FId0MsRUFLekNILEdBTHlDLENBS3JDO0FBQUEsaUJBQUtJLGFBQWFHLENBQWIsRUFBZ0JNLEVBQXJCO0FBQUEsU0FMcUMsQ0FBWDtBQUFBLE9BQWxCLENBQWY7O0FBT0FSLGlCQUFXUyxPQUFPQyxJQUFQLENBQVlWLFFBQVosRUFBc0JXLE1BQXRCLENBQTZCLFVBQUNDLElBQUQsRUFBT0MsSUFBUDtBQUFBLGVBQWdCRCxLQUFLRSxNQUFMLENBQVlkLFNBQVNhLElBQVQsQ0FBWixDQUFoQjtBQUFBLE9BQTdCLEVBQTBFLEVBQTFFLENBQVg7O0FBRUE7QUFDQTtBQUNBLFVBQU1FLG1CQUFtQixFQUF6QjtBQUNBLFdBQUssSUFBSWQsSUFBSSxDQUFiLEVBQWdCQSxJQUFJRCxTQUFTZ0IsTUFBN0IsRUFBcUNmLEtBQUssQ0FBMUMsRUFBNkM7QUFDM0MsWUFBTU8sS0FBS1IsU0FBU0MsQ0FBVCxDQUFYO0FBQ0EsWUFBSWdCLFFBQVEsQ0FBWixDQUYyQyxDQUU1QjtBQUNmLGFBQUssSUFBSWYsSUFBSSxDQUFiLEVBQWdCQSxJQUFJLEtBQUtULFFBQUwsQ0FBY3VCLE1BQWxDLEVBQTBDZCxLQUFLLENBQS9DLEVBQWtEO0FBQ2hELGVBQUssSUFBSWdCLElBQUksQ0FBYixFQUFnQkEsSUFBSSxLQUFLekIsUUFBTCxDQUFjUyxDQUFkLEVBQWlCaUIsS0FBakIsQ0FBdUJILE1BQTNDLEVBQW1ERSxLQUFLLENBQXhELEVBQTJEO0FBQ3pELGdCQUFJLEtBQUt6QixRQUFMLENBQWNTLENBQWQsRUFBaUJpQixLQUFqQixDQUF1QkQsQ0FBdkIsRUFBMEJWLEVBQTFCLEtBQWlDQSxFQUFyQyxFQUF5QztBQUN2Q1MsdUJBQVMsS0FBS3hCLFFBQUwsQ0FBY1MsQ0FBZCxFQUFpQmlCLEtBQWpCLENBQXVCRCxDQUF2QixFQUEwQkUsUUFBbkM7QUFDRDtBQUNGO0FBQ0Y7O0FBRURILGlCQUFTLEtBQUt4QixRQUFMLENBQWN1QixNQUF2QjtBQUNBRCx5QkFBaUJQLEVBQWpCLElBQXVCUyxLQUF2QjtBQUNEOztBQUVEO0FBQ0EsVUFBSUksY0FBY3JCLFFBQWxCO0FBQ0EsVUFBTXNCLFVBQVUsU0FBVkEsT0FBVSxDQUFDQyxLQUFELEVBQVc7QUFDekIsWUFBSUMsZUFBZUQsTUFBTVAsTUFBekI7QUFDQSxZQUFJUyx1QkFBSjtBQUNBLFlBQUlDLG9CQUFKOztBQUVBO0FBQ0EsZUFBT0YsaUJBQWlCLENBQXhCLEVBQTJCO0FBQ3pCO0FBQ0FFLHdCQUFjQyxLQUFLQyxLQUFMLENBQVdELEtBQUtFLE1BQUwsS0FBZ0JMLFlBQTNCLENBQWQ7QUFDQUEsMEJBQWdCLENBQWhCOztBQUVBO0FBQ0FDLDJCQUFpQkYsTUFBTUMsWUFBTixDQUFqQjtBQUNBRCxnQkFBTUMsWUFBTixJQUFzQkQsTUFBTUcsV0FBTixDQUF0QjtBQUNBSCxnQkFBTUcsV0FBTixJQUFxQkQsY0FBckI7QUFDRDs7QUFFRCxlQUFPRixLQUFQO0FBQ0QsT0FsQkQ7O0FBb0JBRixvQkFBY0MsUUFBUUQsV0FBUixDQUFkO0FBQ0FBLGtCQUFZUyxJQUFaLENBQWlCLFVBQUNDLENBQUQsRUFBSUMsQ0FBSjtBQUFBLGVBQVVqQixpQkFBaUJpQixDQUFqQixJQUFzQmpCLGlCQUFpQmdCLENBQWpCLENBQWhDO0FBQUEsT0FBakI7O0FBRUEsV0FBS0UsS0FBTCxHQUFhWixZQUFZYSxLQUFaLENBQWtCLENBQWxCLEVBQXFCLENBQXJCLENBQWI7QUFDQSxhQUFPLEtBQUtELEtBQVo7QUFDRDs7O3VDQVVrQmxDLFksRUFBYztBQUMvQixVQUFJLENBQUMsS0FBS29DLGlCQUFWLEVBQTZCO0FBQzNCOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQSxZQUFNbkMsV0FBVyxLQUFLUCxRQUFMLENBQWNFLEdBQWQsQ0FBa0I7QUFBQSxpQkFBV0ksYUFDM0NKLEdBRDJDLENBQ3ZDLFVBQUNNLENBQUQsRUFBSUMsQ0FBSjtBQUFBLG1CQUFVa0MsT0FBT2xDLENBQVAsQ0FBVjtBQUFBLFdBRHVDLEVBRTNDQyxNQUYyQyxDQUcxQztBQUFBLG1CQUFVSixhQUFhSyxNQUFiLEVBQXFCQyxPQUFyQixDQUE2QkMsS0FBN0IsTUFBd0NWLFFBQVFXLE1BQVIsQ0FBZUQsS0FBZixJQUF3QlYsUUFBUVcsTUFBUixDQUFlVCxJQUEvRSxDQUFWO0FBQUEsV0FIMEMsRUFLM0NILEdBTDJDLENBS3ZDO0FBQUEsbUJBQUtJLGFBQWFHLENBQWIsRUFBZ0JNLEVBQXJCO0FBQUEsV0FMdUMsQ0FBWDtBQUFBLFNBQWxCLENBQWpCOztBQU9BLGFBQUsyQixpQkFBTCxHQUF5Qm5DLFFBQXpCO0FBQ0Q7O0FBRUQsYUFBTyxLQUFLbUMsaUJBQVo7QUFDRDs7O3lDQU1vQjtBQUNuQixVQUFNRSxZQUFZLEVBQWxCO0FBQ0EsV0FBSyxJQUFJcEMsSUFBSSxDQUFiLEVBQWdCQSxJQUFJLEtBQUtSLFFBQUwsQ0FBY3VCLE1BQWxDLEVBQTBDZixLQUFLLENBQS9DLEVBQWtEO0FBQ2hELGFBQUssSUFBSXFDLElBQUksQ0FBYixFQUFnQkEsSUFBSSxLQUFLN0MsUUFBTCxDQUFjUSxDQUFkLEVBQWlCa0IsS0FBakIsQ0FBdUJILE1BQTNDLEVBQW1Ec0IsS0FBSyxDQUF4RCxFQUEyRDtBQUFBLGNBQ2pEOUIsRUFEaUQsR0FDMUMsS0FBS2YsUUFBTCxDQUFjUSxDQUFkLEVBQWlCa0IsS0FBakIsQ0FBdUJtQixDQUF2QixDQUQwQyxDQUNqRDlCLEVBRGlEOztBQUV6RDZCLG9CQUFVN0IsRUFBVixJQUFnQixJQUFoQjtBQUNEO0FBQ0Y7O0FBRUQsYUFBT0MsT0FBT0MsSUFBUCxDQUFZMkIsU0FBWixDQUFQO0FBQ0Q7Ozs0QkFFTztBQUNOO0FBQ0EsVUFBSSxLQUFLNUMsUUFBTCxDQUFjdUIsTUFBZCxJQUF3QixDQUE1QixFQUErQjtBQUM3QixlQUFPLENBQVA7QUFDRDs7QUFFRCxVQUFJQyxRQUFRLENBQVo7QUFDQSxVQUFJc0IsV0FBVyxDQUFmO0FBQ0EsV0FBSyxJQUFJdEMsSUFBSSxDQUFiLEVBQWdCQSxJQUFJLEtBQUtSLFFBQUwsQ0FBY3VCLE1BQWxDLEVBQTBDZixLQUFLLENBQS9DLEVBQWtEO0FBQ2hELGFBQUssSUFBSXFDLElBQUlyQyxJQUFJLENBQWpCLEVBQW9CcUMsSUFBSSxLQUFLN0MsUUFBTCxDQUFjdUIsTUFBdEMsRUFBOENzQixLQUFLLENBQW5ELEVBQXNEO0FBQ3BEckIsbUJBQVMsS0FBS3hCLFFBQUwsQ0FBY1EsQ0FBZCxFQUFpQnVDLFlBQWpCLENBQThCLEtBQUsvQyxRQUFMLENBQWM2QyxDQUFkLENBQTlCLENBQVQ7QUFDQUMsc0JBQVksQ0FBWjtBQUNEO0FBQ0Y7O0FBRUQsYUFBT3RCLFFBQVFzQixRQUFmO0FBQ0Q7Ozt3QkE1S2M7QUFDYixhQUFPLEtBQUs3QyxTQUFaO0FBQ0Q7OztzQkFNUStDLFEsRUFBVTtBQUNqQixXQUFLQyxLQUFMLEdBQWFELFFBQWI7QUFDRCxLO3dCQUVVO0FBQ1QsYUFBTyxLQUFLQyxLQUFaO0FBQ0Q7Ozt3QkFtRm1CO0FBQ2xCLFVBQUksQ0FBQyxLQUFLVCxLQUFWLEVBQWlCO0FBQ2YsYUFBS1UsV0FBTCxDQUFpQixLQUFLQyxPQUF0QjtBQUNEOztBQUVELGFBQU8sS0FBS1gsS0FBWjtBQUNEOzs7d0JBcUNxQjtBQUNwQixhQUFPLEtBQUtZLGtCQUFMLENBQXdCLEtBQUtELE9BQTdCLENBQVA7QUFDRDs7OzBCQWdDWW5ELFEsRUFBVTtBQUNyQixhQUFPLElBQUlELElBQUosQ0FBU0MsUUFBVCxFQUFtQndCLEtBQW5CLEVBQVA7QUFDRDs7Ozs7O2tCQXJMa0J6QixJIiwiZmlsZSI6InRlYW0uanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiBlc2xpbnQtZGlzYWJsZSBuby1wYXJhbS1yZWFzc2lnbiAqL1xuLyogZXNsaW50LWRpc2FibGUgbm8tdW5kZXJzY29yZS1kYW5nbGUgKi9cbmltcG9ydCB7IFN0dWRlbnRQYWlyU2V0LCBkZWZhdWx0IGFzIFN0dWRlbnQsIFN0dWRlbnRTZXQgfSBmcm9tICcuL3N0dWRlbnQnO1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBUZWFtIHtcbiAgY29uc3RydWN0b3Ioc3R1ZGVudHMpIHtcbiAgICB0aGlzLl9zdHVkZW50cyA9IHN0dWRlbnRzO1xuICB9XG5cbiAgZ2V0IHN0dWRlbnRzKCkge1xuICAgIHJldHVybiB0aGlzLl9zdHVkZW50cztcbiAgfVxuXG4gIG5hbWVzKCkge1xuICAgIHJldHVybiB0aGlzLl9zdHVkZW50cy5tYXAoc3R1ZGVudCA9PiBzdHVkZW50Ll9qc29uLm5hbWUpO1xuICB9XG5cbiAgc2V0IGlkZWEobmV3VmFsdWUpIHtcbiAgICB0aGlzLl9pZGVhID0gbmV3VmFsdWU7XG4gIH1cblxuICBnZXQgaWRlYSgpIHtcbiAgICByZXR1cm4gdGhpcy5faWRlYTtcbiAgfVxuXG4gIGdldFRvcElkZWFzKHN0dWRlbnRJZGVhcykge1xuICAgIC8vIGdldCBhbGwgdGhlIGlkZWFzIG9mIGFsbCB0aGUgc3R1ZGVudHNcbiAgICAvLyBmaWd1cmUgb3V0IHRoZSBpZHMgdGhhdCB0aGUgc3R1ZGVudHMgZG8gbm90IHNoYXJlLlxuICAgIC8vIHlheSBhIHNldCBkaWZmZXJlbmNlXG5cbiAgICAvLyBjb25zdCBtYXN0ZXJTZXQgPSB0aGlzLmRldGVybWluZU1hc3RlclNldCgpO1xuXG4gICAgLy8gLy8gYmFrZWQgc3R1ZGVudCBpZGVhcyBpbnRvIFtdIG9mIGhhc2hNYXBzIGZvciBPKDEpXG4gICAgLy8gbGV0IHN0dWRlbnRJZGVhcyA9IHRoaXMuc3R1ZGVudHMubWFwKHN0dWRlbnQgPT4gc3R1ZGVudC5pZGVhcyk7XG4gICAgLy8gc3R1ZGVudElkZWFzID0gc3R1ZGVudElkZWFzLm1hcChpZGVhcyA9PiBpZGVhcy5yZWR1Y2UoKGhhc2hNYXAsIGlkZWEpID0+IHtcbiAgICAvLyAgIGhhc2hNYXBbaWRlYS5pZF0gPSBpZGVhLnN0cmVuZ3RoO1xuICAgIC8vICAgcmV0dXJuIGhhc2hNYXA7XG4gICAgLy8gfSwge30pKTtcblxuICAgIC8vIGZvciBlYWNoIG1hc3RlciBzZXQga2V5LCBzZWUgaWYgc3R1ZGVudCBkb2VzbnQgaGF2ZSB0aGUga2V5XG4gICAgLy8gbGV0IG93bklkZWFzID0ge307XG4gICAgLy8gZm9yIChsZXQgaSA9IDA7IGkgPCBtYXN0ZXJTZXQubGVuZ3RoOyBpICs9IDEpIHtcbiAgICAvLyAgIGNvbnN0IGtleSA9IG1hc3RlclNldFtpXTtcbiAgICAvLyAgIGZvciAobGV0IGogPSAwOyBqIDwgc3R1ZGVudElkZWFzLmxlbmd0aDsgaiArPSAxKSB7IC8vIHNob3VsZCBiZSB0ZWFtIHNpemVcbiAgICAvLyAgICAgaWYgKCFzdHVkZW50SWRlYXNbal1ba2V5XSkge1xuICAgIC8vICAgICAgIG93bklkZWFzW2tleV0gPSB0cnVlO1xuICAgIC8vICAgICB9XG4gICAgLy8gICB9XG4gICAgLy8gfVxuXG4gICAgbGV0IG93bklkZWFzID0gdGhpcy5zdHVkZW50cy5tYXAoc3R1ZGVudCA9PiBzdHVkZW50SWRlYXNcbiAgICAgIC5tYXAoKGksIGspID0+IGspXG4gICAgICAuZmlsdGVyKFxuICAgICAgICBpZGVhSWQgPT4gc3R1ZGVudElkZWFzW2lkZWFJZF0udGhpbmtlci5lbWFpbCA9PT0gKHN0dWRlbnQucF9qc29uLmVtYWlsIHx8IHN0dWRlbnQucF9qc29uLm5hbWUpLFxuICAgICAgKVxuICAgICAgLm1hcChrID0+IHN0dWRlbnRJZGVhc1trXS5pZCkpO1xuXG4gICAgb3duSWRlYXMgPSBPYmplY3Qua2V5cyhvd25JZGVhcykucmVkdWNlKChwcmV2LCBuZXh0KSA9PiBwcmV2LmNvbmNhdChvd25JZGVhc1tuZXh0XSksIFtdKTtcblxuICAgIC8vIG93bklkZWFzID0gT2JqZWN0LmtleXMob3duSWRlYXMpO1xuICAgIC8vIHJhbmsgYnkgY29tcGF0aWJpbGl0eVxuICAgIGNvbnN0IG93bklkZWFUZWFtUmFua3MgPSB7fTtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IG93bklkZWFzLmxlbmd0aDsgaSArPSAxKSB7XG4gICAgICBjb25zdCBpZCA9IG93bklkZWFzW2ldO1xuICAgICAgbGV0IHNjb3JlID0gNDsgLy8gc29tZW9uZSBvbiB0aGUgdGVhbSBtYWRlIGl0XG4gICAgICBmb3IgKGxldCBrID0gMDsgayA8IHRoaXMuc3R1ZGVudHMubGVuZ3RoOyBrICs9IDEpIHtcbiAgICAgICAgZm9yIChsZXQgZyA9IDA7IGcgPCB0aGlzLnN0dWRlbnRzW2tdLmlkZWFzLmxlbmd0aDsgZyArPSAxKSB7XG4gICAgICAgICAgaWYgKHRoaXMuc3R1ZGVudHNba10uaWRlYXNbZ10uaWQgPT09IGlkKSB7XG4gICAgICAgICAgICBzY29yZSArPSB0aGlzLnN0dWRlbnRzW2tdLmlkZWFzW2ddLnN0cmVuZ3RoO1xuICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICBzY29yZSAvPSB0aGlzLnN0dWRlbnRzLmxlbmd0aDtcbiAgICAgIG93bklkZWFUZWFtUmFua3NbaWRdID0gc2NvcmU7XG4gICAgfVxuXG4gICAgLy8gc29ydCBieSByYW5rXG4gICAgbGV0IG93bklkZWFzQXJyID0gb3duSWRlYXM7XG4gICAgY29uc3Qgc2h1ZmZsZSA9IChhcnJheSkgPT4ge1xuICAgICAgbGV0IGN1cnJlbnRJbmRleCA9IGFycmF5Lmxlbmd0aDtcbiAgICAgIGxldCB0ZW1wb3JhcnlWYWx1ZTtcbiAgICAgIGxldCByYW5kb21JbmRleDtcblxuICAgICAgLy8gV2hpbGUgdGhlcmUgcmVtYWluIGVsZW1lbnRzIHRvIHNodWZmbGUuLi5cbiAgICAgIHdoaWxlIChjdXJyZW50SW5kZXggIT09IDApIHtcbiAgICAgICAgLy8gUGljayBhIHJlbWFpbmluZyBlbGVtZW50Li4uXG4gICAgICAgIHJhbmRvbUluZGV4ID0gTWF0aC5mbG9vcihNYXRoLnJhbmRvbSgpICogY3VycmVudEluZGV4KTtcbiAgICAgICAgY3VycmVudEluZGV4IC09IDE7XG5cbiAgICAgICAgLy8gQW5kIHN3YXAgaXQgd2l0aCB0aGUgY3VycmVudCBlbGVtZW50LlxuICAgICAgICB0ZW1wb3JhcnlWYWx1ZSA9IGFycmF5W2N1cnJlbnRJbmRleF07XG4gICAgICAgIGFycmF5W2N1cnJlbnRJbmRleF0gPSBhcnJheVtyYW5kb21JbmRleF07XG4gICAgICAgIGFycmF5W3JhbmRvbUluZGV4XSA9IHRlbXBvcmFyeVZhbHVlO1xuICAgICAgfVxuXG4gICAgICByZXR1cm4gYXJyYXk7XG4gICAgfTtcblxuICAgIG93bklkZWFzQXJyID0gc2h1ZmZsZShvd25JZGVhc0Fycik7XG4gICAgb3duSWRlYXNBcnIuc29ydCgoYSwgYikgPT4gb3duSWRlYVRlYW1SYW5rc1tiXSAtIG93bklkZWFUZWFtUmFua3NbYV0pO1xuXG4gICAgdGhpcy5iZXN0NSA9IG93bklkZWFzQXJyLnNsaWNlKDAsIDUpO1xuICAgIHJldHVybiB0aGlzLmJlc3Q1O1xuICB9XG5cbiAgZ2V0IGJlc3RGaXZlSWRlYXMoKSB7XG4gICAgaWYgKCF0aGlzLmJlc3Q1KSB7XG4gICAgICB0aGlzLmdldFRvcElkZWFzKHRoaXMucF9pZGVhcyk7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRoaXMuYmVzdDU7XG4gIH1cblxuICBnZXRTdHVkZW50T3duSWRlYXMoc3R1ZGVudElkZWFzKSB7XG4gICAgaWYgKCF0aGlzLnBfc3R1ZGVudE93bklkZWFzKSB7XG4gICAgICAvLyBjb25zdCBtYXN0ZXJTZXQgPSB0aGlzLmRldGVybWluZU1hc3RlclNldCgpO1xuXG4gICAgICAvLyAvLyBiYWtlZCBzdHVkZW50IGlkZWFzIGludG8gW10gb2YgaGFzaE1hcHMgZm9yIE8oMSlcbiAgICAgIC8vIGxldCBzdHVkZW50SWRlYXMgPSB0aGlzLnN0dWRlbnRzLm1hcChzdHVkZW50ID0+IHN0dWRlbnQuaWRlYXMpO1xuICAgICAgLy8gc3R1ZGVudElkZWFzID0gc3R1ZGVudElkZWFzLm1hcChpZGVhcyA9PiBpZGVhcy5yZWR1Y2UoKGhhc2hNYXAsIGlkZWEpID0+IHtcbiAgICAgIC8vICAgaGFzaE1hcFtpZGVhLmlkXSA9IGlkZWEuc3RyZW5ndGg7XG4gICAgICAvLyAgIHJldHVybiBoYXNoTWFwO1xuICAgICAgLy8gfSwge30pKTtcblxuICAgICAgLy8gLy8gZm9yIGVhY2ggbWFzdGVyIHNldCBrZXksIHNlZSBpZiBzdHVkZW50IGRvZXNudCBoYXZlIHRoZSBrZXlcbiAgICAgIC8vIGxldCBvd25JZGVhcyA9IHRoaXMuc3R1ZGVudHMubWFwKCgpID0+ICh7fSkpO1xuICAgICAgLy8gZm9yIChsZXQgaSA9IDA7IGkgPCBtYXN0ZXJTZXQubGVuZ3RoOyBpICs9IDEpIHtcbiAgICAgIC8vICAgY29uc3Qga2V5ID0gbWFzdGVyU2V0W2ldO1xuICAgICAgLy8gICBmb3IgKGxldCBqID0gMDsgaiA8IHN0dWRlbnRJZGVhcy5sZW5ndGg7IGogKz0gMSkgeyAvLyBzaG91bGQgYmUgdGVhbSBzaXplXG4gICAgICAvLyAgICAgaWYgKCFzdHVkZW50SWRlYXNbal1ba2V5XSkge1xuICAgICAgLy8gICAgICAgb3duSWRlYXNbal1ba2V5XSA9IHRydWU7XG4gICAgICAvLyAgICAgfVxuICAgICAgLy8gICB9XG4gICAgICAvLyB9XG5cbiAgICAgIGNvbnN0IG93bklkZWFzID0gdGhpcy5zdHVkZW50cy5tYXAoc3R1ZGVudCA9PiBzdHVkZW50SWRlYXNcbiAgICAgICAgLm1hcCgoaSwgaykgPT4gTnVtYmVyKGspKVxuICAgICAgICAuZmlsdGVyKFxuICAgICAgICAgIGlkZWFJZCA9PiBzdHVkZW50SWRlYXNbaWRlYUlkXS50aGlua2VyLmVtYWlsID09PSAoc3R1ZGVudC5wX2pzb24uZW1haWwgfHwgc3R1ZGVudC5wX2pzb24ubmFtZSksXG4gICAgICAgIClcbiAgICAgICAgLm1hcChrID0+IHN0dWRlbnRJZGVhc1trXS5pZCkpO1xuXG4gICAgICB0aGlzLnBfc3R1ZGVudE93bklkZWFzID0gb3duSWRlYXM7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRoaXMucF9zdHVkZW50T3duSWRlYXM7XG4gIH1cblxuICBnZXQgc3R1ZGVudE93bklkZWFzKCkge1xuICAgIHJldHVybiB0aGlzLmdldFN0dWRlbnRPd25JZGVhcyh0aGlzLnBfaWRlYXMpO1xuICB9XG5cbiAgZGV0ZXJtaW5lTWFzdGVyU2V0KCkge1xuICAgIGNvbnN0IG1hc3RlclNldCA9IHt9O1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5zdHVkZW50cy5sZW5ndGg7IGkgKz0gMSkge1xuICAgICAgZm9yIChsZXQgaiA9IDA7IGogPCB0aGlzLnN0dWRlbnRzW2ldLmlkZWFzLmxlbmd0aDsgaiArPSAxKSB7XG4gICAgICAgIGNvbnN0IHsgaWQgfSA9IHRoaXMuc3R1ZGVudHNbaV0uaWRlYXNbal07XG4gICAgICAgIG1hc3RlclNldFtpZF0gPSB0cnVlO1xuICAgICAgfVxuICAgIH1cblxuICAgIHJldHVybiBPYmplY3Qua2V5cyhtYXN0ZXJTZXQpO1xuICB9XG5cbiAgc2NvcmUoKSB7XG4gICAgLy8gcGFpcndpc2UgdGVhbSBhbGwgbWVtYmVycyBhZ2FpbnN0IGVhY2ggb3RoZXJcbiAgICBpZiAodGhpcy5zdHVkZW50cy5sZW5ndGggPD0gMSkge1xuICAgICAgcmV0dXJuIDA7XG4gICAgfVxuXG4gICAgbGV0IHNjb3JlID0gMDtcbiAgICBsZXQgbnVtUGVybXMgPSAwO1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5zdHVkZW50cy5sZW5ndGg7IGkgKz0gMSkge1xuICAgICAgZm9yIChsZXQgaiA9IGkgKyAxOyBqIDwgdGhpcy5zdHVkZW50cy5sZW5ndGg7IGogKz0gMSkge1xuICAgICAgICBzY29yZSArPSB0aGlzLnN0dWRlbnRzW2ldLnNjb3JlQWdhaW5zdCh0aGlzLnN0dWRlbnRzW2pdKTtcbiAgICAgICAgbnVtUGVybXMgKz0gMTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gc2NvcmUgLyBudW1QZXJtcztcbiAgfVxuXG4gIHN0YXRpYyBzY29yZShzdHVkZW50cykge1xuICAgIHJldHVybiBuZXcgVGVhbShzdHVkZW50cykuc2NvcmUoKTtcbiAgfVxufVxuIl0sInNvdXJjZVJvb3QiOiIvbW50L2MvVXNlcnMvcHVyZXUvRG9jdW1lbnRzL3Byb2plY3RzL2xhdW5jaHgtdGVhbWluZy9zcmMifQ==
