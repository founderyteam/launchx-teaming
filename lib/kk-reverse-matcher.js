'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); /* eslint-disable class-methods-use-this */
/* eslint-disable no-console */
/* eslint-disable no-continue */


var _student = require('./student');

var _student2 = _interopRequireDefault(_student);

var _team = require('./team');

var _team2 = _interopRequireDefault(_team);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/*

Operations needed:
- determine number of teams
- determine if U can be broken into N teams of size minS or maxS
- incremement N
- is Set U empty?
- arbitrarily select team from set B
- find member from set U with best score
- find member from set U with best matching to team
- add member to team
- move team from set B to set A
- move member from set U to set V
- find member from set V with best score not in team
- find member from set V with best matching to team not in team
- find team of member b
- move team from set B to set A

*/

var KKMatchReverse = function () {
  // public
  function KKMatchReverse(students, teamingConfig) {
    var debug = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

    _classCallCheck(this, KKMatchReverse);

    this.students = students.map(function (student) {
      return new _student2.default(student, teamingConfig);
    });
    this.debug = debug;

    this.initializeAdjacencyMatrix();

    if (this.debug) {
      console.log('KKMatch: Removing duplicates...');
    }

    this.removeDuplicatesStudents();
  }

  _createClass(KKMatchReverse, [{
    key: 'getStudents',
    value: function getStudents() {
      return this.students;
    }
  }, {
    key: 'determineNumberOfTeams',
    value: function determineNumberOfTeams() {
      var numberOfStudents = this.getStudents().length;
      var maximumNumberOfTeams = Math.floor(numberOfStudents / this.minimumStudentsPerTeam);
      var mimimumNumberOfTeams = Math.ceil(numberOfStudents / this.maximumStudentsPerTeam);

      var difference = maximumNumberOfTeams - mimimumNumberOfTeams;

      return Math.round(this.balanceFactor * difference) + mimimumNumberOfTeams;
    }
  }, {
    key: 'canBeDistributed',
    value: function canBeDistributed(numberOfTeams, minS, maxS) {
      // can this number of teams be the sum of multiple numbers divisible
      // by a number between the min and max

      var avgStudentsPerTeam = this.students.length / numberOfTeams;
      return avgStudentsPerTeam <= maxS && avgStudentsPerTeam >= minS;

      // const potentialDivisors = [];
      // for (let i = minS; i < maxS + 1; i += 1) {
      //   potentialDivisors.push(i);
      // }
      // potentialDivisors.sort();

      // // we have [3, 4, 5] for instance

      // const potentialDivisor = potentialDivisors.pop(); // biggest divisor

      // if (numberOfTeamsCopy % potentialDivisor === 0) {
      //   return true;
      // }

      // let newNumberOfTeams = numberOfTeamsCopy - potentialDivisor;
      // while (newNumberOfTeams > 0) {
      //   const remainder = (newNumberOfTeams % potentialDivisor) + potentialDivisor;

      //   // is remainder divisible by any other divisors
      //   for (let i = 0; i < potentialDivisors.length; i += 1) {
      //     if (remainder % potentialDivisors[i] === 0) {
      //       return true;
      //     }
      //   }

      //   newNumberOfTeams -= potentialDivisor;
      // }

      // return false;
    }
  }, {
    key: 'incremementN',
    value: function incremementN(numberOfTeams) {
      if (this.balanceFactor < 0.5) {
        return numberOfTeams - 1;
      }

      return numberOfTeams + 1;
    }
  }, {
    key: 'initializeSets',
    value: function initializeSets() {
      this.visitedStudents = this.getStudents().map(function () {
        return false;
      });

      this.theTeams = [];
      this.teamIsFull = [];
      for (var i = 0; i < this.numberOfTeams; i += 1) {
        this.theTeams.push([]);
        this.teamIsFull.push(false);
      }
    }
  }, {
    key: 'hasUnvisitedStudents',
    value: function hasUnvisitedStudents() {
      for (var i = 0; i < this.visitedStudents.length; i += 1) {
        if (this.visitedStudents[i] === false) {
          return true;
        }
      }

      return false;
    }
  }, {
    key: 'sizeOfTeam',
    value: function sizeOfTeam(team) {
      return this.theTeams[team].length;
    }
  }, {
    key: 'arbitrarilySelectTeamFromSetB',
    value: function arbitrarilySelectTeamFromSetB() {
      var sizeOfSetB = this.teamIsFull.length;
      var arbitrarySelectionIndex = void 0;
      do {
        arbitrarySelectionIndex = Math.round(Math.random() * (sizeOfSetB - 1));
      } while (this.teamIsFull[arbitrarySelectionIndex]);

      return arbitrarySelectionIndex;
    }
  }, {
    key: 'memberWithBestScoreInSetU',
    value: function memberWithBestScoreInSetU() {
      // since its empty it must be random
      var randomStudent = void 0;
      do {
        randomStudent = Math.round((this.visitedStudents.length - 1) * Math.random());
      } while (this.visitedStudents[randomStudent] === true);

      return randomStudent;
    }
  }, {
    key: 'memberWithBestMatchingInSetU',
    value: function memberWithBestMatchingInSetU(team) {
      var bestMember = -1;
      var bestScore = -1;
      for (var i = 0; i < this.visitedStudents.length; i += 1) {
        if (!this.visitedStudents[i]) {
          // student is unvisited
          var teamCopy = Array.from(this.theTeams[team]);
          teamCopy.push(i);
          var score = this.scoreTeam(teamCopy);

          if (score > bestScore) {
            bestScore = score;
            bestMember = i;
          }
        }
      }

      if (bestMember === -1) {
        throw new Error('No unvisited students');
      }

      return bestMember;
    }
  }, {
    key: 'addMemberToTeam',
    value: function addMemberToTeam(member, team) {
      this.theTeams[team].push(member);
    }
  }, {
    key: 'moveMemberFromSetUToSetV',
    value: function moveMemberFromSetUToSetV(member) {
      this.visitedStudents[member] = true;
    }
  }, {
    key: 'moveTeamFromSetBToSetA',
    value: function moveTeamFromSetBToSetA(team) {
      this.teamIsFull[team] = true;
    }
  }, {
    key: 'moveTeamFromSetAToSetB',
    value: function moveTeamFromSetAToSetB(team) {
      this.teamIsFull[team] = false;
    }
  }, {
    key: 'runGreedyInsertionStep',
    value: function runGreedyInsertionStep() {
      var teamInQuestion = this.arbitrarilySelectTeamFromSetB();

      var selectedStudent = -1;
      if (this.sizeOfTeam(teamInQuestion) === 0) {
        selectedStudent = this.memberWithBestScoreInSetU();
      } else {
        selectedStudent = this.memberWithBestMatchingInSetU(teamInQuestion);
      }

      if (selectedStudent === -1) {
        throw new Error('There should be students left but there are not');
      }

      this.addMemberToTeam(selectedStudent, teamInQuestion);
      this.moveMemberFromSetUToSetV(selectedStudent);

      if (this.sizeOfTeam(teamInQuestion) >= this.maximumStudentsPerTeam) {
        this.moveTeamFromSetBToSetA(teamInQuestion);
      }
    }
  }, {
    key: 'hasTeamsInSetBUnderMinimum',
    value: function hasTeamsInSetBUnderMinimum() {
      for (var i = 0; i < this.teamIsFull.length; i += 1) {
        if (!this.teamIsFull[i] && this.theTeams[i].length < this.minimumStudentsPerTeam) {
          return true;
        }
      }

      return false;
    }
  }, {
    key: 'memberWithBestScoreInVOnTeamInA',
    value: function memberWithBestScoreInVOnTeamInA() {
      var randomTeam = void 0;
      do {
        randomTeam = Math.round((this.teamIsFull.length - 1) * Math.random());
      } while (this.teamIsFull[randomTeam] === false);

      // now team should be a full team
      var randomStudent = Math.round((this.theTeams[randomTeam].length - 1) * Math.random());

      return this.theTeams[randomTeam][randomStudent];
    }
  }, {
    key: 'isMemberOnTeam',
    value: function isMemberOnTeam(member, team) {
      for (var i = 0; i < this.theTeams[team].length; i += 1) {
        if (this.theTeams[team][i] === member) {
          return true;
        }
      }

      return false;
    }
  }, {
    key: 'findTeamInAOfMemberInV',
    value: function findTeamInAOfMemberInV(member) {
      for (var i = 0; i < this.theTeams.length; i += 1) {
        if (this.teamIsFull[i]) {
          // team is a full team
          if (this.isMemberOnTeam(member, i)) {
            return i;
          }
        }
      }

      return -1;
    }
  }, {
    key: 'removeMemberFromTeam',
    value: function removeMemberFromTeam(member, team) {
      var theTeam = this.theTeams[team];
      var theNewTeam = [];
      for (var i = 0; i < theTeam.length; i += 1) {
        if (theTeam[i] !== member) {
          theNewTeam.push(theTeam[i]);
        }
      }

      this.theTeams[team] = theNewTeam;
    }
  }, {
    key: 'memberWithBestMatchingInVOnTeamInANotOnTeam',
    value: function memberWithBestMatchingInVOnTeamInANotOnTeam(team) {
      var bestMember = -1;
      var bestScore = -1;
      for (var i = 0; i < this.visitedStudents.length; i += 1) {
        if (this.visitedStudents[i] && this.findTeamInAOfMemberInV(i) !== -1 && !this.isMemberOnTeam(i, team)) {
          // student is visited and not on team team
          var teamCopy = Array.from(this.theTeams[team]);
          teamCopy.push(i);
          var score = this.scoreTeam(teamCopy);

          if (score > bestScore) {
            bestScore = score;
            bestMember = i;
          }
        }
      }

      if (bestMember === -1) {
        throw new Error('No unvisited students');
      }

      return bestMember;
    }
  }, {
    key: 'greedilyFixTeams',
    value: function greedilyFixTeams() {
      for (var i = 0; i < this.teamIsFull.length; i += 1) {
        if (!this.teamIsFull[i] && this.theTeams[i].length < this.minimumStudentsPerTeam) {
          // we have a team that needs to be fixied

          while (this.theTeams[i].length < this.minimumStudentsPerTeam) {
            var selectedStudent = -1;
            if (this.theTeams[i].length === 0) {
              selectedStudent = this.memberWithBestScoreInVOnTeamInA();
            } else {
              selectedStudent = this.memberWithBestMatchingInVOnTeamInANotOnTeam(i);
            }

            if (selectedStudent === -1) {
              throw new Error('Something is wrong');
            }

            var selectedStudentTeam = this.findTeamInAOfMemberInV(selectedStudent);
            if (selectedStudentTeam === -1) {
              console.log(this.theTeams[i].length);
              throw new Error('Something is wrong team');
            }

            this.removeMemberFromTeam(selectedStudent, selectedStudentTeam);
            this.moveTeamFromSetAToSetB(selectedStudentTeam);
            this.addMemberToTeam(selectedStudent, i);
            if (this.sizeOfTeam(i) >= this.maximumStudentsPerTeam) {
              this.moveTeamFromSetBToSetA(i);
            }
          }
        }
      }
    }
  }, {
    key: 'team',
    value: function team(_ref) {
      var _ref$minimumStudentsP = _ref.minimumStudentsPerTeam,
          minimumStudentsPerTeam = _ref$minimumStudentsP === undefined ? 3 : _ref$minimumStudentsP,
          _ref$maximumStudentsP = _ref.maximumStudentsPerTeam,
          maximumStudentsPerTeam = _ref$maximumStudentsP === undefined ? 4 : _ref$maximumStudentsP,
          _ref$balanceFactor = _ref.balanceFactor,
          balanceFactor = _ref$balanceFactor === undefined ? 3 : _ref$balanceFactor;

      this.minimumStudentsPerTeam = minimumStudentsPerTeam;
      this.maximumStudentsPerTeam = maximumStudentsPerTeam;
      this.balanceFactor = balanceFactor / 10.0;

      if (this.debug) {
        console.log('KKMatchReverse: teaming...');console.time('KKMatchReverse');
      }

      this.numberOfTeams = this.determineNumberOfTeams();
      while (!this.canBeDistributed(this.numberOfTeams, this.minimumStudentsPerTeam, this.maximumStudentsPerTeam)) {
        this.numberOfTeams = this.incremementN(this.numberOfTeams);
      }

      this.initializeSets();
      while (this.hasUnvisitedStudents()) {
        this.runGreedyInsertionStep();
      }

      while (this.hasTeamsInSetBUnderMinimum()) {
        this.greedilyFixTeams();
      }

      if (this.debug) {
        console.log('KKMatchReverse: done...');console.timeEnd('KKMatchReverse');
      }

      if (this.debug) {
        console.log('KKMatchReverse: generating teams from match...');
      }
      return this.pathToTeams(this.theTeams);
    }
  }, {
    key: 'pathToTeams',
    value: function pathToTeams(solutionPath) {
      var _this = this;

      var teams = [];
      for (var i = 0; i < solutionPath.length; i += 1) {
        var team = solutionPath[i].map(function (j) {
          return _this.students[j];
        });
        teams.push(new _team2.default(team));
      }

      return teams;
    }

    // private

  }, {
    key: 'removeDuplicatesStudents',
    value: function removeDuplicatesStudents() {
      var dupArray = {};
      this.getStudents().forEach(function (student) {
        dupArray[student.hash] = student;
      });

      this.students = Object.values(dupArray);
    }
  }, {
    key: 'scoreTeam',
    value: function scoreTeam(team) {
      // pairwise team all members against each other
      if (team.length <= 1) {
        return 0;
      }

      var score = 0;
      var numPerms = 0;
      for (var i = 0; i < team.length; i += 1) {
        for (var j = i + 1; j < team.length; j += 1) {
          score += this.scoreStudents(team[i], team[j]);
          numPerms += 1;
        }
      }

      return score / numPerms;
    }
  }, {
    key: 'scoreStudents',
    value: function scoreStudents(a, b) {
      return this.scoredAgainst[a][b];
    }
  }, {
    key: 'initializeAdjacencyMatrix',
    value: function initializeAdjacencyMatrix() {
      this.scoredAgainst = [];
      for (var i = 0; i < this.students.length; i += 1) {
        this.scoredAgainst.push([]);
        for (var j = 0; j < this.students.length; j += 1) {
          if (i !== j) {
            this.scoredAgainst[i].push(this.students[i].scoreAgainst(this.students[j]));
          } else {
            this.scoredAgainst[i].push(-1);
          }
        }
      }
    }
  }]);

  return KKMatchReverse;
}();

exports.default = KKMatchReverse;
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImtrLXJldmVyc2UtbWF0Y2hlci5qcyJdLCJuYW1lcyI6WyJLS01hdGNoUmV2ZXJzZSIsInN0dWRlbnRzIiwidGVhbWluZ0NvbmZpZyIsImRlYnVnIiwibWFwIiwiU3R1ZGVudCIsInN0dWRlbnQiLCJpbml0aWFsaXplQWRqYWNlbmN5TWF0cml4IiwiY29uc29sZSIsImxvZyIsInJlbW92ZUR1cGxpY2F0ZXNTdHVkZW50cyIsIm51bWJlck9mU3R1ZGVudHMiLCJnZXRTdHVkZW50cyIsImxlbmd0aCIsIm1heGltdW1OdW1iZXJPZlRlYW1zIiwiTWF0aCIsImZsb29yIiwibWluaW11bVN0dWRlbnRzUGVyVGVhbSIsIm1pbWltdW1OdW1iZXJPZlRlYW1zIiwiY2VpbCIsIm1heGltdW1TdHVkZW50c1BlclRlYW0iLCJkaWZmZXJlbmNlIiwicm91bmQiLCJiYWxhbmNlRmFjdG9yIiwibnVtYmVyT2ZUZWFtcyIsIm1pblMiLCJtYXhTIiwiYXZnU3R1ZGVudHNQZXJUZWFtIiwidmlzaXRlZFN0dWRlbnRzIiwidGhlVGVhbXMiLCJ0ZWFtSXNGdWxsIiwiaSIsInB1c2giLCJ0ZWFtIiwic2l6ZU9mU2V0QiIsImFyYml0cmFyeVNlbGVjdGlvbkluZGV4IiwicmFuZG9tIiwicmFuZG9tU3R1ZGVudCIsImJlc3RNZW1iZXIiLCJiZXN0U2NvcmUiLCJ0ZWFtQ29weSIsIkFycmF5IiwiZnJvbSIsInNjb3JlIiwic2NvcmVUZWFtIiwiRXJyb3IiLCJtZW1iZXIiLCJ0ZWFtSW5RdWVzdGlvbiIsImFyYml0cmFyaWx5U2VsZWN0VGVhbUZyb21TZXRCIiwic2VsZWN0ZWRTdHVkZW50Iiwic2l6ZU9mVGVhbSIsIm1lbWJlcldpdGhCZXN0U2NvcmVJblNldFUiLCJtZW1iZXJXaXRoQmVzdE1hdGNoaW5nSW5TZXRVIiwiYWRkTWVtYmVyVG9UZWFtIiwibW92ZU1lbWJlckZyb21TZXRVVG9TZXRWIiwibW92ZVRlYW1Gcm9tU2V0QlRvU2V0QSIsInJhbmRvbVRlYW0iLCJpc01lbWJlck9uVGVhbSIsInRoZVRlYW0iLCJ0aGVOZXdUZWFtIiwiZmluZFRlYW1JbkFPZk1lbWJlckluViIsIm1lbWJlcldpdGhCZXN0U2NvcmVJblZPblRlYW1JbkEiLCJtZW1iZXJXaXRoQmVzdE1hdGNoaW5nSW5WT25UZWFtSW5BTm90T25UZWFtIiwic2VsZWN0ZWRTdHVkZW50VGVhbSIsInJlbW92ZU1lbWJlckZyb21UZWFtIiwibW92ZVRlYW1Gcm9tU2V0QVRvU2V0QiIsInRpbWUiLCJkZXRlcm1pbmVOdW1iZXJPZlRlYW1zIiwiY2FuQmVEaXN0cmlidXRlZCIsImluY3JlbWVtZW50TiIsImluaXRpYWxpemVTZXRzIiwiaGFzVW52aXNpdGVkU3R1ZGVudHMiLCJydW5HcmVlZHlJbnNlcnRpb25TdGVwIiwiaGFzVGVhbXNJblNldEJVbmRlck1pbmltdW0iLCJncmVlZGlseUZpeFRlYW1zIiwidGltZUVuZCIsInBhdGhUb1RlYW1zIiwic29sdXRpb25QYXRoIiwidGVhbXMiLCJqIiwiVGVhbSIsImR1cEFycmF5IiwiZm9yRWFjaCIsImhhc2giLCJPYmplY3QiLCJ2YWx1ZXMiLCJudW1QZXJtcyIsInNjb3JlU3R1ZGVudHMiLCJhIiwiYiIsInNjb3JlZEFnYWluc3QiLCJzY29yZUFnYWluc3QiXSwibWFwcGluZ3MiOiI7Ozs7OztxakJBQUE7QUFDQTtBQUNBOzs7QUFDQTs7OztBQUNBOzs7Ozs7OztBQUVBOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQW9CcUJBLGM7QUFDbkI7QUFDQSwwQkFBWUMsUUFBWixFQUFzQkMsYUFBdEIsRUFBb0Q7QUFBQSxRQUFmQyxLQUFlLHVFQUFQLEtBQU87O0FBQUE7O0FBQ2xELFNBQUtGLFFBQUwsR0FBZ0JBLFNBQVNHLEdBQVQsQ0FBYTtBQUFBLGFBQVcsSUFBSUMsaUJBQUosQ0FBWUMsT0FBWixFQUFxQkosYUFBckIsQ0FBWDtBQUFBLEtBQWIsQ0FBaEI7QUFDQSxTQUFLQyxLQUFMLEdBQWFBLEtBQWI7O0FBRUEsU0FBS0kseUJBQUw7O0FBRUEsUUFBSSxLQUFLSixLQUFULEVBQWdCO0FBQUVLLGNBQVFDLEdBQVIsQ0FBWSxpQ0FBWjtBQUFpRDs7QUFFbkUsU0FBS0Msd0JBQUw7QUFDRDs7OztrQ0FFYTtBQUNaLGFBQU8sS0FBS1QsUUFBWjtBQUNEOzs7NkNBRXdCO0FBQ3ZCLFVBQU1VLG1CQUFtQixLQUFLQyxXQUFMLEdBQW1CQyxNQUE1QztBQUNBLFVBQU1DLHVCQUF1QkMsS0FBS0MsS0FBTCxDQUFXTCxtQkFBbUIsS0FBS00sc0JBQW5DLENBQTdCO0FBQ0EsVUFBTUMsdUJBQXVCSCxLQUFLSSxJQUFMLENBQVVSLG1CQUFtQixLQUFLUyxzQkFBbEMsQ0FBN0I7O0FBRUEsVUFBTUMsYUFBYVAsdUJBQXVCSSxvQkFBMUM7O0FBRUEsYUFBT0gsS0FBS08sS0FBTCxDQUFXLEtBQUtDLGFBQUwsR0FBcUJGLFVBQWhDLElBQThDSCxvQkFBckQ7QUFDRDs7O3FDQUVnQk0sYSxFQUFlQyxJLEVBQU1DLEksRUFBTTtBQUMxQztBQUNBOztBQUVBLFVBQU1DLHFCQUFzQixLQUFLMUIsUUFBTCxDQUFjWSxNQUFkLEdBQXVCVyxhQUFuRDtBQUNBLGFBQU9HLHNCQUFzQkQsSUFBdEIsSUFBOEJDLHNCQUFzQkYsSUFBM0Q7O0FBRUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTs7QUFFQTs7QUFFQTtBQUNBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0Q7OztpQ0FFWUQsYSxFQUFlO0FBQzFCLFVBQUksS0FBS0QsYUFBTCxHQUFxQixHQUF6QixFQUE4QjtBQUM1QixlQUFPQyxnQkFBZ0IsQ0FBdkI7QUFDRDs7QUFFRCxhQUFPQSxnQkFBZ0IsQ0FBdkI7QUFDRDs7O3FDQUVnQjtBQUNmLFdBQUtJLGVBQUwsR0FBdUIsS0FBS2hCLFdBQUwsR0FBbUJSLEdBQW5CLENBQXVCO0FBQUEsZUFBTSxLQUFOO0FBQUEsT0FBdkIsQ0FBdkI7O0FBRUEsV0FBS3lCLFFBQUwsR0FBZ0IsRUFBaEI7QUFDQSxXQUFLQyxVQUFMLEdBQWtCLEVBQWxCO0FBQ0EsV0FBSyxJQUFJQyxJQUFJLENBQWIsRUFBZ0JBLElBQUksS0FBS1AsYUFBekIsRUFBd0NPLEtBQUssQ0FBN0MsRUFBZ0Q7QUFDOUMsYUFBS0YsUUFBTCxDQUFjRyxJQUFkLENBQW1CLEVBQW5CO0FBQ0EsYUFBS0YsVUFBTCxDQUFnQkUsSUFBaEIsQ0FBcUIsS0FBckI7QUFDRDtBQUNGOzs7MkNBRXNCO0FBQ3JCLFdBQUssSUFBSUQsSUFBSSxDQUFiLEVBQWdCQSxJQUFJLEtBQUtILGVBQUwsQ0FBcUJmLE1BQXpDLEVBQWlEa0IsS0FBSyxDQUF0RCxFQUF5RDtBQUN2RCxZQUFJLEtBQUtILGVBQUwsQ0FBcUJHLENBQXJCLE1BQTRCLEtBQWhDLEVBQXVDO0FBQ3JDLGlCQUFPLElBQVA7QUFDRDtBQUNGOztBQUVELGFBQU8sS0FBUDtBQUNEOzs7K0JBRVVFLEksRUFBTTtBQUNmLGFBQU8sS0FBS0osUUFBTCxDQUFjSSxJQUFkLEVBQW9CcEIsTUFBM0I7QUFDRDs7O29EQUUrQjtBQUM5QixVQUFNcUIsYUFBYSxLQUFLSixVQUFMLENBQWdCakIsTUFBbkM7QUFDQSxVQUFJc0IsZ0NBQUo7QUFDQSxTQUFHO0FBQ0RBLGtDQUEwQnBCLEtBQUtPLEtBQUwsQ0FBV1AsS0FBS3FCLE1BQUwsTUFBaUJGLGFBQWEsQ0FBOUIsQ0FBWCxDQUExQjtBQUNELE9BRkQsUUFHRSxLQUFLSixVQUFMLENBQWdCSyx1QkFBaEIsQ0FIRjs7QUFNQSxhQUFPQSx1QkFBUDtBQUNEOzs7Z0RBRTJCO0FBQzFCO0FBQ0EsVUFBSUUsc0JBQUo7QUFDQSxTQUFHO0FBQ0RBLHdCQUFnQnRCLEtBQUtPLEtBQUwsQ0FBVyxDQUFDLEtBQUtNLGVBQUwsQ0FBcUJmLE1BQXJCLEdBQThCLENBQS9CLElBQW9DRSxLQUFLcUIsTUFBTCxFQUEvQyxDQUFoQjtBQUNELE9BRkQsUUFFUyxLQUFLUixlQUFMLENBQXFCUyxhQUFyQixNQUF3QyxJQUZqRDs7QUFJQSxhQUFPQSxhQUFQO0FBQ0Q7OztpREFFNEJKLEksRUFBTTtBQUNqQyxVQUFJSyxhQUFhLENBQUMsQ0FBbEI7QUFDQSxVQUFJQyxZQUFZLENBQUMsQ0FBakI7QUFDQSxXQUFLLElBQUlSLElBQUksQ0FBYixFQUFnQkEsSUFBSSxLQUFLSCxlQUFMLENBQXFCZixNQUF6QyxFQUFpRGtCLEtBQUssQ0FBdEQsRUFBeUQ7QUFDdkQsWUFBSSxDQUFDLEtBQUtILGVBQUwsQ0FBcUJHLENBQXJCLENBQUwsRUFBOEI7QUFDNUI7QUFDQSxjQUFNUyxXQUFXQyxNQUFNQyxJQUFOLENBQVcsS0FBS2IsUUFBTCxDQUFjSSxJQUFkLENBQVgsQ0FBakI7QUFDQU8sbUJBQVNSLElBQVQsQ0FBY0QsQ0FBZDtBQUNBLGNBQU1ZLFFBQVEsS0FBS0MsU0FBTCxDQUFlSixRQUFmLENBQWQ7O0FBRUEsY0FBSUcsUUFBUUosU0FBWixFQUF1QjtBQUNyQkEsd0JBQVlJLEtBQVo7QUFDQUwseUJBQWFQLENBQWI7QUFDRDtBQUNGO0FBQ0Y7O0FBRUQsVUFBSU8sZUFBZSxDQUFDLENBQXBCLEVBQXVCO0FBQ3JCLGNBQU0sSUFBSU8sS0FBSixDQUFVLHVCQUFWLENBQU47QUFDRDs7QUFFRCxhQUFPUCxVQUFQO0FBQ0Q7OztvQ0FFZVEsTSxFQUFRYixJLEVBQU07QUFDNUIsV0FBS0osUUFBTCxDQUFjSSxJQUFkLEVBQW9CRCxJQUFwQixDQUF5QmMsTUFBekI7QUFDRDs7OzZDQUV3QkEsTSxFQUFRO0FBQy9CLFdBQUtsQixlQUFMLENBQXFCa0IsTUFBckIsSUFBK0IsSUFBL0I7QUFDRDs7OzJDQUVzQmIsSSxFQUFNO0FBQzNCLFdBQUtILFVBQUwsQ0FBZ0JHLElBQWhCLElBQXdCLElBQXhCO0FBQ0Q7OzsyQ0FFc0JBLEksRUFBTTtBQUMzQixXQUFLSCxVQUFMLENBQWdCRyxJQUFoQixJQUF3QixLQUF4QjtBQUNEOzs7NkNBRXdCO0FBQ3ZCLFVBQU1jLGlCQUFpQixLQUFLQyw2QkFBTCxFQUF2Qjs7QUFFQSxVQUFJQyxrQkFBa0IsQ0FBQyxDQUF2QjtBQUNBLFVBQUksS0FBS0MsVUFBTCxDQUFnQkgsY0FBaEIsTUFBb0MsQ0FBeEMsRUFBMkM7QUFDekNFLDBCQUFrQixLQUFLRSx5QkFBTCxFQUFsQjtBQUNELE9BRkQsTUFFTztBQUNMRiwwQkFBa0IsS0FBS0csNEJBQUwsQ0FBa0NMLGNBQWxDLENBQWxCO0FBQ0Q7O0FBRUQsVUFBSUUsb0JBQW9CLENBQUMsQ0FBekIsRUFBNEI7QUFDMUIsY0FBTSxJQUFJSixLQUFKLENBQVUsaURBQVYsQ0FBTjtBQUNEOztBQUVELFdBQUtRLGVBQUwsQ0FBcUJKLGVBQXJCLEVBQXNDRixjQUF0QztBQUNBLFdBQUtPLHdCQUFMLENBQThCTCxlQUE5Qjs7QUFFQSxVQUFJLEtBQUtDLFVBQUwsQ0FBZ0JILGNBQWhCLEtBQW1DLEtBQUszQixzQkFBNUMsRUFBb0U7QUFDbEUsYUFBS21DLHNCQUFMLENBQTRCUixjQUE1QjtBQUNEO0FBQ0Y7OztpREFFNEI7QUFDM0IsV0FBSyxJQUFJaEIsSUFBSSxDQUFiLEVBQWdCQSxJQUFJLEtBQUtELFVBQUwsQ0FBZ0JqQixNQUFwQyxFQUE0Q2tCLEtBQUssQ0FBakQsRUFBb0Q7QUFDbEQsWUFBSSxDQUFDLEtBQUtELFVBQUwsQ0FBZ0JDLENBQWhCLENBQUQsSUFBdUIsS0FBS0YsUUFBTCxDQUFjRSxDQUFkLEVBQWlCbEIsTUFBakIsR0FBMEIsS0FBS0ksc0JBQTFELEVBQWtGO0FBQ2hGLGlCQUFPLElBQVA7QUFDRDtBQUNGOztBQUVELGFBQU8sS0FBUDtBQUNEOzs7c0RBRWlDO0FBQ2hDLFVBQUl1QyxtQkFBSjtBQUNBLFNBQUc7QUFDREEscUJBQWF6QyxLQUFLTyxLQUFMLENBQVcsQ0FBQyxLQUFLUSxVQUFMLENBQWdCakIsTUFBaEIsR0FBeUIsQ0FBMUIsSUFBK0JFLEtBQUtxQixNQUFMLEVBQTFDLENBQWI7QUFDRCxPQUZELFFBRVMsS0FBS04sVUFBTCxDQUFnQjBCLFVBQWhCLE1BQWdDLEtBRnpDOztBQUlBO0FBQ0EsVUFBTW5CLGdCQUFnQnRCLEtBQUtPLEtBQUwsQ0FBVyxDQUFDLEtBQUtPLFFBQUwsQ0FBYzJCLFVBQWQsRUFBMEIzQyxNQUExQixHQUFtQyxDQUFwQyxJQUF5Q0UsS0FBS3FCLE1BQUwsRUFBcEQsQ0FBdEI7O0FBRUEsYUFBTyxLQUFLUCxRQUFMLENBQWMyQixVQUFkLEVBQTBCbkIsYUFBMUIsQ0FBUDtBQUNEOzs7bUNBRWNTLE0sRUFBUWIsSSxFQUFNO0FBQzNCLFdBQUssSUFBSUYsSUFBSSxDQUFiLEVBQWdCQSxJQUFJLEtBQUtGLFFBQUwsQ0FBY0ksSUFBZCxFQUFvQnBCLE1BQXhDLEVBQWdEa0IsS0FBSyxDQUFyRCxFQUF3RDtBQUN0RCxZQUFJLEtBQUtGLFFBQUwsQ0FBY0ksSUFBZCxFQUFvQkYsQ0FBcEIsTUFBMkJlLE1BQS9CLEVBQXVDO0FBQ3JDLGlCQUFPLElBQVA7QUFDRDtBQUNGOztBQUVELGFBQU8sS0FBUDtBQUNEOzs7MkNBRXNCQSxNLEVBQVE7QUFDN0IsV0FBSyxJQUFJZixJQUFJLENBQWIsRUFBZ0JBLElBQUksS0FBS0YsUUFBTCxDQUFjaEIsTUFBbEMsRUFBMENrQixLQUFLLENBQS9DLEVBQWtEO0FBQ2hELFlBQUksS0FBS0QsVUFBTCxDQUFnQkMsQ0FBaEIsQ0FBSixFQUF3QjtBQUN0QjtBQUNBLGNBQUksS0FBSzBCLGNBQUwsQ0FBb0JYLE1BQXBCLEVBQTRCZixDQUE1QixDQUFKLEVBQW9DO0FBQ2xDLG1CQUFPQSxDQUFQO0FBQ0Q7QUFDRjtBQUNGOztBQUVELGFBQU8sQ0FBQyxDQUFSO0FBQ0Q7Ozt5Q0FFb0JlLE0sRUFBUWIsSSxFQUFNO0FBQ2pDLFVBQU15QixVQUFVLEtBQUs3QixRQUFMLENBQWNJLElBQWQsQ0FBaEI7QUFDQSxVQUFNMEIsYUFBYSxFQUFuQjtBQUNBLFdBQUssSUFBSTVCLElBQUksQ0FBYixFQUFnQkEsSUFBSTJCLFFBQVE3QyxNQUE1QixFQUFvQ2tCLEtBQUssQ0FBekMsRUFBNEM7QUFDMUMsWUFBSTJCLFFBQVEzQixDQUFSLE1BQWVlLE1BQW5CLEVBQTJCO0FBQ3pCYSxxQkFBVzNCLElBQVgsQ0FBZ0IwQixRQUFRM0IsQ0FBUixDQUFoQjtBQUNEO0FBQ0Y7O0FBRUQsV0FBS0YsUUFBTCxDQUFjSSxJQUFkLElBQXNCMEIsVUFBdEI7QUFDRDs7O2dFQUUyQzFCLEksRUFBTTtBQUNoRCxVQUFJSyxhQUFhLENBQUMsQ0FBbEI7QUFDQSxVQUFJQyxZQUFZLENBQUMsQ0FBakI7QUFDQSxXQUFLLElBQUlSLElBQUksQ0FBYixFQUFnQkEsSUFBSSxLQUFLSCxlQUFMLENBQXFCZixNQUF6QyxFQUFpRGtCLEtBQUssQ0FBdEQsRUFBeUQ7QUFDdkQsWUFBSSxLQUFLSCxlQUFMLENBQXFCRyxDQUFyQixLQUEyQixLQUFLNkIsc0JBQUwsQ0FBNEI3QixDQUE1QixNQUFtQyxDQUFDLENBQS9ELElBQW9FLENBQUMsS0FBSzBCLGNBQUwsQ0FBb0IxQixDQUFwQixFQUF1QkUsSUFBdkIsQ0FBekUsRUFBdUc7QUFDckc7QUFDQSxjQUFNTyxXQUFXQyxNQUFNQyxJQUFOLENBQVcsS0FBS2IsUUFBTCxDQUFjSSxJQUFkLENBQVgsQ0FBakI7QUFDQU8sbUJBQVNSLElBQVQsQ0FBY0QsQ0FBZDtBQUNBLGNBQU1ZLFFBQVEsS0FBS0MsU0FBTCxDQUFlSixRQUFmLENBQWQ7O0FBRUEsY0FBSUcsUUFBUUosU0FBWixFQUF1QjtBQUNyQkEsd0JBQVlJLEtBQVo7QUFDQUwseUJBQWFQLENBQWI7QUFDRDtBQUNGO0FBQ0Y7O0FBRUQsVUFBSU8sZUFBZSxDQUFDLENBQXBCLEVBQXVCO0FBQ3JCLGNBQU0sSUFBSU8sS0FBSixDQUFVLHVCQUFWLENBQU47QUFDRDs7QUFFRCxhQUFPUCxVQUFQO0FBQ0Q7Ozt1Q0FFa0I7QUFDakIsV0FBSyxJQUFJUCxJQUFJLENBQWIsRUFBZ0JBLElBQUksS0FBS0QsVUFBTCxDQUFnQmpCLE1BQXBDLEVBQTRDa0IsS0FBSyxDQUFqRCxFQUFvRDtBQUNsRCxZQUFJLENBQUMsS0FBS0QsVUFBTCxDQUFnQkMsQ0FBaEIsQ0FBRCxJQUF1QixLQUFLRixRQUFMLENBQWNFLENBQWQsRUFBaUJsQixNQUFqQixHQUEwQixLQUFLSSxzQkFBMUQsRUFBa0Y7QUFDaEY7O0FBRUEsaUJBQU8sS0FBS1ksUUFBTCxDQUFjRSxDQUFkLEVBQWlCbEIsTUFBakIsR0FBMEIsS0FBS0ksc0JBQXRDLEVBQThEO0FBQzVELGdCQUFJZ0Msa0JBQWtCLENBQUMsQ0FBdkI7QUFDQSxnQkFBSSxLQUFLcEIsUUFBTCxDQUFjRSxDQUFkLEVBQWlCbEIsTUFBakIsS0FBNEIsQ0FBaEMsRUFBbUM7QUFDakNvQyxnQ0FBa0IsS0FBS1ksK0JBQUwsRUFBbEI7QUFDRCxhQUZELE1BRU87QUFDTFosZ0NBQWtCLEtBQUthLDJDQUFMLENBQWlEL0IsQ0FBakQsQ0FBbEI7QUFDRDs7QUFFRCxnQkFBSWtCLG9CQUFvQixDQUFDLENBQXpCLEVBQTRCO0FBQzFCLG9CQUFNLElBQUlKLEtBQUosQ0FBVSxvQkFBVixDQUFOO0FBQ0Q7O0FBRUQsZ0JBQU1rQixzQkFBc0IsS0FBS0gsc0JBQUwsQ0FBNEJYLGVBQTVCLENBQTVCO0FBQ0EsZ0JBQUljLHdCQUF3QixDQUFDLENBQTdCLEVBQWdDO0FBQzlCdkQsc0JBQVFDLEdBQVIsQ0FBWSxLQUFLb0IsUUFBTCxDQUFjRSxDQUFkLEVBQWlCbEIsTUFBN0I7QUFDQSxvQkFBTSxJQUFJZ0MsS0FBSixDQUFVLHlCQUFWLENBQU47QUFDRDs7QUFFRCxpQkFBS21CLG9CQUFMLENBQTBCZixlQUExQixFQUEyQ2MsbUJBQTNDO0FBQ0EsaUJBQUtFLHNCQUFMLENBQTRCRixtQkFBNUI7QUFDQSxpQkFBS1YsZUFBTCxDQUFxQkosZUFBckIsRUFBc0NsQixDQUF0QztBQUNBLGdCQUFJLEtBQUttQixVQUFMLENBQWdCbkIsQ0FBaEIsS0FBc0IsS0FBS1gsc0JBQS9CLEVBQXVEO0FBQ3JELG1CQUFLbUMsc0JBQUwsQ0FBNEJ4QixDQUE1QjtBQUNEO0FBQ0Y7QUFDRjtBQUNGO0FBQ0Y7OzsrQkFFbUY7QUFBQSx1Q0FBN0VkLHNCQUE2RTtBQUFBLFVBQTdFQSxzQkFBNkUseUNBQXBELENBQW9EO0FBQUEsdUNBQWpERyxzQkFBaUQ7QUFBQSxVQUFqREEsc0JBQWlELHlDQUF4QixDQUF3QjtBQUFBLG9DQUFyQkcsYUFBcUI7QUFBQSxVQUFyQkEsYUFBcUIsc0NBQUwsQ0FBSzs7QUFDbEYsV0FBS04sc0JBQUwsR0FBOEJBLHNCQUE5QjtBQUNBLFdBQUtHLHNCQUFMLEdBQThCQSxzQkFBOUI7QUFDQSxXQUFLRyxhQUFMLEdBQXFCQSxnQkFBZ0IsSUFBckM7O0FBRUEsVUFBSSxLQUFLcEIsS0FBVCxFQUFnQjtBQUFFSyxnQkFBUUMsR0FBUixDQUFZLDRCQUFaLEVBQTJDRCxRQUFRMEQsSUFBUixDQUFhLGdCQUFiO0FBQWlDOztBQUU5RixXQUFLMUMsYUFBTCxHQUFxQixLQUFLMkMsc0JBQUwsRUFBckI7QUFDQSxhQUFPLENBQUMsS0FBS0MsZ0JBQUwsQ0FDTixLQUFLNUMsYUFEQyxFQUVOLEtBQUtQLHNCQUZDLEVBR04sS0FBS0csc0JBSEMsQ0FBUixFQUlHO0FBQ0QsYUFBS0ksYUFBTCxHQUFxQixLQUFLNkMsWUFBTCxDQUFrQixLQUFLN0MsYUFBdkIsQ0FBckI7QUFDRDs7QUFFRCxXQUFLOEMsY0FBTDtBQUNBLGFBQU8sS0FBS0Msb0JBQUwsRUFBUCxFQUFvQztBQUNsQyxhQUFLQyxzQkFBTDtBQUNEOztBQUVELGFBQU8sS0FBS0MsMEJBQUwsRUFBUCxFQUEwQztBQUN4QyxhQUFLQyxnQkFBTDtBQUNEOztBQUdELFVBQUksS0FBS3ZFLEtBQVQsRUFBZ0I7QUFBRUssZ0JBQVFDLEdBQVIsQ0FBWSx5QkFBWixFQUF3Q0QsUUFBUW1FLE9BQVIsQ0FBZ0IsZ0JBQWhCO0FBQW9DOztBQUU5RixVQUFJLEtBQUt4RSxLQUFULEVBQWdCO0FBQUVLLGdCQUFRQyxHQUFSLENBQVksZ0RBQVo7QUFBZ0U7QUFDbEYsYUFBTyxLQUFLbUUsV0FBTCxDQUFpQixLQUFLL0MsUUFBdEIsQ0FBUDtBQUNEOzs7Z0NBRVdnRCxZLEVBQWM7QUFBQTs7QUFDeEIsVUFBTUMsUUFBUSxFQUFkO0FBQ0EsV0FBSyxJQUFJL0MsSUFBSSxDQUFiLEVBQWdCQSxJQUFJOEMsYUFBYWhFLE1BQWpDLEVBQXlDa0IsS0FBSyxDQUE5QyxFQUFpRDtBQUMvQyxZQUFNRSxPQUFPNEMsYUFBYTlDLENBQWIsRUFBZ0IzQixHQUFoQixDQUFvQjtBQUFBLGlCQUFLLE1BQUtILFFBQUwsQ0FBYzhFLENBQWQsQ0FBTDtBQUFBLFNBQXBCLENBQWI7QUFDQUQsY0FBTTlDLElBQU4sQ0FBVyxJQUFJZ0QsY0FBSixDQUFTL0MsSUFBVCxDQUFYO0FBQ0Q7O0FBRUQsYUFBTzZDLEtBQVA7QUFDRDs7QUFHRDs7OzsrQ0FDMkI7QUFDekIsVUFBTUcsV0FBVyxFQUFqQjtBQUNBLFdBQUtyRSxXQUFMLEdBQW1Cc0UsT0FBbkIsQ0FBMkIsVUFBQzVFLE9BQUQsRUFBYTtBQUN0QzJFLGlCQUFTM0UsUUFBUTZFLElBQWpCLElBQXlCN0UsT0FBekI7QUFDRCxPQUZEOztBQUlBLFdBQUtMLFFBQUwsR0FBZ0JtRixPQUFPQyxNQUFQLENBQWNKLFFBQWQsQ0FBaEI7QUFDRDs7OzhCQUVTaEQsSSxFQUFNO0FBQ2Q7QUFDQSxVQUFJQSxLQUFLcEIsTUFBTCxJQUFlLENBQW5CLEVBQXNCO0FBQ3BCLGVBQU8sQ0FBUDtBQUNEOztBQUVELFVBQUk4QixRQUFRLENBQVo7QUFDQSxVQUFJMkMsV0FBVyxDQUFmO0FBQ0EsV0FBSyxJQUFJdkQsSUFBSSxDQUFiLEVBQWdCQSxJQUFJRSxLQUFLcEIsTUFBekIsRUFBaUNrQixLQUFLLENBQXRDLEVBQXlDO0FBQ3ZDLGFBQUssSUFBSWdELElBQUloRCxJQUFJLENBQWpCLEVBQW9CZ0QsSUFBSTlDLEtBQUtwQixNQUE3QixFQUFxQ2tFLEtBQUssQ0FBMUMsRUFBNkM7QUFDM0NwQyxtQkFBUyxLQUFLNEMsYUFBTCxDQUFtQnRELEtBQUtGLENBQUwsQ0FBbkIsRUFBNEJFLEtBQUs4QyxDQUFMLENBQTVCLENBQVQ7QUFDQU8sc0JBQVksQ0FBWjtBQUNEO0FBQ0Y7O0FBRUQsYUFBTzNDLFFBQVEyQyxRQUFmO0FBQ0Q7OztrQ0FFYUUsQyxFQUFHQyxDLEVBQUc7QUFDbEIsYUFBTyxLQUFLQyxhQUFMLENBQW1CRixDQUFuQixFQUFzQkMsQ0FBdEIsQ0FBUDtBQUNEOzs7Z0RBRTJCO0FBQzFCLFdBQUtDLGFBQUwsR0FBcUIsRUFBckI7QUFDQSxXQUFLLElBQUkzRCxJQUFJLENBQWIsRUFBZ0JBLElBQUksS0FBSzlCLFFBQUwsQ0FBY1ksTUFBbEMsRUFBMENrQixLQUFLLENBQS9DLEVBQWtEO0FBQ2hELGFBQUsyRCxhQUFMLENBQW1CMUQsSUFBbkIsQ0FBd0IsRUFBeEI7QUFDQSxhQUFLLElBQUkrQyxJQUFJLENBQWIsRUFBZ0JBLElBQUksS0FBSzlFLFFBQUwsQ0FBY1ksTUFBbEMsRUFBMENrRSxLQUFLLENBQS9DLEVBQWtEO0FBQ2hELGNBQUloRCxNQUFNZ0QsQ0FBVixFQUFhO0FBQ1gsaUJBQUtXLGFBQUwsQ0FBbUIzRCxDQUFuQixFQUFzQkMsSUFBdEIsQ0FBMkIsS0FBSy9CLFFBQUwsQ0FBYzhCLENBQWQsRUFBaUI0RCxZQUFqQixDQUE4QixLQUFLMUYsUUFBTCxDQUFjOEUsQ0FBZCxDQUE5QixDQUEzQjtBQUNELFdBRkQsTUFFTztBQUNMLGlCQUFLVyxhQUFMLENBQW1CM0QsQ0FBbkIsRUFBc0JDLElBQXRCLENBQTJCLENBQUMsQ0FBNUI7QUFDRDtBQUNGO0FBQ0Y7QUFDRjs7Ozs7O2tCQWhZa0JoQyxjIiwiZmlsZSI6ImtrLXJldmVyc2UtbWF0Y2hlci5qcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIGVzbGludC1kaXNhYmxlIGNsYXNzLW1ldGhvZHMtdXNlLXRoaXMgKi9cbi8qIGVzbGludC1kaXNhYmxlIG5vLWNvbnNvbGUgKi9cbi8qIGVzbGludC1kaXNhYmxlIG5vLWNvbnRpbnVlICovXG5pbXBvcnQgU3R1ZGVudCBmcm9tICcuL3N0dWRlbnQnO1xuaW1wb3J0IFRlYW0gZnJvbSAnLi90ZWFtJztcblxuLypcblxuT3BlcmF0aW9ucyBuZWVkZWQ6XG4tIGRldGVybWluZSBudW1iZXIgb2YgdGVhbXNcbi0gZGV0ZXJtaW5lIGlmIFUgY2FuIGJlIGJyb2tlbiBpbnRvIE4gdGVhbXMgb2Ygc2l6ZSBtaW5TIG9yIG1heFNcbi0gaW5jcmVtZW1lbnQgTlxuLSBpcyBTZXQgVSBlbXB0eT9cbi0gYXJiaXRyYXJpbHkgc2VsZWN0IHRlYW0gZnJvbSBzZXQgQlxuLSBmaW5kIG1lbWJlciBmcm9tIHNldCBVIHdpdGggYmVzdCBzY29yZVxuLSBmaW5kIG1lbWJlciBmcm9tIHNldCBVIHdpdGggYmVzdCBtYXRjaGluZyB0byB0ZWFtXG4tIGFkZCBtZW1iZXIgdG8gdGVhbVxuLSBtb3ZlIHRlYW0gZnJvbSBzZXQgQiB0byBzZXQgQVxuLSBtb3ZlIG1lbWJlciBmcm9tIHNldCBVIHRvIHNldCBWXG4tIGZpbmQgbWVtYmVyIGZyb20gc2V0IFYgd2l0aCBiZXN0IHNjb3JlIG5vdCBpbiB0ZWFtXG4tIGZpbmQgbWVtYmVyIGZyb20gc2V0IFYgd2l0aCBiZXN0IG1hdGNoaW5nIHRvIHRlYW0gbm90IGluIHRlYW1cbi0gZmluZCB0ZWFtIG9mIG1lbWJlciBiXG4tIG1vdmUgdGVhbSBmcm9tIHNldCBCIHRvIHNldCBBXG5cbiovXG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEtLTWF0Y2hSZXZlcnNlIHtcbiAgLy8gcHVibGljXG4gIGNvbnN0cnVjdG9yKHN0dWRlbnRzLCB0ZWFtaW5nQ29uZmlnLCBkZWJ1ZyA9IGZhbHNlKSB7XG4gICAgdGhpcy5zdHVkZW50cyA9IHN0dWRlbnRzLm1hcChzdHVkZW50ID0+IG5ldyBTdHVkZW50KHN0dWRlbnQsIHRlYW1pbmdDb25maWcpKTtcbiAgICB0aGlzLmRlYnVnID0gZGVidWc7XG5cbiAgICB0aGlzLmluaXRpYWxpemVBZGphY2VuY3lNYXRyaXgoKTtcblxuICAgIGlmICh0aGlzLmRlYnVnKSB7IGNvbnNvbGUubG9nKCdLS01hdGNoOiBSZW1vdmluZyBkdXBsaWNhdGVzLi4uJyk7IH1cblxuICAgIHRoaXMucmVtb3ZlRHVwbGljYXRlc1N0dWRlbnRzKCk7XG4gIH1cblxuICBnZXRTdHVkZW50cygpIHtcbiAgICByZXR1cm4gdGhpcy5zdHVkZW50cztcbiAgfVxuXG4gIGRldGVybWluZU51bWJlck9mVGVhbXMoKSB7XG4gICAgY29uc3QgbnVtYmVyT2ZTdHVkZW50cyA9IHRoaXMuZ2V0U3R1ZGVudHMoKS5sZW5ndGg7XG4gICAgY29uc3QgbWF4aW11bU51bWJlck9mVGVhbXMgPSBNYXRoLmZsb29yKG51bWJlck9mU3R1ZGVudHMgLyB0aGlzLm1pbmltdW1TdHVkZW50c1BlclRlYW0pO1xuICAgIGNvbnN0IG1pbWltdW1OdW1iZXJPZlRlYW1zID0gTWF0aC5jZWlsKG51bWJlck9mU3R1ZGVudHMgLyB0aGlzLm1heGltdW1TdHVkZW50c1BlclRlYW0pO1xuXG4gICAgY29uc3QgZGlmZmVyZW5jZSA9IG1heGltdW1OdW1iZXJPZlRlYW1zIC0gbWltaW11bU51bWJlck9mVGVhbXM7XG5cbiAgICByZXR1cm4gTWF0aC5yb3VuZCh0aGlzLmJhbGFuY2VGYWN0b3IgKiBkaWZmZXJlbmNlKSArIG1pbWltdW1OdW1iZXJPZlRlYW1zO1xuICB9XG5cbiAgY2FuQmVEaXN0cmlidXRlZChudW1iZXJPZlRlYW1zLCBtaW5TLCBtYXhTKSB7XG4gICAgLy8gY2FuIHRoaXMgbnVtYmVyIG9mIHRlYW1zIGJlIHRoZSBzdW0gb2YgbXVsdGlwbGUgbnVtYmVycyBkaXZpc2libGVcbiAgICAvLyBieSBhIG51bWJlciBiZXR3ZWVuIHRoZSBtaW4gYW5kIG1heFxuXG4gICAgY29uc3QgYXZnU3R1ZGVudHNQZXJUZWFtID0gKHRoaXMuc3R1ZGVudHMubGVuZ3RoIC8gbnVtYmVyT2ZUZWFtcyk7XG4gICAgcmV0dXJuIGF2Z1N0dWRlbnRzUGVyVGVhbSA8PSBtYXhTICYmIGF2Z1N0dWRlbnRzUGVyVGVhbSA+PSBtaW5TO1xuXG4gICAgLy8gY29uc3QgcG90ZW50aWFsRGl2aXNvcnMgPSBbXTtcbiAgICAvLyBmb3IgKGxldCBpID0gbWluUzsgaSA8IG1heFMgKyAxOyBpICs9IDEpIHtcbiAgICAvLyAgIHBvdGVudGlhbERpdmlzb3JzLnB1c2goaSk7XG4gICAgLy8gfVxuICAgIC8vIHBvdGVudGlhbERpdmlzb3JzLnNvcnQoKTtcblxuICAgIC8vIC8vIHdlIGhhdmUgWzMsIDQsIDVdIGZvciBpbnN0YW5jZVxuXG4gICAgLy8gY29uc3QgcG90ZW50aWFsRGl2aXNvciA9IHBvdGVudGlhbERpdmlzb3JzLnBvcCgpOyAvLyBiaWdnZXN0IGRpdmlzb3JcblxuICAgIC8vIGlmIChudW1iZXJPZlRlYW1zQ29weSAlIHBvdGVudGlhbERpdmlzb3IgPT09IDApIHtcbiAgICAvLyAgIHJldHVybiB0cnVlO1xuICAgIC8vIH1cblxuICAgIC8vIGxldCBuZXdOdW1iZXJPZlRlYW1zID0gbnVtYmVyT2ZUZWFtc0NvcHkgLSBwb3RlbnRpYWxEaXZpc29yO1xuICAgIC8vIHdoaWxlIChuZXdOdW1iZXJPZlRlYW1zID4gMCkge1xuICAgIC8vICAgY29uc3QgcmVtYWluZGVyID0gKG5ld051bWJlck9mVGVhbXMgJSBwb3RlbnRpYWxEaXZpc29yKSArIHBvdGVudGlhbERpdmlzb3I7XG5cbiAgICAvLyAgIC8vIGlzIHJlbWFpbmRlciBkaXZpc2libGUgYnkgYW55IG90aGVyIGRpdmlzb3JzXG4gICAgLy8gICBmb3IgKGxldCBpID0gMDsgaSA8IHBvdGVudGlhbERpdmlzb3JzLmxlbmd0aDsgaSArPSAxKSB7XG4gICAgLy8gICAgIGlmIChyZW1haW5kZXIgJSBwb3RlbnRpYWxEaXZpc29yc1tpXSA9PT0gMCkge1xuICAgIC8vICAgICAgIHJldHVybiB0cnVlO1xuICAgIC8vICAgICB9XG4gICAgLy8gICB9XG5cbiAgICAvLyAgIG5ld051bWJlck9mVGVhbXMgLT0gcG90ZW50aWFsRGl2aXNvcjtcbiAgICAvLyB9XG5cbiAgICAvLyByZXR1cm4gZmFsc2U7XG4gIH1cblxuICBpbmNyZW1lbWVudE4obnVtYmVyT2ZUZWFtcykge1xuICAgIGlmICh0aGlzLmJhbGFuY2VGYWN0b3IgPCAwLjUpIHtcbiAgICAgIHJldHVybiBudW1iZXJPZlRlYW1zIC0gMTtcbiAgICB9XG5cbiAgICByZXR1cm4gbnVtYmVyT2ZUZWFtcyArIDE7XG4gIH1cblxuICBpbml0aWFsaXplU2V0cygpIHtcbiAgICB0aGlzLnZpc2l0ZWRTdHVkZW50cyA9IHRoaXMuZ2V0U3R1ZGVudHMoKS5tYXAoKCkgPT4gZmFsc2UpO1xuXG4gICAgdGhpcy50aGVUZWFtcyA9IFtdO1xuICAgIHRoaXMudGVhbUlzRnVsbCA9IFtdO1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5udW1iZXJPZlRlYW1zOyBpICs9IDEpIHtcbiAgICAgIHRoaXMudGhlVGVhbXMucHVzaChbXSk7XG4gICAgICB0aGlzLnRlYW1Jc0Z1bGwucHVzaChmYWxzZSk7XG4gICAgfVxuICB9XG5cbiAgaGFzVW52aXNpdGVkU3R1ZGVudHMoKSB7XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLnZpc2l0ZWRTdHVkZW50cy5sZW5ndGg7IGkgKz0gMSkge1xuICAgICAgaWYgKHRoaXMudmlzaXRlZFN0dWRlbnRzW2ldID09PSBmYWxzZSkge1xuICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gZmFsc2U7XG4gIH1cblxuICBzaXplT2ZUZWFtKHRlYW0pIHtcbiAgICByZXR1cm4gdGhpcy50aGVUZWFtc1t0ZWFtXS5sZW5ndGg7XG4gIH1cblxuICBhcmJpdHJhcmlseVNlbGVjdFRlYW1Gcm9tU2V0QigpIHtcbiAgICBjb25zdCBzaXplT2ZTZXRCID0gdGhpcy50ZWFtSXNGdWxsLmxlbmd0aDtcbiAgICBsZXQgYXJiaXRyYXJ5U2VsZWN0aW9uSW5kZXg7XG4gICAgZG8ge1xuICAgICAgYXJiaXRyYXJ5U2VsZWN0aW9uSW5kZXggPSBNYXRoLnJvdW5kKE1hdGgucmFuZG9tKCkgKiAoc2l6ZU9mU2V0QiAtIDEpKTtcbiAgICB9IHdoaWxlIChcbiAgICAgIHRoaXMudGVhbUlzRnVsbFthcmJpdHJhcnlTZWxlY3Rpb25JbmRleF1cbiAgICApO1xuXG4gICAgcmV0dXJuIGFyYml0cmFyeVNlbGVjdGlvbkluZGV4O1xuICB9XG5cbiAgbWVtYmVyV2l0aEJlc3RTY29yZUluU2V0VSgpIHtcbiAgICAvLyBzaW5jZSBpdHMgZW1wdHkgaXQgbXVzdCBiZSByYW5kb21cbiAgICBsZXQgcmFuZG9tU3R1ZGVudDtcbiAgICBkbyB7XG4gICAgICByYW5kb21TdHVkZW50ID0gTWF0aC5yb3VuZCgodGhpcy52aXNpdGVkU3R1ZGVudHMubGVuZ3RoIC0gMSkgKiBNYXRoLnJhbmRvbSgpKTtcbiAgICB9IHdoaWxlICh0aGlzLnZpc2l0ZWRTdHVkZW50c1tyYW5kb21TdHVkZW50XSA9PT0gdHJ1ZSk7XG5cbiAgICByZXR1cm4gcmFuZG9tU3R1ZGVudDtcbiAgfVxuXG4gIG1lbWJlcldpdGhCZXN0TWF0Y2hpbmdJblNldFUodGVhbSkge1xuICAgIGxldCBiZXN0TWVtYmVyID0gLTE7XG4gICAgbGV0IGJlc3RTY29yZSA9IC0xO1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy52aXNpdGVkU3R1ZGVudHMubGVuZ3RoOyBpICs9IDEpIHtcbiAgICAgIGlmICghdGhpcy52aXNpdGVkU3R1ZGVudHNbaV0pIHtcbiAgICAgICAgLy8gc3R1ZGVudCBpcyB1bnZpc2l0ZWRcbiAgICAgICAgY29uc3QgdGVhbUNvcHkgPSBBcnJheS5mcm9tKHRoaXMudGhlVGVhbXNbdGVhbV0pO1xuICAgICAgICB0ZWFtQ29weS5wdXNoKGkpO1xuICAgICAgICBjb25zdCBzY29yZSA9IHRoaXMuc2NvcmVUZWFtKHRlYW1Db3B5KTtcblxuICAgICAgICBpZiAoc2NvcmUgPiBiZXN0U2NvcmUpIHtcbiAgICAgICAgICBiZXN0U2NvcmUgPSBzY29yZTtcbiAgICAgICAgICBiZXN0TWVtYmVyID0gaTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuICAgIGlmIChiZXN0TWVtYmVyID09PSAtMSkge1xuICAgICAgdGhyb3cgbmV3IEVycm9yKCdObyB1bnZpc2l0ZWQgc3R1ZGVudHMnKTtcbiAgICB9XG5cbiAgICByZXR1cm4gYmVzdE1lbWJlcjtcbiAgfVxuXG4gIGFkZE1lbWJlclRvVGVhbShtZW1iZXIsIHRlYW0pIHtcbiAgICB0aGlzLnRoZVRlYW1zW3RlYW1dLnB1c2gobWVtYmVyKTtcbiAgfVxuXG4gIG1vdmVNZW1iZXJGcm9tU2V0VVRvU2V0VihtZW1iZXIpIHtcbiAgICB0aGlzLnZpc2l0ZWRTdHVkZW50c1ttZW1iZXJdID0gdHJ1ZTtcbiAgfVxuXG4gIG1vdmVUZWFtRnJvbVNldEJUb1NldEEodGVhbSkge1xuICAgIHRoaXMudGVhbUlzRnVsbFt0ZWFtXSA9IHRydWU7XG4gIH1cblxuICBtb3ZlVGVhbUZyb21TZXRBVG9TZXRCKHRlYW0pIHtcbiAgICB0aGlzLnRlYW1Jc0Z1bGxbdGVhbV0gPSBmYWxzZTtcbiAgfVxuXG4gIHJ1bkdyZWVkeUluc2VydGlvblN0ZXAoKSB7XG4gICAgY29uc3QgdGVhbUluUXVlc3Rpb24gPSB0aGlzLmFyYml0cmFyaWx5U2VsZWN0VGVhbUZyb21TZXRCKCk7XG5cbiAgICBsZXQgc2VsZWN0ZWRTdHVkZW50ID0gLTE7XG4gICAgaWYgKHRoaXMuc2l6ZU9mVGVhbSh0ZWFtSW5RdWVzdGlvbikgPT09IDApIHtcbiAgICAgIHNlbGVjdGVkU3R1ZGVudCA9IHRoaXMubWVtYmVyV2l0aEJlc3RTY29yZUluU2V0VSgpO1xuICAgIH0gZWxzZSB7XG4gICAgICBzZWxlY3RlZFN0dWRlbnQgPSB0aGlzLm1lbWJlcldpdGhCZXN0TWF0Y2hpbmdJblNldFUodGVhbUluUXVlc3Rpb24pO1xuICAgIH1cblxuICAgIGlmIChzZWxlY3RlZFN0dWRlbnQgPT09IC0xKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ1RoZXJlIHNob3VsZCBiZSBzdHVkZW50cyBsZWZ0IGJ1dCB0aGVyZSBhcmUgbm90Jyk7XG4gICAgfVxuXG4gICAgdGhpcy5hZGRNZW1iZXJUb1RlYW0oc2VsZWN0ZWRTdHVkZW50LCB0ZWFtSW5RdWVzdGlvbik7XG4gICAgdGhpcy5tb3ZlTWVtYmVyRnJvbVNldFVUb1NldFYoc2VsZWN0ZWRTdHVkZW50KTtcblxuICAgIGlmICh0aGlzLnNpemVPZlRlYW0odGVhbUluUXVlc3Rpb24pID49IHRoaXMubWF4aW11bVN0dWRlbnRzUGVyVGVhbSkge1xuICAgICAgdGhpcy5tb3ZlVGVhbUZyb21TZXRCVG9TZXRBKHRlYW1JblF1ZXN0aW9uKTtcbiAgICB9XG4gIH1cblxuICBoYXNUZWFtc0luU2V0QlVuZGVyTWluaW11bSgpIHtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMudGVhbUlzRnVsbC5sZW5ndGg7IGkgKz0gMSkge1xuICAgICAgaWYgKCF0aGlzLnRlYW1Jc0Z1bGxbaV0gJiYgdGhpcy50aGVUZWFtc1tpXS5sZW5ndGggPCB0aGlzLm1pbmltdW1TdHVkZW50c1BlclRlYW0pIHtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgbWVtYmVyV2l0aEJlc3RTY29yZUluVk9uVGVhbUluQSgpIHtcbiAgICBsZXQgcmFuZG9tVGVhbTtcbiAgICBkbyB7XG4gICAgICByYW5kb21UZWFtID0gTWF0aC5yb3VuZCgodGhpcy50ZWFtSXNGdWxsLmxlbmd0aCAtIDEpICogTWF0aC5yYW5kb20oKSk7XG4gICAgfSB3aGlsZSAodGhpcy50ZWFtSXNGdWxsW3JhbmRvbVRlYW1dID09PSBmYWxzZSk7XG5cbiAgICAvLyBub3cgdGVhbSBzaG91bGQgYmUgYSBmdWxsIHRlYW1cbiAgICBjb25zdCByYW5kb21TdHVkZW50ID0gTWF0aC5yb3VuZCgodGhpcy50aGVUZWFtc1tyYW5kb21UZWFtXS5sZW5ndGggLSAxKSAqIE1hdGgucmFuZG9tKCkpO1xuXG4gICAgcmV0dXJuIHRoaXMudGhlVGVhbXNbcmFuZG9tVGVhbV1bcmFuZG9tU3R1ZGVudF07XG4gIH1cblxuICBpc01lbWJlck9uVGVhbShtZW1iZXIsIHRlYW0pIHtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMudGhlVGVhbXNbdGVhbV0ubGVuZ3RoOyBpICs9IDEpIHtcbiAgICAgIGlmICh0aGlzLnRoZVRlYW1zW3RlYW1dW2ldID09PSBtZW1iZXIpIHtcbiAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG5cbiAgZmluZFRlYW1JbkFPZk1lbWJlckluVihtZW1iZXIpIHtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMudGhlVGVhbXMubGVuZ3RoOyBpICs9IDEpIHtcbiAgICAgIGlmICh0aGlzLnRlYW1Jc0Z1bGxbaV0pIHtcbiAgICAgICAgLy8gdGVhbSBpcyBhIGZ1bGwgdGVhbVxuICAgICAgICBpZiAodGhpcy5pc01lbWJlck9uVGVhbShtZW1iZXIsIGkpKSB7XG4gICAgICAgICAgcmV0dXJuIGk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gLTE7XG4gIH1cblxuICByZW1vdmVNZW1iZXJGcm9tVGVhbShtZW1iZXIsIHRlYW0pIHtcbiAgICBjb25zdCB0aGVUZWFtID0gdGhpcy50aGVUZWFtc1t0ZWFtXTtcbiAgICBjb25zdCB0aGVOZXdUZWFtID0gW107XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGVUZWFtLmxlbmd0aDsgaSArPSAxKSB7XG4gICAgICBpZiAodGhlVGVhbVtpXSAhPT0gbWVtYmVyKSB7XG4gICAgICAgIHRoZU5ld1RlYW0ucHVzaCh0aGVUZWFtW2ldKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICB0aGlzLnRoZVRlYW1zW3RlYW1dID0gdGhlTmV3VGVhbTtcbiAgfVxuXG4gIG1lbWJlcldpdGhCZXN0TWF0Y2hpbmdJblZPblRlYW1JbkFOb3RPblRlYW0odGVhbSkge1xuICAgIGxldCBiZXN0TWVtYmVyID0gLTE7XG4gICAgbGV0IGJlc3RTY29yZSA9IC0xO1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy52aXNpdGVkU3R1ZGVudHMubGVuZ3RoOyBpICs9IDEpIHtcbiAgICAgIGlmICh0aGlzLnZpc2l0ZWRTdHVkZW50c1tpXSAmJiB0aGlzLmZpbmRUZWFtSW5BT2ZNZW1iZXJJblYoaSkgIT09IC0xICYmICF0aGlzLmlzTWVtYmVyT25UZWFtKGksIHRlYW0pKSB7XG4gICAgICAgIC8vIHN0dWRlbnQgaXMgdmlzaXRlZCBhbmQgbm90IG9uIHRlYW0gdGVhbVxuICAgICAgICBjb25zdCB0ZWFtQ29weSA9IEFycmF5LmZyb20odGhpcy50aGVUZWFtc1t0ZWFtXSk7XG4gICAgICAgIHRlYW1Db3B5LnB1c2goaSk7XG4gICAgICAgIGNvbnN0IHNjb3JlID0gdGhpcy5zY29yZVRlYW0odGVhbUNvcHkpO1xuXG4gICAgICAgIGlmIChzY29yZSA+IGJlc3RTY29yZSkge1xuICAgICAgICAgIGJlc3RTY29yZSA9IHNjb3JlO1xuICAgICAgICAgIGJlc3RNZW1iZXIgPSBpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgaWYgKGJlc3RNZW1iZXIgPT09IC0xKSB7XG4gICAgICB0aHJvdyBuZXcgRXJyb3IoJ05vIHVudmlzaXRlZCBzdHVkZW50cycpO1xuICAgIH1cblxuICAgIHJldHVybiBiZXN0TWVtYmVyO1xuICB9XG5cbiAgZ3JlZWRpbHlGaXhUZWFtcygpIHtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMudGVhbUlzRnVsbC5sZW5ndGg7IGkgKz0gMSkge1xuICAgICAgaWYgKCF0aGlzLnRlYW1Jc0Z1bGxbaV0gJiYgdGhpcy50aGVUZWFtc1tpXS5sZW5ndGggPCB0aGlzLm1pbmltdW1TdHVkZW50c1BlclRlYW0pIHtcbiAgICAgICAgLy8gd2UgaGF2ZSBhIHRlYW0gdGhhdCBuZWVkcyB0byBiZSBmaXhpZWRcblxuICAgICAgICB3aGlsZSAodGhpcy50aGVUZWFtc1tpXS5sZW5ndGggPCB0aGlzLm1pbmltdW1TdHVkZW50c1BlclRlYW0pIHtcbiAgICAgICAgICBsZXQgc2VsZWN0ZWRTdHVkZW50ID0gLTE7XG4gICAgICAgICAgaWYgKHRoaXMudGhlVGVhbXNbaV0ubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICBzZWxlY3RlZFN0dWRlbnQgPSB0aGlzLm1lbWJlcldpdGhCZXN0U2NvcmVJblZPblRlYW1JbkEoKTtcbiAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgc2VsZWN0ZWRTdHVkZW50ID0gdGhpcy5tZW1iZXJXaXRoQmVzdE1hdGNoaW5nSW5WT25UZWFtSW5BTm90T25UZWFtKGkpO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIGlmIChzZWxlY3RlZFN0dWRlbnQgPT09IC0xKSB7XG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1NvbWV0aGluZyBpcyB3cm9uZycpO1xuICAgICAgICAgIH1cblxuICAgICAgICAgIGNvbnN0IHNlbGVjdGVkU3R1ZGVudFRlYW0gPSB0aGlzLmZpbmRUZWFtSW5BT2ZNZW1iZXJJblYoc2VsZWN0ZWRTdHVkZW50KTtcbiAgICAgICAgICBpZiAoc2VsZWN0ZWRTdHVkZW50VGVhbSA9PT0gLTEpIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKHRoaXMudGhlVGVhbXNbaV0ubGVuZ3RoKTtcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignU29tZXRoaW5nIGlzIHdyb25nIHRlYW0nKTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICB0aGlzLnJlbW92ZU1lbWJlckZyb21UZWFtKHNlbGVjdGVkU3R1ZGVudCwgc2VsZWN0ZWRTdHVkZW50VGVhbSk7XG4gICAgICAgICAgdGhpcy5tb3ZlVGVhbUZyb21TZXRBVG9TZXRCKHNlbGVjdGVkU3R1ZGVudFRlYW0pO1xuICAgICAgICAgIHRoaXMuYWRkTWVtYmVyVG9UZWFtKHNlbGVjdGVkU3R1ZGVudCwgaSk7XG4gICAgICAgICAgaWYgKHRoaXMuc2l6ZU9mVGVhbShpKSA+PSB0aGlzLm1heGltdW1TdHVkZW50c1BlclRlYW0pIHtcbiAgICAgICAgICAgIHRoaXMubW92ZVRlYW1Gcm9tU2V0QlRvU2V0QShpKTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICB0ZWFtKHsgbWluaW11bVN0dWRlbnRzUGVyVGVhbSA9IDMsIG1heGltdW1TdHVkZW50c1BlclRlYW0gPSA0LCBiYWxhbmNlRmFjdG9yID0gMyB9KSB7XG4gICAgdGhpcy5taW5pbXVtU3R1ZGVudHNQZXJUZWFtID0gbWluaW11bVN0dWRlbnRzUGVyVGVhbTtcbiAgICB0aGlzLm1heGltdW1TdHVkZW50c1BlclRlYW0gPSBtYXhpbXVtU3R1ZGVudHNQZXJUZWFtO1xuICAgIHRoaXMuYmFsYW5jZUZhY3RvciA9IGJhbGFuY2VGYWN0b3IgLyAxMC4wO1xuXG4gICAgaWYgKHRoaXMuZGVidWcpIHsgY29uc29sZS5sb2coJ0tLTWF0Y2hSZXZlcnNlOiB0ZWFtaW5nLi4uJyk7IGNvbnNvbGUudGltZSgnS0tNYXRjaFJldmVyc2UnKTsgfVxuXG4gICAgdGhpcy5udW1iZXJPZlRlYW1zID0gdGhpcy5kZXRlcm1pbmVOdW1iZXJPZlRlYW1zKCk7XG4gICAgd2hpbGUgKCF0aGlzLmNhbkJlRGlzdHJpYnV0ZWQoXG4gICAgICB0aGlzLm51bWJlck9mVGVhbXMsXG4gICAgICB0aGlzLm1pbmltdW1TdHVkZW50c1BlclRlYW0sXG4gICAgICB0aGlzLm1heGltdW1TdHVkZW50c1BlclRlYW0sXG4gICAgKSkge1xuICAgICAgdGhpcy5udW1iZXJPZlRlYW1zID0gdGhpcy5pbmNyZW1lbWVudE4odGhpcy5udW1iZXJPZlRlYW1zKTtcbiAgICB9XG5cbiAgICB0aGlzLmluaXRpYWxpemVTZXRzKCk7XG4gICAgd2hpbGUgKHRoaXMuaGFzVW52aXNpdGVkU3R1ZGVudHMoKSkge1xuICAgICAgdGhpcy5ydW5HcmVlZHlJbnNlcnRpb25TdGVwKCk7XG4gICAgfVxuXG4gICAgd2hpbGUgKHRoaXMuaGFzVGVhbXNJblNldEJVbmRlck1pbmltdW0oKSkge1xuICAgICAgdGhpcy5ncmVlZGlseUZpeFRlYW1zKCk7XG4gICAgfVxuXG5cbiAgICBpZiAodGhpcy5kZWJ1ZykgeyBjb25zb2xlLmxvZygnS0tNYXRjaFJldmVyc2U6IGRvbmUuLi4nKTsgY29uc29sZS50aW1lRW5kKCdLS01hdGNoUmV2ZXJzZScpOyB9XG5cbiAgICBpZiAodGhpcy5kZWJ1ZykgeyBjb25zb2xlLmxvZygnS0tNYXRjaFJldmVyc2U6IGdlbmVyYXRpbmcgdGVhbXMgZnJvbSBtYXRjaC4uLicpOyB9XG4gICAgcmV0dXJuIHRoaXMucGF0aFRvVGVhbXModGhpcy50aGVUZWFtcyk7XG4gIH1cblxuICBwYXRoVG9UZWFtcyhzb2x1dGlvblBhdGgpIHtcbiAgICBjb25zdCB0ZWFtcyA9IFtdO1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgc29sdXRpb25QYXRoLmxlbmd0aDsgaSArPSAxKSB7XG4gICAgICBjb25zdCB0ZWFtID0gc29sdXRpb25QYXRoW2ldLm1hcChqID0+IHRoaXMuc3R1ZGVudHNbal0pO1xuICAgICAgdGVhbXMucHVzaChuZXcgVGVhbSh0ZWFtKSk7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRlYW1zO1xuICB9XG5cblxuICAvLyBwcml2YXRlXG4gIHJlbW92ZUR1cGxpY2F0ZXNTdHVkZW50cygpIHtcbiAgICBjb25zdCBkdXBBcnJheSA9IHt9O1xuICAgIHRoaXMuZ2V0U3R1ZGVudHMoKS5mb3JFYWNoKChzdHVkZW50KSA9PiB7XG4gICAgICBkdXBBcnJheVtzdHVkZW50Lmhhc2hdID0gc3R1ZGVudDtcbiAgICB9KTtcblxuICAgIHRoaXMuc3R1ZGVudHMgPSBPYmplY3QudmFsdWVzKGR1cEFycmF5KTtcbiAgfVxuXG4gIHNjb3JlVGVhbSh0ZWFtKSB7XG4gICAgLy8gcGFpcndpc2UgdGVhbSBhbGwgbWVtYmVycyBhZ2FpbnN0IGVhY2ggb3RoZXJcbiAgICBpZiAodGVhbS5sZW5ndGggPD0gMSkge1xuICAgICAgcmV0dXJuIDA7XG4gICAgfVxuXG4gICAgbGV0IHNjb3JlID0gMDtcbiAgICBsZXQgbnVtUGVybXMgPSAwO1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGVhbS5sZW5ndGg7IGkgKz0gMSkge1xuICAgICAgZm9yIChsZXQgaiA9IGkgKyAxOyBqIDwgdGVhbS5sZW5ndGg7IGogKz0gMSkge1xuICAgICAgICBzY29yZSArPSB0aGlzLnNjb3JlU3R1ZGVudHModGVhbVtpXSwgdGVhbVtqXSk7XG4gICAgICAgIG51bVBlcm1zICs9IDE7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIHNjb3JlIC8gbnVtUGVybXM7XG4gIH1cblxuICBzY29yZVN0dWRlbnRzKGEsIGIpIHtcbiAgICByZXR1cm4gdGhpcy5zY29yZWRBZ2FpbnN0W2FdW2JdO1xuICB9XG5cbiAgaW5pdGlhbGl6ZUFkamFjZW5jeU1hdHJpeCgpIHtcbiAgICB0aGlzLnNjb3JlZEFnYWluc3QgPSBbXTtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuc3R1ZGVudHMubGVuZ3RoOyBpICs9IDEpIHtcbiAgICAgIHRoaXMuc2NvcmVkQWdhaW5zdC5wdXNoKFtdKTtcbiAgICAgIGZvciAobGV0IGogPSAwOyBqIDwgdGhpcy5zdHVkZW50cy5sZW5ndGg7IGogKz0gMSkge1xuICAgICAgICBpZiAoaSAhPT0gaikge1xuICAgICAgICAgIHRoaXMuc2NvcmVkQWdhaW5zdFtpXS5wdXNoKHRoaXMuc3R1ZGVudHNbaV0uc2NvcmVBZ2FpbnN0KHRoaXMuc3R1ZGVudHNbal0pKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB0aGlzLnNjb3JlZEFnYWluc3RbaV0ucHVzaCgtMSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cbiJdLCJzb3VyY2VSb290IjoiL21udC9jL1VzZXJzL3B1cmV1L0RvY3VtZW50cy9wcm9qZWN0cy9sYXVuY2h4LXRlYW1pbmcvc3JjIn0=
