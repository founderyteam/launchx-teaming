'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); /* eslint-disable no-console */
/* eslint-disable no-continue */


var _student = require('./student');

var _student2 = _interopRequireDefault(_student);

var _team = require('./team');

var _team2 = _interopRequireDefault(_team);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// basically stars and bars
// we have students and some seperators for them
// we need an inital estimate tho

var KKMatch = function () {
  // public
  function KKMatch(students, teamingConfig) {
    var debug = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

    _classCallCheck(this, KKMatch);

    this.students = students.map(function (student) {
      return new _student2.default(student, teamingConfig);
    });
    this.debug = debug;

    this.initializeAdjacencyMatrix();

    if (this.debug) {
      console.log('KKMatch: Removing duplicates...');
    }

    this.removeDuplicatesStudents();
  }

  _createClass(KKMatch, [{
    key: 'getStudents',
    value: function getStudents() {
      return this.students;
    }
  }, {
    key: 'initializeUnvisitedSet',
    value: function initializeUnvisitedSet() {
      this.setU = this.students.map(function () {
        return true;
      });
      this.numVisited = 0;
    }
  }, {
    key: 'formCluster',
    value: function formCluster(vertex) {
      var bestCluster = this.formClusterOfSize(vertex, this.minimumStudentsPerTeam);
      var bestClusterScore = this.scoreTeam(bestCluster);
      for (var i = this.minimumStudentsPerTeam + 1; i < this.maximumStudentsPerTeam + 1; i += 1) {
        var cluster = this.formClusterOfSize(vertex, i);
        var score = this.scoreTeam(cluster);
        if (score > bestClusterScore) {
          bestCluster = cluster;
          bestClusterScore = score;
        }
      }

      return bestCluster;
    }
  }, {
    key: 'formClusterOfSize',
    value: function formClusterOfSize(vertex, s) {
      var setS = [vertex];

      // find s - 1 closest verticies in U test both min up to max
      // so find s - 1 verticies with largest edgewise scores
      // s in this case is the minimum

      for (var i = 0; i < s - 1; i += 1) {
        var mostOptimalEdge = -1;
        var mostOptimalScore = -1;

        for (var j = 0; j < this.setU.length; j += 1) {
          if (this.setU[j] === true && j !== vertex && setS.indexOf(j) === -1) {
            // for each unvisited vertex j where j != vertex
            // find most optimal edge
            var score = this.scoredAgainst[vertex][j];
            if (score > mostOptimalScore) {
              mostOptimalScore = score;
              mostOptimalEdge = j;
            }
          }
        }

        if (mostOptimalEdge > -1) {
          setS.push(mostOptimalEdge);
        }
      }

      return setS;
    }

    // formCluster (vertex) {
    //   const setS = [vertex];

    //   // find s - 1 closest verticies in U
    //   // so find s - 1 verticies with largest edgewise scores
    //   // s in this case is the minimum
    //   const s = this.minimumStudentsPerTeam;

    //   for(let i = 0; i < s - 1; i += 1) {
    //     let mostOptimalEdge = -1;
    //     let mostOptimalScore = -1;

    //     for(let j = 0; j < this.setU.length; j += 1) {
    //       if(this.setU[j] === true && j !== vertex && setS.indexOf(j) === -1) {

    //         // for each unvisited vertex j where j != vertex
    //         // find most optimal edge
    //         const score = this.scoredAgainst[vertex][j];
    //         if(score > mostOptimalScore) {
    //           mostOptimalScore = score;
    //           mostOptimalEdge = j;
    //         }
    //       }
    //     }

    //     setS.push(mostOptimalEdge);
    //   }

    //   return setS;
    // }

  }, {
    key: 'visitVertex',
    value: function visitVertex(v) {
      this.setU[v] = false;
      this.numVisited += 1;
    }
  }, {
    key: 'initializeClusters',
    value: function initializeClusters() {
      this.clusters = [];
    }

    // farthest in this case means minimal compatibility
    // dont forget about pidgenhole

  }, {
    key: 'edgewiseFarthestUnvisitedVerticies',
    value: function edgewiseFarthestUnvisitedVerticies() {
      if (this.numVisited === this.students.length - 1) {
        // pidgenhole
        return [];
      }

      var lowestScore = -1;
      var lowestI = -1;
      var lowestJ = -1;
      // traverse all unvisited verticies
      for (var i = 0; i < this.setU.length; i += 1) {
        if (this.setU[i] === true) {
          // if unvisited
          // pairwise traverse other unvisited in adjacency matrix
          for (var j = 0; j < this.scoredAgainst[i].length; j += 1) {
            var score = this.scoredAgainst[i][j];
            if (this.setU[j] === true && i !== j) {
              // if this is also unvisisted
              if (score < lowestScore || lowestScore === -1) {
                lowestScore = score;
                lowestI = i;
                lowestJ = j;
              }
            }
          }
        }
      }

      return [lowestI, lowestJ];
    }
  }, {
    key: 'removeSetFromU',
    value: function removeSetFromU(set) {
      for (var i = 0; i < set.length; i += 1) {
        this.visitVertex(set[i]);
      }
    }
  }, {
    key: 'initializeAdjacencyMatrix',
    value: function initializeAdjacencyMatrix() {
      this.scoredAgainst = [];
      for (var i = 0; i < this.students.length; i += 1) {
        this.scoredAgainst.push([]);
        for (var j = 0; j < this.students.length; j += 1) {
          if (i !== j) {
            this.scoredAgainst[i].push(this.students[i].scoreAgainst(this.students[j]));
          } else {
            this.scoredAgainst[i].push(-1);
          }
        }
      }
    }
  }, {
    key: 'determineDistribution',
    value: function determineDistribution() {
      var _this = this;

      // this function will preteam all the unvisited members if they are over a certain standard
      // in this instance, that standard is... entrepreneur type

      // first, detect if this is neccesary
      // we will use 2/5 as an example

      var unvisited = this.allUnvisitedVerticies();
      var allEntrepreneurTypes = unvisited.map(function (studentID) {
        return _this.students[studentID].launchSkills;
      });
      var uniqueEntrepreneurTypes = Object.keys(allEntrepreneurTypes.reduce(function (hashmap, types) {
        types.forEach(function (type) {
          // eslint-disable-next-line no-param-reassign
          hashmap[type] = true;
        });
        return hashmap;
      }, {}));

      var amountPerType = uniqueEntrepreneurTypes.map(function (type) {
        var amt = 0;
        for (var i = 0; i < allEntrepreneurTypes.length; i += 1) {
          var studentTypes = allEntrepreneurTypes[i] || [];
          if (studentTypes.indexOf(type) > -1) {
            amt += 1;
          }
        }

        return { type: type, amt: amt };
      });

      amountPerType.sort(function (a, b) {
        return a.amt - b.amt;
      });

      var topType = amountPerType.pop();
      if (topType.amt < 2 / 5 * this.students.length) {
        // ok we dont need to distribute
        return;
      }

      console.log('distributing...');
      // get all of the students with that type
      var studentsOfType = unvisited.filter(function (studentID) {
        return _this.students[studentID].launchSkills.indexOf(topType.type) > -1;
      });

      // distribute and visit these students
      while (studentsOfType.length > 0 && this.clusters.length <= this.students.length / 2.0) {
        var studentID = studentsOfType.pop();
        this.clusters.push([studentID]);
        this.visitVertex(studentID);
      }

      // if there are over half however,
      // then we must distribute multiple to same team, so we can just let the alg do this
    }
  }, {
    key: 'team',
    value: function team(_ref) {
      var _ref$minimumStudentsP = _ref.minimumStudentsPerTeam,
          minimumStudentsPerTeam = _ref$minimumStudentsP === undefined ? 3 : _ref$minimumStudentsP,
          _ref$maximumStudentsP = _ref.maximumStudentsPerTeam,
          maximumStudentsPerTeam = _ref$maximumStudentsP === undefined ? 4 : _ref$maximumStudentsP;

      // gen perms
      // dp this shiz
      this.minimumStudentsPerTeam = minimumStudentsPerTeam;
      this.maximumStudentsPerTeam = maximumStudentsPerTeam;

      this.initializeUnvisitedSet();
      this.initializeClusters();

      if (this.debug) {
        console.log('KKMatch: teaming...');console.time('KKMatch');
      }

      // this.determineDistribution();

      while (this.numVisited < this.students.length) {
        this.approximate();
      }

      if (this.debug) {
        console.log('KKMatch: done...');console.timeEnd('KKMatch');
      }

      if (this.debug) {
        console.log('KKMatch: generating teams from match...');
      }
      return this.pathToTeams(this.clusters);
    }
  }, {
    key: 'allUnvisitedVerticies',
    value: function allUnvisitedVerticies() {
      var unvisited = [];
      for (var i = 0; i < this.students.length; i += 1) {
        if (this.setU[i] === true) {
          unvisited.push(i);
        }
      }

      return unvisited;
    }
  }, {
    key: 'closestNeighbor',
    value: function closestNeighbor(node) {
      // find closest neighbor that doesn't break constraints
      var closestNode = -1;
      var farthestScore = 0;
      var closestCluster = -1;
      var adjacentToNode = this.scoredAgainst[node];
      for (var i = 0; i < adjacentToNode.length; i += 1) {
        if (i !== node) {
          // all nodes not i
          var score = adjacentToNode[i];
          if (closestNode === -1 || score > farthestScore) {
            var cluster = this.findCluster(i);
            if (cluster !== -1 && this.clusters[cluster].length < this.maximumStudentsPerTeam) {
              closestNode = i;
              closestCluster = cluster;
              farthestScore = score;
            }
          }
        }
      }

      return { closestNeighbor: closestNode, closestCluster: closestCluster };
    }
  }, {
    key: 'findCluster',
    value: function findCluster(node) {
      for (var i = 0; i < this.clusters.length; i += 1) {
        for (var j = 0; j < this.clusters[i].length; j += 1) {
          if (this.clusters[i][j] === node) {
            return i;
          }
        }
      }

      return -1;
    }
  }, {
    key: 'calcNumVisited',
    value: function calcNumVisited() {
      return this.students.length - this.allUnvisitedVerticies().length;
    }
  }, {
    key: 'matchLast',
    value: function matchLast() {
      // const numUnvisited = this.setU.length - this.numVisited;
      // console.log(`unvisited: ${numUnvisited}`);
      var unvisited = this.allUnvisitedVerticies();

      if (unvisited.length <= 0) {
        return;
      }

      if (this.maximumStudentsPerTeam === this.minimumStudentsPerTeam) {
        // trim the difference into a new team
        var last = unvisited.pop();
        if (last) {
          this.clusters.push(last);
          this.visitVertex(last);
        }
      }

      for (var i = 0; i < unvisited.length; i += 1) {
        var _closestNeighbor = this.closestNeighbor(unvisited[i]),
            closestCluster = _closestNeighbor.closestCluster;
        // console.log(`closest to ${unvisited[i]}: ${closestCluster}`);


        if (closestCluster !== -1) {
          this.clusters[closestCluster].push(unvisited[i]);
          this.visitVertex(unvisited[i]);
        }
      }
    }
  }, {
    key: 'approximate',
    value: function approximate() {
      var mostDistantVerticies = this.edgewiseFarthestUnvisitedVerticies(); // step 2
      var clusterA = this.formCluster(mostDistantVerticies[0]);
      this.removeSetFromU(clusterA);
      var clusterB = this.formCluster(mostDistantVerticies[1]);
      this.removeSetFromU(clusterB);

      this.clusters.push(clusterA);
      this.clusters.push(clusterB);

      var numUnvisited = this.setU.length - this.numVisited;

      if (numUnvisited >= this.minimumStudentsPerTeam && numUnvisited < 2 * this.minimumStudentsPerTeam && this.minimumStudentsPerTeam !== this.maximumStudentsPerTeam) {
        // this is now a cluster
        var unvisitedRaw = this.allUnvisitedVerticies();
        var cluster = this.formCluster(unvisitedRaw[0]);

        this.clusters.push(cluster);
        this.removeSetFromU(cluster);

        numUnvisited = this.setU.length - this.numVisited;

        if (numUnvisited >= 0) {
          this.matchLast();
        }
      } else if (numUnvisited < this.minimumStudentsPerTeam) {
        // numVisited is less than minimum
        // nearest neighbor the overflow
        this.matchLast();
      }
    }

    // private

  }, {
    key: 'removeDuplicatesStudents',
    value: function removeDuplicatesStudents() {
      var dupArray = {};
      this.getStudents().forEach(function (student) {
        dupArray[student.hash] = student;
      });

      this.students = Object.values(dupArray);
    }
  }, {
    key: 'addNextStudent',
    value: function addNextStudent() {
      var studentIndex = this.nextUnvisitedStudent();

      var greatestIncreaseIndex = -1;
      var greatestIncrease = -1;
      var maxNumberOfRemainingTeams = Math.floor((this.students.length - this.numVisited) / this.minimumStudentsPerTeam);
      for (var i = 0; i < this.numTeams + maxNumberOfRemainingTeams; i += 1) {
        if (i < this.solutionPath.length && this.solutionPath[i].length < this.maximumStudentsPerTeam) {
          // test ass student
          var scoreBefore = this.scoreTeam(this.solutionPath[i]);
          this.solutionPath[i].push(studentIndex);
          var scoreAfter = this.scoreTeam(this.solutionPath[i]);
          this.solutionPath[i].pop();

          if (scoreAfter > scoreBefore && scoreAfter - scoreBefore > greatestIncrease) {
            greatestIncreaseIndex = i;
            greatestIncrease = scoreAfter - scoreBefore;
          }
        }
      }

      if (greatestIncrease === -1) {
        // if more teams than max teams, rematch
        if (this.numTeams >= this.numFilledTeams + maxNumberOfRemainingTeams) {
          // find team with least decrease
          var bestTeamIndex = 0;
          var leastDecrease = -1;
          for (var _i = 0; _i < this.solutionPath.length; _i += 1) {
            if (this.solutionPath[_i].length < this.maximumStudentsPerTeam) {
              // test ass student
              var _scoreBefore = this.scoreTeam(this.solutionPath[_i]);
              this.solutionPath[_i].push(studentIndex);
              var _scoreAfter = this.scoreTeam(this.solutionPath[_i]);
              this.solutionPath[_i].pop();

              if (leastDecrease === -1 || _scoreBefore - _scoreAfter < leastDecrease) {
                leastDecrease = _scoreBefore - _scoreAfter;
                bestTeamIndex = _i;
              }
            }
          }

          this.solutionPath[bestTeamIndex].push(studentIndex);
          if (this.solutionPath[bestTeamIndex].length >= this.maximumStudentsPerTeam) {
            this.numFilledTeams += 1;
          }
        } else {
          this.solutionPath.push([studentIndex]);
          this.numTeams += 1;
        }
      } else {
        this.solutionPath[greatestIncreaseIndex].push(studentIndex);
        if (this.solutionPath[greatestIncreaseIndex].length >= this.maximumStudentsPerTeam) {
          this.numFilledTeams += 1;
        }
      }

      this.visited[studentIndex] = true;
      this.numVisited += 1;
    }
  }, {
    key: 'nextUnvisitedStudent',
    value: function nextUnvisitedStudent() {
      var i = 0;
      while (this.visited[i]) {
        i += 1;
      }

      return i;
    }
  }, {
    key: 'pathToTeams',
    value: function pathToTeams(solutionPath) {
      var _this2 = this;

      var teams = [];
      for (var i = 0; i < solutionPath.length; i += 1) {
        var team = solutionPath[i].map(function (j) {
          return _this2.students[j];
        });
        teams.push(new _team2.default(team));
      }

      return teams;
    }
  }, {
    key: 'calculateScore',
    value: function calculateScore(solutionPath, permLength) {
      var firstBarIndex = -1;
      var nextBarIndex = 0;

      var score = 0;
      var findFunc = function findFunc(v, i) {
        return i > nextBarIndex && v.type === 'bar';
      };
      while (nextBarIndex !== permLength) {
        firstBarIndex = nextBarIndex;
        nextBarIndex = solutionPath.findIndex(findFunc);
        if (nextBarIndex === -1 || nextBarIndex > permLength) {
          nextBarIndex = permLength;
        }

        var team = [];
        for (var i = firstBarIndex + 1; i < nextBarIndex; i += 1) {
          team.push(solutionPath[i].student);
        }

        score += this.scoreTeam(team);
      }

      return score;
    }
  }, {
    key: 'scoreTeam',
    value: function scoreTeam(team) {
      // pairwise team all members against each other
      if (team.length <= 1) {
        return 0;
      }

      var score = 0;
      var numPerms = 0;
      for (var i = 0; i < team.length; i += 1) {
        for (var j = i + 1; j < team.length; j += 1) {
          score += this.scoreStudents(team[i], team[j]);
          numPerms += 1;
        }
      }

      return score / numPerms;
    }
  }, {
    key: 'scoreStudents',
    value: function scoreStudents(a, b) {
      return this.scoredAgainst[a][b];
    }
  }, {
    key: 'bestMatch',
    value: function bestMatch(studentIndex) {
      var bestElement = -1;
      var bestScore = 0;
      for (var i = 0; i < this.students.length; i += 1) {
        if (i !== studentIndex) {
          var score = this.scoredAgainst[studentIndex][i];
          if (score > bestScore) {
            bestElement = i;
            bestScore = score;
          }
        }
      }

      return bestElement;
    }
  }]);

  return KKMatch;
}();

exports.default = KKMatch;
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImtrLW1hdGNoZXIuanMiXSwibmFtZXMiOlsiS0tNYXRjaCIsInN0dWRlbnRzIiwidGVhbWluZ0NvbmZpZyIsImRlYnVnIiwibWFwIiwiU3R1ZGVudCIsInN0dWRlbnQiLCJpbml0aWFsaXplQWRqYWNlbmN5TWF0cml4IiwiY29uc29sZSIsImxvZyIsInJlbW92ZUR1cGxpY2F0ZXNTdHVkZW50cyIsInNldFUiLCJudW1WaXNpdGVkIiwidmVydGV4IiwiYmVzdENsdXN0ZXIiLCJmb3JtQ2x1c3Rlck9mU2l6ZSIsIm1pbmltdW1TdHVkZW50c1BlclRlYW0iLCJiZXN0Q2x1c3RlclNjb3JlIiwic2NvcmVUZWFtIiwiaSIsIm1heGltdW1TdHVkZW50c1BlclRlYW0iLCJjbHVzdGVyIiwic2NvcmUiLCJzIiwic2V0UyIsIm1vc3RPcHRpbWFsRWRnZSIsIm1vc3RPcHRpbWFsU2NvcmUiLCJqIiwibGVuZ3RoIiwiaW5kZXhPZiIsInNjb3JlZEFnYWluc3QiLCJwdXNoIiwidiIsImNsdXN0ZXJzIiwibG93ZXN0U2NvcmUiLCJsb3dlc3RJIiwibG93ZXN0SiIsInNldCIsInZpc2l0VmVydGV4Iiwic2NvcmVBZ2FpbnN0IiwidW52aXNpdGVkIiwiYWxsVW52aXNpdGVkVmVydGljaWVzIiwiYWxsRW50cmVwcmVuZXVyVHlwZXMiLCJzdHVkZW50SUQiLCJsYXVuY2hTa2lsbHMiLCJ1bmlxdWVFbnRyZXByZW5ldXJUeXBlcyIsIk9iamVjdCIsImtleXMiLCJyZWR1Y2UiLCJoYXNobWFwIiwidHlwZXMiLCJmb3JFYWNoIiwidHlwZSIsImFtb3VudFBlclR5cGUiLCJhbXQiLCJzdHVkZW50VHlwZXMiLCJzb3J0IiwiYSIsImIiLCJ0b3BUeXBlIiwicG9wIiwic3R1ZGVudHNPZlR5cGUiLCJmaWx0ZXIiLCJpbml0aWFsaXplVW52aXNpdGVkU2V0IiwiaW5pdGlhbGl6ZUNsdXN0ZXJzIiwidGltZSIsImFwcHJveGltYXRlIiwidGltZUVuZCIsInBhdGhUb1RlYW1zIiwibm9kZSIsImNsb3Nlc3ROb2RlIiwiZmFydGhlc3RTY29yZSIsImNsb3Nlc3RDbHVzdGVyIiwiYWRqYWNlbnRUb05vZGUiLCJmaW5kQ2x1c3RlciIsImNsb3Nlc3ROZWlnaGJvciIsImxhc3QiLCJtb3N0RGlzdGFudFZlcnRpY2llcyIsImVkZ2V3aXNlRmFydGhlc3RVbnZpc2l0ZWRWZXJ0aWNpZXMiLCJjbHVzdGVyQSIsImZvcm1DbHVzdGVyIiwicmVtb3ZlU2V0RnJvbVUiLCJjbHVzdGVyQiIsIm51bVVudmlzaXRlZCIsInVudmlzaXRlZFJhdyIsIm1hdGNoTGFzdCIsImR1cEFycmF5IiwiZ2V0U3R1ZGVudHMiLCJoYXNoIiwidmFsdWVzIiwic3R1ZGVudEluZGV4IiwibmV4dFVudmlzaXRlZFN0dWRlbnQiLCJncmVhdGVzdEluY3JlYXNlSW5kZXgiLCJncmVhdGVzdEluY3JlYXNlIiwibWF4TnVtYmVyT2ZSZW1haW5pbmdUZWFtcyIsIk1hdGgiLCJmbG9vciIsIm51bVRlYW1zIiwic29sdXRpb25QYXRoIiwic2NvcmVCZWZvcmUiLCJzY29yZUFmdGVyIiwibnVtRmlsbGVkVGVhbXMiLCJiZXN0VGVhbUluZGV4IiwibGVhc3REZWNyZWFzZSIsInZpc2l0ZWQiLCJ0ZWFtcyIsInRlYW0iLCJUZWFtIiwicGVybUxlbmd0aCIsImZpcnN0QmFySW5kZXgiLCJuZXh0QmFySW5kZXgiLCJmaW5kRnVuYyIsImZpbmRJbmRleCIsIm51bVBlcm1zIiwic2NvcmVTdHVkZW50cyIsImJlc3RFbGVtZW50IiwiYmVzdFNjb3JlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7cWpCQUFBO0FBQ0E7OztBQUNBOzs7O0FBQ0E7Ozs7Ozs7O0FBRUE7QUFDQTtBQUNBOztJQUVxQkEsTztBQUNuQjtBQUNBLG1CQUFZQyxRQUFaLEVBQXNCQyxhQUF0QixFQUFvRDtBQUFBLFFBQWZDLEtBQWUsdUVBQVAsS0FBTzs7QUFBQTs7QUFDbEQsU0FBS0YsUUFBTCxHQUFnQkEsU0FBU0csR0FBVCxDQUFhO0FBQUEsYUFBVyxJQUFJQyxpQkFBSixDQUFZQyxPQUFaLEVBQXFCSixhQUFyQixDQUFYO0FBQUEsS0FBYixDQUFoQjtBQUNBLFNBQUtDLEtBQUwsR0FBYUEsS0FBYjs7QUFFQSxTQUFLSSx5QkFBTDs7QUFFQSxRQUFJLEtBQUtKLEtBQVQsRUFBZ0I7QUFBRUssY0FBUUMsR0FBUixDQUFZLGlDQUFaO0FBQWlEOztBQUVuRSxTQUFLQyx3QkFBTDtBQUNEOzs7O2tDQUVhO0FBQ1osYUFBTyxLQUFLVCxRQUFaO0FBQ0Q7Ozs2Q0FFd0I7QUFDdkIsV0FBS1UsSUFBTCxHQUFZLEtBQUtWLFFBQUwsQ0FBY0csR0FBZCxDQUFrQjtBQUFBLGVBQU0sSUFBTjtBQUFBLE9BQWxCLENBQVo7QUFDQSxXQUFLUSxVQUFMLEdBQWtCLENBQWxCO0FBQ0Q7OztnQ0FFV0MsTSxFQUFRO0FBQ2xCLFVBQUlDLGNBQWMsS0FBS0MsaUJBQUwsQ0FBdUJGLE1BQXZCLEVBQStCLEtBQUtHLHNCQUFwQyxDQUFsQjtBQUNBLFVBQUlDLG1CQUFtQixLQUFLQyxTQUFMLENBQWVKLFdBQWYsQ0FBdkI7QUFDQSxXQUFLLElBQUlLLElBQUksS0FBS0gsc0JBQUwsR0FBOEIsQ0FBM0MsRUFBOENHLElBQUksS0FBS0Msc0JBQUwsR0FBOEIsQ0FBaEYsRUFBbUZELEtBQUssQ0FBeEYsRUFBMkY7QUFDekYsWUFBTUUsVUFBVSxLQUFLTixpQkFBTCxDQUF1QkYsTUFBdkIsRUFBK0JNLENBQS9CLENBQWhCO0FBQ0EsWUFBTUcsUUFBUSxLQUFLSixTQUFMLENBQWVHLE9BQWYsQ0FBZDtBQUNBLFlBQUlDLFFBQVFMLGdCQUFaLEVBQThCO0FBQzVCSCx3QkFBY08sT0FBZDtBQUNBSiw2QkFBbUJLLEtBQW5CO0FBQ0Q7QUFDRjs7QUFFRCxhQUFPUixXQUFQO0FBQ0Q7OztzQ0FFaUJELE0sRUFBUVUsQyxFQUFHO0FBQzNCLFVBQU1DLE9BQU8sQ0FBQ1gsTUFBRCxDQUFiOztBQUVBO0FBQ0E7QUFDQTs7QUFFQSxXQUFLLElBQUlNLElBQUksQ0FBYixFQUFnQkEsSUFBSUksSUFBSSxDQUF4QixFQUEyQkosS0FBSyxDQUFoQyxFQUFtQztBQUNqQyxZQUFJTSxrQkFBa0IsQ0FBQyxDQUF2QjtBQUNBLFlBQUlDLG1CQUFtQixDQUFDLENBQXhCOztBQUVBLGFBQUssSUFBSUMsSUFBSSxDQUFiLEVBQWdCQSxJQUFJLEtBQUtoQixJQUFMLENBQVVpQixNQUE5QixFQUFzQ0QsS0FBSyxDQUEzQyxFQUE4QztBQUM1QyxjQUFJLEtBQUtoQixJQUFMLENBQVVnQixDQUFWLE1BQWlCLElBQWpCLElBQXlCQSxNQUFNZCxNQUEvQixJQUF5Q1csS0FBS0ssT0FBTCxDQUFhRixDQUFiLE1BQW9CLENBQUMsQ0FBbEUsRUFBcUU7QUFDbkU7QUFDQTtBQUNBLGdCQUFNTCxRQUFRLEtBQUtRLGFBQUwsQ0FBbUJqQixNQUFuQixFQUEyQmMsQ0FBM0IsQ0FBZDtBQUNBLGdCQUFJTCxRQUFRSSxnQkFBWixFQUE4QjtBQUM1QkEsaUNBQW1CSixLQUFuQjtBQUNBRyxnQ0FBa0JFLENBQWxCO0FBQ0Q7QUFDRjtBQUNGOztBQUVELFlBQUlGLGtCQUFrQixDQUFDLENBQXZCLEVBQTBCO0FBQ3hCRCxlQUFLTyxJQUFMLENBQVVOLGVBQVY7QUFDRDtBQUNGOztBQUVELGFBQU9ELElBQVA7QUFDRDs7QUFFRDtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7Ozs7Z0NBRVlRLEMsRUFBRztBQUNiLFdBQUtyQixJQUFMLENBQVVxQixDQUFWLElBQWUsS0FBZjtBQUNBLFdBQUtwQixVQUFMLElBQW1CLENBQW5CO0FBQ0Q7Ozt5Q0FFb0I7QUFDbkIsV0FBS3FCLFFBQUwsR0FBZ0IsRUFBaEI7QUFDRDs7QUFFRDtBQUNBOzs7O3lEQUNxQztBQUNuQyxVQUFJLEtBQUtyQixVQUFMLEtBQW9CLEtBQUtYLFFBQUwsQ0FBYzJCLE1BQWQsR0FBdUIsQ0FBL0MsRUFBa0Q7QUFDaEQ7QUFDQSxlQUFPLEVBQVA7QUFDRDs7QUFFRCxVQUFJTSxjQUFjLENBQUMsQ0FBbkI7QUFDQSxVQUFJQyxVQUFVLENBQUMsQ0FBZjtBQUNBLFVBQUlDLFVBQVUsQ0FBQyxDQUFmO0FBQ0E7QUFDQSxXQUFLLElBQUlqQixJQUFJLENBQWIsRUFBZ0JBLElBQUksS0FBS1IsSUFBTCxDQUFVaUIsTUFBOUIsRUFBc0NULEtBQUssQ0FBM0MsRUFBOEM7QUFDNUMsWUFBSSxLQUFLUixJQUFMLENBQVVRLENBQVYsTUFBaUIsSUFBckIsRUFBMkI7QUFDekI7QUFDQTtBQUNBLGVBQUssSUFBSVEsSUFBSSxDQUFiLEVBQWdCQSxJQUFJLEtBQUtHLGFBQUwsQ0FBbUJYLENBQW5CLEVBQXNCUyxNQUExQyxFQUFrREQsS0FBSyxDQUF2RCxFQUEwRDtBQUN4RCxnQkFBTUwsUUFBUSxLQUFLUSxhQUFMLENBQW1CWCxDQUFuQixFQUFzQlEsQ0FBdEIsQ0FBZDtBQUNBLGdCQUFJLEtBQUtoQixJQUFMLENBQVVnQixDQUFWLE1BQWlCLElBQWpCLElBQXlCUixNQUFNUSxDQUFuQyxFQUFzQztBQUNwQztBQUNBLGtCQUFJTCxRQUFRWSxXQUFSLElBQXVCQSxnQkFBZ0IsQ0FBQyxDQUE1QyxFQUErQztBQUM3Q0EsOEJBQWNaLEtBQWQ7QUFDQWEsMEJBQVVoQixDQUFWO0FBQ0FpQiwwQkFBVVQsQ0FBVjtBQUNEO0FBQ0Y7QUFDRjtBQUNGO0FBQ0Y7O0FBRUQsYUFBTyxDQUFDUSxPQUFELEVBQVVDLE9BQVYsQ0FBUDtBQUNEOzs7bUNBRWNDLEcsRUFBSztBQUNsQixXQUFLLElBQUlsQixJQUFJLENBQWIsRUFBZ0JBLElBQUlrQixJQUFJVCxNQUF4QixFQUFnQ1QsS0FBSyxDQUFyQyxFQUF3QztBQUN0QyxhQUFLbUIsV0FBTCxDQUFpQkQsSUFBSWxCLENBQUosQ0FBakI7QUFDRDtBQUNGOzs7Z0RBRTJCO0FBQzFCLFdBQUtXLGFBQUwsR0FBcUIsRUFBckI7QUFDQSxXQUFLLElBQUlYLElBQUksQ0FBYixFQUFnQkEsSUFBSSxLQUFLbEIsUUFBTCxDQUFjMkIsTUFBbEMsRUFBMENULEtBQUssQ0FBL0MsRUFBa0Q7QUFDaEQsYUFBS1csYUFBTCxDQUFtQkMsSUFBbkIsQ0FBd0IsRUFBeEI7QUFDQSxhQUFLLElBQUlKLElBQUksQ0FBYixFQUFnQkEsSUFBSSxLQUFLMUIsUUFBTCxDQUFjMkIsTUFBbEMsRUFBMENELEtBQUssQ0FBL0MsRUFBa0Q7QUFDaEQsY0FBSVIsTUFBTVEsQ0FBVixFQUFhO0FBQ1gsaUJBQUtHLGFBQUwsQ0FBbUJYLENBQW5CLEVBQXNCWSxJQUF0QixDQUEyQixLQUFLOUIsUUFBTCxDQUFja0IsQ0FBZCxFQUFpQm9CLFlBQWpCLENBQThCLEtBQUt0QyxRQUFMLENBQWMwQixDQUFkLENBQTlCLENBQTNCO0FBQ0QsV0FGRCxNQUVPO0FBQ0wsaUJBQUtHLGFBQUwsQ0FBbUJYLENBQW5CLEVBQXNCWSxJQUF0QixDQUEyQixDQUFDLENBQTVCO0FBQ0Q7QUFDRjtBQUNGO0FBQ0Y7Ozs0Q0FFdUI7QUFBQTs7QUFDdEI7QUFDQTs7QUFFQTtBQUNBOztBQUVBLFVBQU1TLFlBQVksS0FBS0MscUJBQUwsRUFBbEI7QUFDQSxVQUFNQyx1QkFBdUJGLFVBQVVwQyxHQUFWLENBQWM7QUFBQSxlQUFhLE1BQUtILFFBQUwsQ0FBYzBDLFNBQWQsRUFBeUJDLFlBQXRDO0FBQUEsT0FBZCxDQUE3QjtBQUNBLFVBQU1DLDBCQUEwQkMsT0FBT0MsSUFBUCxDQUM5QkwscUJBQXFCTSxNQUFyQixDQUNFLFVBQUNDLE9BQUQsRUFBVUMsS0FBVixFQUFvQjtBQUNsQkEsY0FBTUMsT0FBTixDQUFjLFVBQUNDLElBQUQsRUFBVTtBQUN4QjtBQUNFSCxrQkFBUUcsSUFBUixJQUFnQixJQUFoQjtBQUNELFNBSEQ7QUFJQSxlQUFPSCxPQUFQO0FBQ0QsT0FQSCxFQU9LLEVBUEwsQ0FEOEIsQ0FBaEM7O0FBWUEsVUFBTUksZ0JBQWdCUix3QkFBd0J6QyxHQUF4QixDQUE0QixVQUFDZ0QsSUFBRCxFQUFVO0FBQzFELFlBQUlFLE1BQU0sQ0FBVjtBQUNBLGFBQUssSUFBSW5DLElBQUksQ0FBYixFQUFnQkEsSUFBSXVCLHFCQUFxQmQsTUFBekMsRUFBaURULEtBQUssQ0FBdEQsRUFBeUQ7QUFDdkQsY0FBTW9DLGVBQWViLHFCQUFxQnZCLENBQXJCLEtBQTJCLEVBQWhEO0FBQ0EsY0FBSW9DLGFBQWExQixPQUFiLENBQXFCdUIsSUFBckIsSUFBNkIsQ0FBQyxDQUFsQyxFQUFxQztBQUNuQ0UsbUJBQU8sQ0FBUDtBQUNEO0FBQ0Y7O0FBRUQsZUFBTyxFQUFFRixVQUFGLEVBQVFFLFFBQVIsRUFBUDtBQUNELE9BVnFCLENBQXRCOztBQVlBRCxvQkFBY0csSUFBZCxDQUFtQixVQUFDQyxDQUFELEVBQUlDLENBQUo7QUFBQSxlQUFVRCxFQUFFSCxHQUFGLEdBQVFJLEVBQUVKLEdBQXBCO0FBQUEsT0FBbkI7O0FBRUEsVUFBTUssVUFBVU4sY0FBY08sR0FBZCxFQUFoQjtBQUNBLFVBQUlELFFBQVFMLEdBQVIsR0FBZSxJQUFJLENBQUwsR0FBVSxLQUFLckQsUUFBTCxDQUFjMkIsTUFBMUMsRUFBa0Q7QUFDaEQ7QUFDQTtBQUNEOztBQUVEcEIsY0FBUUMsR0FBUixDQUFZLGlCQUFaO0FBQ0E7QUFDQSxVQUFNb0QsaUJBQWlCckIsVUFBVXNCLE1BQVYsQ0FDckI7QUFBQSxlQUFhLE1BQUs3RCxRQUFMLENBQWMwQyxTQUFkLEVBQXlCQyxZQUF6QixDQUFzQ2YsT0FBdEMsQ0FBOEM4QixRQUFRUCxJQUF0RCxJQUE4RCxDQUFDLENBQTVFO0FBQUEsT0FEcUIsQ0FBdkI7O0FBSUE7QUFDQSxhQUFPUyxlQUFlakMsTUFBZixHQUF3QixDQUF4QixJQUE2QixLQUFLSyxRQUFMLENBQWNMLE1BQWQsSUFBeUIsS0FBSzNCLFFBQUwsQ0FBYzJCLE1BQWQsR0FBdUIsR0FBcEYsRUFBMEY7QUFDeEYsWUFBTWUsWUFBWWtCLGVBQWVELEdBQWYsRUFBbEI7QUFDQSxhQUFLM0IsUUFBTCxDQUFjRixJQUFkLENBQW1CLENBQUNZLFNBQUQsQ0FBbkI7QUFDQSxhQUFLTCxXQUFMLENBQWlCSyxTQUFqQjtBQUNEOztBQUVEO0FBQ0E7QUFDRDs7OytCQUVnRTtBQUFBLHVDQUExRDNCLHNCQUEwRDtBQUFBLFVBQTFEQSxzQkFBMEQseUNBQWpDLENBQWlDO0FBQUEsdUNBQTlCSSxzQkFBOEI7QUFBQSxVQUE5QkEsc0JBQThCLHlDQUFMLENBQUs7O0FBQy9EO0FBQ0E7QUFDQSxXQUFLSixzQkFBTCxHQUE4QkEsc0JBQTlCO0FBQ0EsV0FBS0ksc0JBQUwsR0FBOEJBLHNCQUE5Qjs7QUFFQSxXQUFLMkMsc0JBQUw7QUFDQSxXQUFLQyxrQkFBTDs7QUFFQSxVQUFJLEtBQUs3RCxLQUFULEVBQWdCO0FBQUVLLGdCQUFRQyxHQUFSLENBQVkscUJBQVosRUFBb0NELFFBQVF5RCxJQUFSLENBQWEsU0FBYjtBQUEwQjs7QUFFaEY7O0FBRUEsYUFBTyxLQUFLckQsVUFBTCxHQUFrQixLQUFLWCxRQUFMLENBQWMyQixNQUF2QyxFQUErQztBQUM3QyxhQUFLc0MsV0FBTDtBQUNEOztBQUVELFVBQUksS0FBSy9ELEtBQVQsRUFBZ0I7QUFBRUssZ0JBQVFDLEdBQVIsQ0FBWSxrQkFBWixFQUFpQ0QsUUFBUTJELE9BQVIsQ0FBZ0IsU0FBaEI7QUFBNkI7O0FBRWhGLFVBQUksS0FBS2hFLEtBQVQsRUFBZ0I7QUFBRUssZ0JBQVFDLEdBQVIsQ0FBWSx5Q0FBWjtBQUF5RDtBQUMzRSxhQUFPLEtBQUsyRCxXQUFMLENBQWlCLEtBQUtuQyxRQUF0QixDQUFQO0FBQ0Q7Ozs0Q0FFdUI7QUFDdEIsVUFBTU8sWUFBWSxFQUFsQjtBQUNBLFdBQUssSUFBSXJCLElBQUksQ0FBYixFQUFnQkEsSUFBSSxLQUFLbEIsUUFBTCxDQUFjMkIsTUFBbEMsRUFBMENULEtBQUssQ0FBL0MsRUFBa0Q7QUFDaEQsWUFBSSxLQUFLUixJQUFMLENBQVVRLENBQVYsTUFBaUIsSUFBckIsRUFBMkI7QUFDekJxQixvQkFBVVQsSUFBVixDQUFlWixDQUFmO0FBQ0Q7QUFDRjs7QUFFRCxhQUFPcUIsU0FBUDtBQUNEOzs7b0NBRWU2QixJLEVBQU07QUFDcEI7QUFDQSxVQUFJQyxjQUFjLENBQUMsQ0FBbkI7QUFDQSxVQUFJQyxnQkFBZ0IsQ0FBcEI7QUFDQSxVQUFJQyxpQkFBaUIsQ0FBQyxDQUF0QjtBQUNBLFVBQU1DLGlCQUFpQixLQUFLM0MsYUFBTCxDQUFtQnVDLElBQW5CLENBQXZCO0FBQ0EsV0FBSyxJQUFJbEQsSUFBSSxDQUFiLEVBQWdCQSxJQUFJc0QsZUFBZTdDLE1BQW5DLEVBQTJDVCxLQUFLLENBQWhELEVBQW1EO0FBQ2pELFlBQUlBLE1BQU1rRCxJQUFWLEVBQWdCO0FBQ2Q7QUFDQSxjQUFNL0MsUUFBUW1ELGVBQWV0RCxDQUFmLENBQWQ7QUFDQSxjQUFJbUQsZ0JBQWdCLENBQUMsQ0FBakIsSUFBc0JoRCxRQUFRaUQsYUFBbEMsRUFBaUQ7QUFDL0MsZ0JBQU1sRCxVQUFVLEtBQUtxRCxXQUFMLENBQWlCdkQsQ0FBakIsQ0FBaEI7QUFDQSxnQkFBSUUsWUFBWSxDQUFDLENBQWIsSUFBa0IsS0FBS1ksUUFBTCxDQUFjWixPQUFkLEVBQXVCTyxNQUF2QixHQUFnQyxLQUFLUixzQkFBM0QsRUFBbUY7QUFDakZrRCw0QkFBY25ELENBQWQ7QUFDQXFELCtCQUFpQm5ELE9BQWpCO0FBQ0FrRCw4QkFBZ0JqRCxLQUFoQjtBQUNEO0FBQ0Y7QUFDRjtBQUNGOztBQUVELGFBQU8sRUFBRXFELGlCQUFpQkwsV0FBbkIsRUFBZ0NFLDhCQUFoQyxFQUFQO0FBQ0Q7OztnQ0FFV0gsSSxFQUFNO0FBQ2hCLFdBQUssSUFBSWxELElBQUksQ0FBYixFQUFnQkEsSUFBSSxLQUFLYyxRQUFMLENBQWNMLE1BQWxDLEVBQTBDVCxLQUFLLENBQS9DLEVBQWtEO0FBQ2hELGFBQUssSUFBSVEsSUFBSSxDQUFiLEVBQWdCQSxJQUFJLEtBQUtNLFFBQUwsQ0FBY2QsQ0FBZCxFQUFpQlMsTUFBckMsRUFBNkNELEtBQUssQ0FBbEQsRUFBcUQ7QUFDbkQsY0FBSSxLQUFLTSxRQUFMLENBQWNkLENBQWQsRUFBaUJRLENBQWpCLE1BQXdCMEMsSUFBNUIsRUFBa0M7QUFDaEMsbUJBQU9sRCxDQUFQO0FBQ0Q7QUFDRjtBQUNGOztBQUVELGFBQU8sQ0FBQyxDQUFSO0FBQ0Q7OztxQ0FFZ0I7QUFDZixhQUFPLEtBQUtsQixRQUFMLENBQWMyQixNQUFkLEdBQXVCLEtBQUthLHFCQUFMLEdBQTZCYixNQUEzRDtBQUNEOzs7Z0NBRVc7QUFDVjtBQUNBO0FBQ0EsVUFBTVksWUFBWSxLQUFLQyxxQkFBTCxFQUFsQjs7QUFFQSxVQUFJRCxVQUFVWixNQUFWLElBQW9CLENBQXhCLEVBQTJCO0FBQ3pCO0FBQ0Q7O0FBRUQsVUFBSSxLQUFLUixzQkFBTCxLQUFnQyxLQUFLSixzQkFBekMsRUFBaUU7QUFDL0Q7QUFDQSxZQUFNNEQsT0FBT3BDLFVBQVVvQixHQUFWLEVBQWI7QUFDQSxZQUFJZ0IsSUFBSixFQUFVO0FBQ1IsZUFBSzNDLFFBQUwsQ0FBY0YsSUFBZCxDQUFtQjZDLElBQW5CO0FBQ0EsZUFBS3RDLFdBQUwsQ0FBaUJzQyxJQUFqQjtBQUNEO0FBQ0Y7O0FBRUQsV0FBSyxJQUFJekQsSUFBSSxDQUFiLEVBQWdCQSxJQUFJcUIsVUFBVVosTUFBOUIsRUFBc0NULEtBQUssQ0FBM0MsRUFBOEM7QUFBQSwrQkFDakIsS0FBS3dELGVBQUwsQ0FBcUJuQyxVQUFVckIsQ0FBVixDQUFyQixDQURpQjtBQUFBLFlBQ3BDcUQsY0FEb0Msb0JBQ3BDQSxjQURvQztBQUU1Qzs7O0FBQ0EsWUFBSUEsbUJBQW1CLENBQUMsQ0FBeEIsRUFBMkI7QUFDekIsZUFBS3ZDLFFBQUwsQ0FBY3VDLGNBQWQsRUFBOEJ6QyxJQUE5QixDQUFtQ1MsVUFBVXJCLENBQVYsQ0FBbkM7QUFDQSxlQUFLbUIsV0FBTCxDQUFpQkUsVUFBVXJCLENBQVYsQ0FBakI7QUFDRDtBQUNGO0FBQ0Y7OztrQ0FFYTtBQUNaLFVBQU0wRCx1QkFBdUIsS0FBS0Msa0NBQUwsRUFBN0IsQ0FEWSxDQUM0RDtBQUN4RSxVQUFNQyxXQUFXLEtBQUtDLFdBQUwsQ0FBaUJILHFCQUFxQixDQUFyQixDQUFqQixDQUFqQjtBQUNBLFdBQUtJLGNBQUwsQ0FBb0JGLFFBQXBCO0FBQ0EsVUFBTUcsV0FBVyxLQUFLRixXQUFMLENBQWlCSCxxQkFBcUIsQ0FBckIsQ0FBakIsQ0FBakI7QUFDQSxXQUFLSSxjQUFMLENBQW9CQyxRQUFwQjs7QUFFQSxXQUFLakQsUUFBTCxDQUFjRixJQUFkLENBQW1CZ0QsUUFBbkI7QUFDQSxXQUFLOUMsUUFBTCxDQUFjRixJQUFkLENBQW1CbUQsUUFBbkI7O0FBRUEsVUFBSUMsZUFBZSxLQUFLeEUsSUFBTCxDQUFVaUIsTUFBVixHQUFtQixLQUFLaEIsVUFBM0M7O0FBRUEsVUFBSXVFLGdCQUFnQixLQUFLbkUsc0JBQXJCLElBQ0dtRSxlQUFlLElBQUksS0FBS25FLHNCQUQzQixJQUVHLEtBQUtBLHNCQUFMLEtBQWdDLEtBQUtJLHNCQUY1QyxFQUVvRTtBQUNsRTtBQUNBLFlBQU1nRSxlQUFlLEtBQUszQyxxQkFBTCxFQUFyQjtBQUNBLFlBQU1wQixVQUFVLEtBQUsyRCxXQUFMLENBQWlCSSxhQUFhLENBQWIsQ0FBakIsQ0FBaEI7O0FBRUEsYUFBS25ELFFBQUwsQ0FBY0YsSUFBZCxDQUFtQlYsT0FBbkI7QUFDQSxhQUFLNEQsY0FBTCxDQUFvQjVELE9BQXBCOztBQUVBOEQsdUJBQWUsS0FBS3hFLElBQUwsQ0FBVWlCLE1BQVYsR0FBbUIsS0FBS2hCLFVBQXZDOztBQUVBLFlBQUl1RSxnQkFBZ0IsQ0FBcEIsRUFBdUI7QUFDckIsZUFBS0UsU0FBTDtBQUNEO0FBQ0YsT0FmRCxNQWVPLElBQUlGLGVBQWUsS0FBS25FLHNCQUF4QixFQUFnRDtBQUNyRDtBQUNBO0FBQ0EsYUFBS3FFLFNBQUw7QUFDRDtBQUNGOztBQUVEOzs7OytDQUMyQjtBQUN6QixVQUFNQyxXQUFXLEVBQWpCO0FBQ0EsV0FBS0MsV0FBTCxHQUFtQnBDLE9BQW5CLENBQTJCLFVBQUM3QyxPQUFELEVBQWE7QUFDdENnRixpQkFBU2hGLFFBQVFrRixJQUFqQixJQUF5QmxGLE9BQXpCO0FBQ0QsT0FGRDs7QUFJQSxXQUFLTCxRQUFMLEdBQWdCNkMsT0FBTzJDLE1BQVAsQ0FBY0gsUUFBZCxDQUFoQjtBQUNEOzs7cUNBRWdCO0FBQ2YsVUFBTUksZUFBZSxLQUFLQyxvQkFBTCxFQUFyQjs7QUFFQSxVQUFJQyx3QkFBd0IsQ0FBQyxDQUE3QjtBQUNBLFVBQUlDLG1CQUFtQixDQUFDLENBQXhCO0FBQ0EsVUFBTUMsNEJBQTRCQyxLQUFLQyxLQUFMLENBQVcsQ0FBQyxLQUFLL0YsUUFBTCxDQUFjMkIsTUFBZCxHQUF1QixLQUFLaEIsVUFBN0IsSUFDUCxLQUFLSSxzQkFEVCxDQUFsQztBQUVBLFdBQUssSUFBSUcsSUFBSSxDQUFiLEVBQWdCQSxJQUFJLEtBQUs4RSxRQUFMLEdBQWdCSCx5QkFBcEMsRUFBK0QzRSxLQUFLLENBQXBFLEVBQXVFO0FBQ3JFLFlBQUlBLElBQUksS0FBSytFLFlBQUwsQ0FBa0J0RSxNQUF0QixJQUNHLEtBQUtzRSxZQUFMLENBQWtCL0UsQ0FBbEIsRUFBcUJTLE1BQXJCLEdBQThCLEtBQUtSLHNCQUQxQyxFQUNrRTtBQUNoRTtBQUNBLGNBQU0rRSxjQUFjLEtBQUtqRixTQUFMLENBQWUsS0FBS2dGLFlBQUwsQ0FBa0IvRSxDQUFsQixDQUFmLENBQXBCO0FBQ0EsZUFBSytFLFlBQUwsQ0FBa0IvRSxDQUFsQixFQUFxQlksSUFBckIsQ0FBMEIyRCxZQUExQjtBQUNBLGNBQU1VLGFBQWEsS0FBS2xGLFNBQUwsQ0FBZSxLQUFLZ0YsWUFBTCxDQUFrQi9FLENBQWxCLENBQWYsQ0FBbkI7QUFDQSxlQUFLK0UsWUFBTCxDQUFrQi9FLENBQWxCLEVBQXFCeUMsR0FBckI7O0FBRUEsY0FBSXdDLGFBQWFELFdBQWIsSUFBNkJDLGFBQWFELFdBQWIsR0FBMkJOLGdCQUE1RCxFQUErRTtBQUM3RUQsb0NBQXdCekUsQ0FBeEI7QUFDQTBFLCtCQUFtQk8sYUFBYUQsV0FBaEM7QUFDRDtBQUNGO0FBQ0Y7O0FBR0QsVUFBSU4scUJBQXFCLENBQUMsQ0FBMUIsRUFBNkI7QUFDM0I7QUFDQSxZQUFJLEtBQUtJLFFBQUwsSUFBaUIsS0FBS0ksY0FBTCxHQUFzQlAseUJBQTNDLEVBQXNFO0FBQ3BFO0FBQ0EsY0FBSVEsZ0JBQWdCLENBQXBCO0FBQ0EsY0FBSUMsZ0JBQWdCLENBQUMsQ0FBckI7QUFDQSxlQUFLLElBQUlwRixLQUFJLENBQWIsRUFBZ0JBLEtBQUksS0FBSytFLFlBQUwsQ0FBa0J0RSxNQUF0QyxFQUE4Q1QsTUFBSyxDQUFuRCxFQUFzRDtBQUNwRCxnQkFBSSxLQUFLK0UsWUFBTCxDQUFrQi9FLEVBQWxCLEVBQXFCUyxNQUFyQixHQUE4QixLQUFLUixzQkFBdkMsRUFBK0Q7QUFDN0Q7QUFDQSxrQkFBTStFLGVBQWMsS0FBS2pGLFNBQUwsQ0FBZSxLQUFLZ0YsWUFBTCxDQUFrQi9FLEVBQWxCLENBQWYsQ0FBcEI7QUFDQSxtQkFBSytFLFlBQUwsQ0FBa0IvRSxFQUFsQixFQUFxQlksSUFBckIsQ0FBMEIyRCxZQUExQjtBQUNBLGtCQUFNVSxjQUFhLEtBQUtsRixTQUFMLENBQWUsS0FBS2dGLFlBQUwsQ0FBa0IvRSxFQUFsQixDQUFmLENBQW5CO0FBQ0EsbUJBQUsrRSxZQUFMLENBQWtCL0UsRUFBbEIsRUFBcUJ5QyxHQUFyQjs7QUFFQSxrQkFBSTJDLGtCQUFrQixDQUFDLENBQW5CLElBQXdCSixlQUFjQyxXQUFkLEdBQTJCRyxhQUF2RCxFQUFzRTtBQUNwRUEsZ0NBQWdCSixlQUFjQyxXQUE5QjtBQUNBRSxnQ0FBZ0JuRixFQUFoQjtBQUNEO0FBQ0Y7QUFDRjs7QUFFRCxlQUFLK0UsWUFBTCxDQUFrQkksYUFBbEIsRUFBaUN2RSxJQUFqQyxDQUFzQzJELFlBQXRDO0FBQ0EsY0FBSSxLQUFLUSxZQUFMLENBQWtCSSxhQUFsQixFQUFpQzFFLE1BQWpDLElBQTJDLEtBQUtSLHNCQUFwRCxFQUE0RTtBQUMxRSxpQkFBS2lGLGNBQUwsSUFBdUIsQ0FBdkI7QUFDRDtBQUNGLFNBdkJELE1BdUJPO0FBQ0wsZUFBS0gsWUFBTCxDQUFrQm5FLElBQWxCLENBQXVCLENBQUMyRCxZQUFELENBQXZCO0FBQ0EsZUFBS08sUUFBTCxJQUFpQixDQUFqQjtBQUNEO0FBQ0YsT0E3QkQsTUE2Qk87QUFDTCxhQUFLQyxZQUFMLENBQWtCTixxQkFBbEIsRUFBeUM3RCxJQUF6QyxDQUE4QzJELFlBQTlDO0FBQ0EsWUFBSSxLQUFLUSxZQUFMLENBQWtCTixxQkFBbEIsRUFBeUNoRSxNQUF6QyxJQUFtRCxLQUFLUixzQkFBNUQsRUFBb0Y7QUFDbEYsZUFBS2lGLGNBQUwsSUFBdUIsQ0FBdkI7QUFDRDtBQUNGOztBQUdELFdBQUtHLE9BQUwsQ0FBYWQsWUFBYixJQUE2QixJQUE3QjtBQUNBLFdBQUs5RSxVQUFMLElBQW1CLENBQW5CO0FBQ0Q7OzsyQ0FFc0I7QUFDckIsVUFBSU8sSUFBSSxDQUFSO0FBQ0EsYUFBTyxLQUFLcUYsT0FBTCxDQUFhckYsQ0FBYixDQUFQLEVBQXdCO0FBQUVBLGFBQUssQ0FBTDtBQUFTOztBQUVuQyxhQUFPQSxDQUFQO0FBQ0Q7OztnQ0FFVytFLFksRUFBYztBQUFBOztBQUN4QixVQUFNTyxRQUFRLEVBQWQ7QUFDQSxXQUFLLElBQUl0RixJQUFJLENBQWIsRUFBZ0JBLElBQUkrRSxhQUFhdEUsTUFBakMsRUFBeUNULEtBQUssQ0FBOUMsRUFBaUQ7QUFDL0MsWUFBTXVGLE9BQU9SLGFBQWEvRSxDQUFiLEVBQWdCZixHQUFoQixDQUFvQjtBQUFBLGlCQUFLLE9BQUtILFFBQUwsQ0FBYzBCLENBQWQsQ0FBTDtBQUFBLFNBQXBCLENBQWI7QUFDQThFLGNBQU0xRSxJQUFOLENBQVcsSUFBSTRFLGNBQUosQ0FBU0QsSUFBVCxDQUFYO0FBQ0Q7O0FBRUQsYUFBT0QsS0FBUDtBQUNEOzs7bUNBRWNQLFksRUFBY1UsVSxFQUFZO0FBQ3ZDLFVBQUlDLGdCQUFnQixDQUFDLENBQXJCO0FBQ0EsVUFBSUMsZUFBZSxDQUFuQjs7QUFFQSxVQUFJeEYsUUFBUSxDQUFaO0FBQ0EsVUFBTXlGLFdBQVcsU0FBWEEsUUFBVyxDQUFDL0UsQ0FBRCxFQUFJYixDQUFKO0FBQUEsZUFBVUEsSUFBSzJGLFlBQUwsSUFBc0I5RSxFQUFFb0IsSUFBRixLQUFXLEtBQTNDO0FBQUEsT0FBakI7QUFDQSxhQUFPMEQsaUJBQWlCRixVQUF4QixFQUFvQztBQUNsQ0Msd0JBQWdCQyxZQUFoQjtBQUNBQSx1QkFBZVosYUFBYWMsU0FBYixDQUF1QkQsUUFBdkIsQ0FBZjtBQUNBLFlBQUlELGlCQUFpQixDQUFDLENBQWxCLElBQXVCQSxlQUFlRixVQUExQyxFQUFzRDtBQUFFRSx5QkFBZUYsVUFBZjtBQUE0Qjs7QUFFcEYsWUFBTUYsT0FBTyxFQUFiO0FBQ0EsYUFBSyxJQUFJdkYsSUFBSTBGLGdCQUFnQixDQUE3QixFQUFnQzFGLElBQUkyRixZQUFwQyxFQUFrRDNGLEtBQUssQ0FBdkQsRUFBMEQ7QUFDeER1RixlQUFLM0UsSUFBTCxDQUFVbUUsYUFBYS9FLENBQWIsRUFBZ0JiLE9BQTFCO0FBQ0Q7O0FBRURnQixpQkFBUyxLQUFLSixTQUFMLENBQWV3RixJQUFmLENBQVQ7QUFDRDs7QUFFRCxhQUFPcEYsS0FBUDtBQUNEOzs7OEJBRVNvRixJLEVBQU07QUFDZDtBQUNBLFVBQUlBLEtBQUs5RSxNQUFMLElBQWUsQ0FBbkIsRUFBc0I7QUFDcEIsZUFBTyxDQUFQO0FBQ0Q7O0FBRUQsVUFBSU4sUUFBUSxDQUFaO0FBQ0EsVUFBSTJGLFdBQVcsQ0FBZjtBQUNBLFdBQUssSUFBSTlGLElBQUksQ0FBYixFQUFnQkEsSUFBSXVGLEtBQUs5RSxNQUF6QixFQUFpQ1QsS0FBSyxDQUF0QyxFQUF5QztBQUN2QyxhQUFLLElBQUlRLElBQUlSLElBQUksQ0FBakIsRUFBb0JRLElBQUkrRSxLQUFLOUUsTUFBN0IsRUFBcUNELEtBQUssQ0FBMUMsRUFBNkM7QUFDM0NMLG1CQUFTLEtBQUs0RixhQUFMLENBQW1CUixLQUFLdkYsQ0FBTCxDQUFuQixFQUE0QnVGLEtBQUsvRSxDQUFMLENBQTVCLENBQVQ7QUFDQXNGLHNCQUFZLENBQVo7QUFDRDtBQUNGOztBQUVELGFBQU8zRixRQUFRMkYsUUFBZjtBQUNEOzs7a0NBRWF4RCxDLEVBQUdDLEMsRUFBRztBQUNsQixhQUFPLEtBQUs1QixhQUFMLENBQW1CMkIsQ0FBbkIsRUFBc0JDLENBQXRCLENBQVA7QUFDRDs7OzhCQUVTZ0MsWSxFQUFjO0FBQ3RCLFVBQUl5QixjQUFjLENBQUMsQ0FBbkI7QUFDQSxVQUFJQyxZQUFZLENBQWhCO0FBQ0EsV0FBSyxJQUFJakcsSUFBSSxDQUFiLEVBQWdCQSxJQUFJLEtBQUtsQixRQUFMLENBQWMyQixNQUFsQyxFQUEwQ1QsS0FBSyxDQUEvQyxFQUFrRDtBQUNoRCxZQUFJQSxNQUFNdUUsWUFBVixFQUF3QjtBQUN0QixjQUFNcEUsUUFBUSxLQUFLUSxhQUFMLENBQW1CNEQsWUFBbkIsRUFBaUN2RSxDQUFqQyxDQUFkO0FBQ0EsY0FBSUcsUUFBUThGLFNBQVosRUFBdUI7QUFDckJELDBCQUFjaEcsQ0FBZDtBQUNBaUcsd0JBQVk5RixLQUFaO0FBQ0Q7QUFDRjtBQUNGOztBQUVELGFBQU82RixXQUFQO0FBQ0Q7Ozs7OztrQkF6ZmtCbkgsTyIsImZpbGUiOiJray1tYXRjaGVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLyogZXNsaW50LWRpc2FibGUgbm8tY29uc29sZSAqL1xuLyogZXNsaW50LWRpc2FibGUgbm8tY29udGludWUgKi9cbmltcG9ydCBTdHVkZW50IGZyb20gJy4vc3R1ZGVudCc7XG5pbXBvcnQgVGVhbSBmcm9tICcuL3RlYW0nO1xuXG4vLyBiYXNpY2FsbHkgc3RhcnMgYW5kIGJhcnNcbi8vIHdlIGhhdmUgc3R1ZGVudHMgYW5kIHNvbWUgc2VwZXJhdG9ycyBmb3IgdGhlbVxuLy8gd2UgbmVlZCBhbiBpbml0YWwgZXN0aW1hdGUgdGhvXG5cbmV4cG9ydCBkZWZhdWx0IGNsYXNzIEtLTWF0Y2gge1xuICAvLyBwdWJsaWNcbiAgY29uc3RydWN0b3Ioc3R1ZGVudHMsIHRlYW1pbmdDb25maWcsIGRlYnVnID0gZmFsc2UpIHtcbiAgICB0aGlzLnN0dWRlbnRzID0gc3R1ZGVudHMubWFwKHN0dWRlbnQgPT4gbmV3IFN0dWRlbnQoc3R1ZGVudCwgdGVhbWluZ0NvbmZpZykpO1xuICAgIHRoaXMuZGVidWcgPSBkZWJ1ZztcblxuICAgIHRoaXMuaW5pdGlhbGl6ZUFkamFjZW5jeU1hdHJpeCgpO1xuXG4gICAgaWYgKHRoaXMuZGVidWcpIHsgY29uc29sZS5sb2coJ0tLTWF0Y2g6IFJlbW92aW5nIGR1cGxpY2F0ZXMuLi4nKTsgfVxuXG4gICAgdGhpcy5yZW1vdmVEdXBsaWNhdGVzU3R1ZGVudHMoKTtcbiAgfVxuXG4gIGdldFN0dWRlbnRzKCkge1xuICAgIHJldHVybiB0aGlzLnN0dWRlbnRzO1xuICB9XG5cbiAgaW5pdGlhbGl6ZVVudmlzaXRlZFNldCgpIHtcbiAgICB0aGlzLnNldFUgPSB0aGlzLnN0dWRlbnRzLm1hcCgoKSA9PiB0cnVlKTtcbiAgICB0aGlzLm51bVZpc2l0ZWQgPSAwO1xuICB9XG5cbiAgZm9ybUNsdXN0ZXIodmVydGV4KSB7XG4gICAgbGV0IGJlc3RDbHVzdGVyID0gdGhpcy5mb3JtQ2x1c3Rlck9mU2l6ZSh2ZXJ0ZXgsIHRoaXMubWluaW11bVN0dWRlbnRzUGVyVGVhbSk7XG4gICAgbGV0IGJlc3RDbHVzdGVyU2NvcmUgPSB0aGlzLnNjb3JlVGVhbShiZXN0Q2x1c3Rlcik7XG4gICAgZm9yIChsZXQgaSA9IHRoaXMubWluaW11bVN0dWRlbnRzUGVyVGVhbSArIDE7IGkgPCB0aGlzLm1heGltdW1TdHVkZW50c1BlclRlYW0gKyAxOyBpICs9IDEpIHtcbiAgICAgIGNvbnN0IGNsdXN0ZXIgPSB0aGlzLmZvcm1DbHVzdGVyT2ZTaXplKHZlcnRleCwgaSk7XG4gICAgICBjb25zdCBzY29yZSA9IHRoaXMuc2NvcmVUZWFtKGNsdXN0ZXIpO1xuICAgICAgaWYgKHNjb3JlID4gYmVzdENsdXN0ZXJTY29yZSkge1xuICAgICAgICBiZXN0Q2x1c3RlciA9IGNsdXN0ZXI7XG4gICAgICAgIGJlc3RDbHVzdGVyU2NvcmUgPSBzY29yZTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gYmVzdENsdXN0ZXI7XG4gIH1cblxuICBmb3JtQ2x1c3Rlck9mU2l6ZSh2ZXJ0ZXgsIHMpIHtcbiAgICBjb25zdCBzZXRTID0gW3ZlcnRleF07XG5cbiAgICAvLyBmaW5kIHMgLSAxIGNsb3Nlc3QgdmVydGljaWVzIGluIFUgdGVzdCBib3RoIG1pbiB1cCB0byBtYXhcbiAgICAvLyBzbyBmaW5kIHMgLSAxIHZlcnRpY2llcyB3aXRoIGxhcmdlc3QgZWRnZXdpc2Ugc2NvcmVzXG4gICAgLy8gcyBpbiB0aGlzIGNhc2UgaXMgdGhlIG1pbmltdW1cblxuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcyAtIDE7IGkgKz0gMSkge1xuICAgICAgbGV0IG1vc3RPcHRpbWFsRWRnZSA9IC0xO1xuICAgICAgbGV0IG1vc3RPcHRpbWFsU2NvcmUgPSAtMTtcblxuICAgICAgZm9yIChsZXQgaiA9IDA7IGogPCB0aGlzLnNldFUubGVuZ3RoOyBqICs9IDEpIHtcbiAgICAgICAgaWYgKHRoaXMuc2V0VVtqXSA9PT0gdHJ1ZSAmJiBqICE9PSB2ZXJ0ZXggJiYgc2V0Uy5pbmRleE9mKGopID09PSAtMSkge1xuICAgICAgICAgIC8vIGZvciBlYWNoIHVudmlzaXRlZCB2ZXJ0ZXggaiB3aGVyZSBqICE9IHZlcnRleFxuICAgICAgICAgIC8vIGZpbmQgbW9zdCBvcHRpbWFsIGVkZ2VcbiAgICAgICAgICBjb25zdCBzY29yZSA9IHRoaXMuc2NvcmVkQWdhaW5zdFt2ZXJ0ZXhdW2pdO1xuICAgICAgICAgIGlmIChzY29yZSA+IG1vc3RPcHRpbWFsU2NvcmUpIHtcbiAgICAgICAgICAgIG1vc3RPcHRpbWFsU2NvcmUgPSBzY29yZTtcbiAgICAgICAgICAgIG1vc3RPcHRpbWFsRWRnZSA9IGo7XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGlmIChtb3N0T3B0aW1hbEVkZ2UgPiAtMSkge1xuICAgICAgICBzZXRTLnB1c2gobW9zdE9wdGltYWxFZGdlKTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gc2V0UztcbiAgfVxuXG4gIC8vIGZvcm1DbHVzdGVyICh2ZXJ0ZXgpIHtcbiAgLy8gICBjb25zdCBzZXRTID0gW3ZlcnRleF07XG5cbiAgLy8gICAvLyBmaW5kIHMgLSAxIGNsb3Nlc3QgdmVydGljaWVzIGluIFVcbiAgLy8gICAvLyBzbyBmaW5kIHMgLSAxIHZlcnRpY2llcyB3aXRoIGxhcmdlc3QgZWRnZXdpc2Ugc2NvcmVzXG4gIC8vICAgLy8gcyBpbiB0aGlzIGNhc2UgaXMgdGhlIG1pbmltdW1cbiAgLy8gICBjb25zdCBzID0gdGhpcy5taW5pbXVtU3R1ZGVudHNQZXJUZWFtO1xuXG4gIC8vICAgZm9yKGxldCBpID0gMDsgaSA8IHMgLSAxOyBpICs9IDEpIHtcbiAgLy8gICAgIGxldCBtb3N0T3B0aW1hbEVkZ2UgPSAtMTtcbiAgLy8gICAgIGxldCBtb3N0T3B0aW1hbFNjb3JlID0gLTE7XG5cbiAgLy8gICAgIGZvcihsZXQgaiA9IDA7IGogPCB0aGlzLnNldFUubGVuZ3RoOyBqICs9IDEpIHtcbiAgLy8gICAgICAgaWYodGhpcy5zZXRVW2pdID09PSB0cnVlICYmIGogIT09IHZlcnRleCAmJiBzZXRTLmluZGV4T2YoaikgPT09IC0xKSB7XG5cbiAgLy8gICAgICAgICAvLyBmb3IgZWFjaCB1bnZpc2l0ZWQgdmVydGV4IGogd2hlcmUgaiAhPSB2ZXJ0ZXhcbiAgLy8gICAgICAgICAvLyBmaW5kIG1vc3Qgb3B0aW1hbCBlZGdlXG4gIC8vICAgICAgICAgY29uc3Qgc2NvcmUgPSB0aGlzLnNjb3JlZEFnYWluc3RbdmVydGV4XVtqXTtcbiAgLy8gICAgICAgICBpZihzY29yZSA+IG1vc3RPcHRpbWFsU2NvcmUpIHtcbiAgLy8gICAgICAgICAgIG1vc3RPcHRpbWFsU2NvcmUgPSBzY29yZTtcbiAgLy8gICAgICAgICAgIG1vc3RPcHRpbWFsRWRnZSA9IGo7XG4gIC8vICAgICAgICAgfVxuICAvLyAgICAgICB9XG4gIC8vICAgICB9XG5cbiAgLy8gICAgIHNldFMucHVzaChtb3N0T3B0aW1hbEVkZ2UpO1xuICAvLyAgIH1cblxuICAvLyAgIHJldHVybiBzZXRTO1xuICAvLyB9XG5cbiAgdmlzaXRWZXJ0ZXgodikge1xuICAgIHRoaXMuc2V0VVt2XSA9IGZhbHNlO1xuICAgIHRoaXMubnVtVmlzaXRlZCArPSAxO1xuICB9XG5cbiAgaW5pdGlhbGl6ZUNsdXN0ZXJzKCkge1xuICAgIHRoaXMuY2x1c3RlcnMgPSBbXTtcbiAgfVxuXG4gIC8vIGZhcnRoZXN0IGluIHRoaXMgY2FzZSBtZWFucyBtaW5pbWFsIGNvbXBhdGliaWxpdHlcbiAgLy8gZG9udCBmb3JnZXQgYWJvdXQgcGlkZ2VuaG9sZVxuICBlZGdld2lzZUZhcnRoZXN0VW52aXNpdGVkVmVydGljaWVzKCkge1xuICAgIGlmICh0aGlzLm51bVZpc2l0ZWQgPT09IHRoaXMuc3R1ZGVudHMubGVuZ3RoIC0gMSkge1xuICAgICAgLy8gcGlkZ2VuaG9sZVxuICAgICAgcmV0dXJuIFtdO1xuICAgIH1cblxuICAgIGxldCBsb3dlc3RTY29yZSA9IC0xO1xuICAgIGxldCBsb3dlc3RJID0gLTE7XG4gICAgbGV0IGxvd2VzdEogPSAtMTtcbiAgICAvLyB0cmF2ZXJzZSBhbGwgdW52aXNpdGVkIHZlcnRpY2llc1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5zZXRVLmxlbmd0aDsgaSArPSAxKSB7XG4gICAgICBpZiAodGhpcy5zZXRVW2ldID09PSB0cnVlKSB7XG4gICAgICAgIC8vIGlmIHVudmlzaXRlZFxuICAgICAgICAvLyBwYWlyd2lzZSB0cmF2ZXJzZSBvdGhlciB1bnZpc2l0ZWQgaW4gYWRqYWNlbmN5IG1hdHJpeFxuICAgICAgICBmb3IgKGxldCBqID0gMDsgaiA8IHRoaXMuc2NvcmVkQWdhaW5zdFtpXS5sZW5ndGg7IGogKz0gMSkge1xuICAgICAgICAgIGNvbnN0IHNjb3JlID0gdGhpcy5zY29yZWRBZ2FpbnN0W2ldW2pdO1xuICAgICAgICAgIGlmICh0aGlzLnNldFVbal0gPT09IHRydWUgJiYgaSAhPT0gaikge1xuICAgICAgICAgICAgLy8gaWYgdGhpcyBpcyBhbHNvIHVudmlzaXN0ZWRcbiAgICAgICAgICAgIGlmIChzY29yZSA8IGxvd2VzdFNjb3JlIHx8IGxvd2VzdFNjb3JlID09PSAtMSkge1xuICAgICAgICAgICAgICBsb3dlc3RTY29yZSA9IHNjb3JlO1xuICAgICAgICAgICAgICBsb3dlc3RJID0gaTtcbiAgICAgICAgICAgICAgbG93ZXN0SiA9IGo7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIFtsb3dlc3RJLCBsb3dlc3RKXTtcbiAgfVxuXG4gIHJlbW92ZVNldEZyb21VKHNldCkge1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgc2V0Lmxlbmd0aDsgaSArPSAxKSB7XG4gICAgICB0aGlzLnZpc2l0VmVydGV4KHNldFtpXSk7XG4gICAgfVxuICB9XG5cbiAgaW5pdGlhbGl6ZUFkamFjZW5jeU1hdHJpeCgpIHtcbiAgICB0aGlzLnNjb3JlZEFnYWluc3QgPSBbXTtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuc3R1ZGVudHMubGVuZ3RoOyBpICs9IDEpIHtcbiAgICAgIHRoaXMuc2NvcmVkQWdhaW5zdC5wdXNoKFtdKTtcbiAgICAgIGZvciAobGV0IGogPSAwOyBqIDwgdGhpcy5zdHVkZW50cy5sZW5ndGg7IGogKz0gMSkge1xuICAgICAgICBpZiAoaSAhPT0gaikge1xuICAgICAgICAgIHRoaXMuc2NvcmVkQWdhaW5zdFtpXS5wdXNoKHRoaXMuc3R1ZGVudHNbaV0uc2NvcmVBZ2FpbnN0KHRoaXMuc3R1ZGVudHNbal0pKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICB0aGlzLnNjb3JlZEFnYWluc3RbaV0ucHVzaCgtMSk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gIH1cblxuICBkZXRlcm1pbmVEaXN0cmlidXRpb24oKSB7XG4gICAgLy8gdGhpcyBmdW5jdGlvbiB3aWxsIHByZXRlYW0gYWxsIHRoZSB1bnZpc2l0ZWQgbWVtYmVycyBpZiB0aGV5IGFyZSBvdmVyIGEgY2VydGFpbiBzdGFuZGFyZFxuICAgIC8vIGluIHRoaXMgaW5zdGFuY2UsIHRoYXQgc3RhbmRhcmQgaXMuLi4gZW50cmVwcmVuZXVyIHR5cGVcblxuICAgIC8vIGZpcnN0LCBkZXRlY3QgaWYgdGhpcyBpcyBuZWNjZXNhcnlcbiAgICAvLyB3ZSB3aWxsIHVzZSAyLzUgYXMgYW4gZXhhbXBsZVxuXG4gICAgY29uc3QgdW52aXNpdGVkID0gdGhpcy5hbGxVbnZpc2l0ZWRWZXJ0aWNpZXMoKTtcbiAgICBjb25zdCBhbGxFbnRyZXByZW5ldXJUeXBlcyA9IHVudmlzaXRlZC5tYXAoc3R1ZGVudElEID0+IHRoaXMuc3R1ZGVudHNbc3R1ZGVudElEXS5sYXVuY2hTa2lsbHMpO1xuICAgIGNvbnN0IHVuaXF1ZUVudHJlcHJlbmV1clR5cGVzID0gT2JqZWN0LmtleXMoXG4gICAgICBhbGxFbnRyZXByZW5ldXJUeXBlcy5yZWR1Y2UoXG4gICAgICAgIChoYXNobWFwLCB0eXBlcykgPT4ge1xuICAgICAgICAgIHR5cGVzLmZvckVhY2goKHR5cGUpID0+IHtcbiAgICAgICAgICAvLyBlc2xpbnQtZGlzYWJsZS1uZXh0LWxpbmUgbm8tcGFyYW0tcmVhc3NpZ25cbiAgICAgICAgICAgIGhhc2htYXBbdHlwZV0gPSB0cnVlO1xuICAgICAgICAgIH0pO1xuICAgICAgICAgIHJldHVybiBoYXNobWFwO1xuICAgICAgICB9LCB7fSxcbiAgICAgICksXG4gICAgKTtcblxuICAgIGNvbnN0IGFtb3VudFBlclR5cGUgPSB1bmlxdWVFbnRyZXByZW5ldXJUeXBlcy5tYXAoKHR5cGUpID0+IHtcbiAgICAgIGxldCBhbXQgPSAwO1xuICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBhbGxFbnRyZXByZW5ldXJUeXBlcy5sZW5ndGg7IGkgKz0gMSkge1xuICAgICAgICBjb25zdCBzdHVkZW50VHlwZXMgPSBhbGxFbnRyZXByZW5ldXJUeXBlc1tpXSB8fCBbXTtcbiAgICAgICAgaWYgKHN0dWRlbnRUeXBlcy5pbmRleE9mKHR5cGUpID4gLTEpIHtcbiAgICAgICAgICBhbXQgKz0gMTtcbiAgICAgICAgfVxuICAgICAgfVxuXG4gICAgICByZXR1cm4geyB0eXBlLCBhbXQgfTtcbiAgICB9KTtcblxuICAgIGFtb3VudFBlclR5cGUuc29ydCgoYSwgYikgPT4gYS5hbXQgLSBiLmFtdCk7XG5cbiAgICBjb25zdCB0b3BUeXBlID0gYW1vdW50UGVyVHlwZS5wb3AoKTtcbiAgICBpZiAodG9wVHlwZS5hbXQgPCAoMiAvIDUpICogdGhpcy5zdHVkZW50cy5sZW5ndGgpIHtcbiAgICAgIC8vIG9rIHdlIGRvbnQgbmVlZCB0byBkaXN0cmlidXRlXG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgY29uc29sZS5sb2coJ2Rpc3RyaWJ1dGluZy4uLicpO1xuICAgIC8vIGdldCBhbGwgb2YgdGhlIHN0dWRlbnRzIHdpdGggdGhhdCB0eXBlXG4gICAgY29uc3Qgc3R1ZGVudHNPZlR5cGUgPSB1bnZpc2l0ZWQuZmlsdGVyKFxuICAgICAgc3R1ZGVudElEID0+IHRoaXMuc3R1ZGVudHNbc3R1ZGVudElEXS5sYXVuY2hTa2lsbHMuaW5kZXhPZih0b3BUeXBlLnR5cGUpID4gLTEsXG4gICAgKTtcblxuICAgIC8vIGRpc3RyaWJ1dGUgYW5kIHZpc2l0IHRoZXNlIHN0dWRlbnRzXG4gICAgd2hpbGUgKHN0dWRlbnRzT2ZUeXBlLmxlbmd0aCA+IDAgJiYgdGhpcy5jbHVzdGVycy5sZW5ndGggPD0gKHRoaXMuc3R1ZGVudHMubGVuZ3RoIC8gMi4wKSkge1xuICAgICAgY29uc3Qgc3R1ZGVudElEID0gc3R1ZGVudHNPZlR5cGUucG9wKCk7XG4gICAgICB0aGlzLmNsdXN0ZXJzLnB1c2goW3N0dWRlbnRJRF0pO1xuICAgICAgdGhpcy52aXNpdFZlcnRleChzdHVkZW50SUQpO1xuICAgIH1cblxuICAgIC8vIGlmIHRoZXJlIGFyZSBvdmVyIGhhbGYgaG93ZXZlcixcbiAgICAvLyB0aGVuIHdlIG11c3QgZGlzdHJpYnV0ZSBtdWx0aXBsZSB0byBzYW1lIHRlYW0sIHNvIHdlIGNhbiBqdXN0IGxldCB0aGUgYWxnIGRvIHRoaXNcbiAgfVxuXG4gIHRlYW0oeyBtaW5pbXVtU3R1ZGVudHNQZXJUZWFtID0gMywgbWF4aW11bVN0dWRlbnRzUGVyVGVhbSA9IDQgfSkge1xuICAgIC8vIGdlbiBwZXJtc1xuICAgIC8vIGRwIHRoaXMgc2hpelxuICAgIHRoaXMubWluaW11bVN0dWRlbnRzUGVyVGVhbSA9IG1pbmltdW1TdHVkZW50c1BlclRlYW07XG4gICAgdGhpcy5tYXhpbXVtU3R1ZGVudHNQZXJUZWFtID0gbWF4aW11bVN0dWRlbnRzUGVyVGVhbTtcblxuICAgIHRoaXMuaW5pdGlhbGl6ZVVudmlzaXRlZFNldCgpO1xuICAgIHRoaXMuaW5pdGlhbGl6ZUNsdXN0ZXJzKCk7XG5cbiAgICBpZiAodGhpcy5kZWJ1ZykgeyBjb25zb2xlLmxvZygnS0tNYXRjaDogdGVhbWluZy4uLicpOyBjb25zb2xlLnRpbWUoJ0tLTWF0Y2gnKTsgfVxuXG4gICAgLy8gdGhpcy5kZXRlcm1pbmVEaXN0cmlidXRpb24oKTtcblxuICAgIHdoaWxlICh0aGlzLm51bVZpc2l0ZWQgPCB0aGlzLnN0dWRlbnRzLmxlbmd0aCkge1xuICAgICAgdGhpcy5hcHByb3hpbWF0ZSgpO1xuICAgIH1cblxuICAgIGlmICh0aGlzLmRlYnVnKSB7IGNvbnNvbGUubG9nKCdLS01hdGNoOiBkb25lLi4uJyk7IGNvbnNvbGUudGltZUVuZCgnS0tNYXRjaCcpOyB9XG5cbiAgICBpZiAodGhpcy5kZWJ1ZykgeyBjb25zb2xlLmxvZygnS0tNYXRjaDogZ2VuZXJhdGluZyB0ZWFtcyBmcm9tIG1hdGNoLi4uJyk7IH1cbiAgICByZXR1cm4gdGhpcy5wYXRoVG9UZWFtcyh0aGlzLmNsdXN0ZXJzKTtcbiAgfVxuXG4gIGFsbFVudmlzaXRlZFZlcnRpY2llcygpIHtcbiAgICBjb25zdCB1bnZpc2l0ZWQgPSBbXTtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuc3R1ZGVudHMubGVuZ3RoOyBpICs9IDEpIHtcbiAgICAgIGlmICh0aGlzLnNldFVbaV0gPT09IHRydWUpIHtcbiAgICAgICAgdW52aXNpdGVkLnB1c2goaSk7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIHVudmlzaXRlZDtcbiAgfVxuXG4gIGNsb3Nlc3ROZWlnaGJvcihub2RlKSB7XG4gICAgLy8gZmluZCBjbG9zZXN0IG5laWdoYm9yIHRoYXQgZG9lc24ndCBicmVhayBjb25zdHJhaW50c1xuICAgIGxldCBjbG9zZXN0Tm9kZSA9IC0xO1xuICAgIGxldCBmYXJ0aGVzdFNjb3JlID0gMDtcbiAgICBsZXQgY2xvc2VzdENsdXN0ZXIgPSAtMTtcbiAgICBjb25zdCBhZGphY2VudFRvTm9kZSA9IHRoaXMuc2NvcmVkQWdhaW5zdFtub2RlXTtcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IGFkamFjZW50VG9Ob2RlLmxlbmd0aDsgaSArPSAxKSB7XG4gICAgICBpZiAoaSAhPT0gbm9kZSkge1xuICAgICAgICAvLyBhbGwgbm9kZXMgbm90IGlcbiAgICAgICAgY29uc3Qgc2NvcmUgPSBhZGphY2VudFRvTm9kZVtpXTtcbiAgICAgICAgaWYgKGNsb3Nlc3ROb2RlID09PSAtMSB8fCBzY29yZSA+IGZhcnRoZXN0U2NvcmUpIHtcbiAgICAgICAgICBjb25zdCBjbHVzdGVyID0gdGhpcy5maW5kQ2x1c3RlcihpKTtcbiAgICAgICAgICBpZiAoY2x1c3RlciAhPT0gLTEgJiYgdGhpcy5jbHVzdGVyc1tjbHVzdGVyXS5sZW5ndGggPCB0aGlzLm1heGltdW1TdHVkZW50c1BlclRlYW0pIHtcbiAgICAgICAgICAgIGNsb3Nlc3ROb2RlID0gaTtcbiAgICAgICAgICAgIGNsb3Nlc3RDbHVzdGVyID0gY2x1c3RlcjtcbiAgICAgICAgICAgIGZhcnRoZXN0U2NvcmUgPSBzY29yZTtcbiAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4geyBjbG9zZXN0TmVpZ2hib3I6IGNsb3Nlc3ROb2RlLCBjbG9zZXN0Q2x1c3RlciB9O1xuICB9XG5cbiAgZmluZENsdXN0ZXIobm9kZSkge1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5jbHVzdGVycy5sZW5ndGg7IGkgKz0gMSkge1xuICAgICAgZm9yIChsZXQgaiA9IDA7IGogPCB0aGlzLmNsdXN0ZXJzW2ldLmxlbmd0aDsgaiArPSAxKSB7XG4gICAgICAgIGlmICh0aGlzLmNsdXN0ZXJzW2ldW2pdID09PSBub2RlKSB7XG4gICAgICAgICAgcmV0dXJuIGk7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gLTE7XG4gIH1cblxuICBjYWxjTnVtVmlzaXRlZCgpIHtcbiAgICByZXR1cm4gdGhpcy5zdHVkZW50cy5sZW5ndGggLSB0aGlzLmFsbFVudmlzaXRlZFZlcnRpY2llcygpLmxlbmd0aDtcbiAgfVxuXG4gIG1hdGNoTGFzdCgpIHtcbiAgICAvLyBjb25zdCBudW1VbnZpc2l0ZWQgPSB0aGlzLnNldFUubGVuZ3RoIC0gdGhpcy5udW1WaXNpdGVkO1xuICAgIC8vIGNvbnNvbGUubG9nKGB1bnZpc2l0ZWQ6ICR7bnVtVW52aXNpdGVkfWApO1xuICAgIGNvbnN0IHVudmlzaXRlZCA9IHRoaXMuYWxsVW52aXNpdGVkVmVydGljaWVzKCk7XG5cbiAgICBpZiAodW52aXNpdGVkLmxlbmd0aCA8PSAwKSB7XG4gICAgICByZXR1cm47XG4gICAgfVxuXG4gICAgaWYgKHRoaXMubWF4aW11bVN0dWRlbnRzUGVyVGVhbSA9PT0gdGhpcy5taW5pbXVtU3R1ZGVudHNQZXJUZWFtKSB7XG4gICAgICAvLyB0cmltIHRoZSBkaWZmZXJlbmNlIGludG8gYSBuZXcgdGVhbVxuICAgICAgY29uc3QgbGFzdCA9IHVudmlzaXRlZC5wb3AoKTtcbiAgICAgIGlmIChsYXN0KSB7XG4gICAgICAgIHRoaXMuY2x1c3RlcnMucHVzaChsYXN0KTtcbiAgICAgICAgdGhpcy52aXNpdFZlcnRleChsYXN0KTtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IHVudmlzaXRlZC5sZW5ndGg7IGkgKz0gMSkge1xuICAgICAgY29uc3QgeyBjbG9zZXN0Q2x1c3RlciB9ID0gdGhpcy5jbG9zZXN0TmVpZ2hib3IodW52aXNpdGVkW2ldKTtcbiAgICAgIC8vIGNvbnNvbGUubG9nKGBjbG9zZXN0IHRvICR7dW52aXNpdGVkW2ldfTogJHtjbG9zZXN0Q2x1c3Rlcn1gKTtcbiAgICAgIGlmIChjbG9zZXN0Q2x1c3RlciAhPT0gLTEpIHtcbiAgICAgICAgdGhpcy5jbHVzdGVyc1tjbG9zZXN0Q2x1c3Rlcl0ucHVzaCh1bnZpc2l0ZWRbaV0pO1xuICAgICAgICB0aGlzLnZpc2l0VmVydGV4KHVudmlzaXRlZFtpXSk7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgYXBwcm94aW1hdGUoKSB7XG4gICAgY29uc3QgbW9zdERpc3RhbnRWZXJ0aWNpZXMgPSB0aGlzLmVkZ2V3aXNlRmFydGhlc3RVbnZpc2l0ZWRWZXJ0aWNpZXMoKTsgLy8gc3RlcCAyXG4gICAgY29uc3QgY2x1c3RlckEgPSB0aGlzLmZvcm1DbHVzdGVyKG1vc3REaXN0YW50VmVydGljaWVzWzBdKTtcbiAgICB0aGlzLnJlbW92ZVNldEZyb21VKGNsdXN0ZXJBKTtcbiAgICBjb25zdCBjbHVzdGVyQiA9IHRoaXMuZm9ybUNsdXN0ZXIobW9zdERpc3RhbnRWZXJ0aWNpZXNbMV0pO1xuICAgIHRoaXMucmVtb3ZlU2V0RnJvbVUoY2x1c3RlckIpO1xuXG4gICAgdGhpcy5jbHVzdGVycy5wdXNoKGNsdXN0ZXJBKTtcbiAgICB0aGlzLmNsdXN0ZXJzLnB1c2goY2x1c3RlckIpO1xuXG4gICAgbGV0IG51bVVudmlzaXRlZCA9IHRoaXMuc2V0VS5sZW5ndGggLSB0aGlzLm51bVZpc2l0ZWQ7XG5cbiAgICBpZiAobnVtVW52aXNpdGVkID49IHRoaXMubWluaW11bVN0dWRlbnRzUGVyVGVhbVxuICAgICAgICAmJiBudW1VbnZpc2l0ZWQgPCAyICogdGhpcy5taW5pbXVtU3R1ZGVudHNQZXJUZWFtXG4gICAgICAgICYmIHRoaXMubWluaW11bVN0dWRlbnRzUGVyVGVhbSAhPT0gdGhpcy5tYXhpbXVtU3R1ZGVudHNQZXJUZWFtKSB7XG4gICAgICAvLyB0aGlzIGlzIG5vdyBhIGNsdXN0ZXJcbiAgICAgIGNvbnN0IHVudmlzaXRlZFJhdyA9IHRoaXMuYWxsVW52aXNpdGVkVmVydGljaWVzKCk7XG4gICAgICBjb25zdCBjbHVzdGVyID0gdGhpcy5mb3JtQ2x1c3Rlcih1bnZpc2l0ZWRSYXdbMF0pO1xuXG4gICAgICB0aGlzLmNsdXN0ZXJzLnB1c2goY2x1c3Rlcik7XG4gICAgICB0aGlzLnJlbW92ZVNldEZyb21VKGNsdXN0ZXIpO1xuXG4gICAgICBudW1VbnZpc2l0ZWQgPSB0aGlzLnNldFUubGVuZ3RoIC0gdGhpcy5udW1WaXNpdGVkO1xuXG4gICAgICBpZiAobnVtVW52aXNpdGVkID49IDApIHtcbiAgICAgICAgdGhpcy5tYXRjaExhc3QoKTtcbiAgICAgIH1cbiAgICB9IGVsc2UgaWYgKG51bVVudmlzaXRlZCA8IHRoaXMubWluaW11bVN0dWRlbnRzUGVyVGVhbSkge1xuICAgICAgLy8gbnVtVmlzaXRlZCBpcyBsZXNzIHRoYW4gbWluaW11bVxuICAgICAgLy8gbmVhcmVzdCBuZWlnaGJvciB0aGUgb3ZlcmZsb3dcbiAgICAgIHRoaXMubWF0Y2hMYXN0KCk7XG4gICAgfVxuICB9XG5cbiAgLy8gcHJpdmF0ZVxuICByZW1vdmVEdXBsaWNhdGVzU3R1ZGVudHMoKSB7XG4gICAgY29uc3QgZHVwQXJyYXkgPSB7fTtcbiAgICB0aGlzLmdldFN0dWRlbnRzKCkuZm9yRWFjaCgoc3R1ZGVudCkgPT4ge1xuICAgICAgZHVwQXJyYXlbc3R1ZGVudC5oYXNoXSA9IHN0dWRlbnQ7XG4gICAgfSk7XG5cbiAgICB0aGlzLnN0dWRlbnRzID0gT2JqZWN0LnZhbHVlcyhkdXBBcnJheSk7XG4gIH1cblxuICBhZGROZXh0U3R1ZGVudCgpIHtcbiAgICBjb25zdCBzdHVkZW50SW5kZXggPSB0aGlzLm5leHRVbnZpc2l0ZWRTdHVkZW50KCk7XG5cbiAgICBsZXQgZ3JlYXRlc3RJbmNyZWFzZUluZGV4ID0gLTE7XG4gICAgbGV0IGdyZWF0ZXN0SW5jcmVhc2UgPSAtMTtcbiAgICBjb25zdCBtYXhOdW1iZXJPZlJlbWFpbmluZ1RlYW1zID0gTWF0aC5mbG9vcigodGhpcy5zdHVkZW50cy5sZW5ndGggLSB0aGlzLm51bVZpc2l0ZWQpXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgLyB0aGlzLm1pbmltdW1TdHVkZW50c1BlclRlYW0pO1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5udW1UZWFtcyArIG1heE51bWJlck9mUmVtYWluaW5nVGVhbXM7IGkgKz0gMSkge1xuICAgICAgaWYgKGkgPCB0aGlzLnNvbHV0aW9uUGF0aC5sZW5ndGhcbiAgICAgICAgICAmJiB0aGlzLnNvbHV0aW9uUGF0aFtpXS5sZW5ndGggPCB0aGlzLm1heGltdW1TdHVkZW50c1BlclRlYW0pIHtcbiAgICAgICAgLy8gdGVzdCBhc3Mgc3R1ZGVudFxuICAgICAgICBjb25zdCBzY29yZUJlZm9yZSA9IHRoaXMuc2NvcmVUZWFtKHRoaXMuc29sdXRpb25QYXRoW2ldKTtcbiAgICAgICAgdGhpcy5zb2x1dGlvblBhdGhbaV0ucHVzaChzdHVkZW50SW5kZXgpO1xuICAgICAgICBjb25zdCBzY29yZUFmdGVyID0gdGhpcy5zY29yZVRlYW0odGhpcy5zb2x1dGlvblBhdGhbaV0pO1xuICAgICAgICB0aGlzLnNvbHV0aW9uUGF0aFtpXS5wb3AoKTtcblxuICAgICAgICBpZiAoc2NvcmVBZnRlciA+IHNjb3JlQmVmb3JlICYmIChzY29yZUFmdGVyIC0gc2NvcmVCZWZvcmUgPiBncmVhdGVzdEluY3JlYXNlKSkge1xuICAgICAgICAgIGdyZWF0ZXN0SW5jcmVhc2VJbmRleCA9IGk7XG4gICAgICAgICAgZ3JlYXRlc3RJbmNyZWFzZSA9IHNjb3JlQWZ0ZXIgLSBzY29yZUJlZm9yZTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuXG4gICAgaWYgKGdyZWF0ZXN0SW5jcmVhc2UgPT09IC0xKSB7XG4gICAgICAvLyBpZiBtb3JlIHRlYW1zIHRoYW4gbWF4IHRlYW1zLCByZW1hdGNoXG4gICAgICBpZiAodGhpcy5udW1UZWFtcyA+PSB0aGlzLm51bUZpbGxlZFRlYW1zICsgbWF4TnVtYmVyT2ZSZW1haW5pbmdUZWFtcykge1xuICAgICAgICAvLyBmaW5kIHRlYW0gd2l0aCBsZWFzdCBkZWNyZWFzZVxuICAgICAgICBsZXQgYmVzdFRlYW1JbmRleCA9IDA7XG4gICAgICAgIGxldCBsZWFzdERlY3JlYXNlID0gLTE7XG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5zb2x1dGlvblBhdGgubGVuZ3RoOyBpICs9IDEpIHtcbiAgICAgICAgICBpZiAodGhpcy5zb2x1dGlvblBhdGhbaV0ubGVuZ3RoIDwgdGhpcy5tYXhpbXVtU3R1ZGVudHNQZXJUZWFtKSB7XG4gICAgICAgICAgICAvLyB0ZXN0IGFzcyBzdHVkZW50XG4gICAgICAgICAgICBjb25zdCBzY29yZUJlZm9yZSA9IHRoaXMuc2NvcmVUZWFtKHRoaXMuc29sdXRpb25QYXRoW2ldKTtcbiAgICAgICAgICAgIHRoaXMuc29sdXRpb25QYXRoW2ldLnB1c2goc3R1ZGVudEluZGV4KTtcbiAgICAgICAgICAgIGNvbnN0IHNjb3JlQWZ0ZXIgPSB0aGlzLnNjb3JlVGVhbSh0aGlzLnNvbHV0aW9uUGF0aFtpXSk7XG4gICAgICAgICAgICB0aGlzLnNvbHV0aW9uUGF0aFtpXS5wb3AoKTtcblxuICAgICAgICAgICAgaWYgKGxlYXN0RGVjcmVhc2UgPT09IC0xIHx8IHNjb3JlQmVmb3JlIC0gc2NvcmVBZnRlciA8IGxlYXN0RGVjcmVhc2UpIHtcbiAgICAgICAgICAgICAgbGVhc3REZWNyZWFzZSA9IHNjb3JlQmVmb3JlIC0gc2NvcmVBZnRlcjtcbiAgICAgICAgICAgICAgYmVzdFRlYW1JbmRleCA9IGk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5zb2x1dGlvblBhdGhbYmVzdFRlYW1JbmRleF0ucHVzaChzdHVkZW50SW5kZXgpO1xuICAgICAgICBpZiAodGhpcy5zb2x1dGlvblBhdGhbYmVzdFRlYW1JbmRleF0ubGVuZ3RoID49IHRoaXMubWF4aW11bVN0dWRlbnRzUGVyVGVhbSkge1xuICAgICAgICAgIHRoaXMubnVtRmlsbGVkVGVhbXMgKz0gMTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5zb2x1dGlvblBhdGgucHVzaChbc3R1ZGVudEluZGV4XSk7XG4gICAgICAgIHRoaXMubnVtVGVhbXMgKz0gMTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5zb2x1dGlvblBhdGhbZ3JlYXRlc3RJbmNyZWFzZUluZGV4XS5wdXNoKHN0dWRlbnRJbmRleCk7XG4gICAgICBpZiAodGhpcy5zb2x1dGlvblBhdGhbZ3JlYXRlc3RJbmNyZWFzZUluZGV4XS5sZW5ndGggPj0gdGhpcy5tYXhpbXVtU3R1ZGVudHNQZXJUZWFtKSB7XG4gICAgICAgIHRoaXMubnVtRmlsbGVkVGVhbXMgKz0gMTtcbiAgICAgIH1cbiAgICB9XG5cblxuICAgIHRoaXMudmlzaXRlZFtzdHVkZW50SW5kZXhdID0gdHJ1ZTtcbiAgICB0aGlzLm51bVZpc2l0ZWQgKz0gMTtcbiAgfVxuXG4gIG5leHRVbnZpc2l0ZWRTdHVkZW50KCkge1xuICAgIGxldCBpID0gMDtcbiAgICB3aGlsZSAodGhpcy52aXNpdGVkW2ldKSB7IGkgKz0gMTsgfVxuXG4gICAgcmV0dXJuIGk7XG4gIH1cblxuICBwYXRoVG9UZWFtcyhzb2x1dGlvblBhdGgpIHtcbiAgICBjb25zdCB0ZWFtcyA9IFtdO1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgc29sdXRpb25QYXRoLmxlbmd0aDsgaSArPSAxKSB7XG4gICAgICBjb25zdCB0ZWFtID0gc29sdXRpb25QYXRoW2ldLm1hcChqID0+IHRoaXMuc3R1ZGVudHNbal0pO1xuICAgICAgdGVhbXMucHVzaChuZXcgVGVhbSh0ZWFtKSk7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRlYW1zO1xuICB9XG5cbiAgY2FsY3VsYXRlU2NvcmUoc29sdXRpb25QYXRoLCBwZXJtTGVuZ3RoKSB7XG4gICAgbGV0IGZpcnN0QmFySW5kZXggPSAtMTtcbiAgICBsZXQgbmV4dEJhckluZGV4ID0gMDtcblxuICAgIGxldCBzY29yZSA9IDA7XG4gICAgY29uc3QgZmluZEZ1bmMgPSAodiwgaSkgPT4gaSA+IChuZXh0QmFySW5kZXgpICYmIHYudHlwZSA9PT0gJ2Jhcic7XG4gICAgd2hpbGUgKG5leHRCYXJJbmRleCAhPT0gcGVybUxlbmd0aCkge1xuICAgICAgZmlyc3RCYXJJbmRleCA9IG5leHRCYXJJbmRleDtcbiAgICAgIG5leHRCYXJJbmRleCA9IHNvbHV0aW9uUGF0aC5maW5kSW5kZXgoZmluZEZ1bmMpO1xuICAgICAgaWYgKG5leHRCYXJJbmRleCA9PT0gLTEgfHwgbmV4dEJhckluZGV4ID4gcGVybUxlbmd0aCkgeyBuZXh0QmFySW5kZXggPSBwZXJtTGVuZ3RoOyB9XG5cbiAgICAgIGNvbnN0IHRlYW0gPSBbXTtcbiAgICAgIGZvciAobGV0IGkgPSBmaXJzdEJhckluZGV4ICsgMTsgaSA8IG5leHRCYXJJbmRleDsgaSArPSAxKSB7XG4gICAgICAgIHRlYW0ucHVzaChzb2x1dGlvblBhdGhbaV0uc3R1ZGVudCk7XG4gICAgICB9XG5cbiAgICAgIHNjb3JlICs9IHRoaXMuc2NvcmVUZWFtKHRlYW0pO1xuICAgIH1cblxuICAgIHJldHVybiBzY29yZTtcbiAgfVxuXG4gIHNjb3JlVGVhbSh0ZWFtKSB7XG4gICAgLy8gcGFpcndpc2UgdGVhbSBhbGwgbWVtYmVycyBhZ2FpbnN0IGVhY2ggb3RoZXJcbiAgICBpZiAodGVhbS5sZW5ndGggPD0gMSkge1xuICAgICAgcmV0dXJuIDA7XG4gICAgfVxuXG4gICAgbGV0IHNjb3JlID0gMDtcbiAgICBsZXQgbnVtUGVybXMgPSAwO1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGVhbS5sZW5ndGg7IGkgKz0gMSkge1xuICAgICAgZm9yIChsZXQgaiA9IGkgKyAxOyBqIDwgdGVhbS5sZW5ndGg7IGogKz0gMSkge1xuICAgICAgICBzY29yZSArPSB0aGlzLnNjb3JlU3R1ZGVudHModGVhbVtpXSwgdGVhbVtqXSk7XG4gICAgICAgIG51bVBlcm1zICs9IDE7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIHNjb3JlIC8gbnVtUGVybXM7XG4gIH1cblxuICBzY29yZVN0dWRlbnRzKGEsIGIpIHtcbiAgICByZXR1cm4gdGhpcy5zY29yZWRBZ2FpbnN0W2FdW2JdO1xuICB9XG5cbiAgYmVzdE1hdGNoKHN0dWRlbnRJbmRleCkge1xuICAgIGxldCBiZXN0RWxlbWVudCA9IC0xO1xuICAgIGxldCBiZXN0U2NvcmUgPSAwO1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5zdHVkZW50cy5sZW5ndGg7IGkgKz0gMSkge1xuICAgICAgaWYgKGkgIT09IHN0dWRlbnRJbmRleCkge1xuICAgICAgICBjb25zdCBzY29yZSA9IHRoaXMuc2NvcmVkQWdhaW5zdFtzdHVkZW50SW5kZXhdW2ldO1xuICAgICAgICBpZiAoc2NvcmUgPiBiZXN0U2NvcmUpIHtcbiAgICAgICAgICBiZXN0RWxlbWVudCA9IGk7XG4gICAgICAgICAgYmVzdFNjb3JlID0gc2NvcmU7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gYmVzdEVsZW1lbnQ7XG4gIH1cbn1cbiJdLCJzb3VyY2VSb290IjoiL21udC9jL1VzZXJzL3B1cmV1L0RvY3VtZW50cy9wcm9qZWN0cy9sYXVuY2h4LXRlYW1pbmcvc3JjIn0=
