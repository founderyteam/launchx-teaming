'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

exports.generateRandomStudents = generateRandomStudents;
exports.generateRandomIdeas = generateRandomIdeas;

var _student2 = require('./student');

var _student3 = _interopRequireDefault(_student2);

var _team2 = require('./team');

var _team3 = _interopRequireDefault(_team2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var greedyPairing = function greedyPairing(students, studentsPerTeam, minPerTeam, maxStudentsPerTeam) {
  var studentArray = Array.from(students);
  var mutableStudents = studentArray.map(function (student) {
    return student.p_hash;
  });
  var studentHashes = studentArray.reduce(function (prev, curr) {
    prev[curr.p_hash] = curr;
    return prev;
  }, {});

  // if we have say... 10 students and 3 per team then there is one left over. Lob onto the previous
  // if we have say... 11 students and 3 per team then there is two left over, distribute to the previous
  // if we have say... 11 students and 4 per team then there is 2 teams and three left over so make a team
  var totalStudents = mutableStudents.length;
  var leftOver = totalStudents % studentsPerTeam;
  console.log('Left Over ', leftOver);
  var teamSize = Math.floor(totalStudents / studentsPerTeam);
  if (leftOver >= minPerTeam) {
    console.log('spillover');
    teamSize = Math.floor(totalStudents / studentsPerTeam) + 1;
  } else if (leftOver + studentsPerTeam > maxStudentsPerTeam) {
    teamSize += 1;
  }

  console.log('Number of teams ', teamSize);

  var teams = mutableStudents.slice(0, teamSize).map(function (student) {
    return [student];
  });
  mutableStudents = mutableStudents.slice(teamSize);

  for (var i = studentsPerTeam - 1; i > 0; i--) {
    for (var teamId = 0, totalTeams = teams.length; teamId < totalTeams; teamId++) {
      var team = teams[teamId];
      var bestScore = -1;
      var bestStudentId = null;
      var bestTeam = null;

      for (var studentId = 0; studentId < Math.floor(mutableStudents.length / i); studentId++) {
        var student = mutableStudents[studentId];
        var newTeam = team.concat([student]);
        var mappedNewTeam = newTeam.map(function (studentHash) {
          return studentHashes[studentHash];
        });
        var score = _team3.default.score(mappedNewTeam);
        score = score < 0 ? 0 : score;

        if (score > bestScore) {
          bestScore = score;
          bestTeam = newTeam;
          bestStudentId = studentId;
        }
      }

      if (bestTeam !== null) {
        teams[teamId] = Array.from(bestTeam);
      }
      if (bestStudentId !== null) {
        var pre = mutableStudents.length;
        mutableStudents = mutableStudents.slice(0, bestStudentId).concat(mutableStudents.slice(bestStudentId + 1));
      }
    }
  }

  if (mutableStudents.length > 0) {
    // distribute
    for (var _studentId = 0; _studentId < mutableStudents.length; _studentId++) {
      var _student = mutableStudents[_studentId];
      var _bestTeam = null;
      var bestTeamId = -1;
      var _bestScore = -1;

      for (var _teamId = 0, _totalTeams = teams.length; _teamId < _totalTeams; _teamId++) {
        var _team = teams[_teamId];

        var _newTeam = _team.concat([_student]);
        var _mappedNewTeam = _newTeam.map(function (studentHash) {
          return studentHashes[studentHash];
        });
        var _score = _team3.default.score(_mappedNewTeam);
        _score = _score < 0 ? 0 : _score;

        if (_score > _bestScore) {
          _bestScore = _score;
          _bestTeam = _newTeam;
          bestTeamId = _teamId;
        }
      }

      if (_bestTeam !== null) {
        teams[bestTeamId] = Array.from(_bestTeam);
      }
    }
  }

  return teams.map(function (team) {
    return new _team3.default(team.map(function (studentHash) {
      return studentHashes[studentHash];
    }));
  });
};

var pairIdeasWithTeams = function pairIdeasWithTeams(teams) {
  for (var teamId = 0, r = teams.length; teamId < r; teamId++) {
    var team = teams[teamId];
    var ideas = team.students.reduce(function (old, curr) {
      return old.concat(curr.ideas);
    }, []);

    var bestIdeaScore = 0;
    var bestIdeaId = null;

    var _loop = function _loop(ideaId, totIdeas) {
      var idea = ideas[ideaId];
      var score = -1;
      team.students.forEach(function (student) {
        var compat = _team3.default.ideaCompatability(student, idea);
        if (!compat) score -= 1000;
        score += compat;
      });

      if (score > bestIdeaScore) {
        bestIdeaScore = score;
        bestIdeaId = ideaId;
      }
    };

    for (var ideaId = 0, totIdeas = ideas.length; ideaId < totIdeas; ideaId++) {
      _loop(ideaId, totIdeas);
    }

    if (bestIdeaId !== null) teams[teamId].idea = ideas[bestIdeaId];
  }

  return teams;
};

var Match = function () {
  function Match(students, teamingConfig) {
    _classCallCheck(this, Match);

    this._students = new Set(students.map(function (student) {
      return new _student3.default(student, teamingConfig);
    }));
  }

  _createClass(Match, [{
    key: 'team',
    value: function team() {
      var timing = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
      var studentsPerTeam = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 3;
      var minPerTeam = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 3;
      var maxStudentsPerTeam = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 4;

      if (timing === true) console.time('team');

      var teams = greedyPairing(this._students, studentsPerTeam, minPerTeam, maxStudentsPerTeam);

      if (timing === true) console.timeEnd('team');

      return teams;
    }
  }], [{
    key: 'test',
    value: function test(teams) {
      var students = [];
      var _iteratorNormalCompletion = true;
      var _didIteratorError = false;
      var _iteratorError = undefined;

      try {
        for (var _iterator = teams[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
          var team = _step.value;
          var _iteratorNormalCompletion3 = true;
          var _didIteratorError3 = false;
          var _iteratorError3 = undefined;

          try {
            for (var _iterator3 = team._students[Symbol.iterator](), _step3; !(_iteratorNormalCompletion3 = (_step3 = _iterator3.next()).done); _iteratorNormalCompletion3 = true) {
              var student = _step3.value;

              students.push(student);
            }
          } catch (err) {
            _didIteratorError3 = true;
            _iteratorError3 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion3 && _iterator3.return) {
                _iterator3.return();
              }
            } finally {
              if (_didIteratorError3) {
                throw _iteratorError3;
              }
            }
          }
        }
      } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion && _iterator.return) {
            _iterator.return();
          }
        } finally {
          if (_didIteratorError) {
            throw _iteratorError;
          }
        }
      }

      var faultCount = 0;
      var _iteratorNormalCompletion2 = true;
      var _didIteratorError2 = false;
      var _iteratorError2 = undefined;

      try {
        for (var _iterator2 = students[Symbol.iterator](), _step2; !(_iteratorNormalCompletion2 = (_step2 = _iterator2.next()).done); _iteratorNormalCompletion2 = true) {
          var student1 = _step2.value;
          var _iteratorNormalCompletion4 = true;
          var _didIteratorError4 = false;
          var _iteratorError4 = undefined;

          try {
            for (var _iterator4 = students[Symbol.iterator](), _step4; !(_iteratorNormalCompletion4 = (_step4 = _iterator4.next()).done); _iteratorNormalCompletion4 = true) {
              var student2 = _step4.value;

              if (student1.p_hash === student2.p_hash) {
                console.log('duplicate: ', student1.p_json.name);
                faultCount += 1;
              }
            }
          } catch (err) {
            _didIteratorError4 = true;
            _iteratorError4 = err;
          } finally {
            try {
              if (!_iteratorNormalCompletion4 && _iterator4.return) {
                _iterator4.return();
              }
            } finally {
              if (_didIteratorError4) {
                throw _iteratorError4;
              }
            }
          }
        }
      } catch (err) {
        _didIteratorError2 = true;
        _iteratorError2 = err;
      } finally {
        try {
          if (!_iteratorNormalCompletion2 && _iterator2.return) {
            _iterator2.return();
          }
        } finally {
          if (_didIteratorError2) {
            throw _iteratorError2;
          }
        }
      }

      faultCount -= students.length;
      return faultCount;
    }
  }]);

  return Match;
}();

exports.default = Match;


function randomVoteForIdea(idea) {
  var newIdea = Object.assign({}, idea);
  newIdea.vote = ['strong_yes', 'yes', 'no', 'strong_no'][Math.round(Math.random() * 3)];
  newIdea.strength = ['strong_yes', 'yes', 'no', 'strong_no'].indexOf(newIdea.vote);
  return newIdea;
}

function generateRandomStudents(numStudents, ideas, names) {
  var students = [];
  for (var i = 0; i < numStudents; i += 1) {
    var ids = ideas.map(function (idea) {
      return randomVoteForIdea(idea);
    });
    var studentJson = {
      learningStyle: ['abcd', 'cdef'][Math.round(Math.random())],
      workStyle: ['abcd', 'cdef', 'abcd/cdef', 'abcd/efg'][Math.round(Math.random() * 3)],
      name: names[i],
      interests: ['abcdefg'[Math.round(Math.random() * 6)], 'abcdefg'[Math.round(Math.random() * 6)], 'abcdefg'[Math.round(Math.random() * 6)], 'abcdefg'[Math.round(Math.random() * 6)], 'abcdefg'[Math.round(Math.random() * 6)]],
      role: ['a', 'b', 'c', 'd'][Math.round(Math.random() * 3)],
      launchSkills: [['a', 'b', 'c', 'd'][Math.round(Math.random() * 3)]],
      skills: {
        graphicDesign: Math.round(Math.random() * 9),
        making: Math.round(Math.random() * 9),
        marketing: Math.round(Math.random() * 9),
        operations: Math.round(Math.random() * 9),
        programming: Math.round(Math.random() * 9),
        selling: Math.round(Math.random() * 9)
      },
      gender: ['Male', 'Female'][Math.round(Math.random())],
      ethnicity: ['Male', 'Female'][Math.round(Math.random())],
      entrepreneurType: 'Confidence, Independence, Risk, Knowledge',
      personalityType: ['E', 'I'][Math.round(Math.random())] + ['N', 'S'][Math.round(Math.random())] + ['T', 'F'][Math.round(Math.random())] + ['P', 'J'][Math.round(Math.random())],
      ideas: ids
    };
    students.push(studentJson);
  }
  return students;
}

function generateRandomIdeas(numIdeas, emails) {
  var ideas = [];
  for (var i = 0; i < numIdeas; i++) {
    ideas.push({
      name: Math.random().toString(36).substr(2, 5),
      id: i,
      thinker: {
        email: emails[Math.round(Math.random() * emails.length)]
      },
      vote: ['strong_yes', 'yes', 'no', 'strong_no'][Math.round(Math.random() * 3)]
    });
  }

  return ideas;
}
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1hdGNoLmpzIl0sIm5hbWVzIjpbImdlbmVyYXRlUmFuZG9tU3R1ZGVudHMiLCJnZW5lcmF0ZVJhbmRvbUlkZWFzIiwiZ3JlZWR5UGFpcmluZyIsInN0dWRlbnRzIiwic3R1ZGVudHNQZXJUZWFtIiwibWluUGVyVGVhbSIsIm1heFN0dWRlbnRzUGVyVGVhbSIsInN0dWRlbnRBcnJheSIsIkFycmF5IiwiZnJvbSIsIm11dGFibGVTdHVkZW50cyIsIm1hcCIsInN0dWRlbnQiLCJwX2hhc2giLCJzdHVkZW50SGFzaGVzIiwicmVkdWNlIiwicHJldiIsImN1cnIiLCJ0b3RhbFN0dWRlbnRzIiwibGVuZ3RoIiwibGVmdE92ZXIiLCJjb25zb2xlIiwibG9nIiwidGVhbVNpemUiLCJNYXRoIiwiZmxvb3IiLCJ0ZWFtcyIsInNsaWNlIiwiaSIsInRlYW1JZCIsInRvdGFsVGVhbXMiLCJ0ZWFtIiwiYmVzdFNjb3JlIiwiYmVzdFN0dWRlbnRJZCIsImJlc3RUZWFtIiwic3R1ZGVudElkIiwibmV3VGVhbSIsImNvbmNhdCIsIm1hcHBlZE5ld1RlYW0iLCJzdHVkZW50SGFzaCIsInNjb3JlIiwiVGVhbSIsInByZSIsImJlc3RUZWFtSWQiLCJwYWlySWRlYXNXaXRoVGVhbXMiLCJyIiwiaWRlYXMiLCJvbGQiLCJiZXN0SWRlYVNjb3JlIiwiYmVzdElkZWFJZCIsImlkZWFJZCIsInRvdElkZWFzIiwiaWRlYSIsImZvckVhY2giLCJjb21wYXQiLCJpZGVhQ29tcGF0YWJpbGl0eSIsIk1hdGNoIiwidGVhbWluZ0NvbmZpZyIsIl9zdHVkZW50cyIsIlNldCIsIlN0dWRlbnQiLCJ0aW1pbmciLCJ0aW1lIiwidGltZUVuZCIsInB1c2giLCJmYXVsdENvdW50Iiwic3R1ZGVudDEiLCJzdHVkZW50MiIsInBfanNvbiIsIm5hbWUiLCJyYW5kb21Wb3RlRm9ySWRlYSIsIm5ld0lkZWEiLCJPYmplY3QiLCJhc3NpZ24iLCJ2b3RlIiwicm91bmQiLCJyYW5kb20iLCJzdHJlbmd0aCIsImluZGV4T2YiLCJudW1TdHVkZW50cyIsIm5hbWVzIiwiaWRzIiwic3R1ZGVudEpzb24iLCJsZWFybmluZ1N0eWxlIiwid29ya1N0eWxlIiwiaW50ZXJlc3RzIiwicm9sZSIsImxhdW5jaFNraWxscyIsInNraWxscyIsImdyYXBoaWNEZXNpZ24iLCJtYWtpbmciLCJtYXJrZXRpbmciLCJvcGVyYXRpb25zIiwicHJvZ3JhbW1pbmciLCJzZWxsaW5nIiwiZ2VuZGVyIiwiZXRobmljaXR5IiwiZW50cmVwcmVuZXVyVHlwZSIsInBlcnNvbmFsaXR5VHlwZSIsIm51bUlkZWFzIiwiZW1haWxzIiwidG9TdHJpbmciLCJzdWJzdHIiLCJpZCIsInRoaW5rZXIiLCJlbWFpbCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7UUF3S2dCQSxzQixHQUFBQSxzQjtRQXVDQUMsbUIsR0FBQUEsbUI7O0FBL01oQjs7OztBQUdBOzs7Ozs7OztBQUVBLElBQU1DLGdCQUFnQixTQUFoQkEsYUFBZ0IsQ0FBQ0MsUUFBRCxFQUFXQyxlQUFYLEVBQTRCQyxVQUE1QixFQUF3Q0Msa0JBQXhDLEVBQStEO0FBQ25GLE1BQU1DLGVBQWVDLE1BQU1DLElBQU4sQ0FBV04sUUFBWCxDQUFyQjtBQUNBLE1BQUlPLGtCQUFrQkgsYUFBYUksR0FBYixDQUFpQjtBQUFBLFdBQVdDLFFBQVFDLE1BQW5CO0FBQUEsR0FBakIsQ0FBdEI7QUFDQSxNQUFNQyxnQkFBZ0JQLGFBQWFRLE1BQWIsQ0FBb0IsVUFBQ0MsSUFBRCxFQUFPQyxJQUFQLEVBQWdCO0FBQ3hERCxTQUFLQyxLQUFLSixNQUFWLElBQW9CSSxJQUFwQjtBQUNBLFdBQU9ELElBQVA7QUFDRCxHQUhxQixFQUduQixFQUhtQixDQUF0Qjs7QUFLQTtBQUNBO0FBQ0E7QUFDQSxNQUFNRSxnQkFBZ0JSLGdCQUFnQlMsTUFBdEM7QUFDQSxNQUFNQyxXQUFXRixnQkFBZ0JkLGVBQWpDO0FBQ0FpQixVQUFRQyxHQUFSLENBQVksWUFBWixFQUEwQkYsUUFBMUI7QUFDQSxNQUFJRyxXQUFXQyxLQUFLQyxLQUFMLENBQVdQLGdCQUFnQmQsZUFBM0IsQ0FBZjtBQUNBLE1BQUlnQixZQUFZZixVQUFoQixFQUE0QjtBQUMxQmdCLFlBQVFDLEdBQVIsQ0FBWSxXQUFaO0FBQ0FDLGVBQVdDLEtBQUtDLEtBQUwsQ0FBV1AsZ0JBQWdCZCxlQUEzQixJQUE4QyxDQUF6RDtBQUNELEdBSEQsTUFHTyxJQUFJZ0IsV0FBV2hCLGVBQVgsR0FBNkJFLGtCQUFqQyxFQUFxRDtBQUMxRGlCLGdCQUFZLENBQVo7QUFDRDs7QUFFREYsVUFBUUMsR0FBUixDQUFZLGtCQUFaLEVBQWdDQyxRQUFoQzs7QUFFQSxNQUFNRyxRQUFRaEIsZ0JBQWdCaUIsS0FBaEIsQ0FBc0IsQ0FBdEIsRUFBeUJKLFFBQXpCLEVBQW1DWixHQUFuQyxDQUF1QztBQUFBLFdBQVcsQ0FBQ0MsT0FBRCxDQUFYO0FBQUEsR0FBdkMsQ0FBZDtBQUNBRixvQkFBa0JBLGdCQUFnQmlCLEtBQWhCLENBQXNCSixRQUF0QixDQUFsQjs7QUFFQSxPQUFLLElBQUlLLElBQUl4QixrQkFBa0IsQ0FBL0IsRUFBa0N3QixJQUFJLENBQXRDLEVBQXlDQSxHQUF6QyxFQUE4QztBQUM1QyxTQUFLLElBQUlDLFNBQVMsQ0FBYixFQUFnQkMsYUFBYUosTUFBTVAsTUFBeEMsRUFBZ0RVLFNBQVNDLFVBQXpELEVBQXFFRCxRQUFyRSxFQUErRTtBQUM3RSxVQUFNRSxPQUFPTCxNQUFNRyxNQUFOLENBQWI7QUFDQSxVQUFJRyxZQUFZLENBQUMsQ0FBakI7QUFDQSxVQUFJQyxnQkFBZ0IsSUFBcEI7QUFDQSxVQUFJQyxXQUFXLElBQWY7O0FBRUEsV0FBSyxJQUFJQyxZQUFZLENBQXJCLEVBQXdCQSxZQUFZWCxLQUFLQyxLQUFMLENBQVdmLGdCQUFnQlMsTUFBaEIsR0FBeUJTLENBQXBDLENBQXBDLEVBQTRFTyxXQUE1RSxFQUF5RjtBQUN2RixZQUFNdkIsVUFBVUYsZ0JBQWdCeUIsU0FBaEIsQ0FBaEI7QUFDQSxZQUFNQyxVQUFVTCxLQUFLTSxNQUFMLENBQVksQ0FBQ3pCLE9BQUQsQ0FBWixDQUFoQjtBQUNBLFlBQU0wQixnQkFBZ0JGLFFBQVF6QixHQUFSLENBQVk7QUFBQSxpQkFBZUcsY0FBY3lCLFdBQWQsQ0FBZjtBQUFBLFNBQVosQ0FBdEI7QUFDQSxZQUFJQyxRQUFRQyxlQUFLRCxLQUFMLENBQVdGLGFBQVgsQ0FBWjtBQUNBRSxnQkFBUUEsUUFBUSxDQUFSLEdBQVksQ0FBWixHQUFnQkEsS0FBeEI7O0FBRUEsWUFBSUEsUUFBUVIsU0FBWixFQUF1QjtBQUNyQkEsc0JBQVlRLEtBQVo7QUFDQU4scUJBQVdFLE9BQVg7QUFDQUgsMEJBQWdCRSxTQUFoQjtBQUNEO0FBQ0Y7O0FBRUQsVUFBSUQsYUFBYSxJQUFqQixFQUF1QjtBQUNyQlIsY0FBTUcsTUFBTixJQUFnQnJCLE1BQU1DLElBQU4sQ0FBV3lCLFFBQVgsQ0FBaEI7QUFDRDtBQUNELFVBQUlELGtCQUFrQixJQUF0QixFQUE0QjtBQUMxQixZQUFNUyxNQUFNaEMsZ0JBQWdCUyxNQUE1QjtBQUNBVCwwQkFBa0JBLGdCQUFnQmlCLEtBQWhCLENBQXNCLENBQXRCLEVBQXlCTSxhQUF6QixFQUF3Q0ksTUFBeEMsQ0FBK0MzQixnQkFBZ0JpQixLQUFoQixDQUFzQk0sZ0JBQWdCLENBQXRDLENBQS9DLENBQWxCO0FBQ0Q7QUFDRjtBQUNGOztBQUVELE1BQUl2QixnQkFBZ0JTLE1BQWhCLEdBQXlCLENBQTdCLEVBQWdDO0FBQzlCO0FBQ0EsU0FBSyxJQUFJZ0IsYUFBWSxDQUFyQixFQUF3QkEsYUFBWXpCLGdCQUFnQlMsTUFBcEQsRUFBNERnQixZQUE1RCxFQUF5RTtBQUN2RSxVQUFNdkIsV0FBVUYsZ0JBQWdCeUIsVUFBaEIsQ0FBaEI7QUFDQSxVQUFJRCxZQUFXLElBQWY7QUFDQSxVQUFJUyxhQUFhLENBQUMsQ0FBbEI7QUFDQSxVQUFJWCxhQUFZLENBQUMsQ0FBakI7O0FBRUEsV0FBSyxJQUFJSCxVQUFTLENBQWIsRUFBZ0JDLGNBQWFKLE1BQU1QLE1BQXhDLEVBQWdEVSxVQUFTQyxXQUF6RCxFQUFxRUQsU0FBckUsRUFBK0U7QUFDN0UsWUFBTUUsUUFBT0wsTUFBTUcsT0FBTixDQUFiOztBQUVBLFlBQU1PLFdBQVVMLE1BQUtNLE1BQUwsQ0FBWSxDQUFDekIsUUFBRCxDQUFaLENBQWhCO0FBQ0EsWUFBTTBCLGlCQUFnQkYsU0FBUXpCLEdBQVIsQ0FBWTtBQUFBLGlCQUFlRyxjQUFjeUIsV0FBZCxDQUFmO0FBQUEsU0FBWixDQUF0QjtBQUNBLFlBQUlDLFNBQVFDLGVBQUtELEtBQUwsQ0FBV0YsY0FBWCxDQUFaO0FBQ0FFLGlCQUFRQSxTQUFRLENBQVIsR0FBWSxDQUFaLEdBQWdCQSxNQUF4Qjs7QUFFQSxZQUFJQSxTQUFRUixVQUFaLEVBQXVCO0FBQ3JCQSx1QkFBWVEsTUFBWjtBQUNBTixzQkFBV0UsUUFBWDtBQUNBTyx1QkFBYWQsT0FBYjtBQUNEO0FBQ0Y7O0FBRUQsVUFBSUssY0FBYSxJQUFqQixFQUF1QjtBQUNyQlIsY0FBTWlCLFVBQU4sSUFBb0JuQyxNQUFNQyxJQUFOLENBQVd5QixTQUFYLENBQXBCO0FBQ0Q7QUFDRjtBQUNGOztBQUVELFNBQU9SLE1BQU1mLEdBQU4sQ0FBVTtBQUFBLFdBQVEsSUFBSThCLGNBQUosQ0FBU1YsS0FBS3BCLEdBQUwsQ0FBUztBQUFBLGFBQWVHLGNBQWN5QixXQUFkLENBQWY7QUFBQSxLQUFULENBQVQsQ0FBUjtBQUFBLEdBQVYsQ0FBUDtBQUNELENBeEZEOztBQTBGQSxJQUFNSyxxQkFBcUIsU0FBckJBLGtCQUFxQixDQUFDbEIsS0FBRCxFQUFXO0FBQ3BDLE9BQUssSUFBSUcsU0FBUyxDQUFiLEVBQWdCZ0IsSUFBSW5CLE1BQU1QLE1BQS9CLEVBQXVDVSxTQUFTZ0IsQ0FBaEQsRUFBbURoQixRQUFuRCxFQUE2RDtBQUMzRCxRQUFNRSxPQUFPTCxNQUFNRyxNQUFOLENBQWI7QUFDQSxRQUFNaUIsUUFBUWYsS0FBSzVCLFFBQUwsQ0FBY1ksTUFBZCxDQUFxQixVQUFDZ0MsR0FBRCxFQUFNOUIsSUFBTjtBQUFBLGFBQWU4QixJQUFJVixNQUFKLENBQVdwQixLQUFLNkIsS0FBaEIsQ0FBZjtBQUFBLEtBQXJCLEVBQTRELEVBQTVELENBQWQ7O0FBRUEsUUFBSUUsZ0JBQWdCLENBQXBCO0FBQ0EsUUFBSUMsYUFBYSxJQUFqQjs7QUFMMkQsK0JBT2xEQyxNQVBrRCxFQU90Q0MsUUFQc0M7QUFRekQsVUFBTUMsT0FBT04sTUFBTUksTUFBTixDQUFiO0FBQ0EsVUFBSVYsUUFBUSxDQUFDLENBQWI7QUFDQVQsV0FBSzVCLFFBQUwsQ0FBY2tELE9BQWQsQ0FBc0IsVUFBQ3pDLE9BQUQsRUFBYTtBQUNqQyxZQUFNMEMsU0FBU2IsZUFBS2MsaUJBQUwsQ0FBdUIzQyxPQUF2QixFQUFnQ3dDLElBQWhDLENBQWY7QUFDQSxZQUFJLENBQUNFLE1BQUwsRUFBYWQsU0FBUyxJQUFUO0FBQ2JBLGlCQUFTYyxNQUFUO0FBQ0QsT0FKRDs7QUFNQSxVQUFJZCxRQUFRUSxhQUFaLEVBQTJCO0FBQ3pCQSx3QkFBZ0JSLEtBQWhCO0FBQ0FTLHFCQUFhQyxNQUFiO0FBQ0Q7QUFuQndEOztBQU8zRCxTQUFLLElBQUlBLFNBQVMsQ0FBYixFQUFnQkMsV0FBV0wsTUFBTTNCLE1BQXRDLEVBQThDK0IsU0FBU0MsUUFBdkQsRUFBaUVELFFBQWpFLEVBQTJFO0FBQUEsWUFBbEVBLE1BQWtFLEVBQXREQyxRQUFzRDtBQWExRTs7QUFFRCxRQUFJRixlQUFlLElBQW5CLEVBQXlCdkIsTUFBTUcsTUFBTixFQUFjdUIsSUFBZCxHQUFxQk4sTUFBTUcsVUFBTixDQUFyQjtBQUMxQjs7QUFFRCxTQUFPdkIsS0FBUDtBQUNELENBM0JEOztJQTZCcUI4QixLO0FBQ25CLGlCQUFZckQsUUFBWixFQUFzQnNELGFBQXRCLEVBQXFDO0FBQUE7O0FBQ25DLFNBQUtDLFNBQUwsR0FBaUIsSUFBSUMsR0FBSixDQUFReEQsU0FBU1EsR0FBVCxDQUFhO0FBQUEsYUFBVyxJQUFJaUQsaUJBQUosQ0FBWWhELE9BQVosRUFBcUI2QyxhQUFyQixDQUFYO0FBQUEsS0FBYixDQUFSLENBQWpCO0FBQ0Q7Ozs7MkJBRWlGO0FBQUEsVUFBN0VJLE1BQTZFLHVFQUFwRSxLQUFvRTtBQUFBLFVBQTdEekQsZUFBNkQsdUVBQTNDLENBQTJDO0FBQUEsVUFBeENDLFVBQXdDLHVFQUEzQixDQUEyQjtBQUFBLFVBQXhCQyxrQkFBd0IsdUVBQUgsQ0FBRzs7QUFDaEYsVUFBSXVELFdBQVcsSUFBZixFQUFxQnhDLFFBQVF5QyxJQUFSLENBQWEsTUFBYjs7QUFFckIsVUFBTXBDLFFBQVF4QixjQUFjLEtBQUt3RCxTQUFuQixFQUE4QnRELGVBQTlCLEVBQStDQyxVQUEvQyxFQUEyREMsa0JBQTNELENBQWQ7O0FBRUEsVUFBSXVELFdBQVcsSUFBZixFQUFxQnhDLFFBQVEwQyxPQUFSLENBQWdCLE1BQWhCOztBQUVyQixhQUFPckMsS0FBUDtBQUNEOzs7eUJBRVdBLEssRUFBTztBQUNqQixVQUFNdkIsV0FBVyxFQUFqQjtBQURpQjtBQUFBO0FBQUE7O0FBQUE7QUFFakIsNkJBQW1CdUIsS0FBbkIsOEhBQTBCO0FBQUEsY0FBZkssSUFBZTtBQUFBO0FBQUE7QUFBQTs7QUFBQTtBQUN4QixrQ0FBc0JBLEtBQUsyQixTQUEzQixtSUFBc0M7QUFBQSxrQkFBM0I5QyxPQUEyQjs7QUFDcENULHVCQUFTNkQsSUFBVCxDQUFjcEQsT0FBZDtBQUNEO0FBSHVCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFJekI7QUFOZ0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFRakIsVUFBSXFELGFBQWEsQ0FBakI7QUFSaUI7QUFBQTtBQUFBOztBQUFBO0FBU2pCLDhCQUF1QjlELFFBQXZCLG1JQUFpQztBQUFBLGNBQXRCK0QsUUFBc0I7QUFBQTtBQUFBO0FBQUE7O0FBQUE7QUFDL0Isa0NBQXVCL0QsUUFBdkIsbUlBQWlDO0FBQUEsa0JBQXRCZ0UsUUFBc0I7O0FBQy9CLGtCQUFJRCxTQUFTckQsTUFBVCxLQUFvQnNELFNBQVN0RCxNQUFqQyxFQUF5QztBQUN2Q1Esd0JBQVFDLEdBQVIsQ0FBWSxhQUFaLEVBQTJCNEMsU0FBU0UsTUFBVCxDQUFnQkMsSUFBM0M7QUFDQUosOEJBQWMsQ0FBZDtBQUNEO0FBQ0Y7QUFOOEI7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQU9oQztBQWhCZ0I7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTs7QUFpQmpCQSxvQkFBYzlELFNBQVNnQixNQUF2QjtBQUNBLGFBQU84QyxVQUFQO0FBQ0Q7Ozs7OztrQkFsQ2tCVCxLOzs7QUFxQ3JCLFNBQVNjLGlCQUFULENBQTJCbEIsSUFBM0IsRUFBaUM7QUFDL0IsTUFBTW1CLFVBQVVDLE9BQU9DLE1BQVAsQ0FBYyxFQUFkLEVBQWtCckIsSUFBbEIsQ0FBaEI7QUFDQW1CLFVBQVFHLElBQVIsR0FBZSxDQUFDLFlBQUQsRUFBZSxLQUFmLEVBQXNCLElBQXRCLEVBQTRCLFdBQTVCLEVBQXlDbEQsS0FBS21ELEtBQUwsQ0FBV25ELEtBQUtvRCxNQUFMLEtBQWdCLENBQTNCLENBQXpDLENBQWY7QUFDQUwsVUFBUU0sUUFBUixHQUFtQixDQUFDLFlBQUQsRUFBZSxLQUFmLEVBQXNCLElBQXRCLEVBQTRCLFdBQTVCLEVBQXlDQyxPQUF6QyxDQUFpRFAsUUFBUUcsSUFBekQsQ0FBbkI7QUFDQSxTQUFPSCxPQUFQO0FBQ0Q7O0FBRU0sU0FBU3ZFLHNCQUFULENBQWdDK0UsV0FBaEMsRUFBNkNqQyxLQUE3QyxFQUFvRGtDLEtBQXBELEVBQTJEO0FBQ2hFLE1BQU03RSxXQUFXLEVBQWpCO0FBQ0EsT0FBSyxJQUFJeUIsSUFBSSxDQUFiLEVBQWdCQSxJQUFJbUQsV0FBcEIsRUFBaUNuRCxLQUFLLENBQXRDLEVBQXlDO0FBQ3ZDLFFBQU1xRCxNQUFNbkMsTUFBTW5DLEdBQU4sQ0FBVTtBQUFBLGFBQVEyRCxrQkFBa0JsQixJQUFsQixDQUFSO0FBQUEsS0FBVixDQUFaO0FBQ0EsUUFBTThCLGNBQWM7QUFDbEJDLHFCQUFlLENBQUMsTUFBRCxFQUFTLE1BQVQsRUFBaUIzRCxLQUFLbUQsS0FBTCxDQUFXbkQsS0FBS29ELE1BQUwsRUFBWCxDQUFqQixDQURHO0FBRWxCUSxpQkFBVyxDQUFDLE1BQUQsRUFBUyxNQUFULEVBQWlCLFdBQWpCLEVBQThCLFVBQTlCLEVBQTBDNUQsS0FBS21ELEtBQUwsQ0FBV25ELEtBQUtvRCxNQUFMLEtBQWdCLENBQTNCLENBQTFDLENBRk87QUFHbEJQLFlBQU1XLE1BQU1wRCxDQUFOLENBSFk7QUFJbEJ5RCxpQkFBVyxDQUNULFVBQVU3RCxLQUFLbUQsS0FBTCxDQUFXbkQsS0FBS29ELE1BQUwsS0FBZ0IsQ0FBM0IsQ0FBVixDQURTLEVBRVQsVUFBVXBELEtBQUttRCxLQUFMLENBQVduRCxLQUFLb0QsTUFBTCxLQUFnQixDQUEzQixDQUFWLENBRlMsRUFHVCxVQUFVcEQsS0FBS21ELEtBQUwsQ0FBV25ELEtBQUtvRCxNQUFMLEtBQWdCLENBQTNCLENBQVYsQ0FIUyxFQUlULFVBQVVwRCxLQUFLbUQsS0FBTCxDQUFXbkQsS0FBS29ELE1BQUwsS0FBZ0IsQ0FBM0IsQ0FBVixDQUpTLEVBS1QsVUFBVXBELEtBQUttRCxLQUFMLENBQVduRCxLQUFLb0QsTUFBTCxLQUFnQixDQUEzQixDQUFWLENBTFMsQ0FKTztBQVdsQlUsWUFBTSxDQUFDLEdBQUQsRUFBTSxHQUFOLEVBQVcsR0FBWCxFQUFnQixHQUFoQixFQUFxQjlELEtBQUttRCxLQUFMLENBQVduRCxLQUFLb0QsTUFBTCxLQUFnQixDQUEzQixDQUFyQixDQVhZO0FBWWxCVyxvQkFBYyxDQUFDLENBQUMsR0FBRCxFQUFNLEdBQU4sRUFBVyxHQUFYLEVBQWdCLEdBQWhCLEVBQXFCL0QsS0FBS21ELEtBQUwsQ0FBV25ELEtBQUtvRCxNQUFMLEtBQWdCLENBQTNCLENBQXJCLENBQUQsQ0FaSTtBQWFsQlksY0FBUTtBQUNOQyx1QkFBZWpFLEtBQUttRCxLQUFMLENBQVduRCxLQUFLb0QsTUFBTCxLQUFnQixDQUEzQixDQURUO0FBRU5jLGdCQUFRbEUsS0FBS21ELEtBQUwsQ0FBV25ELEtBQUtvRCxNQUFMLEtBQWdCLENBQTNCLENBRkY7QUFHTmUsbUJBQVduRSxLQUFLbUQsS0FBTCxDQUFXbkQsS0FBS29ELE1BQUwsS0FBZ0IsQ0FBM0IsQ0FITDtBQUlOZ0Isb0JBQVlwRSxLQUFLbUQsS0FBTCxDQUFXbkQsS0FBS29ELE1BQUwsS0FBZ0IsQ0FBM0IsQ0FKTjtBQUtOaUIscUJBQWFyRSxLQUFLbUQsS0FBTCxDQUFXbkQsS0FBS29ELE1BQUwsS0FBZ0IsQ0FBM0IsQ0FMUDtBQU1Oa0IsaUJBQVN0RSxLQUFLbUQsS0FBTCxDQUFXbkQsS0FBS29ELE1BQUwsS0FBZ0IsQ0FBM0I7QUFOSCxPQWJVO0FBcUJsQm1CLGNBQVEsQ0FBQyxNQUFELEVBQVMsUUFBVCxFQUFtQnZFLEtBQUttRCxLQUFMLENBQVduRCxLQUFLb0QsTUFBTCxFQUFYLENBQW5CLENBckJVO0FBc0JsQm9CLGlCQUFXLENBQUMsTUFBRCxFQUFTLFFBQVQsRUFBbUJ4RSxLQUFLbUQsS0FBTCxDQUFXbkQsS0FBS29ELE1BQUwsRUFBWCxDQUFuQixDQXRCTztBQXVCbEJxQix3QkFBa0IsMkNBdkJBO0FBd0JsQkMsdUJBQWlCLENBQUMsR0FBRCxFQUFNLEdBQU4sRUFBVzFFLEtBQUttRCxLQUFMLENBQVduRCxLQUFLb0QsTUFBTCxFQUFYLENBQVgsSUFDRyxDQUFDLEdBQUQsRUFBTSxHQUFOLEVBQVdwRCxLQUFLbUQsS0FBTCxDQUFXbkQsS0FBS29ELE1BQUwsRUFBWCxDQUFYLENBREgsR0FFRyxDQUFDLEdBQUQsRUFBTSxHQUFOLEVBQVdwRCxLQUFLbUQsS0FBTCxDQUFXbkQsS0FBS29ELE1BQUwsRUFBWCxDQUFYLENBRkgsR0FHRyxDQUFDLEdBQUQsRUFBTSxHQUFOLEVBQVdwRCxLQUFLbUQsS0FBTCxDQUFXbkQsS0FBS29ELE1BQUwsRUFBWCxDQUFYLENBM0JGO0FBNEJsQjlCLGFBQU9tQztBQTVCVyxLQUFwQjtBQThCQTlFLGFBQVM2RCxJQUFULENBQWNrQixXQUFkO0FBQ0Q7QUFDRCxTQUFPL0UsUUFBUDtBQUNEOztBQUVNLFNBQVNGLG1CQUFULENBQTZCa0csUUFBN0IsRUFBdUNDLE1BQXZDLEVBQStDO0FBQ3BELE1BQU10RCxRQUFRLEVBQWQ7QUFDQSxPQUFLLElBQUlsQixJQUFJLENBQWIsRUFBZ0JBLElBQUl1RSxRQUFwQixFQUE4QnZFLEdBQTlCLEVBQW1DO0FBQ2pDa0IsVUFBTWtCLElBQU4sQ0FBVztBQUNUSyxZQUFNN0MsS0FBS29ELE1BQUwsR0FBY3lCLFFBQWQsQ0FBdUIsRUFBdkIsRUFBMkJDLE1BQTNCLENBQWtDLENBQWxDLEVBQXFDLENBQXJDLENBREc7QUFFVEMsVUFBSTNFLENBRks7QUFHVDRFLGVBQVM7QUFDUEMsZUFBT0wsT0FBTzVFLEtBQUttRCxLQUFMLENBQVduRCxLQUFLb0QsTUFBTCxLQUFnQndCLE9BQU9qRixNQUFsQyxDQUFQO0FBREEsT0FIQTtBQU1UdUQsWUFBTSxDQUFDLFlBQUQsRUFBZSxLQUFmLEVBQXNCLElBQXRCLEVBQTRCLFdBQTVCLEVBQXlDbEQsS0FBS21ELEtBQUwsQ0FBV25ELEtBQUtvRCxNQUFMLEtBQWdCLENBQTNCLENBQXpDO0FBTkcsS0FBWDtBQVFEOztBQUVELFNBQU85QixLQUFQO0FBQ0QiLCJmaWxlIjoibWF0Y2guanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge1xuICBTdHVkZW50UGFpclNldCwgU3R1ZGVudFNldCwgU29ydGVkU3R1ZGVudFNldCwgZGVmYXVsdCBhcyBTdHVkZW50LFxufSBmcm9tICcuL3N0dWRlbnQnO1xuaW1wb3J0IFRlYW0gZnJvbSAnLi90ZWFtJztcblxuY29uc3QgZ3JlZWR5UGFpcmluZyA9IChzdHVkZW50cywgc3R1ZGVudHNQZXJUZWFtLCBtaW5QZXJUZWFtLCBtYXhTdHVkZW50c1BlclRlYW0pID0+IHtcbiAgY29uc3Qgc3R1ZGVudEFycmF5ID0gQXJyYXkuZnJvbShzdHVkZW50cyk7XG4gIGxldCBtdXRhYmxlU3R1ZGVudHMgPSBzdHVkZW50QXJyYXkubWFwKHN0dWRlbnQgPT4gc3R1ZGVudC5wX2hhc2gpO1xuICBjb25zdCBzdHVkZW50SGFzaGVzID0gc3R1ZGVudEFycmF5LnJlZHVjZSgocHJldiwgY3VycikgPT4ge1xuICAgIHByZXZbY3Vyci5wX2hhc2hdID0gY3VycjtcbiAgICByZXR1cm4gcHJldjtcbiAgfSwge30pO1xuXG4gIC8vIGlmIHdlIGhhdmUgc2F5Li4uIDEwIHN0dWRlbnRzIGFuZCAzIHBlciB0ZWFtIHRoZW4gdGhlcmUgaXMgb25lIGxlZnQgb3Zlci4gTG9iIG9udG8gdGhlIHByZXZpb3VzXG4gIC8vIGlmIHdlIGhhdmUgc2F5Li4uIDExIHN0dWRlbnRzIGFuZCAzIHBlciB0ZWFtIHRoZW4gdGhlcmUgaXMgdHdvIGxlZnQgb3ZlciwgZGlzdHJpYnV0ZSB0byB0aGUgcHJldmlvdXNcbiAgLy8gaWYgd2UgaGF2ZSBzYXkuLi4gMTEgc3R1ZGVudHMgYW5kIDQgcGVyIHRlYW0gdGhlbiB0aGVyZSBpcyAyIHRlYW1zIGFuZCB0aHJlZSBsZWZ0IG92ZXIgc28gbWFrZSBhIHRlYW1cbiAgY29uc3QgdG90YWxTdHVkZW50cyA9IG11dGFibGVTdHVkZW50cy5sZW5ndGg7XG4gIGNvbnN0IGxlZnRPdmVyID0gdG90YWxTdHVkZW50cyAlIHN0dWRlbnRzUGVyVGVhbTtcbiAgY29uc29sZS5sb2coJ0xlZnQgT3ZlciAnLCBsZWZ0T3Zlcik7XG4gIGxldCB0ZWFtU2l6ZSA9IE1hdGguZmxvb3IodG90YWxTdHVkZW50cyAvIHN0dWRlbnRzUGVyVGVhbSk7XG4gIGlmIChsZWZ0T3ZlciA+PSBtaW5QZXJUZWFtKSB7XG4gICAgY29uc29sZS5sb2coJ3NwaWxsb3ZlcicpO1xuICAgIHRlYW1TaXplID0gTWF0aC5mbG9vcih0b3RhbFN0dWRlbnRzIC8gc3R1ZGVudHNQZXJUZWFtKSArIDE7XG4gIH0gZWxzZSBpZiAobGVmdE92ZXIgKyBzdHVkZW50c1BlclRlYW0gPiBtYXhTdHVkZW50c1BlclRlYW0pIHtcbiAgICB0ZWFtU2l6ZSArPSAxO1xuICB9XG5cbiAgY29uc29sZS5sb2coJ051bWJlciBvZiB0ZWFtcyAnLCB0ZWFtU2l6ZSk7XG5cbiAgY29uc3QgdGVhbXMgPSBtdXRhYmxlU3R1ZGVudHMuc2xpY2UoMCwgdGVhbVNpemUpLm1hcChzdHVkZW50ID0+IFtzdHVkZW50XSk7XG4gIG11dGFibGVTdHVkZW50cyA9IG11dGFibGVTdHVkZW50cy5zbGljZSh0ZWFtU2l6ZSk7XG5cbiAgZm9yIChsZXQgaSA9IHN0dWRlbnRzUGVyVGVhbSAtIDE7IGkgPiAwOyBpLS0pIHtcbiAgICBmb3IgKGxldCB0ZWFtSWQgPSAwLCB0b3RhbFRlYW1zID0gdGVhbXMubGVuZ3RoOyB0ZWFtSWQgPCB0b3RhbFRlYW1zOyB0ZWFtSWQrKykge1xuICAgICAgY29uc3QgdGVhbSA9IHRlYW1zW3RlYW1JZF07XG4gICAgICBsZXQgYmVzdFNjb3JlID0gLTE7XG4gICAgICBsZXQgYmVzdFN0dWRlbnRJZCA9IG51bGw7XG4gICAgICBsZXQgYmVzdFRlYW0gPSBudWxsO1xuXG4gICAgICBmb3IgKGxldCBzdHVkZW50SWQgPSAwOyBzdHVkZW50SWQgPCBNYXRoLmZsb29yKG11dGFibGVTdHVkZW50cy5sZW5ndGggLyBpKTsgc3R1ZGVudElkKyspIHtcbiAgICAgICAgY29uc3Qgc3R1ZGVudCA9IG11dGFibGVTdHVkZW50c1tzdHVkZW50SWRdO1xuICAgICAgICBjb25zdCBuZXdUZWFtID0gdGVhbS5jb25jYXQoW3N0dWRlbnRdKTtcbiAgICAgICAgY29uc3QgbWFwcGVkTmV3VGVhbSA9IG5ld1RlYW0ubWFwKHN0dWRlbnRIYXNoID0+IHN0dWRlbnRIYXNoZXNbc3R1ZGVudEhhc2hdKTtcbiAgICAgICAgbGV0IHNjb3JlID0gVGVhbS5zY29yZShtYXBwZWROZXdUZWFtKTtcbiAgICAgICAgc2NvcmUgPSBzY29yZSA8IDAgPyAwIDogc2NvcmU7XG5cbiAgICAgICAgaWYgKHNjb3JlID4gYmVzdFNjb3JlKSB7XG4gICAgICAgICAgYmVzdFNjb3JlID0gc2NvcmU7XG4gICAgICAgICAgYmVzdFRlYW0gPSBuZXdUZWFtO1xuICAgICAgICAgIGJlc3RTdHVkZW50SWQgPSBzdHVkZW50SWQ7XG4gICAgICAgIH1cbiAgICAgIH1cblxuICAgICAgaWYgKGJlc3RUZWFtICE9PSBudWxsKSB7XG4gICAgICAgIHRlYW1zW3RlYW1JZF0gPSBBcnJheS5mcm9tKGJlc3RUZWFtKTtcbiAgICAgIH1cbiAgICAgIGlmIChiZXN0U3R1ZGVudElkICE9PSBudWxsKSB7XG4gICAgICAgIGNvbnN0IHByZSA9IG11dGFibGVTdHVkZW50cy5sZW5ndGg7XG4gICAgICAgIG11dGFibGVTdHVkZW50cyA9IG11dGFibGVTdHVkZW50cy5zbGljZSgwLCBiZXN0U3R1ZGVudElkKS5jb25jYXQobXV0YWJsZVN0dWRlbnRzLnNsaWNlKGJlc3RTdHVkZW50SWQgKyAxKSk7XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgaWYgKG11dGFibGVTdHVkZW50cy5sZW5ndGggPiAwKSB7XG4gICAgLy8gZGlzdHJpYnV0ZVxuICAgIGZvciAobGV0IHN0dWRlbnRJZCA9IDA7IHN0dWRlbnRJZCA8IG11dGFibGVTdHVkZW50cy5sZW5ndGg7IHN0dWRlbnRJZCsrKSB7XG4gICAgICBjb25zdCBzdHVkZW50ID0gbXV0YWJsZVN0dWRlbnRzW3N0dWRlbnRJZF07XG4gICAgICBsZXQgYmVzdFRlYW0gPSBudWxsO1xuICAgICAgbGV0IGJlc3RUZWFtSWQgPSAtMTtcbiAgICAgIGxldCBiZXN0U2NvcmUgPSAtMTtcblxuICAgICAgZm9yIChsZXQgdGVhbUlkID0gMCwgdG90YWxUZWFtcyA9IHRlYW1zLmxlbmd0aDsgdGVhbUlkIDwgdG90YWxUZWFtczsgdGVhbUlkKyspIHtcbiAgICAgICAgY29uc3QgdGVhbSA9IHRlYW1zW3RlYW1JZF07XG5cbiAgICAgICAgY29uc3QgbmV3VGVhbSA9IHRlYW0uY29uY2F0KFtzdHVkZW50XSk7XG4gICAgICAgIGNvbnN0IG1hcHBlZE5ld1RlYW0gPSBuZXdUZWFtLm1hcChzdHVkZW50SGFzaCA9PiBzdHVkZW50SGFzaGVzW3N0dWRlbnRIYXNoXSk7XG4gICAgICAgIGxldCBzY29yZSA9IFRlYW0uc2NvcmUobWFwcGVkTmV3VGVhbSk7XG4gICAgICAgIHNjb3JlID0gc2NvcmUgPCAwID8gMCA6IHNjb3JlO1xuXG4gICAgICAgIGlmIChzY29yZSA+IGJlc3RTY29yZSkge1xuICAgICAgICAgIGJlc3RTY29yZSA9IHNjb3JlO1xuICAgICAgICAgIGJlc3RUZWFtID0gbmV3VGVhbTtcbiAgICAgICAgICBiZXN0VGVhbUlkID0gdGVhbUlkO1xuICAgICAgICB9XG4gICAgICB9XG5cbiAgICAgIGlmIChiZXN0VGVhbSAhPT0gbnVsbCkge1xuICAgICAgICB0ZWFtc1tiZXN0VGVhbUlkXSA9IEFycmF5LmZyb20oYmVzdFRlYW0pO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHJldHVybiB0ZWFtcy5tYXAodGVhbSA9PiBuZXcgVGVhbSh0ZWFtLm1hcChzdHVkZW50SGFzaCA9PiBzdHVkZW50SGFzaGVzW3N0dWRlbnRIYXNoXSkpKTtcbn07XG5cbmNvbnN0IHBhaXJJZGVhc1dpdGhUZWFtcyA9ICh0ZWFtcykgPT4ge1xuICBmb3IgKGxldCB0ZWFtSWQgPSAwLCByID0gdGVhbXMubGVuZ3RoOyB0ZWFtSWQgPCByOyB0ZWFtSWQrKykge1xuICAgIGNvbnN0IHRlYW0gPSB0ZWFtc1t0ZWFtSWRdO1xuICAgIGNvbnN0IGlkZWFzID0gdGVhbS5zdHVkZW50cy5yZWR1Y2UoKG9sZCwgY3VycikgPT4gb2xkLmNvbmNhdChjdXJyLmlkZWFzKSwgW10pO1xuXG4gICAgbGV0IGJlc3RJZGVhU2NvcmUgPSAwO1xuICAgIGxldCBiZXN0SWRlYUlkID0gbnVsbDtcblxuICAgIGZvciAobGV0IGlkZWFJZCA9IDAsIHRvdElkZWFzID0gaWRlYXMubGVuZ3RoOyBpZGVhSWQgPCB0b3RJZGVhczsgaWRlYUlkKyspIHtcbiAgICAgIGNvbnN0IGlkZWEgPSBpZGVhc1tpZGVhSWRdO1xuICAgICAgbGV0IHNjb3JlID0gLTE7XG4gICAgICB0ZWFtLnN0dWRlbnRzLmZvckVhY2goKHN0dWRlbnQpID0+IHtcbiAgICAgICAgY29uc3QgY29tcGF0ID0gVGVhbS5pZGVhQ29tcGF0YWJpbGl0eShzdHVkZW50LCBpZGVhKTtcbiAgICAgICAgaWYgKCFjb21wYXQpIHNjb3JlIC09IDEwMDA7XG4gICAgICAgIHNjb3JlICs9IGNvbXBhdDtcbiAgICAgIH0pO1xuXG4gICAgICBpZiAoc2NvcmUgPiBiZXN0SWRlYVNjb3JlKSB7XG4gICAgICAgIGJlc3RJZGVhU2NvcmUgPSBzY29yZTtcbiAgICAgICAgYmVzdElkZWFJZCA9IGlkZWFJZDtcbiAgICAgIH1cbiAgICB9XG5cbiAgICBpZiAoYmVzdElkZWFJZCAhPT0gbnVsbCkgdGVhbXNbdGVhbUlkXS5pZGVhID0gaWRlYXNbYmVzdElkZWFJZF07XG4gIH1cblxuICByZXR1cm4gdGVhbXM7XG59O1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBNYXRjaCB7XG4gIGNvbnN0cnVjdG9yKHN0dWRlbnRzLCB0ZWFtaW5nQ29uZmlnKSB7XG4gICAgdGhpcy5fc3R1ZGVudHMgPSBuZXcgU2V0KHN0dWRlbnRzLm1hcChzdHVkZW50ID0+IG5ldyBTdHVkZW50KHN0dWRlbnQsIHRlYW1pbmdDb25maWcpKSk7XG4gIH1cblxuICB0ZWFtKHRpbWluZyA9IGZhbHNlLCBzdHVkZW50c1BlclRlYW0gPSAzLCBtaW5QZXJUZWFtID0gMywgbWF4U3R1ZGVudHNQZXJUZWFtID0gNCkge1xuICAgIGlmICh0aW1pbmcgPT09IHRydWUpIGNvbnNvbGUudGltZSgndGVhbScpO1xuXG4gICAgY29uc3QgdGVhbXMgPSBncmVlZHlQYWlyaW5nKHRoaXMuX3N0dWRlbnRzLCBzdHVkZW50c1BlclRlYW0sIG1pblBlclRlYW0sIG1heFN0dWRlbnRzUGVyVGVhbSk7XG5cbiAgICBpZiAodGltaW5nID09PSB0cnVlKSBjb25zb2xlLnRpbWVFbmQoJ3RlYW0nKTtcblxuICAgIHJldHVybiB0ZWFtcztcbiAgfVxuXG4gIHN0YXRpYyB0ZXN0KHRlYW1zKSB7XG4gICAgY29uc3Qgc3R1ZGVudHMgPSBbXTtcbiAgICBmb3IgKGNvbnN0IHRlYW0gb2YgdGVhbXMpIHtcbiAgICAgIGZvciAoY29uc3Qgc3R1ZGVudCBvZiB0ZWFtLl9zdHVkZW50cykge1xuICAgICAgICBzdHVkZW50cy5wdXNoKHN0dWRlbnQpO1xuICAgICAgfVxuICAgIH1cblxuICAgIGxldCBmYXVsdENvdW50ID0gMDtcbiAgICBmb3IgKGNvbnN0IHN0dWRlbnQxIG9mIHN0dWRlbnRzKSB7XG4gICAgICBmb3IgKGNvbnN0IHN0dWRlbnQyIG9mIHN0dWRlbnRzKSB7XG4gICAgICAgIGlmIChzdHVkZW50MS5wX2hhc2ggPT09IHN0dWRlbnQyLnBfaGFzaCkge1xuICAgICAgICAgIGNvbnNvbGUubG9nKCdkdXBsaWNhdGU6ICcsIHN0dWRlbnQxLnBfanNvbi5uYW1lKTtcbiAgICAgICAgICBmYXVsdENvdW50ICs9IDE7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG4gICAgZmF1bHRDb3VudCAtPSBzdHVkZW50cy5sZW5ndGg7XG4gICAgcmV0dXJuIGZhdWx0Q291bnQ7XG4gIH1cbn1cblxuZnVuY3Rpb24gcmFuZG9tVm90ZUZvcklkZWEoaWRlYSkge1xuICBjb25zdCBuZXdJZGVhID0gT2JqZWN0LmFzc2lnbih7fSwgaWRlYSk7XG4gIG5ld0lkZWEudm90ZSA9IFsnc3Ryb25nX3llcycsICd5ZXMnLCAnbm8nLCAnc3Ryb25nX25vJ11bTWF0aC5yb3VuZChNYXRoLnJhbmRvbSgpICogMyldO1xuICBuZXdJZGVhLnN0cmVuZ3RoID0gWydzdHJvbmdfeWVzJywgJ3llcycsICdubycsICdzdHJvbmdfbm8nXS5pbmRleE9mKG5ld0lkZWEudm90ZSk7XG4gIHJldHVybiBuZXdJZGVhO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2VuZXJhdGVSYW5kb21TdHVkZW50cyhudW1TdHVkZW50cywgaWRlYXMsIG5hbWVzKSB7XG4gIGNvbnN0IHN0dWRlbnRzID0gW107XG4gIGZvciAobGV0IGkgPSAwOyBpIDwgbnVtU3R1ZGVudHM7IGkgKz0gMSkge1xuICAgIGNvbnN0IGlkcyA9IGlkZWFzLm1hcChpZGVhID0+IHJhbmRvbVZvdGVGb3JJZGVhKGlkZWEpKTtcbiAgICBjb25zdCBzdHVkZW50SnNvbiA9IHtcbiAgICAgIGxlYXJuaW5nU3R5bGU6IFsnYWJjZCcsICdjZGVmJ11bTWF0aC5yb3VuZChNYXRoLnJhbmRvbSgpKV0sXG4gICAgICB3b3JrU3R5bGU6IFsnYWJjZCcsICdjZGVmJywgJ2FiY2QvY2RlZicsICdhYmNkL2VmZyddW01hdGgucm91bmQoTWF0aC5yYW5kb20oKSAqIDMpXSxcbiAgICAgIG5hbWU6IG5hbWVzW2ldLFxuICAgICAgaW50ZXJlc3RzOiBbXG4gICAgICAgICdhYmNkZWZnJ1tNYXRoLnJvdW5kKE1hdGgucmFuZG9tKCkgKiA2KV0sXG4gICAgICAgICdhYmNkZWZnJ1tNYXRoLnJvdW5kKE1hdGgucmFuZG9tKCkgKiA2KV0sXG4gICAgICAgICdhYmNkZWZnJ1tNYXRoLnJvdW5kKE1hdGgucmFuZG9tKCkgKiA2KV0sXG4gICAgICAgICdhYmNkZWZnJ1tNYXRoLnJvdW5kKE1hdGgucmFuZG9tKCkgKiA2KV0sXG4gICAgICAgICdhYmNkZWZnJ1tNYXRoLnJvdW5kKE1hdGgucmFuZG9tKCkgKiA2KV0sXG4gICAgICBdLFxuICAgICAgcm9sZTogWydhJywgJ2InLCAnYycsICdkJ11bTWF0aC5yb3VuZChNYXRoLnJhbmRvbSgpICogMyldLFxuICAgICAgbGF1bmNoU2tpbGxzOiBbWydhJywgJ2InLCAnYycsICdkJ11bTWF0aC5yb3VuZChNYXRoLnJhbmRvbSgpICogMyldXSxcbiAgICAgIHNraWxsczoge1xuICAgICAgICBncmFwaGljRGVzaWduOiBNYXRoLnJvdW5kKE1hdGgucmFuZG9tKCkgKiA5KSxcbiAgICAgICAgbWFraW5nOiBNYXRoLnJvdW5kKE1hdGgucmFuZG9tKCkgKiA5KSxcbiAgICAgICAgbWFya2V0aW5nOiBNYXRoLnJvdW5kKE1hdGgucmFuZG9tKCkgKiA5KSxcbiAgICAgICAgb3BlcmF0aW9uczogTWF0aC5yb3VuZChNYXRoLnJhbmRvbSgpICogOSksXG4gICAgICAgIHByb2dyYW1taW5nOiBNYXRoLnJvdW5kKE1hdGgucmFuZG9tKCkgKiA5KSxcbiAgICAgICAgc2VsbGluZzogTWF0aC5yb3VuZChNYXRoLnJhbmRvbSgpICogOSksXG4gICAgICB9LFxuICAgICAgZ2VuZGVyOiBbJ01hbGUnLCAnRmVtYWxlJ11bTWF0aC5yb3VuZChNYXRoLnJhbmRvbSgpKV0sXG4gICAgICBldGhuaWNpdHk6IFsnTWFsZScsICdGZW1hbGUnXVtNYXRoLnJvdW5kKE1hdGgucmFuZG9tKCkpXSxcbiAgICAgIGVudHJlcHJlbmV1clR5cGU6ICdDb25maWRlbmNlLCBJbmRlcGVuZGVuY2UsIFJpc2ssIEtub3dsZWRnZScsXG4gICAgICBwZXJzb25hbGl0eVR5cGU6IFsnRScsICdJJ11bTWF0aC5yb3VuZChNYXRoLnJhbmRvbSgpKV1cbiAgICAgICAgICAgICAgICAgICAgICAgICsgWydOJywgJ1MnXVtNYXRoLnJvdW5kKE1hdGgucmFuZG9tKCkpXVxuICAgICAgICAgICAgICAgICAgICAgICAgKyBbJ1QnLCAnRiddW01hdGgucm91bmQoTWF0aC5yYW5kb20oKSldXG4gICAgICAgICAgICAgICAgICAgICAgICArIFsnUCcsICdKJ11bTWF0aC5yb3VuZChNYXRoLnJhbmRvbSgpKV0sXG4gICAgICBpZGVhczogaWRzLFxuICAgIH07XG4gICAgc3R1ZGVudHMucHVzaChzdHVkZW50SnNvbik7XG4gIH1cbiAgcmV0dXJuIHN0dWRlbnRzO1xufVxuXG5leHBvcnQgZnVuY3Rpb24gZ2VuZXJhdGVSYW5kb21JZGVhcyhudW1JZGVhcywgZW1haWxzKSB7XG4gIGNvbnN0IGlkZWFzID0gW107XG4gIGZvciAobGV0IGkgPSAwOyBpIDwgbnVtSWRlYXM7IGkrKykge1xuICAgIGlkZWFzLnB1c2goe1xuICAgICAgbmFtZTogTWF0aC5yYW5kb20oKS50b1N0cmluZygzNikuc3Vic3RyKDIsIDUpLFxuICAgICAgaWQ6IGksXG4gICAgICB0aGlua2VyOiB7XG4gICAgICAgIGVtYWlsOiBlbWFpbHNbTWF0aC5yb3VuZChNYXRoLnJhbmRvbSgpICogZW1haWxzLmxlbmd0aCldLFxuICAgICAgfSxcbiAgICAgIHZvdGU6IFsnc3Ryb25nX3llcycsICd5ZXMnLCAnbm8nLCAnc3Ryb25nX25vJ11bTWF0aC5yb3VuZChNYXRoLnJhbmRvbSgpICogMyldLFxuICAgIH0pO1xuICB9XG5cbiAgcmV0dXJuIGlkZWFzO1xufVxuIl0sInNvdXJjZVJvb3QiOiIvbW50L2MvVXNlcnMvcHVyZXUvRG9jdW1lbnRzL3Byb2plY3RzL2xhdW5jaHgtdGVhbWluZy9zcmMifQ==
