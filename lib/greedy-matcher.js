'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }(); /* eslint-disable no-console */
/* eslint-disable no-continue */


var _student = require('./student');

var _student2 = _interopRequireDefault(_student);

var _team = require('./team');

var _team2 = _interopRequireDefault(_team);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

// basically stars and bars
// we have students and some seperators for them
// we need an inital estimate tho

var GreedyMatch = function () {
  // public
  function GreedyMatch(students, teamingConfig) {
    var debug = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;

    _classCallCheck(this, GreedyMatch);

    this.students = students.map(function (student) {
      return new _student2.default(student, teamingConfig);
    });
    this.debug = debug;
    this.numVisited = 0;

    this.initializeAdjacencyMatrix();

    if (this.debug) {
      console.log('DPMatch: Removing duplicates...');
    }

    this.removeDuplicatesStudents();
  }

  _createClass(GreedyMatch, [{
    key: 'getStudents',
    value: function getStudents() {
      return this.students;
    }
  }, {
    key: 'initializeAdjacencyMatrix',
    value: function initializeAdjacencyMatrix() {
      this.scoredAgainst = [];
      for (var i = 0; i < this.students.length; i += 1) {
        this.scoredAgainst.push([]);
        for (var j = 0; j < this.students.length; j += 1) {
          if (i !== j) {
            this.scoredAgainst[i].push(this.students[i].scoreAgainst(this.students[j]));
          }
        }
      }
    }
  }, {
    key: 'team',
    value: function team(_ref) {
      var _ref$minimumStudentsP = _ref.minimumStudentsPerTeam,
          minimumStudentsPerTeam = _ref$minimumStudentsP === undefined ? 3 : _ref$minimumStudentsP,
          _ref$maximumStudentsP = _ref.maximumStudentsPerTeam,
          maximumStudentsPerTeam = _ref$maximumStudentsP === undefined ? 4 : _ref$maximumStudentsP;

      // gen perms
      // dp this shiz
      this.minimumStudentsPerTeam = minimumStudentsPerTeam;
      this.maximumStudentsPerTeam = maximumStudentsPerTeam;
      this.solutionPath = [];
      this.visited = this.students.map(function () {
        return false;
      });
      this.numTeams = 0;

      if (this.debug) {
        console.log('GreedyMatch: teaming...');console.time('GreedyMatch');
      }
      this.visited[0] = true;
      this.solutionPath.push([0]);
      this.numVisited += 1;
      this.numTeams += 1;
      this.numFilledTeams = 0;
      while (this.numVisited < this.students.length) {
        this.addNextStudent();
      }
      if (this.debug) {
        console.log('GreedyMatch: done...');console.timeEnd('GreedyMatch');
      }

      if (this.debug) {
        console.log('GreedyMatch: generating teams from match...');
      }
      return this.pathToTeams(this.solutionPath);
    }

    // private

  }, {
    key: 'removeDuplicatesStudents',
    value: function removeDuplicatesStudents() {
      var dupArray = {};
      this.getStudents().forEach(function (student) {
        dupArray[student.hash] = student;
      });

      this.students = Object.values(dupArray);
    }
  }, {
    key: 'addNextStudent',
    value: function addNextStudent() {
      var studentIndex = this.nextUnvisitedStudent();

      var greatestIncreaseIndex = -1;
      var greatestIncrease = -1;
      var maxNumberOfRemainingTeams = Math.floor((this.students.length - this.numVisited) / this.minimumStudentsPerTeam);
      for (var i = 0; i < this.numTeams + maxNumberOfRemainingTeams; i += 1) {
        if (i < this.solutionPath.length && this.solutionPath[i].length < this.maximumStudentsPerTeam) {
          // test ass student
          var scoreBefore = this.scoreTeam(this.solutionPath[i]);
          this.solutionPath[i].push(studentIndex);
          var scoreAfter = this.scoreTeam(this.solutionPath[i]);
          this.solutionPath[i].pop();

          if (scoreAfter > scoreBefore && scoreAfter - scoreBefore > greatestIncrease) {
            greatestIncreaseIndex = i;
            greatestIncrease = scoreAfter - scoreBefore;
          }
        }
      }

      if (greatestIncrease === -1) {
        // if more teams than max teams, rematch
        if (this.numTeams >= this.numFilledTeams + maxNumberOfRemainingTeams) {
          // find team with least decrease
          var bestTeamIndex = 0;
          var leastDecrease = -1;
          for (var _i = 0; _i < this.solutionPath.length; _i += 1) {
            if (this.solutionPath[_i].length < this.maximumStudentsPerTeam) {
              // test ass student
              var _scoreBefore = this.scoreTeam(this.solutionPath[_i]);
              this.solutionPath[_i].push(studentIndex);
              var _scoreAfter = this.scoreTeam(this.solutionPath[_i]);
              this.solutionPath[_i].pop();

              if (leastDecrease === -1 || _scoreBefore - _scoreAfter < leastDecrease) {
                leastDecrease = _scoreBefore - _scoreAfter;
                bestTeamIndex = _i;
              }
            }
          }

          this.solutionPath[bestTeamIndex].push(studentIndex);
          if (this.solutionPath[bestTeamIndex].length >= this.maximumStudentsPerTeam) {
            this.numFilledTeams += 1;
          }
        } else {
          this.solutionPath.push([studentIndex]);
          this.numTeams += 1;
        }
      } else {
        this.solutionPath[greatestIncreaseIndex].push(studentIndex);
        if (this.solutionPath[greatestIncreaseIndex].length >= this.maximumStudentsPerTeam) {
          this.numFilledTeams += 1;
        }
      }

      this.visited[studentIndex] = true;
      this.numVisited += 1;
    }
  }, {
    key: 'nextUnvisitedStudent',
    value: function nextUnvisitedStudent() {
      var i = 0;
      while (this.visited[i]) {
        i += 1;
      }

      return i;
    }
  }, {
    key: 'pathToTeams',
    value: function pathToTeams(solutionPath) {
      var _this = this;

      var teams = [];
      for (var i = 0; i < solutionPath.length; i += 1) {
        var team = solutionPath[i].map(function (j) {
          return _this.students[j];
        });
        teams.push(new _team2.default(team));
      }

      return teams;
    }
  }, {
    key: 'calculateScore',
    value: function calculateScore(solutionPath, permLength) {
      var firstBarIndex = -1;
      var nextBarIndex = 0;

      var score = 0;
      var findFunc = function findFunc(v, i) {
        return i > nextBarIndex && v.type === 'bar';
      };
      while (nextBarIndex !== permLength) {
        firstBarIndex = nextBarIndex;
        nextBarIndex = solutionPath.findIndex(findFunc);
        if (nextBarIndex === -1 || nextBarIndex > permLength) {
          nextBarIndex = permLength;
        }

        var team = [];
        for (var i = firstBarIndex + 1; i < nextBarIndex; i += 1) {
          team.push(solutionPath[i].student);
        }

        score += this.scoreTeam(team);
      }

      return score;
    }
  }, {
    key: 'scoreTeam',
    value: function scoreTeam(team) {
      // pairwise team all members against each other
      if (team.length <= 1) {
        return 0;
      }

      var score = 0;
      var numPerms = 0;
      for (var i = 0; i < team.length; i += 1) {
        for (var j = i + 1; j < team.length; j += 1) {
          score += this.scoreStudents(team[i], team[j]);
          numPerms += 1;
        }
      }

      return score / numPerms;
    }
  }, {
    key: 'scoreStudents',
    value: function scoreStudents(a, b) {
      return this.scoredAgainst[a][b];
    }
  }, {
    key: 'bestMatch',
    value: function bestMatch(studentIndex) {
      var bestElement = -1;
      var bestScore = 0;
      for (var i = 0; i < this.students.length; i += 1) {
        if (i !== studentIndex) {
          var score = this.scoredAgainst[studentIndex][i];
          if (score > bestScore) {
            bestElement = i;
            bestScore = score;
          }
        }
      }

      return bestElement;
    }
  }]);

  return GreedyMatch;
}();

exports.default = GreedyMatch;
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImdyZWVkeS1tYXRjaGVyLmpzIl0sIm5hbWVzIjpbIkdyZWVkeU1hdGNoIiwic3R1ZGVudHMiLCJ0ZWFtaW5nQ29uZmlnIiwiZGVidWciLCJtYXAiLCJTdHVkZW50Iiwic3R1ZGVudCIsIm51bVZpc2l0ZWQiLCJpbml0aWFsaXplQWRqYWNlbmN5TWF0cml4IiwiY29uc29sZSIsImxvZyIsInJlbW92ZUR1cGxpY2F0ZXNTdHVkZW50cyIsInNjb3JlZEFnYWluc3QiLCJpIiwibGVuZ3RoIiwicHVzaCIsImoiLCJzY29yZUFnYWluc3QiLCJtaW5pbXVtU3R1ZGVudHNQZXJUZWFtIiwibWF4aW11bVN0dWRlbnRzUGVyVGVhbSIsInNvbHV0aW9uUGF0aCIsInZpc2l0ZWQiLCJudW1UZWFtcyIsInRpbWUiLCJudW1GaWxsZWRUZWFtcyIsImFkZE5leHRTdHVkZW50IiwidGltZUVuZCIsInBhdGhUb1RlYW1zIiwiZHVwQXJyYXkiLCJnZXRTdHVkZW50cyIsImZvckVhY2giLCJoYXNoIiwiT2JqZWN0IiwidmFsdWVzIiwic3R1ZGVudEluZGV4IiwibmV4dFVudmlzaXRlZFN0dWRlbnQiLCJncmVhdGVzdEluY3JlYXNlSW5kZXgiLCJncmVhdGVzdEluY3JlYXNlIiwibWF4TnVtYmVyT2ZSZW1haW5pbmdUZWFtcyIsIk1hdGgiLCJmbG9vciIsInNjb3JlQmVmb3JlIiwic2NvcmVUZWFtIiwic2NvcmVBZnRlciIsInBvcCIsImJlc3RUZWFtSW5kZXgiLCJsZWFzdERlY3JlYXNlIiwidGVhbXMiLCJ0ZWFtIiwiVGVhbSIsInBlcm1MZW5ndGgiLCJmaXJzdEJhckluZGV4IiwibmV4dEJhckluZGV4Iiwic2NvcmUiLCJmaW5kRnVuYyIsInYiLCJ0eXBlIiwiZmluZEluZGV4IiwibnVtUGVybXMiLCJzY29yZVN0dWRlbnRzIiwiYSIsImIiLCJiZXN0RWxlbWVudCIsImJlc3RTY29yZSJdLCJtYXBwaW5ncyI6Ijs7Ozs7O3FqQkFBQTtBQUNBOzs7QUFDQTs7OztBQUNBOzs7Ozs7OztBQUVBO0FBQ0E7QUFDQTs7SUFFcUJBLFc7QUFDbkI7QUFDQSx1QkFBWUMsUUFBWixFQUFzQkMsYUFBdEIsRUFBb0Q7QUFBQSxRQUFmQyxLQUFlLHVFQUFQLEtBQU87O0FBQUE7O0FBQ2xELFNBQUtGLFFBQUwsR0FBZ0JBLFNBQVNHLEdBQVQsQ0FBYTtBQUFBLGFBQVcsSUFBSUMsaUJBQUosQ0FBWUMsT0FBWixFQUFxQkosYUFBckIsQ0FBWDtBQUFBLEtBQWIsQ0FBaEI7QUFDQSxTQUFLQyxLQUFMLEdBQWFBLEtBQWI7QUFDQSxTQUFLSSxVQUFMLEdBQWtCLENBQWxCOztBQUVBLFNBQUtDLHlCQUFMOztBQUVBLFFBQUksS0FBS0wsS0FBVCxFQUFnQjtBQUFFTSxjQUFRQyxHQUFSLENBQVksaUNBQVo7QUFBaUQ7O0FBRW5FLFNBQUtDLHdCQUFMO0FBQ0Q7Ozs7a0NBRWE7QUFDWixhQUFPLEtBQUtWLFFBQVo7QUFDRDs7O2dEQUUyQjtBQUMxQixXQUFLVyxhQUFMLEdBQXFCLEVBQXJCO0FBQ0EsV0FBSyxJQUFJQyxJQUFJLENBQWIsRUFBZ0JBLElBQUksS0FBS1osUUFBTCxDQUFjYSxNQUFsQyxFQUEwQ0QsS0FBSyxDQUEvQyxFQUFrRDtBQUNoRCxhQUFLRCxhQUFMLENBQW1CRyxJQUFuQixDQUF3QixFQUF4QjtBQUNBLGFBQUssSUFBSUMsSUFBSSxDQUFiLEVBQWdCQSxJQUFJLEtBQUtmLFFBQUwsQ0FBY2EsTUFBbEMsRUFBMENFLEtBQUssQ0FBL0MsRUFBa0Q7QUFDaEQsY0FBSUgsTUFBTUcsQ0FBVixFQUFhO0FBQ1gsaUJBQUtKLGFBQUwsQ0FBbUJDLENBQW5CLEVBQXNCRSxJQUF0QixDQUEyQixLQUFLZCxRQUFMLENBQWNZLENBQWQsRUFBaUJJLFlBQWpCLENBQThCLEtBQUtoQixRQUFMLENBQWNlLENBQWQsQ0FBOUIsQ0FBM0I7QUFDRDtBQUNGO0FBQ0Y7QUFDRjs7OytCQUVnRTtBQUFBLHVDQUExREUsc0JBQTBEO0FBQUEsVUFBMURBLHNCQUEwRCx5Q0FBakMsQ0FBaUM7QUFBQSx1Q0FBOUJDLHNCQUE4QjtBQUFBLFVBQTlCQSxzQkFBOEIseUNBQUwsQ0FBSzs7QUFDL0Q7QUFDQTtBQUNBLFdBQUtELHNCQUFMLEdBQThCQSxzQkFBOUI7QUFDQSxXQUFLQyxzQkFBTCxHQUE4QkEsc0JBQTlCO0FBQ0EsV0FBS0MsWUFBTCxHQUFvQixFQUFwQjtBQUNBLFdBQUtDLE9BQUwsR0FBZSxLQUFLcEIsUUFBTCxDQUFjRyxHQUFkLENBQWtCO0FBQUEsZUFBTSxLQUFOO0FBQUEsT0FBbEIsQ0FBZjtBQUNBLFdBQUtrQixRQUFMLEdBQWdCLENBQWhCOztBQUVBLFVBQUksS0FBS25CLEtBQVQsRUFBZ0I7QUFBRU0sZ0JBQVFDLEdBQVIsQ0FBWSx5QkFBWixFQUF3Q0QsUUFBUWMsSUFBUixDQUFhLGFBQWI7QUFBOEI7QUFDeEYsV0FBS0YsT0FBTCxDQUFhLENBQWIsSUFBa0IsSUFBbEI7QUFDQSxXQUFLRCxZQUFMLENBQWtCTCxJQUFsQixDQUF1QixDQUFDLENBQUQsQ0FBdkI7QUFDQSxXQUFLUixVQUFMLElBQW1CLENBQW5CO0FBQ0EsV0FBS2UsUUFBTCxJQUFpQixDQUFqQjtBQUNBLFdBQUtFLGNBQUwsR0FBc0IsQ0FBdEI7QUFDQSxhQUFPLEtBQUtqQixVQUFMLEdBQWtCLEtBQUtOLFFBQUwsQ0FBY2EsTUFBdkMsRUFBK0M7QUFDN0MsYUFBS1csY0FBTDtBQUNEO0FBQ0QsVUFBSSxLQUFLdEIsS0FBVCxFQUFnQjtBQUFFTSxnQkFBUUMsR0FBUixDQUFZLHNCQUFaLEVBQXFDRCxRQUFRaUIsT0FBUixDQUFnQixhQUFoQjtBQUFpQzs7QUFFeEYsVUFBSSxLQUFLdkIsS0FBVCxFQUFnQjtBQUFFTSxnQkFBUUMsR0FBUixDQUFZLDZDQUFaO0FBQTZEO0FBQy9FLGFBQU8sS0FBS2lCLFdBQUwsQ0FBaUIsS0FBS1AsWUFBdEIsQ0FBUDtBQUNEOztBQUVEOzs7OytDQUMyQjtBQUN6QixVQUFNUSxXQUFXLEVBQWpCO0FBQ0EsV0FBS0MsV0FBTCxHQUFtQkMsT0FBbkIsQ0FBMkIsVUFBQ3hCLE9BQUQsRUFBYTtBQUN0Q3NCLGlCQUFTdEIsUUFBUXlCLElBQWpCLElBQXlCekIsT0FBekI7QUFDRCxPQUZEOztBQUlBLFdBQUtMLFFBQUwsR0FBZ0IrQixPQUFPQyxNQUFQLENBQWNMLFFBQWQsQ0FBaEI7QUFDRDs7O3FDQUVnQjtBQUNmLFVBQU1NLGVBQWUsS0FBS0Msb0JBQUwsRUFBckI7O0FBRUEsVUFBSUMsd0JBQXdCLENBQUMsQ0FBN0I7QUFDQSxVQUFJQyxtQkFBbUIsQ0FBQyxDQUF4QjtBQUNBLFVBQU1DLDRCQUE0QkMsS0FBS0MsS0FBTCxDQUFXLENBQUMsS0FBS3ZDLFFBQUwsQ0FBY2EsTUFBZCxHQUF1QixLQUFLUCxVQUE3QixJQUEyQyxLQUFLVyxzQkFBM0QsQ0FBbEM7QUFDQSxXQUFLLElBQUlMLElBQUksQ0FBYixFQUFnQkEsSUFBSSxLQUFLUyxRQUFMLEdBQWdCZ0IseUJBQXBDLEVBQStEekIsS0FBSyxDQUFwRSxFQUF1RTtBQUNyRSxZQUFJQSxJQUFJLEtBQUtPLFlBQUwsQ0FBa0JOLE1BQXRCLElBQWdDLEtBQUtNLFlBQUwsQ0FBa0JQLENBQWxCLEVBQXFCQyxNQUFyQixHQUE4QixLQUFLSyxzQkFBdkUsRUFBK0Y7QUFDN0Y7QUFDQSxjQUFNc0IsY0FBYyxLQUFLQyxTQUFMLENBQWUsS0FBS3RCLFlBQUwsQ0FBa0JQLENBQWxCLENBQWYsQ0FBcEI7QUFDQSxlQUFLTyxZQUFMLENBQWtCUCxDQUFsQixFQUFxQkUsSUFBckIsQ0FBMEJtQixZQUExQjtBQUNBLGNBQU1TLGFBQWEsS0FBS0QsU0FBTCxDQUFlLEtBQUt0QixZQUFMLENBQWtCUCxDQUFsQixDQUFmLENBQW5CO0FBQ0EsZUFBS08sWUFBTCxDQUFrQlAsQ0FBbEIsRUFBcUIrQixHQUFyQjs7QUFFQSxjQUFJRCxhQUFhRixXQUFiLElBQTZCRSxhQUFhRixXQUFiLEdBQTJCSixnQkFBNUQsRUFBK0U7QUFDN0VELG9DQUF3QnZCLENBQXhCO0FBQ0F3QiwrQkFBbUJNLGFBQWFGLFdBQWhDO0FBQ0Q7QUFDRjtBQUNGOztBQUdELFVBQUlKLHFCQUFxQixDQUFDLENBQTFCLEVBQTZCO0FBQzNCO0FBQ0EsWUFBSSxLQUFLZixRQUFMLElBQWlCLEtBQUtFLGNBQUwsR0FBc0JjLHlCQUEzQyxFQUFzRTtBQUNwRTtBQUNBLGNBQUlPLGdCQUFnQixDQUFwQjtBQUNBLGNBQUlDLGdCQUFnQixDQUFDLENBQXJCO0FBQ0EsZUFBSyxJQUFJakMsS0FBSSxDQUFiLEVBQWdCQSxLQUFJLEtBQUtPLFlBQUwsQ0FBa0JOLE1BQXRDLEVBQThDRCxNQUFLLENBQW5ELEVBQXNEO0FBQ3BELGdCQUFJLEtBQUtPLFlBQUwsQ0FBa0JQLEVBQWxCLEVBQXFCQyxNQUFyQixHQUE4QixLQUFLSyxzQkFBdkMsRUFBK0Q7QUFDN0Q7QUFDQSxrQkFBTXNCLGVBQWMsS0FBS0MsU0FBTCxDQUFlLEtBQUt0QixZQUFMLENBQWtCUCxFQUFsQixDQUFmLENBQXBCO0FBQ0EsbUJBQUtPLFlBQUwsQ0FBa0JQLEVBQWxCLEVBQXFCRSxJQUFyQixDQUEwQm1CLFlBQTFCO0FBQ0Esa0JBQU1TLGNBQWEsS0FBS0QsU0FBTCxDQUFlLEtBQUt0QixZQUFMLENBQWtCUCxFQUFsQixDQUFmLENBQW5CO0FBQ0EsbUJBQUtPLFlBQUwsQ0FBa0JQLEVBQWxCLEVBQXFCK0IsR0FBckI7O0FBRUEsa0JBQUlFLGtCQUFrQixDQUFDLENBQW5CLElBQXdCTCxlQUFjRSxXQUFkLEdBQTJCRyxhQUF2RCxFQUFzRTtBQUNwRUEsZ0NBQWdCTCxlQUFjRSxXQUE5QjtBQUNBRSxnQ0FBZ0JoQyxFQUFoQjtBQUNEO0FBQ0Y7QUFDRjs7QUFFRCxlQUFLTyxZQUFMLENBQWtCeUIsYUFBbEIsRUFBaUM5QixJQUFqQyxDQUFzQ21CLFlBQXRDO0FBQ0EsY0FBSSxLQUFLZCxZQUFMLENBQWtCeUIsYUFBbEIsRUFBaUMvQixNQUFqQyxJQUEyQyxLQUFLSyxzQkFBcEQsRUFBNEU7QUFDMUUsaUJBQUtLLGNBQUwsSUFBdUIsQ0FBdkI7QUFDRDtBQUNGLFNBdkJELE1BdUJPO0FBQ0wsZUFBS0osWUFBTCxDQUFrQkwsSUFBbEIsQ0FBdUIsQ0FBQ21CLFlBQUQsQ0FBdkI7QUFDQSxlQUFLWixRQUFMLElBQWlCLENBQWpCO0FBQ0Q7QUFDRixPQTdCRCxNQTZCTztBQUNMLGFBQUtGLFlBQUwsQ0FBa0JnQixxQkFBbEIsRUFBeUNyQixJQUF6QyxDQUE4Q21CLFlBQTlDO0FBQ0EsWUFBSSxLQUFLZCxZQUFMLENBQWtCZ0IscUJBQWxCLEVBQXlDdEIsTUFBekMsSUFBbUQsS0FBS0ssc0JBQTVELEVBQW9GO0FBQ2xGLGVBQUtLLGNBQUwsSUFBdUIsQ0FBdkI7QUFDRDtBQUNGOztBQUdELFdBQUtILE9BQUwsQ0FBYWEsWUFBYixJQUE2QixJQUE3QjtBQUNBLFdBQUszQixVQUFMLElBQW1CLENBQW5CO0FBQ0Q7OzsyQ0FFc0I7QUFDckIsVUFBSU0sSUFBSSxDQUFSO0FBQ0EsYUFBTyxLQUFLUSxPQUFMLENBQWFSLENBQWIsQ0FBUCxFQUF3QjtBQUFFQSxhQUFLLENBQUw7QUFBUzs7QUFFbkMsYUFBT0EsQ0FBUDtBQUNEOzs7Z0NBRVdPLFksRUFBYztBQUFBOztBQUN4QixVQUFNMkIsUUFBUSxFQUFkO0FBQ0EsV0FBSyxJQUFJbEMsSUFBSSxDQUFiLEVBQWdCQSxJQUFJTyxhQUFhTixNQUFqQyxFQUF5Q0QsS0FBSyxDQUE5QyxFQUFpRDtBQUMvQyxZQUFNbUMsT0FBTzVCLGFBQWFQLENBQWIsRUFBZ0JULEdBQWhCLENBQW9CO0FBQUEsaUJBQUssTUFBS0gsUUFBTCxDQUFjZSxDQUFkLENBQUw7QUFBQSxTQUFwQixDQUFiO0FBQ0ErQixjQUFNaEMsSUFBTixDQUFXLElBQUlrQyxjQUFKLENBQVNELElBQVQsQ0FBWDtBQUNEOztBQUVELGFBQU9ELEtBQVA7QUFDRDs7O21DQUVjM0IsWSxFQUFjOEIsVSxFQUFZO0FBQ3ZDLFVBQUlDLGdCQUFnQixDQUFDLENBQXJCO0FBQ0EsVUFBSUMsZUFBZSxDQUFuQjs7QUFFQSxVQUFJQyxRQUFRLENBQVo7QUFDQSxVQUFNQyxXQUFXLFNBQVhBLFFBQVcsQ0FBQ0MsQ0FBRCxFQUFJMUMsQ0FBSjtBQUFBLGVBQVVBLElBQUt1QyxZQUFMLElBQXNCRyxFQUFFQyxJQUFGLEtBQVcsS0FBM0M7QUFBQSxPQUFqQjtBQUNBLGFBQU9KLGlCQUFpQkYsVUFBeEIsRUFBb0M7QUFDbENDLHdCQUFnQkMsWUFBaEI7QUFDQUEsdUJBQWVoQyxhQUFhcUMsU0FBYixDQUF1QkgsUUFBdkIsQ0FBZjtBQUNBLFlBQUlGLGlCQUFpQixDQUFDLENBQWxCLElBQXVCQSxlQUFlRixVQUExQyxFQUFzRDtBQUFFRSx5QkFBZUYsVUFBZjtBQUE0Qjs7QUFFcEYsWUFBTUYsT0FBTyxFQUFiO0FBQ0EsYUFBSyxJQUFJbkMsSUFBSXNDLGdCQUFnQixDQUE3QixFQUFnQ3RDLElBQUl1QyxZQUFwQyxFQUFrRHZDLEtBQUssQ0FBdkQsRUFBMEQ7QUFDeERtQyxlQUFLakMsSUFBTCxDQUFVSyxhQUFhUCxDQUFiLEVBQWdCUCxPQUExQjtBQUNEOztBQUVEK0MsaUJBQVMsS0FBS1gsU0FBTCxDQUFlTSxJQUFmLENBQVQ7QUFDRDs7QUFFRCxhQUFPSyxLQUFQO0FBQ0Q7Ozs4QkFFU0wsSSxFQUFNO0FBQ2Q7QUFDQSxVQUFJQSxLQUFLbEMsTUFBTCxJQUFlLENBQW5CLEVBQXNCO0FBQ3BCLGVBQU8sQ0FBUDtBQUNEOztBQUVELFVBQUl1QyxRQUFRLENBQVo7QUFDQSxVQUFJSyxXQUFXLENBQWY7QUFDQSxXQUFLLElBQUk3QyxJQUFJLENBQWIsRUFBZ0JBLElBQUltQyxLQUFLbEMsTUFBekIsRUFBaUNELEtBQUssQ0FBdEMsRUFBeUM7QUFDdkMsYUFBSyxJQUFJRyxJQUFJSCxJQUFJLENBQWpCLEVBQW9CRyxJQUFJZ0MsS0FBS2xDLE1BQTdCLEVBQXFDRSxLQUFLLENBQTFDLEVBQTZDO0FBQzNDcUMsbUJBQVMsS0FBS00sYUFBTCxDQUFtQlgsS0FBS25DLENBQUwsQ0FBbkIsRUFBNEJtQyxLQUFLaEMsQ0FBTCxDQUE1QixDQUFUO0FBQ0EwQyxzQkFBWSxDQUFaO0FBQ0Q7QUFDRjs7QUFFRCxhQUFPTCxRQUFRSyxRQUFmO0FBQ0Q7OztrQ0FFYUUsQyxFQUFHQyxDLEVBQUc7QUFDbEIsYUFBTyxLQUFLakQsYUFBTCxDQUFtQmdELENBQW5CLEVBQXNCQyxDQUF0QixDQUFQO0FBQ0Q7Ozs4QkFFUzNCLFksRUFBYztBQUN0QixVQUFJNEIsY0FBYyxDQUFDLENBQW5CO0FBQ0EsVUFBSUMsWUFBWSxDQUFoQjtBQUNBLFdBQUssSUFBSWxELElBQUksQ0FBYixFQUFnQkEsSUFBSSxLQUFLWixRQUFMLENBQWNhLE1BQWxDLEVBQTBDRCxLQUFLLENBQS9DLEVBQWtEO0FBQ2hELFlBQUlBLE1BQU1xQixZQUFWLEVBQXdCO0FBQ3RCLGNBQU1tQixRQUFRLEtBQUt6QyxhQUFMLENBQW1Cc0IsWUFBbkIsRUFBaUNyQixDQUFqQyxDQUFkO0FBQ0EsY0FBSXdDLFFBQVFVLFNBQVosRUFBdUI7QUFDckJELDBCQUFjakQsQ0FBZDtBQUNBa0Qsd0JBQVlWLEtBQVo7QUFDRDtBQUNGO0FBQ0Y7O0FBRUQsYUFBT1MsV0FBUDtBQUNEOzs7Ozs7a0JBMU1rQjlELFciLCJmaWxlIjoiZ3JlZWR5LW1hdGNoZXIuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiBlc2xpbnQtZGlzYWJsZSBuby1jb25zb2xlICovXG4vKiBlc2xpbnQtZGlzYWJsZSBuby1jb250aW51ZSAqL1xuaW1wb3J0IFN0dWRlbnQgZnJvbSAnLi9zdHVkZW50JztcbmltcG9ydCBUZWFtIGZyb20gJy4vdGVhbSc7XG5cbi8vIGJhc2ljYWxseSBzdGFycyBhbmQgYmFyc1xuLy8gd2UgaGF2ZSBzdHVkZW50cyBhbmQgc29tZSBzZXBlcmF0b3JzIGZvciB0aGVtXG4vLyB3ZSBuZWVkIGFuIGluaXRhbCBlc3RpbWF0ZSB0aG9cblxuZXhwb3J0IGRlZmF1bHQgY2xhc3MgR3JlZWR5TWF0Y2gge1xuICAvLyBwdWJsaWNcbiAgY29uc3RydWN0b3Ioc3R1ZGVudHMsIHRlYW1pbmdDb25maWcsIGRlYnVnID0gZmFsc2UpIHtcbiAgICB0aGlzLnN0dWRlbnRzID0gc3R1ZGVudHMubWFwKHN0dWRlbnQgPT4gbmV3IFN0dWRlbnQoc3R1ZGVudCwgdGVhbWluZ0NvbmZpZykpO1xuICAgIHRoaXMuZGVidWcgPSBkZWJ1ZztcbiAgICB0aGlzLm51bVZpc2l0ZWQgPSAwO1xuXG4gICAgdGhpcy5pbml0aWFsaXplQWRqYWNlbmN5TWF0cml4KCk7XG5cbiAgICBpZiAodGhpcy5kZWJ1ZykgeyBjb25zb2xlLmxvZygnRFBNYXRjaDogUmVtb3ZpbmcgZHVwbGljYXRlcy4uLicpOyB9XG5cbiAgICB0aGlzLnJlbW92ZUR1cGxpY2F0ZXNTdHVkZW50cygpO1xuICB9XG5cbiAgZ2V0U3R1ZGVudHMoKSB7XG4gICAgcmV0dXJuIHRoaXMuc3R1ZGVudHM7XG4gIH1cblxuICBpbml0aWFsaXplQWRqYWNlbmN5TWF0cml4KCkge1xuICAgIHRoaXMuc2NvcmVkQWdhaW5zdCA9IFtdO1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5zdHVkZW50cy5sZW5ndGg7IGkgKz0gMSkge1xuICAgICAgdGhpcy5zY29yZWRBZ2FpbnN0LnB1c2goW10pO1xuICAgICAgZm9yIChsZXQgaiA9IDA7IGogPCB0aGlzLnN0dWRlbnRzLmxlbmd0aDsgaiArPSAxKSB7XG4gICAgICAgIGlmIChpICE9PSBqKSB7XG4gICAgICAgICAgdGhpcy5zY29yZWRBZ2FpbnN0W2ldLnB1c2godGhpcy5zdHVkZW50c1tpXS5zY29yZUFnYWluc3QodGhpcy5zdHVkZW50c1tqXSkpO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgdGVhbSh7IG1pbmltdW1TdHVkZW50c1BlclRlYW0gPSAzLCBtYXhpbXVtU3R1ZGVudHNQZXJUZWFtID0gNCB9KSB7XG4gICAgLy8gZ2VuIHBlcm1zXG4gICAgLy8gZHAgdGhpcyBzaGl6XG4gICAgdGhpcy5taW5pbXVtU3R1ZGVudHNQZXJUZWFtID0gbWluaW11bVN0dWRlbnRzUGVyVGVhbTtcbiAgICB0aGlzLm1heGltdW1TdHVkZW50c1BlclRlYW0gPSBtYXhpbXVtU3R1ZGVudHNQZXJUZWFtO1xuICAgIHRoaXMuc29sdXRpb25QYXRoID0gW107XG4gICAgdGhpcy52aXNpdGVkID0gdGhpcy5zdHVkZW50cy5tYXAoKCkgPT4gZmFsc2UpO1xuICAgIHRoaXMubnVtVGVhbXMgPSAwO1xuXG4gICAgaWYgKHRoaXMuZGVidWcpIHsgY29uc29sZS5sb2coJ0dyZWVkeU1hdGNoOiB0ZWFtaW5nLi4uJyk7IGNvbnNvbGUudGltZSgnR3JlZWR5TWF0Y2gnKTsgfVxuICAgIHRoaXMudmlzaXRlZFswXSA9IHRydWU7XG4gICAgdGhpcy5zb2x1dGlvblBhdGgucHVzaChbMF0pO1xuICAgIHRoaXMubnVtVmlzaXRlZCArPSAxO1xuICAgIHRoaXMubnVtVGVhbXMgKz0gMTtcbiAgICB0aGlzLm51bUZpbGxlZFRlYW1zID0gMDtcbiAgICB3aGlsZSAodGhpcy5udW1WaXNpdGVkIDwgdGhpcy5zdHVkZW50cy5sZW5ndGgpIHtcbiAgICAgIHRoaXMuYWRkTmV4dFN0dWRlbnQoKTtcbiAgICB9XG4gICAgaWYgKHRoaXMuZGVidWcpIHsgY29uc29sZS5sb2coJ0dyZWVkeU1hdGNoOiBkb25lLi4uJyk7IGNvbnNvbGUudGltZUVuZCgnR3JlZWR5TWF0Y2gnKTsgfVxuXG4gICAgaWYgKHRoaXMuZGVidWcpIHsgY29uc29sZS5sb2coJ0dyZWVkeU1hdGNoOiBnZW5lcmF0aW5nIHRlYW1zIGZyb20gbWF0Y2guLi4nKTsgfVxuICAgIHJldHVybiB0aGlzLnBhdGhUb1RlYW1zKHRoaXMuc29sdXRpb25QYXRoKTtcbiAgfVxuXG4gIC8vIHByaXZhdGVcbiAgcmVtb3ZlRHVwbGljYXRlc1N0dWRlbnRzKCkge1xuICAgIGNvbnN0IGR1cEFycmF5ID0ge307XG4gICAgdGhpcy5nZXRTdHVkZW50cygpLmZvckVhY2goKHN0dWRlbnQpID0+IHtcbiAgICAgIGR1cEFycmF5W3N0dWRlbnQuaGFzaF0gPSBzdHVkZW50O1xuICAgIH0pO1xuXG4gICAgdGhpcy5zdHVkZW50cyA9IE9iamVjdC52YWx1ZXMoZHVwQXJyYXkpO1xuICB9XG5cbiAgYWRkTmV4dFN0dWRlbnQoKSB7XG4gICAgY29uc3Qgc3R1ZGVudEluZGV4ID0gdGhpcy5uZXh0VW52aXNpdGVkU3R1ZGVudCgpO1xuXG4gICAgbGV0IGdyZWF0ZXN0SW5jcmVhc2VJbmRleCA9IC0xO1xuICAgIGxldCBncmVhdGVzdEluY3JlYXNlID0gLTE7XG4gICAgY29uc3QgbWF4TnVtYmVyT2ZSZW1haW5pbmdUZWFtcyA9IE1hdGguZmxvb3IoKHRoaXMuc3R1ZGVudHMubGVuZ3RoIC0gdGhpcy5udW1WaXNpdGVkKSAvIHRoaXMubWluaW11bVN0dWRlbnRzUGVyVGVhbSk7XG4gICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLm51bVRlYW1zICsgbWF4TnVtYmVyT2ZSZW1haW5pbmdUZWFtczsgaSArPSAxKSB7XG4gICAgICBpZiAoaSA8IHRoaXMuc29sdXRpb25QYXRoLmxlbmd0aCAmJiB0aGlzLnNvbHV0aW9uUGF0aFtpXS5sZW5ndGggPCB0aGlzLm1heGltdW1TdHVkZW50c1BlclRlYW0pIHtcbiAgICAgICAgLy8gdGVzdCBhc3Mgc3R1ZGVudFxuICAgICAgICBjb25zdCBzY29yZUJlZm9yZSA9IHRoaXMuc2NvcmVUZWFtKHRoaXMuc29sdXRpb25QYXRoW2ldKTtcbiAgICAgICAgdGhpcy5zb2x1dGlvblBhdGhbaV0ucHVzaChzdHVkZW50SW5kZXgpO1xuICAgICAgICBjb25zdCBzY29yZUFmdGVyID0gdGhpcy5zY29yZVRlYW0odGhpcy5zb2x1dGlvblBhdGhbaV0pO1xuICAgICAgICB0aGlzLnNvbHV0aW9uUGF0aFtpXS5wb3AoKTtcblxuICAgICAgICBpZiAoc2NvcmVBZnRlciA+IHNjb3JlQmVmb3JlICYmIChzY29yZUFmdGVyIC0gc2NvcmVCZWZvcmUgPiBncmVhdGVzdEluY3JlYXNlKSkge1xuICAgICAgICAgIGdyZWF0ZXN0SW5jcmVhc2VJbmRleCA9IGk7XG4gICAgICAgICAgZ3JlYXRlc3RJbmNyZWFzZSA9IHNjb3JlQWZ0ZXIgLSBzY29yZUJlZm9yZTtcbiAgICAgICAgfVxuICAgICAgfVxuICAgIH1cblxuXG4gICAgaWYgKGdyZWF0ZXN0SW5jcmVhc2UgPT09IC0xKSB7XG4gICAgICAvLyBpZiBtb3JlIHRlYW1zIHRoYW4gbWF4IHRlYW1zLCByZW1hdGNoXG4gICAgICBpZiAodGhpcy5udW1UZWFtcyA+PSB0aGlzLm51bUZpbGxlZFRlYW1zICsgbWF4TnVtYmVyT2ZSZW1haW5pbmdUZWFtcykge1xuICAgICAgICAvLyBmaW5kIHRlYW0gd2l0aCBsZWFzdCBkZWNyZWFzZVxuICAgICAgICBsZXQgYmVzdFRlYW1JbmRleCA9IDA7XG4gICAgICAgIGxldCBsZWFzdERlY3JlYXNlID0gLTE7XG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5zb2x1dGlvblBhdGgubGVuZ3RoOyBpICs9IDEpIHtcbiAgICAgICAgICBpZiAodGhpcy5zb2x1dGlvblBhdGhbaV0ubGVuZ3RoIDwgdGhpcy5tYXhpbXVtU3R1ZGVudHNQZXJUZWFtKSB7XG4gICAgICAgICAgICAvLyB0ZXN0IGFzcyBzdHVkZW50XG4gICAgICAgICAgICBjb25zdCBzY29yZUJlZm9yZSA9IHRoaXMuc2NvcmVUZWFtKHRoaXMuc29sdXRpb25QYXRoW2ldKTtcbiAgICAgICAgICAgIHRoaXMuc29sdXRpb25QYXRoW2ldLnB1c2goc3R1ZGVudEluZGV4KTtcbiAgICAgICAgICAgIGNvbnN0IHNjb3JlQWZ0ZXIgPSB0aGlzLnNjb3JlVGVhbSh0aGlzLnNvbHV0aW9uUGF0aFtpXSk7XG4gICAgICAgICAgICB0aGlzLnNvbHV0aW9uUGF0aFtpXS5wb3AoKTtcblxuICAgICAgICAgICAgaWYgKGxlYXN0RGVjcmVhc2UgPT09IC0xIHx8IHNjb3JlQmVmb3JlIC0gc2NvcmVBZnRlciA8IGxlYXN0RGVjcmVhc2UpIHtcbiAgICAgICAgICAgICAgbGVhc3REZWNyZWFzZSA9IHNjb3JlQmVmb3JlIC0gc2NvcmVBZnRlcjtcbiAgICAgICAgICAgICAgYmVzdFRlYW1JbmRleCA9IGk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5zb2x1dGlvblBhdGhbYmVzdFRlYW1JbmRleF0ucHVzaChzdHVkZW50SW5kZXgpO1xuICAgICAgICBpZiAodGhpcy5zb2x1dGlvblBhdGhbYmVzdFRlYW1JbmRleF0ubGVuZ3RoID49IHRoaXMubWF4aW11bVN0dWRlbnRzUGVyVGVhbSkge1xuICAgICAgICAgIHRoaXMubnVtRmlsbGVkVGVhbXMgKz0gMTtcbiAgICAgICAgfVxuICAgICAgfSBlbHNlIHtcbiAgICAgICAgdGhpcy5zb2x1dGlvblBhdGgucHVzaChbc3R1ZGVudEluZGV4XSk7XG4gICAgICAgIHRoaXMubnVtVGVhbXMgKz0gMTtcbiAgICAgIH1cbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5zb2x1dGlvblBhdGhbZ3JlYXRlc3RJbmNyZWFzZUluZGV4XS5wdXNoKHN0dWRlbnRJbmRleCk7XG4gICAgICBpZiAodGhpcy5zb2x1dGlvblBhdGhbZ3JlYXRlc3RJbmNyZWFzZUluZGV4XS5sZW5ndGggPj0gdGhpcy5tYXhpbXVtU3R1ZGVudHNQZXJUZWFtKSB7XG4gICAgICAgIHRoaXMubnVtRmlsbGVkVGVhbXMgKz0gMTtcbiAgICAgIH1cbiAgICB9XG5cblxuICAgIHRoaXMudmlzaXRlZFtzdHVkZW50SW5kZXhdID0gdHJ1ZTtcbiAgICB0aGlzLm51bVZpc2l0ZWQgKz0gMTtcbiAgfVxuXG4gIG5leHRVbnZpc2l0ZWRTdHVkZW50KCkge1xuICAgIGxldCBpID0gMDtcbiAgICB3aGlsZSAodGhpcy52aXNpdGVkW2ldKSB7IGkgKz0gMTsgfVxuXG4gICAgcmV0dXJuIGk7XG4gIH1cblxuICBwYXRoVG9UZWFtcyhzb2x1dGlvblBhdGgpIHtcbiAgICBjb25zdCB0ZWFtcyA9IFtdO1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgc29sdXRpb25QYXRoLmxlbmd0aDsgaSArPSAxKSB7XG4gICAgICBjb25zdCB0ZWFtID0gc29sdXRpb25QYXRoW2ldLm1hcChqID0+IHRoaXMuc3R1ZGVudHNbal0pO1xuICAgICAgdGVhbXMucHVzaChuZXcgVGVhbSh0ZWFtKSk7XG4gICAgfVxuXG4gICAgcmV0dXJuIHRlYW1zO1xuICB9XG5cbiAgY2FsY3VsYXRlU2NvcmUoc29sdXRpb25QYXRoLCBwZXJtTGVuZ3RoKSB7XG4gICAgbGV0IGZpcnN0QmFySW5kZXggPSAtMTtcbiAgICBsZXQgbmV4dEJhckluZGV4ID0gMDtcblxuICAgIGxldCBzY29yZSA9IDA7XG4gICAgY29uc3QgZmluZEZ1bmMgPSAodiwgaSkgPT4gaSA+IChuZXh0QmFySW5kZXgpICYmIHYudHlwZSA9PT0gJ2Jhcic7XG4gICAgd2hpbGUgKG5leHRCYXJJbmRleCAhPT0gcGVybUxlbmd0aCkge1xuICAgICAgZmlyc3RCYXJJbmRleCA9IG5leHRCYXJJbmRleDtcbiAgICAgIG5leHRCYXJJbmRleCA9IHNvbHV0aW9uUGF0aC5maW5kSW5kZXgoZmluZEZ1bmMpO1xuICAgICAgaWYgKG5leHRCYXJJbmRleCA9PT0gLTEgfHwgbmV4dEJhckluZGV4ID4gcGVybUxlbmd0aCkgeyBuZXh0QmFySW5kZXggPSBwZXJtTGVuZ3RoOyB9XG5cbiAgICAgIGNvbnN0IHRlYW0gPSBbXTtcbiAgICAgIGZvciAobGV0IGkgPSBmaXJzdEJhckluZGV4ICsgMTsgaSA8IG5leHRCYXJJbmRleDsgaSArPSAxKSB7XG4gICAgICAgIHRlYW0ucHVzaChzb2x1dGlvblBhdGhbaV0uc3R1ZGVudCk7XG4gICAgICB9XG5cbiAgICAgIHNjb3JlICs9IHRoaXMuc2NvcmVUZWFtKHRlYW0pO1xuICAgIH1cblxuICAgIHJldHVybiBzY29yZTtcbiAgfVxuXG4gIHNjb3JlVGVhbSh0ZWFtKSB7XG4gICAgLy8gcGFpcndpc2UgdGVhbSBhbGwgbWVtYmVycyBhZ2FpbnN0IGVhY2ggb3RoZXJcbiAgICBpZiAodGVhbS5sZW5ndGggPD0gMSkge1xuICAgICAgcmV0dXJuIDA7XG4gICAgfVxuXG4gICAgbGV0IHNjb3JlID0gMDtcbiAgICBsZXQgbnVtUGVybXMgPSAwO1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGVhbS5sZW5ndGg7IGkgKz0gMSkge1xuICAgICAgZm9yIChsZXQgaiA9IGkgKyAxOyBqIDwgdGVhbS5sZW5ndGg7IGogKz0gMSkge1xuICAgICAgICBzY29yZSArPSB0aGlzLnNjb3JlU3R1ZGVudHModGVhbVtpXSwgdGVhbVtqXSk7XG4gICAgICAgIG51bVBlcm1zICs9IDE7XG4gICAgICB9XG4gICAgfVxuXG4gICAgcmV0dXJuIHNjb3JlIC8gbnVtUGVybXM7XG4gIH1cblxuICBzY29yZVN0dWRlbnRzKGEsIGIpIHtcbiAgICByZXR1cm4gdGhpcy5zY29yZWRBZ2FpbnN0W2FdW2JdO1xuICB9XG5cbiAgYmVzdE1hdGNoKHN0dWRlbnRJbmRleCkge1xuICAgIGxldCBiZXN0RWxlbWVudCA9IC0xO1xuICAgIGxldCBiZXN0U2NvcmUgPSAwO1xuICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5zdHVkZW50cy5sZW5ndGg7IGkgKz0gMSkge1xuICAgICAgaWYgKGkgIT09IHN0dWRlbnRJbmRleCkge1xuICAgICAgICBjb25zdCBzY29yZSA9IHRoaXMuc2NvcmVkQWdhaW5zdFtzdHVkZW50SW5kZXhdW2ldO1xuICAgICAgICBpZiAoc2NvcmUgPiBiZXN0U2NvcmUpIHtcbiAgICAgICAgICBiZXN0RWxlbWVudCA9IGk7XG4gICAgICAgICAgYmVzdFNjb3JlID0gc2NvcmU7XG4gICAgICAgIH1cbiAgICAgIH1cbiAgICB9XG5cbiAgICByZXR1cm4gYmVzdEVsZW1lbnQ7XG4gIH1cbn1cbiJdLCJzb3VyY2VSb290IjoiL21udC9jL1VzZXJzL3B1cmV1L0RvY3VtZW50cy9wcm9qZWN0cy9sYXVuY2h4LXRlYW1pbmcvc3JjIn0=
