
import btoa from 'btoa-lite';
import bs from 'binary-search';
import TeamingConfig from './teaming-config';

export const defaultTeamingConfig = Object.keys(TeamingConfig).reduce((old, next) => {
  old[next] = TeamingConfig[next].default;
  return old;
}, {});

const mapSentiment = (sentiment) => {
  switch (sentiment) {
    case 'strong_yes':
      return 4.0;
    case 'yes':
      return 3.0;
    case 'no':
      return 2.0;
    case 'strong_no':
      return 1.0;

    default:
      return null;
  }
};

class FalseSet {
  constructor(...args) {
    this.p_set = new Set(args);
  }

  forEach(callbackFn) {
    return this.p_set.forEach(callbackFn);
  }

  add(value) {
    return this.p_set.add(value);
  }

  has(value) {
    return this.p_set.has(value);
  }

  clear() {
    return this.p_set.clear();
  }

  delete(value) {
    return this.p_set.delete(value);
  }

  [Symbol.iterator]() {
    return this.p_set[Symbol.iterator]();
  }
}

export class SortedStudentSet extends FalseSet {
  constructor(...args) {
    super(args);
    this.p_hashSet = new Set();
  }

  add(value) {
    const first = value[0].p_hash > value[1].p_hash ? value[0] : value[1];
    const second = value[0].p_hash > value[1].p_hash ? value[1] : value[0];
    super.add([first, second]);
    return this.p_hashSet.add(first.p_hash + second.p_hash);
  }

  has(value) {
    const first = value[0].p_hash > value[1].p_hash ? value[0] : value[1];
    const second = value[0].p_hash > value[1].p_hash ? value[1] : value[0];
    return this.p_hashSet.has(first.p_hash + second.p_hash);
  }
}

export class StudentSet extends FalseSet {
  constructor(...args) {
    super(args);
    this.p_actualSet = new Set(args);
  }

  has(value) {
    return super.has(value.p_hash);
  }

  add(value) {
    this.p_actualSet.add(value);
    return super.add(value.p_hash);
  }

  forEach(callbackFn) {
    return this.p_actualSet.forEach(callbackFn);
  }
}

export class StudentPairSet extends FalseSet {
  constructor(...args) {
    super(args);
    this.p_actualSet = new Set(args);
  }

  hasStudentPair([student, otherStudent]) {
    const possiblity1 = student.p_hash + otherStudent.p_hash;
    const possiblity2 = otherStudent.p_hash + student.p_hash;
    return this.has(possiblity1) || this.has(possiblity2);
  }

  addStudentPair([student, otherStudent]) {
    this.p_actualSet.add([student, otherStudent]);
    return this.add(student.p_hash + otherStudent.p_hash);
  }
}

export default class Student {
  constructor(json, teamingConfig = defaultTeamingConfig) {
    this.p_json = json;
    this.p_hash = btoa(unescape(encodeURIComponent(JSON.stringify(json))));
    this.p_learningStyle = json.learningStyle || 'default';
    this.p_workStyle = json.workStyle || 'default';
    this.p_EP10Type = json.EP10Type || 'default';
    this.p_personalityType = json.personalityType || 'default';
    this.p_entrepreneurType = json.entrepreneurType || 'default';
    this.p_launchSkills = json.launchSkills || 'default';
    this.p_interests = json.interests || [];
    this.p_ideas = json.ideas || [];
    this.p_teamingConfig = teamingConfig;
    this.p_role = json.role || json.metadata.role;
    this.p_skills = json.skills;
    this.p_gender = json.gender || json.metadata.gender;
    this.p_ethnicity = json.ethnicity || json.metadata.ethnicity;
  }

  get hash() {
    return this.p_hash;
  }

  get role() {
    return this.p_role;
  }

  get gender() {
    return this.p_gender;
  }

  get ethnicity() {
    return this.p_ethnicity;
  }

  get ideas() {
    return this.p_ideas;
  }

  get ownIdeas() {
    return (this.p_json.metadata || {}).ideas || [];
  }

  get interests() {
    return this.p_interests;
  }

  get learningStyle() {
    return this.p_learningStyle;
  }

  get personalityType() {
    return this.p_personalityType;
  }

  get EP10Type() {
    return this.p_EP10Type;
  }

  get workStyle() {
    return this.p_workStyle;
  }

  get entrepreneurType() {
    return this.p_entrepreneurType;
  }

  get launchSkills() {
    return this.p_launchSkills;
  }

  get skills() {
    return this.p_skills;
  }

  isEqual(otherStudent) {
    return this.p_hash === otherStudent.p_hash;
  }

  learningStyleOverlap(learningStyle) {
    if (this.learningStyle === learningStyle) {
      return 1;
    }

    return 0;
  }

  workStyleCompliment(workStyle) {
    const comparator = (a, b) => a.localeCompare(b);
    let workStylePoints = 0;

    const style = this.workStyle.split('/').map(s => s.trim()); // ["Amicable", "Analytical"]
    style.sort(comparator);
    const otherStyle = workStyle.split('/').map(s => s.trim());
    otherStyle.sort(comparator);

    const smaller = style.length < otherStyle.length ? style : otherStyle; // get smaller
    const larger = style.length < otherStyle.length ? otherStyle : style;

    // set difference
    for (let i = 0; i < smaller.length; i += 1) {
      if (bs(larger, smaller[i], comparator) < 0) {
        workStylePoints += 1;
      }
    }

    return workStylePoints / smaller.length; // perfectly disjoint is 1
  }

  ep10Compliment(otherStudent) {
    // ep 10 complementary
    if (this.EP10Type !== otherStudent.EP10Type) {
      return this.p_teamingConfig.EP10Type || defaultTeamingConfig.EP10Type;
    }

    return 0;
  }

  personalityComplement(otherPersonalityType) {
    let personalityScore = 0;
    for (let i = 0; i < 4; i += 1) {
      if (this.personalityType[i] !== otherPersonalityType[i]) {
        personalityScore += 1;
      }
    }

    return personalityScore / 4;
  }

  skillsComplement(otherSkills) {
    let normalSkillsPoints = 0.0;
    const normalSkills = this.skills;
    const otherNormalSkills = otherSkills;
    const skillsKeys = Object.keys(normalSkills || {});

    for (let i = 0, r = skillsKeys.length; i < r; i += 1) {
      const skillKey = skillsKeys[i];
      const skill = normalSkills[skillKey];
      const otherSkill = otherNormalSkills[skillKey];
      const skillDifference = Math.abs(skill - otherSkill);

      if (skillDifference > 3) {
        normalSkillsPoints += 1;
      }
    }

    return normalSkillsPoints / skillsKeys.length; // perfectly disjoint is 1
  }

  launchSkillsComplement(otherLaunchSkills = []) {
    let skillsPoints = 0.0;
    const { launchSkills = [] } = this;

    const comparator = (a, b) => a.localeCompare(b);
    launchSkills.sort(comparator);
    otherLaunchSkills.sort(comparator);

    const smaller = launchSkills.length < otherLaunchSkills.length ? launchSkills : otherLaunchSkills;
    const larger = launchSkills.length < otherLaunchSkills.length ? otherLaunchSkills : launchSkills;

    for (let i = 0, r = smaller.length; i < r; i += 1) {
      if (bs(larger, smaller[i], comparator) < 0) {
        skillsPoints += 1;
      }
    }

    return skillsPoints / larger.length;
  }

  interestOverlap(otherInterests) {
    let interestsPoints = 0.0;
    const comparator = (a, b) => a.localeCompare(b);

    const { interests } = this;

    if (interests.length === 0 || otherInterests.length === 0) {
      return 0;
    }

    interests.sort(comparator);

    otherInterests.sort(comparator);

    const smaller = interests.length < otherInterests.length ? interests : otherInterests;
    const larger = interests.length < otherInterests.length ? otherInterests : interests;

    for (let i = 0, r = smaller.length; i < r; i += 1) {
      if (bs(larger, smaller[i], comparator) > -1) {
        interestsPoints += 1;
      }
    }

    return interestsPoints / larger.length;
  }

  ideaOverlap(otherIdeas) {
    let problemInterestOverlap = 0.0;

    const comparator = (a, b) => a.id - b.id;
    const { ideas } = this;
    ideas.sort(comparator);

    otherIdeas.sort(comparator);

    // should be same size, but if not do smaller
    const smaller = ideas.length < otherIdeas.length ? ideas : otherIdeas;
    const larger = ideas.length < otherIdeas.length ? otherIdeas : ideas;

    for (let i = 0; i < smaller.length; i += 1) {
      const myIdea = smaller[i];
      const otherIdeaPos = bs(larger, myIdea, comparator);

      if (otherIdeaPos > -1) {
        const otherIdea = larger[otherIdeaPos];
        const sentiment = myIdea.strength;
        const otherSentiment = otherIdea.strength;

        problemInterestOverlap += 3.0 - Math.abs(sentiment - otherSentiment);
      }
    }

    if (this.ideas.length > 0) {
      return problemInterestOverlap / (3 * larger.length); // max of larger length
    }

    return 0;
  }

  // generate a percentage out of 1 that we scale
  roleComplement(role) {
    // return 1 if complement and 0 if not ?
    if (this.role !== role) {
      return 1;
    }

    return 0;
  }

  genderComplement(gender) {
    // return 1 if complement and 0 if not ?
    if (this.gender !== gender) {
      return 1;
    }

    return 0;
  }

  ethnicityComplement(ethnicity) {
    // return 1 if complement and 0 if not ?
    if (this.ethnicity !== ethnicity) {
      return 1;
    }

    return 0;
  }

  entrepreneurTypeComplement(otherETIn) {
    const comparator = (a, b) => a.localeCompare(b);
    let etPoints = 0;

    const ET = this.entrepreneurType.split(', ');
    ET.sort(comparator);

    const otherET = otherETIn.split(', ');
    otherET.sort(comparator);

    const smaller = ET.length < otherET.length ? ET : otherET;
    const larger = ET.length < otherET.length ? otherET : ET;

    for (let i = 0; i < smaller.length; i += 1) {
      if (bs(larger, smaller[i], comparator) < 0) {
        // if et type not found in other, add a point
        etPoints += 1;
      }
    }

    // max et points is the smaller size

    return etPoints / smaller.length; // how perfectly disjoint
  }

  scoreAgainst(otherStudent) {
    let points = 0.0;

    const teamingConfig = Object.assign({}, defaultTeamingConfig, this.p_teamingConfig);

    points += teamingConfig.rolePoints * this.roleComplement(otherStudent.role);
    // points += teamingConfig.entrepreneurTypePoints * this.entrepreneurTypeComplement(otherStudent.entrepreneurType);
    points += teamingConfig.entrepreneurTypePoints * this.launchSkillsComplement(otherStudent.launchSkills);
    points += teamingConfig.skillsPoints * this.skillsComplement(otherStudent.skills);
    points += teamingConfig.learningStylePoints * this.learningStyleOverlap(otherStudent.learningStyle);
    points += teamingConfig.workStylePoints * this.workStyleCompliment(otherStudent.workStyle);
    points += teamingConfig.personalityPoints * this.personalityComplement(otherStudent.personalityType);
    points += teamingConfig.genderPoints * this.genderComplement(otherStudent.gender);
    points += teamingConfig.ethnicityPoints * this.genderComplement(otherStudent.ethnicity);

    // interest overlap
    points += teamingConfig.interestPoints * this.interestOverlap(otherStudent.interests);

    // Idea overlap
    points += teamingConfig.ideaPoints * this.ideaOverlap(otherStudent.ideas);


    return points;
  }
}
