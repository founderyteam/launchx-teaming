/* eslint-disable no-console */
/* eslint-disable no-continue */
import Student from './student';
import Team from './team';

// basically stars and bars
// we have students and some seperators for them
// we need an inital estimate tho

export default class GreedyMatch {
  // public
  constructor(students, teamingConfig, debug = false) {
    this.students = students.map(student => new Student(student, teamingConfig));
    this.debug = debug;
    this.numVisited = 0;

    this.initializeAdjacencyMatrix();

    if (this.debug) { console.log('DPMatch: Removing duplicates...'); }

    this.removeDuplicatesStudents();
  }

  getStudents() {
    return this.students;
  }

  initializeAdjacencyMatrix() {
    this.scoredAgainst = [];
    for (let i = 0; i < this.students.length; i += 1) {
      this.scoredAgainst.push([]);
      for (let j = 0; j < this.students.length; j += 1) {
        if (i !== j) {
          this.scoredAgainst[i].push(this.students[i].scoreAgainst(this.students[j]));
        }
      }
    }
  }

  team({ minimumStudentsPerTeam = 3, maximumStudentsPerTeam = 4 }) {
    // gen perms
    // dp this shiz
    this.minimumStudentsPerTeam = minimumStudentsPerTeam;
    this.maximumStudentsPerTeam = maximumStudentsPerTeam;
    this.solutionPath = [];
    this.visited = this.students.map(() => false);
    this.numTeams = 0;

    if (this.debug) { console.log('GreedyMatch: teaming...'); console.time('GreedyMatch'); }
    this.visited[0] = true;
    this.solutionPath.push([0]);
    this.numVisited += 1;
    this.numTeams += 1;
    this.numFilledTeams = 0;
    while (this.numVisited < this.students.length) {
      this.addNextStudent();
    }
    if (this.debug) { console.log('GreedyMatch: done...'); console.timeEnd('GreedyMatch'); }

    if (this.debug) { console.log('GreedyMatch: generating teams from match...'); }
    return this.pathToTeams(this.solutionPath);
  }

  // private
  removeDuplicatesStudents() {
    const dupArray = {};
    this.getStudents().forEach((student) => {
      dupArray[student.hash] = student;
    });

    this.students = Object.values(dupArray);
  }

  addNextStudent() {
    const studentIndex = this.nextUnvisitedStudent();

    let greatestIncreaseIndex = -1;
    let greatestIncrease = -1;
    const maxNumberOfRemainingTeams = Math.floor((this.students.length - this.numVisited) / this.minimumStudentsPerTeam);
    for (let i = 0; i < this.numTeams + maxNumberOfRemainingTeams; i += 1) {
      if (i < this.solutionPath.length && this.solutionPath[i].length < this.maximumStudentsPerTeam) {
        // test ass student
        const scoreBefore = this.scoreTeam(this.solutionPath[i]);
        this.solutionPath[i].push(studentIndex);
        const scoreAfter = this.scoreTeam(this.solutionPath[i]);
        this.solutionPath[i].pop();

        if (scoreAfter > scoreBefore && (scoreAfter - scoreBefore > greatestIncrease)) {
          greatestIncreaseIndex = i;
          greatestIncrease = scoreAfter - scoreBefore;
        }
      }
    }


    if (greatestIncrease === -1) {
      // if more teams than max teams, rematch
      if (this.numTeams >= this.numFilledTeams + maxNumberOfRemainingTeams) {
        // find team with least decrease
        let bestTeamIndex = 0;
        let leastDecrease = -1;
        for (let i = 0; i < this.solutionPath.length; i += 1) {
          if (this.solutionPath[i].length < this.maximumStudentsPerTeam) {
            // test ass student
            const scoreBefore = this.scoreTeam(this.solutionPath[i]);
            this.solutionPath[i].push(studentIndex);
            const scoreAfter = this.scoreTeam(this.solutionPath[i]);
            this.solutionPath[i].pop();

            if (leastDecrease === -1 || scoreBefore - scoreAfter < leastDecrease) {
              leastDecrease = scoreBefore - scoreAfter;
              bestTeamIndex = i;
            }
          }
        }

        this.solutionPath[bestTeamIndex].push(studentIndex);
        if (this.solutionPath[bestTeamIndex].length >= this.maximumStudentsPerTeam) {
          this.numFilledTeams += 1;
        }
      } else {
        this.solutionPath.push([studentIndex]);
        this.numTeams += 1;
      }
    } else {
      this.solutionPath[greatestIncreaseIndex].push(studentIndex);
      if (this.solutionPath[greatestIncreaseIndex].length >= this.maximumStudentsPerTeam) {
        this.numFilledTeams += 1;
      }
    }


    this.visited[studentIndex] = true;
    this.numVisited += 1;
  }

  nextUnvisitedStudent() {
    let i = 0;
    while (this.visited[i]) { i += 1; }

    return i;
  }

  pathToTeams(solutionPath) {
    const teams = [];
    for (let i = 0; i < solutionPath.length; i += 1) {
      const team = solutionPath[i].map(j => this.students[j]);
      teams.push(new Team(team));
    }

    return teams;
  }

  calculateScore(solutionPath, permLength) {
    let firstBarIndex = -1;
    let nextBarIndex = 0;

    let score = 0;
    const findFunc = (v, i) => i > (nextBarIndex) && v.type === 'bar';
    while (nextBarIndex !== permLength) {
      firstBarIndex = nextBarIndex;
      nextBarIndex = solutionPath.findIndex(findFunc);
      if (nextBarIndex === -1 || nextBarIndex > permLength) { nextBarIndex = permLength; }

      const team = [];
      for (let i = firstBarIndex + 1; i < nextBarIndex; i += 1) {
        team.push(solutionPath[i].student);
      }

      score += this.scoreTeam(team);
    }

    return score;
  }

  scoreTeam(team) {
    // pairwise team all members against each other
    if (team.length <= 1) {
      return 0;
    }

    let score = 0;
    let numPerms = 0;
    for (let i = 0; i < team.length; i += 1) {
      for (let j = i + 1; j < team.length; j += 1) {
        score += this.scoreStudents(team[i], team[j]);
        numPerms += 1;
      }
    }

    return score / numPerms;
  }

  scoreStudents(a, b) {
    return this.scoredAgainst[a][b];
  }

  bestMatch(studentIndex) {
    let bestElement = -1;
    let bestScore = 0;
    for (let i = 0; i < this.students.length; i += 1) {
      if (i !== studentIndex) {
        const score = this.scoredAgainst[studentIndex][i];
        if (score > bestScore) {
          bestElement = i;
          bestScore = score;
        }
      }
    }

    return bestElement;
  }
}
