/* eslint-disable no-console */
/* eslint-disable no-continue */
import Student from './student';
import Team from './team';

// basically stars and bars
// we have students and some seperators for them
// we need an inital estimate tho

export default class KKMatch {
  // public
  constructor(students, teamingConfig, debug = false) {
    this.students = students.map(student => new Student(student, teamingConfig));
    this.debug = debug;

    this.initializeAdjacencyMatrix();

    if (this.debug) { console.log('KKMatch: Removing duplicates...'); }

    this.removeDuplicatesStudents();
  }

  getStudents() {
    return this.students;
  }

  initializeUnvisitedSet() {
    this.setU = this.students.map(() => true);
    this.numVisited = 0;
  }

  formCluster(vertex) {
    let bestCluster = this.formClusterOfSize(vertex, this.minimumStudentsPerTeam);
    let bestClusterScore = this.scoreTeam(bestCluster);
    for (let i = this.minimumStudentsPerTeam + 1; i < this.maximumStudentsPerTeam + 1; i += 1) {
      const cluster = this.formClusterOfSize(vertex, i);
      const score = this.scoreTeam(cluster);
      if (score > bestClusterScore) {
        bestCluster = cluster;
        bestClusterScore = score;
      }
    }

    return bestCluster;
  }

  formClusterOfSize(vertex, s) {
    const setS = [vertex];

    // find s - 1 closest verticies in U test both min up to max
    // so find s - 1 verticies with largest edgewise scores
    // s in this case is the minimum

    for (let i = 0; i < s - 1; i += 1) {
      let mostOptimalEdge = -1;
      let mostOptimalScore = -1;

      for (let j = 0; j < this.setU.length; j += 1) {
        if (this.setU[j] === true && j !== vertex && setS.indexOf(j) === -1) {
          // for each unvisited vertex j where j != vertex
          // find most optimal edge
          const score = this.scoredAgainst[vertex][j];
          if (score > mostOptimalScore) {
            mostOptimalScore = score;
            mostOptimalEdge = j;
          }
        }
      }

      if (mostOptimalEdge > -1) {
        setS.push(mostOptimalEdge);
      }
    }

    return setS;
  }

  // formCluster (vertex) {
  //   const setS = [vertex];

  //   // find s - 1 closest verticies in U
  //   // so find s - 1 verticies with largest edgewise scores
  //   // s in this case is the minimum
  //   const s = this.minimumStudentsPerTeam;

  //   for(let i = 0; i < s - 1; i += 1) {
  //     let mostOptimalEdge = -1;
  //     let mostOptimalScore = -1;

  //     for(let j = 0; j < this.setU.length; j += 1) {
  //       if(this.setU[j] === true && j !== vertex && setS.indexOf(j) === -1) {

  //         // for each unvisited vertex j where j != vertex
  //         // find most optimal edge
  //         const score = this.scoredAgainst[vertex][j];
  //         if(score > mostOptimalScore) {
  //           mostOptimalScore = score;
  //           mostOptimalEdge = j;
  //         }
  //       }
  //     }

  //     setS.push(mostOptimalEdge);
  //   }

  //   return setS;
  // }

  visitVertex(v) {
    this.setU[v] = false;
    this.numVisited += 1;
  }

  initializeClusters() {
    this.clusters = [];
  }

  // farthest in this case means minimal compatibility
  // dont forget about pidgenhole
  edgewiseFarthestUnvisitedVerticies() {
    if (this.numVisited === this.students.length - 1) {
      // pidgenhole
      return [];
    }

    let lowestScore = -1;
    let lowestI = -1;
    let lowestJ = -1;
    // traverse all unvisited verticies
    for (let i = 0; i < this.setU.length; i += 1) {
      if (this.setU[i] === true) {
        // if unvisited
        // pairwise traverse other unvisited in adjacency matrix
        for (let j = 0; j < this.scoredAgainst[i].length; j += 1) {
          const score = this.scoredAgainst[i][j];
          if (this.setU[j] === true && i !== j) {
            // if this is also unvisisted
            if (score < lowestScore || lowestScore === -1) {
              lowestScore = score;
              lowestI = i;
              lowestJ = j;
            }
          }
        }
      }
    }

    return [lowestI, lowestJ];
  }

  removeSetFromU(set) {
    for (let i = 0; i < set.length; i += 1) {
      this.visitVertex(set[i]);
    }
  }

  initializeAdjacencyMatrix() {
    this.scoredAgainst = [];
    for (let i = 0; i < this.students.length; i += 1) {
      this.scoredAgainst.push([]);
      for (let j = 0; j < this.students.length; j += 1) {
        if (i !== j) {
          this.scoredAgainst[i].push(this.students[i].scoreAgainst(this.students[j]));
        } else {
          this.scoredAgainst[i].push(-1);
        }
      }
    }
  }

  determineDistribution() {
    // this function will preteam all the unvisited members if they are over a certain standard
    // in this instance, that standard is... entrepreneur type

    // first, detect if this is neccesary
    // we will use 2/5 as an example

    const unvisited = this.allUnvisitedVerticies();
    const allEntrepreneurTypes = unvisited.map(studentID => this.students[studentID].launchSkills);
    const uniqueEntrepreneurTypes = Object.keys(
      allEntrepreneurTypes.reduce(
        (hashmap, types) => {
          types.forEach((type) => {
          // eslint-disable-next-line no-param-reassign
            hashmap[type] = true;
          });
          return hashmap;
        }, {},
      ),
    );

    const amountPerType = uniqueEntrepreneurTypes.map((type) => {
      let amt = 0;
      for (let i = 0; i < allEntrepreneurTypes.length; i += 1) {
        const studentTypes = allEntrepreneurTypes[i] || [];
        if (studentTypes.indexOf(type) > -1) {
          amt += 1;
        }
      }

      return { type, amt };
    });

    amountPerType.sort((a, b) => a.amt - b.amt);

    const topType = amountPerType.pop();
    if (topType.amt < (2 / 5) * this.students.length) {
      // ok we dont need to distribute
      return;
    }

    console.log('distributing...');
    // get all of the students with that type
    const studentsOfType = unvisited.filter(
      studentID => this.students[studentID].launchSkills.indexOf(topType.type) > -1,
    );

    // distribute and visit these students
    while (studentsOfType.length > 0 && this.clusters.length <= (this.students.length / 2.0)) {
      const studentID = studentsOfType.pop();
      this.clusters.push([studentID]);
      this.visitVertex(studentID);
    }

    // if there are over half however,
    // then we must distribute multiple to same team, so we can just let the alg do this
  }

  team({ minimumStudentsPerTeam = 3, maximumStudentsPerTeam = 4 }) {
    // gen perms
    // dp this shiz
    this.minimumStudentsPerTeam = minimumStudentsPerTeam;
    this.maximumStudentsPerTeam = maximumStudentsPerTeam;

    this.initializeUnvisitedSet();
    this.initializeClusters();

    if (this.debug) { console.log('KKMatch: teaming...'); console.time('KKMatch'); }

    // this.determineDistribution();

    while (this.numVisited < this.students.length) {
      this.approximate();
    }

    if (this.debug) { console.log('KKMatch: done...'); console.timeEnd('KKMatch'); }

    if (this.debug) { console.log('KKMatch: generating teams from match...'); }
    return this.pathToTeams(this.clusters);
  }

  allUnvisitedVerticies() {
    const unvisited = [];
    for (let i = 0; i < this.students.length; i += 1) {
      if (this.setU[i] === true) {
        unvisited.push(i);
      }
    }

    return unvisited;
  }

  closestNeighbor(node) {
    // find closest neighbor that doesn't break constraints
    let closestNode = -1;
    let farthestScore = 0;
    let closestCluster = -1;
    const adjacentToNode = this.scoredAgainst[node];
    for (let i = 0; i < adjacentToNode.length; i += 1) {
      if (i !== node) {
        // all nodes not i
        const score = adjacentToNode[i];
        if (closestNode === -1 || score > farthestScore) {
          const cluster = this.findCluster(i);
          if (cluster !== -1 && this.clusters[cluster].length < this.maximumStudentsPerTeam) {
            closestNode = i;
            closestCluster = cluster;
            farthestScore = score;
          }
        }
      }
    }

    return { closestNeighbor: closestNode, closestCluster };
  }

  findCluster(node) {
    for (let i = 0; i < this.clusters.length; i += 1) {
      for (let j = 0; j < this.clusters[i].length; j += 1) {
        if (this.clusters[i][j] === node) {
          return i;
        }
      }
    }

    return -1;
  }

  calcNumVisited() {
    return this.students.length - this.allUnvisitedVerticies().length;
  }

  matchLast() {
    // const numUnvisited = this.setU.length - this.numVisited;
    // console.log(`unvisited: ${numUnvisited}`);
    const unvisited = this.allUnvisitedVerticies();

    if (unvisited.length <= 0) {
      return;
    }

    if (this.maximumStudentsPerTeam === this.minimumStudentsPerTeam) {
      // trim the difference into a new team
      const last = unvisited.pop();
      if (last) {
        this.clusters.push(last);
        this.visitVertex(last);
      }
    }

    for (let i = 0; i < unvisited.length; i += 1) {
      const { closestCluster } = this.closestNeighbor(unvisited[i]);
      // console.log(`closest to ${unvisited[i]}: ${closestCluster}`);
      if (closestCluster !== -1) {
        this.clusters[closestCluster].push(unvisited[i]);
        this.visitVertex(unvisited[i]);
      }
    }
  }

  approximate() {
    const mostDistantVerticies = this.edgewiseFarthestUnvisitedVerticies(); // step 2
    const clusterA = this.formCluster(mostDistantVerticies[0]);
    this.removeSetFromU(clusterA);
    const clusterB = this.formCluster(mostDistantVerticies[1]);
    this.removeSetFromU(clusterB);

    this.clusters.push(clusterA);
    this.clusters.push(clusterB);

    let numUnvisited = this.setU.length - this.numVisited;

    if (numUnvisited >= this.minimumStudentsPerTeam
        && numUnvisited < 2 * this.minimumStudentsPerTeam
        && this.minimumStudentsPerTeam !== this.maximumStudentsPerTeam) {
      // this is now a cluster
      const unvisitedRaw = this.allUnvisitedVerticies();
      const cluster = this.formCluster(unvisitedRaw[0]);

      this.clusters.push(cluster);
      this.removeSetFromU(cluster);

      numUnvisited = this.setU.length - this.numVisited;

      if (numUnvisited >= 0) {
        this.matchLast();
      }
    } else if (numUnvisited < this.minimumStudentsPerTeam) {
      // numVisited is less than minimum
      // nearest neighbor the overflow
      this.matchLast();
    }
  }

  // private
  removeDuplicatesStudents() {
    const dupArray = {};
    this.getStudents().forEach((student) => {
      dupArray[student.hash] = student;
    });

    this.students = Object.values(dupArray);
  }

  addNextStudent() {
    const studentIndex = this.nextUnvisitedStudent();

    let greatestIncreaseIndex = -1;
    let greatestIncrease = -1;
    const maxNumberOfRemainingTeams = Math.floor((this.students.length - this.numVisited)
                                        / this.minimumStudentsPerTeam);
    for (let i = 0; i < this.numTeams + maxNumberOfRemainingTeams; i += 1) {
      if (i < this.solutionPath.length
          && this.solutionPath[i].length < this.maximumStudentsPerTeam) {
        // test ass student
        const scoreBefore = this.scoreTeam(this.solutionPath[i]);
        this.solutionPath[i].push(studentIndex);
        const scoreAfter = this.scoreTeam(this.solutionPath[i]);
        this.solutionPath[i].pop();

        if (scoreAfter > scoreBefore && (scoreAfter - scoreBefore > greatestIncrease)) {
          greatestIncreaseIndex = i;
          greatestIncrease = scoreAfter - scoreBefore;
        }
      }
    }


    if (greatestIncrease === -1) {
      // if more teams than max teams, rematch
      if (this.numTeams >= this.numFilledTeams + maxNumberOfRemainingTeams) {
        // find team with least decrease
        let bestTeamIndex = 0;
        let leastDecrease = -1;
        for (let i = 0; i < this.solutionPath.length; i += 1) {
          if (this.solutionPath[i].length < this.maximumStudentsPerTeam) {
            // test ass student
            const scoreBefore = this.scoreTeam(this.solutionPath[i]);
            this.solutionPath[i].push(studentIndex);
            const scoreAfter = this.scoreTeam(this.solutionPath[i]);
            this.solutionPath[i].pop();

            if (leastDecrease === -1 || scoreBefore - scoreAfter < leastDecrease) {
              leastDecrease = scoreBefore - scoreAfter;
              bestTeamIndex = i;
            }
          }
        }

        this.solutionPath[bestTeamIndex].push(studentIndex);
        if (this.solutionPath[bestTeamIndex].length >= this.maximumStudentsPerTeam) {
          this.numFilledTeams += 1;
        }
      } else {
        this.solutionPath.push([studentIndex]);
        this.numTeams += 1;
      }
    } else {
      this.solutionPath[greatestIncreaseIndex].push(studentIndex);
      if (this.solutionPath[greatestIncreaseIndex].length >= this.maximumStudentsPerTeam) {
        this.numFilledTeams += 1;
      }
    }


    this.visited[studentIndex] = true;
    this.numVisited += 1;
  }

  nextUnvisitedStudent() {
    let i = 0;
    while (this.visited[i]) { i += 1; }

    return i;
  }

  pathToTeams(solutionPath) {
    const teams = [];
    for (let i = 0; i < solutionPath.length; i += 1) {
      const team = solutionPath[i].map(j => this.students[j]);
      teams.push(new Team(team));
    }

    return teams;
  }

  calculateScore(solutionPath, permLength) {
    let firstBarIndex = -1;
    let nextBarIndex = 0;

    let score = 0;
    const findFunc = (v, i) => i > (nextBarIndex) && v.type === 'bar';
    while (nextBarIndex !== permLength) {
      firstBarIndex = nextBarIndex;
      nextBarIndex = solutionPath.findIndex(findFunc);
      if (nextBarIndex === -1 || nextBarIndex > permLength) { nextBarIndex = permLength; }

      const team = [];
      for (let i = firstBarIndex + 1; i < nextBarIndex; i += 1) {
        team.push(solutionPath[i].student);
      }

      score += this.scoreTeam(team);
    }

    return score;
  }

  scoreTeam(team) {
    // pairwise team all members against each other
    if (team.length <= 1) {
      return 0;
    }

    let score = 0;
    let numPerms = 0;
    for (let i = 0; i < team.length; i += 1) {
      for (let j = i + 1; j < team.length; j += 1) {
        score += this.scoreStudents(team[i], team[j]);
        numPerms += 1;
      }
    }

    return score / numPerms;
  }

  scoreStudents(a, b) {
    return this.scoredAgainst[a][b];
  }

  bestMatch(studentIndex) {
    let bestElement = -1;
    let bestScore = 0;
    for (let i = 0; i < this.students.length; i += 1) {
      if (i !== studentIndex) {
        const score = this.scoredAgainst[studentIndex][i];
        if (score > bestScore) {
          bestElement = i;
          bestScore = score;
        }
      }
    }

    return bestElement;
  }
}
