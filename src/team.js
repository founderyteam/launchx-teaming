/* eslint-disable no-param-reassign */
/* eslint-disable no-underscore-dangle */
import { StudentPairSet, default as Student, StudentSet } from './student';

export default class Team {
  constructor(students) {
    this._students = students;
  }

  get students() {
    return this._students;
  }

  names() {
    return this._students.map(student => student._json.name);
  }

  set idea(newValue) {
    this._idea = newValue;
  }

  get idea() {
    return this._idea;
  }

  getTopIdeas(studentIdeas) {
    // get all the ideas of all the students
    // figure out the ids that the students do not share.
    // yay a set difference

    // const masterSet = this.determineMasterSet();

    // // baked student ideas into [] of hashMaps for O(1)
    // let studentIdeas = this.students.map(student => student.ideas);
    // studentIdeas = studentIdeas.map(ideas => ideas.reduce((hashMap, idea) => {
    //   hashMap[idea.id] = idea.strength;
    //   return hashMap;
    // }, {}));

    // for each master set key, see if student doesnt have the key
    // let ownIdeas = {};
    // for (let i = 0; i < masterSet.length; i += 1) {
    //   const key = masterSet[i];
    //   for (let j = 0; j < studentIdeas.length; j += 1) { // should be team size
    //     if (!studentIdeas[j][key]) {
    //       ownIdeas[key] = true;
    //     }
    //   }
    // }

    let ownIdeas = this.students.map(student => studentIdeas
      .map((i, k) => k)
      .filter(
        ideaId => studentIdeas[ideaId].thinker.email === (student.p_json.email || student.p_json.name),
      )
      .map(k => studentIdeas[k].id));

    ownIdeas = Object.keys(ownIdeas).reduce((prev, next) => prev.concat(ownIdeas[next]), []);

    // ownIdeas = Object.keys(ownIdeas);
    // rank by compatibility
    const ownIdeaTeamRanks = {};
    for (let i = 0; i < ownIdeas.length; i += 1) {
      const id = ownIdeas[i];
      let score = 4; // someone on the team made it
      for (let k = 0; k < this.students.length; k += 1) {
        for (let g = 0; g < this.students[k].ideas.length; g += 1) {
          if (this.students[k].ideas[g].id === id) {
            score += this.students[k].ideas[g].strength;
          }
        }
      }

      score /= this.students.length;
      ownIdeaTeamRanks[id] = score;
    }

    // sort by rank
    let ownIdeasArr = ownIdeas;
    const shuffle = (array) => {
      let currentIndex = array.length;
      let temporaryValue;
      let randomIndex;

      // While there remain elements to shuffle...
      while (currentIndex !== 0) {
        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
      }

      return array;
    };

    ownIdeasArr = shuffle(ownIdeasArr);
    ownIdeasArr.sort((a, b) => ownIdeaTeamRanks[b] - ownIdeaTeamRanks[a]);

    this.best5 = ownIdeasArr.slice(0, 5);
    return this.best5;
  }

  get bestFiveIdeas() {
    if (!this.best5) {
      this.getTopIdeas(this.p_ideas);
    }

    return this.best5;
  }

  getStudentOwnIdeas(studentIdeas) {
    if (!this.p_studentOwnIdeas) {
      // const masterSet = this.determineMasterSet();

      // // baked student ideas into [] of hashMaps for O(1)
      // let studentIdeas = this.students.map(student => student.ideas);
      // studentIdeas = studentIdeas.map(ideas => ideas.reduce((hashMap, idea) => {
      //   hashMap[idea.id] = idea.strength;
      //   return hashMap;
      // }, {}));

      // // for each master set key, see if student doesnt have the key
      // let ownIdeas = this.students.map(() => ({}));
      // for (let i = 0; i < masterSet.length; i += 1) {
      //   const key = masterSet[i];
      //   for (let j = 0; j < studentIdeas.length; j += 1) { // should be team size
      //     if (!studentIdeas[j][key]) {
      //       ownIdeas[j][key] = true;
      //     }
      //   }
      // }

      const ownIdeas = this.students.map(student => studentIdeas
        .map((i, k) => Number(k))
        .filter(
          ideaId => studentIdeas[ideaId].thinker.email === (student.p_json.email || student.p_json.name),
        )
        .map(k => studentIdeas[k].id));

      this.p_studentOwnIdeas = ownIdeas;
    }

    return this.p_studentOwnIdeas;
  }

  get studentOwnIdeas() {
    return this.getStudentOwnIdeas(this.p_ideas);
  }

  determineMasterSet() {
    const masterSet = {};
    for (let i = 0; i < this.students.length; i += 1) {
      for (let j = 0; j < this.students[i].ideas.length; j += 1) {
        const { id } = this.students[i].ideas[j];
        masterSet[id] = true;
      }
    }

    return Object.keys(masterSet);
  }

  score() {
    // pairwise team all members against each other
    if (this.students.length <= 1) {
      return 0;
    }

    let score = 0;
    let numPerms = 0;
    for (let i = 0; i < this.students.length; i += 1) {
      for (let j = i + 1; j < this.students.length; j += 1) {
        score += this.students[i].scoreAgainst(this.students[j]);
        numPerms += 1;
      }
    }

    return score / numPerms;
  }

  static score(students) {
    return new Team(students).score();
  }
}
