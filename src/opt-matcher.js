/* eslint-disable no-console */
/* eslint-disable no-continue */
import Student from './student';
import Team from './team';

// basically stars and bars
// we have students and some seperators for them
// we need an inital estimate tho

export default class OPTMatch {
  // public
  constructor(students, teamingConfig, debug = false) {
    this.students = students.map(student => new Student(student, teamingConfig));
    this.debug = debug;
    this.numVisited = 0;

    this.initializeAdjacencyMatrix();

    if (this.debug) { console.log('DPMatch: Removing duplicates...'); }

    this.removeDuplicatesStudents();
  }

  getStudents() {
    return this.students;
  }

  initializeAdjacencyMatrix() {
    this.scoredAgainst = [];
    for (let i = 0; i < this.students.length; i += 1) {
      this.scoredAgainst.push([]);
      for (let j = 0; j < this.students.length; j += 1) {
        if (i != j) {
          this.scoredAgainst[i].push(this.students[i].scoreAgainst(this.students[j]));
        }
      }
    }
  }

  team({ minimumStudentsPerTeam = 3, maximumStudentsPerTeam = 4 }) {
    // gen perms
    // dp this shiz
    this.minimumStudentsPerTeam = minimumStudentsPerTeam;
    this.maximumStudentsPerTeam = maximumStudentsPerTeam;

    if (this.debug) { console.log('DPMatch: Generating initial path...'); }
    // this.generateInitialSolutionPath();
    this.initializeTour();
    this.solutionPath = this.bestSolutionPath;

    if (this.debug) { console.log('DPMatch: teaming...'); console.time('DPMatcher'); }
    while (this.numVisited < this.students.length) {
      this.insertSelection(this.selectTour());
    }
    if (this.debug) { console.log('DPMatch: done...'); console.timeEnd('DPMatcher'); }

    if (this.debug) { console.log('DPMatch: generating teams from match...'); }
    return this.pathToTeams(this.bestSolutionPath);
  }

  // private
  removeDuplicatesStudents() {
    const dupArray = {};
    this.getStudents().forEach((student) => {
      dupArray[student.hash] = student;
    });

    this.students = Object.values(dupArray);
  }

  // generateInitialSolutionPath() {
  //   const initialNumberOfBars = Math.floor(this.getStudents().length / this.maximumStudentsPerTeam);
  //   this.bestSolutionPath = [];
  //   this.getStudents().forEach((student, index) => {
  //     this.bestSolutionPath.push({ type: 'student', student: index });
  //   });

  //   // shuffle
  //   for (let i = 0; i < 50; i += 1) {
  //     // swap two random indexes
  //     const i1 = Math.floor(Math.random() * this.getStudents().length) % this.getStudents().length;
  //     const i2 = Math.floor(Math.random() * this.getStudents().length) % this.getStudents().length;
  //     const temp = this.bestSolutionPath[i1];
  //     this.bestSolutionPath[i1] = this.bestSolutionPath[i2];
  //     this.bestSolutionPath[i2] = temp;
  //   }

  //   for (let i = 0; i < initialNumberOfBars; i += 1) {
  //     const index = ((i + 1) * this.maximumStudentsPerTeam) + i;
  //     const swapVal = Object.assign({}, this.bestSolutionPath[index]);
  //     this.bestSolutionPath[index] = { type: 'bar' };
  //     this.bestSolutionPath.push(swapVal);
  //   }

  //   // find out remainder
  //   const remainderStudents = this.getStudents().length % initialNumberOfBars;
  //   if (remainderStudents < this.minimumStudentsPerTeam) {
  //     let lastIndex = this.bestSolutionPath.length;
  //     for (let i = 0; i < remainderStudents; i += 1) {
  //       // move last bar back by remained students
  //       // find last bar index
  //       for (let j = lastIndex - 1; j > 0; j -= 1) {
  //         if (this.bestSolutionPath[j].type === 'bar') {
  //           lastIndex = j;
  //           break;
  //         }
  //       }

  //       for (let k = 0; k < this.minimumStudentsPerTeam - remainderStudents - i; k += 1) {
  //         this.bestSolutionPath = DPMatch.swap(this.bestSolutionPath, lastIndex, lastIndex - k - 1);
  //       }
  //     }
  //   }
  //   // this.bestSolutionPath.push({ type: 'bar' });
  // }

  initializeTour() {
    // start with a team of one
    this.visited = [];
    for (let i = 0; i < this.students.length; i += 1) {
      this.visited[i] = false;
    }

    this.bestSolutionPath = [[0]];
    this.numTeams = 1;
    this.visited[0] = true;
    // find optimal pairing

    const bestMatch = this.bestMatch(0);
    this.bestSolutionPath[0].push(bestMatch);
    this.numVisited = 2;
    this.visited[bestMatch] = true;
  }

  insertSelection(sel) {
    // find optimal team to insert
    let insertionPoint = -1;
    let replacementPoint = -1;
    let currentCost = 0;

    const testTeam = (currentTeam, i, r) => {
      const cost = this.scoreTeam(currentTeam);
      if (cost > currentCost) {
        currentCost = cost;
        insertionPoint = i;
        replacementPoint = r;
      }
    };

    for (let i = 0; i < this.numTeams; i += 1) {
      const currentTeam = Array.from(this.bestSolutionPath[i]);

      if (currentTeam.length < this.maximumStudentsPerTeam) {
        currentTeam.push(sel);
        testTeam(currentTeam, i, -1);
      } else {
        // replace each and try
        for (let j = 0; j < currentTeam.length; j += 1) {
          const oldElement = currentTeam[j];
          currentTeam[j] = sel;
          testTeam(currentTeam, i, j);
          currentTeam[j] = oldElement;
        }

        // but if main team is better, push to new
        const cost = this.scoreTeam(currentTeam);
        if (cost >= currentCost && insertionPoint === i) {
          // skip
          insertionPoint = -1;
          replacementPoint = -1;
        }
      }
    }

    if (insertionPoint === -1) {
      if (this.numTeams >= Math.floor(this.students.length / this.minimumStudentsPerTeam)) {
        // add to random insert
        insertionPoint = 0;
        do {
          insertionPoint += 1;
        } while (this.bestSolutionPath[insertionPoint].length >= this.maximumStudentsPerTeam);
      } else {
        this.bestSolutionPath.push([sel]);
        this.numTeams += 1;
      }
    } else if (replacementPoint > -1) {
      const replacedElement = this.bestSolutionPath[insertionPoint][replacementPoint];
      this.bestSolutionPath[insertionPoint][replacementPoint] = sel;

      this.visited[replacedElement] = false;
      this.numVisited -= 1;
    } else {
      this.bestSolutionPath[insertionPoint].push(sel);
    }

    this.visited[sel] = true;
    this.numVisited += 1;
  }

  selectTour() {
    let randomSelect = 0;
    do {
      randomSelect = Math.floor(Math.random() * this.students.length) % this.students.length;
    } while (this.visited[randomSelect]);

    return randomSelect;
  }

  pathToTeams(solutionPath) {
    const teams = [];
    for (let i = 0; i < solutionPath.length; i += 1) {
      const team = solutionPath[i].map(j => this.students[j]);
      teams.push(new Team(team));
    }

    return teams;
  }

  calculateScore(solutionPath, permLength) {
    let firstBarIndex = -1;
    let nextBarIndex = 0;

    let score = 0;
    const findFunc = (v, i) => i > (nextBarIndex) && v.type === 'bar';
    while (nextBarIndex !== permLength) {
      firstBarIndex = nextBarIndex;
      nextBarIndex = solutionPath.findIndex(findFunc);
      if (nextBarIndex === -1 || nextBarIndex > permLength) { nextBarIndex = permLength; }

      const team = [];
      for (let i = firstBarIndex + 1; i < nextBarIndex; i += 1) {
        team.push(solutionPath[i].student);
      }

      score += this.scoreTeam(team);
    }

    return score;
  }

  scoreTeam(team) {
    // pairwise team all members against each other
    if (team.length <= 1) {
      return 0;
    }

    let score = 0;
    let numPerms = 0;
    for (let i = 0; i < team.length; i += 1) {
      for (let j = i + 1; j < team.length; j += 1) {
        score += this.scoreStudents(team[i], team[j]);
        numPerms += 1;
      }
    }

    return score / numPerms;
  }

  scoreStudents(a, b) {
    return this.scoredAgainst[a][b];
  }

  bestMatch(studentIndex) {
    let bestElement = -1;
    let bestScore = 0;
    for (let i = 0; i < this.students.length; i += 1) {
      if (i !== studentIndex) {
        const score = this.scoredAgainst[studentIndex][i];
        if (score > bestScore) {
          bestElement = i;
          bestScore = score;
        }
      }
    }

    return bestElement;
  }
}
