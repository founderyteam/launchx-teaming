/* eslint-disable no-console */
/* eslint-disable no-continue */
import Student from './student';
import Team from './team';

// basically stars and bars
// we have students and some seperators for them
// we need an inital estimate tho

export default class DPMatch {
  // public
  constructor(students, teamingConfig, debug = false) {
    this.students = students.map(student => new Student(student, teamingConfig));
    this.debug = debug;

    this.initializeAdjacencyMatrix();

    if (this.debug) { console.log('DPMatch: Removing duplicates...'); }

    this.removeDuplicatesStudents();
  }

  getStudents() {
    return this.students;
  }

  initializeAdjacencyMatrix() {
    this.scoredAgainst = [];
    for (let i = 0; i < this.students.length; i += 1) {
      this.scoredAgainst.push([]);
      for (let j = 0; j < this.students.length; j += 1) {
        if (i != j) {
          this.scoredAgainst[i].push(this.students[i].scoreAgainst(this.students[j]));
        }
      }
    }
  }

  team({ minimumStudentsPerTeam = 3, maximumStudentsPerTeam = 4 }) {
    // gen perms
    // dp this shiz
    this.minimumStudentsPerTeam = minimumStudentsPerTeam;
    this.maximumStudentsPerTeam = maximumStudentsPerTeam;

    this.lowerBound = 0;
    if (this.debug) { console.log('DPMatch: Generating initial path...'); }
    this.generateInitialSolutionPath();
    this.solutionPath = this.bestSolutionPath;

    if (this.debug) { console.log('DPMatch: calculating initial upper bound...'); }
    this.upperBound = this.calculateUpperBound(this.bestSolutionPath, 0);

    if (this.debug) { console.log('DPMatch: teaming...'); console.time('DPMatcher'); }
    this.genPerms(0);
    if (this.debug) { console.log('DPMatch: done...'); console.timeEnd('DPMatcher'); }

    if (this.debug) { console.log('DPMatch: generating teams from match...'); }
    return this.pathToTeams(this.bestSolutionPath);
  }

  // private
  removeDuplicatesStudents() {
    const dupArray = {};
    this.getStudents().forEach((student) => {
      dupArray[student.hash] = student;
    });

    this.students = Object.values(dupArray);
  }

  generateInitialSolutionPath() {
    const initialNumberOfBars = Math.floor(this.getStudents().length / this.maximumStudentsPerTeam);
    this.bestSolutionPath = [];
    this.getStudents().forEach((student, index) => {
      this.bestSolutionPath.push({ type: 'student', student: index });
    });

    // shuffle
    for (let i = 0; i < 50; i += 1) {
      // swap two random indexes
      const i1 = Math.floor(Math.random() * this.getStudents().length) % this.getStudents().length;
      const i2 = Math.floor(Math.random() * this.getStudents().length) % this.getStudents().length;
      const temp = this.bestSolutionPath[i1];
      this.bestSolutionPath[i1] = this.bestSolutionPath[i2];
      this.bestSolutionPath[i2] = temp;
    }

    for (let i = 0; i < initialNumberOfBars; i += 1) {
      const index = ((i + 1) * this.maximumStudentsPerTeam) + i;
      const swapVal = Object.assign({}, this.bestSolutionPath[index]);
      this.bestSolutionPath[index] = { type: 'bar' };
      this.bestSolutionPath.push(swapVal);
    }

    // find out remainder
    const remainderStudents = this.getStudents().length % initialNumberOfBars;
    if (remainderStudents < this.minimumStudentsPerTeam) {
      let lastIndex = this.bestSolutionPath.length;
      for (let i = 0; i < remainderStudents; i += 1) {
        // move last bar back by remained students
        // find last bar index
        for (let j = lastIndex - 1; j > 0; j -= 1) {
          if (this.bestSolutionPath[j].type === 'bar') {
            lastIndex = j;
            break;
          }
        }

        for (let k = 0; k < this.minimumStudentsPerTeam - remainderStudents - i; k += 1) {
          this.bestSolutionPath = DPMatch.swap(this.bestSolutionPath, lastIndex, lastIndex - k - 1);
        }
      }
    }
    // this.bestSolutionPath.push({ type: 'bar' });
  }

  pathToTeams(solutionPath) {
    let firstBarIndex = 0;
    let nextBarIndex = 0;
    const teams = [];
    const findFunc = (v, i) => i > (nextBarIndex) && v.type === 'bar';
    while (nextBarIndex !== solutionPath.length) {
      firstBarIndex = nextBarIndex;
      nextBarIndex = solutionPath.findIndex(findFunc);
      if (nextBarIndex === -1 || nextBarIndex > solutionPath.length) { nextBarIndex = solutionPath.length; }

      const team = [];
      for (let i = firstBarIndex + 1; i < nextBarIndex; i += 1) {
        team.push(this.students[solutionPath[i].student]);
      }

      teams.push(new Team(team));
    }

    return teams;
  }

  isSolution(solutionPath) {
    // check for minimum and maximum
    for (let i = 0; i < solutionPath.length; i += 1) {
      if (solutionPath[i].type === 'bar') {
        // count til next bar
        let count = 1;
        while (i + count < solutionPath.length
              && solutionPath[i + count].type !== 'bar') {
          count += 1;
        }
        count -= 1;

        if (count > this.maximumStudentsPerTeam || count < this.minimumStudentsPerTeam) {
          return false;
        }
      }
    }

    return true;
  }

  isValid(solutionPath, permLength) {
    // check for minimum and maximum

    if (permLength === 0) {
      return true;
    }

    for (let i = 0; i < Math.min(this.minimumStudentsPerTeam, permLength); i += 1) {
      if (solutionPath[i].type === 'bar') {
        return false;
      }
    }

    if (permLength <= this.maximumStudentsPerTeam) {
      return true;
    }

    for (let i = 0; i < permLength; i += 1) {
      if (solutionPath[i].type === 'bar' || i === 0) {
        // count til next bar
        let count = 1;
        while (i + count < permLength
              && solutionPath[i + count].type !== 'bar') {
          count += 1;
        }
        count -= 1;

        if ((count > this.maximumStudentsPerTeam
            || count < this.minimumStudentsPerTeam)
            && i <= permLength - this.maximumStudentsPerTeam) {
          return false;
        }
      }
    }

    return true;
  }

  calculateScore(solutionPath, permLength) {
    let firstBarIndex = -1;
    let nextBarIndex = 0;

    let score = 0;
    const findFunc = (v, i) => i > (nextBarIndex) && v.type === 'bar';
    while (nextBarIndex !== permLength) {
      firstBarIndex = nextBarIndex;
      nextBarIndex = solutionPath.findIndex(findFunc);
      if (nextBarIndex === -1 || nextBarIndex > permLength) { nextBarIndex = permLength; }

      const team = [];
      for (let i = firstBarIndex + 1; i < nextBarIndex; i += 1) {
        team.push(solutionPath[i].student);
      }

      score += this.scoreTeam(team);
    }

    return score;
  }

  scoreTeam(team) {
    // pairwise team all members against each other
    if (team.length <= 1) {
      return 0;
    }

    let score = 0;
    let numPerms = 0;
    for (let i = 0; i < team.length; i += 1) {
      for (let j = i + 1; j < team.length; j += 1) {
        score += this.scoreStudents(i, j);
        numPerms += 1;
      }
    }

    return score / numPerms;
  }

  isPromising(solutionPath, permLength) {
    // calculate upper bound

    if (!this.isValid(solutionPath, permLength)) {
      return false;
    }

    // estimate solutionPath score;
    const currentScore = this.calculateScore(solutionPath, permLength);

    // calculate maximum estimate for rest of thing
    // find best pairwise compatibility, then multiply by number of teams remaining?
    const estimate = this.calculateEstimate(solutionPath, permLength);

    const currentEstimate = currentScore + estimate;

    if (currentEstimate < this.lowerBound || currentEstimate > this.upperBound) {
      return false;
    }

    return true;
  }

  static swap(solutionPath, a, b) {
    const newSolPath = solutionPath;
    const temp = Object.assign({}, newSolPath[a]);
    newSolPath[a] = solutionPath[b];
    newSolPath[b] = temp;

    return newSolPath;
  }

  calculateUpperBound(solutionPath, permLength = 0) {
    return this.calculateEstimate(solutionPath, permLength);
  }

  calculateEstimate(solutionPath, permLength = 0) {
    // find best pairwise compatibility in remaining set
    let bestScore = 0;
    const maxRemainingTeams = Math.ceil((solutionPath.length - permLength) / this.minimumStudentsPerTeam);

    for (let i = permLength; i < solutionPath.length; i += 1) {
      if (solutionPath[i].type === 'bar') {
        continue;
      }

      const studentOne = solutionPath[i].student;
      for (let j = i + 1; j < solutionPath.length; j += 1) {
        if (solutionPath[j].type === 'bar') {
          continue;
        }

        const score = this.scoreStudents(solutionPath[j].student, studentOne);
        if (score > bestScore) {
          bestScore = score;
        }
      }
    }

    return maxRemainingTeams * bestScore;
  }

  scoreStudents(a, b) {
    return this.scoredAgainst[a][b];
  }

  genPerms(permLength) {
    if (permLength === this.solutionPath.length) {
      if (this.isSolution(this.solutionPath)) {
        const score = this.calculateScore(this.solutionPath);
        if (score > this.lowerBound) {
          this.lowerBound = score;
          this.bestSolutionPath = this.solutionPath;
          if (this.debug) { console.log(`DPMatch: found a solution (${score})`); }
        }
      }
    }

    if (!this.isPromising(this.solutionPath, permLength)) {
      return;
    }

    for (let i = permLength; i < this.solutionPath.length; i += 1) {
      this.solutionPath = DPMatch.swap(this.solutionPath, permLength, i);
      this.genPerms(permLength + 1);
      this.solutionPath = DPMatch.swap(this.solutionPath, permLength, i);
    }
  }
}
