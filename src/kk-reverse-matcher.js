/* eslint-disable class-methods-use-this */
/* eslint-disable no-console */
/* eslint-disable no-continue */
import Student from './student';
import Team from './team';

/*

Operations needed:
- determine number of teams
- determine if U can be broken into N teams of size minS or maxS
- incremement N
- is Set U empty?
- arbitrarily select team from set B
- find member from set U with best score
- find member from set U with best matching to team
- add member to team
- move team from set B to set A
- move member from set U to set V
- find member from set V with best score not in team
- find member from set V with best matching to team not in team
- find team of member b
- move team from set B to set A

*/

export default class KKMatchReverse {
  // public
  constructor(students, teamingConfig, debug = false) {
    this.students = students.map(student => new Student(student, teamingConfig));
    this.debug = debug;

    this.initializeAdjacencyMatrix();

    if (this.debug) { console.log('KKMatch: Removing duplicates...'); }

    this.removeDuplicatesStudents();
  }

  getStudents() {
    return this.students;
  }

  determineNumberOfTeams() {
    const numberOfStudents = this.getStudents().length;
    const maximumNumberOfTeams = Math.floor(numberOfStudents / this.minimumStudentsPerTeam);
    const mimimumNumberOfTeams = Math.ceil(numberOfStudents / this.maximumStudentsPerTeam);

    const difference = maximumNumberOfTeams - mimimumNumberOfTeams;

    return Math.round(this.balanceFactor * difference) + mimimumNumberOfTeams;
  }

  canBeDistributed(numberOfTeams, minS, maxS) {
    // can this number of teams be the sum of multiple numbers divisible
    // by a number between the min and max

    const avgStudentsPerTeam = (this.students.length / numberOfTeams);
    return avgStudentsPerTeam <= maxS && avgStudentsPerTeam >= minS;

    // const potentialDivisors = [];
    // for (let i = minS; i < maxS + 1; i += 1) {
    //   potentialDivisors.push(i);
    // }
    // potentialDivisors.sort();

    // // we have [3, 4, 5] for instance

    // const potentialDivisor = potentialDivisors.pop(); // biggest divisor

    // if (numberOfTeamsCopy % potentialDivisor === 0) {
    //   return true;
    // }

    // let newNumberOfTeams = numberOfTeamsCopy - potentialDivisor;
    // while (newNumberOfTeams > 0) {
    //   const remainder = (newNumberOfTeams % potentialDivisor) + potentialDivisor;

    //   // is remainder divisible by any other divisors
    //   for (let i = 0; i < potentialDivisors.length; i += 1) {
    //     if (remainder % potentialDivisors[i] === 0) {
    //       return true;
    //     }
    //   }

    //   newNumberOfTeams -= potentialDivisor;
    // }

    // return false;
  }

  incremementN(numberOfTeams) {
    if (this.balanceFactor < 0.5) {
      return numberOfTeams - 1;
    }

    return numberOfTeams + 1;
  }

  initializeSets() {
    this.visitedStudents = this.getStudents().map(() => false);

    this.theTeams = [];
    this.teamIsFull = [];
    for (let i = 0; i < this.numberOfTeams; i += 1) {
      this.theTeams.push([]);
      this.teamIsFull.push(false);
    }
  }

  hasUnvisitedStudents() {
    for (let i = 0; i < this.visitedStudents.length; i += 1) {
      if (this.visitedStudents[i] === false) {
        return true;
      }
    }

    return false;
  }

  sizeOfTeam(team) {
    return this.theTeams[team].length;
  }

  arbitrarilySelectTeamFromSetB() {
    const sizeOfSetB = this.teamIsFull.length;
    let arbitrarySelectionIndex;
    do {
      arbitrarySelectionIndex = Math.round(Math.random() * (sizeOfSetB - 1));
    } while (
      this.teamIsFull[arbitrarySelectionIndex]
    );

    return arbitrarySelectionIndex;
  }

  memberWithBestScoreInSetU() {
    // since its empty it must be random
    let randomStudent;
    do {
      randomStudent = Math.round((this.visitedStudents.length - 1) * Math.random());
    } while (this.visitedStudents[randomStudent] === true);

    return randomStudent;
  }

  memberWithBestMatchingInSetU(team) {
    let bestMember = -1;
    let bestScore = -1;
    for (let i = 0; i < this.visitedStudents.length; i += 1) {
      if (!this.visitedStudents[i]) {
        // student is unvisited
        const teamCopy = Array.from(this.theTeams[team]);
        teamCopy.push(i);
        const score = this.scoreTeam(teamCopy);

        if (score > bestScore) {
          bestScore = score;
          bestMember = i;
        }
      }
    }

    if (bestMember === -1) {
      throw new Error('No unvisited students');
    }

    return bestMember;
  }

  addMemberToTeam(member, team) {
    this.theTeams[team].push(member);
  }

  moveMemberFromSetUToSetV(member) {
    this.visitedStudents[member] = true;
  }

  moveTeamFromSetBToSetA(team) {
    this.teamIsFull[team] = true;
  }

  moveTeamFromSetAToSetB(team) {
    this.teamIsFull[team] = false;
  }

  runGreedyInsertionStep() {
    const teamInQuestion = this.arbitrarilySelectTeamFromSetB();

    let selectedStudent = -1;
    if (this.sizeOfTeam(teamInQuestion) === 0) {
      selectedStudent = this.memberWithBestScoreInSetU();
    } else {
      selectedStudent = this.memberWithBestMatchingInSetU(teamInQuestion);
    }

    if (selectedStudent === -1) {
      throw new Error('There should be students left but there are not');
    }

    this.addMemberToTeam(selectedStudent, teamInQuestion);
    this.moveMemberFromSetUToSetV(selectedStudent);

    if (this.sizeOfTeam(teamInQuestion) >= this.maximumStudentsPerTeam) {
      this.moveTeamFromSetBToSetA(teamInQuestion);
    }
  }

  hasTeamsInSetBUnderMinimum() {
    for (let i = 0; i < this.teamIsFull.length; i += 1) {
      if (!this.teamIsFull[i] && this.theTeams[i].length < this.minimumStudentsPerTeam) {
        return true;
      }
    }

    return false;
  }

  memberWithBestScoreInVOnTeamInA() {
    let randomTeam;
    do {
      randomTeam = Math.round((this.teamIsFull.length - 1) * Math.random());
    } while (this.teamIsFull[randomTeam] === false);

    // now team should be a full team
    const randomStudent = Math.round((this.theTeams[randomTeam].length - 1) * Math.random());

    return this.theTeams[randomTeam][randomStudent];
  }

  isMemberOnTeam(member, team) {
    for (let i = 0; i < this.theTeams[team].length; i += 1) {
      if (this.theTeams[team][i] === member) {
        return true;
      }
    }

    return false;
  }

  findTeamInAOfMemberInV(member) {
    for (let i = 0; i < this.theTeams.length; i += 1) {
      if (this.teamIsFull[i]) {
        // team is a full team
        if (this.isMemberOnTeam(member, i)) {
          return i;
        }
      }
    }

    return -1;
  }

  removeMemberFromTeam(member, team) {
    const theTeam = this.theTeams[team];
    const theNewTeam = [];
    for (let i = 0; i < theTeam.length; i += 1) {
      if (theTeam[i] !== member) {
        theNewTeam.push(theTeam[i]);
      }
    }

    this.theTeams[team] = theNewTeam;
  }

  memberWithBestMatchingInVOnTeamInANotOnTeam(team) {
    let bestMember = -1;
    let bestScore = -1;
    for (let i = 0; i < this.visitedStudents.length; i += 1) {
      if (this.visitedStudents[i] && this.findTeamInAOfMemberInV(i) !== -1 && !this.isMemberOnTeam(i, team)) {
        // student is visited and not on team team
        const teamCopy = Array.from(this.theTeams[team]);
        teamCopy.push(i);
        const score = this.scoreTeam(teamCopy);

        if (score > bestScore) {
          bestScore = score;
          bestMember = i;
        }
      }
    }

    if (bestMember === -1) {
      throw new Error('No unvisited students');
    }

    return bestMember;
  }

  greedilyFixTeams() {
    for (let i = 0; i < this.teamIsFull.length; i += 1) {
      if (!this.teamIsFull[i] && this.theTeams[i].length < this.minimumStudentsPerTeam) {
        // we have a team that needs to be fixied

        while (this.theTeams[i].length < this.minimumStudentsPerTeam) {
          let selectedStudent = -1;
          if (this.theTeams[i].length === 0) {
            selectedStudent = this.memberWithBestScoreInVOnTeamInA();
          } else {
            selectedStudent = this.memberWithBestMatchingInVOnTeamInANotOnTeam(i);
          }

          if (selectedStudent === -1) {
            throw new Error('Something is wrong');
          }

          const selectedStudentTeam = this.findTeamInAOfMemberInV(selectedStudent);
          if (selectedStudentTeam === -1) {
            console.log(this.theTeams[i].length);
            throw new Error('Something is wrong team');
          }

          this.removeMemberFromTeam(selectedStudent, selectedStudentTeam);
          this.moveTeamFromSetAToSetB(selectedStudentTeam);
          this.addMemberToTeam(selectedStudent, i);
          if (this.sizeOfTeam(i) >= this.maximumStudentsPerTeam) {
            this.moveTeamFromSetBToSetA(i);
          }
        }
      }
    }
  }

  team({ minimumStudentsPerTeam = 3, maximumStudentsPerTeam = 4, balanceFactor = 3 }) {
    this.minimumStudentsPerTeam = minimumStudentsPerTeam;
    this.maximumStudentsPerTeam = maximumStudentsPerTeam;
    this.balanceFactor = balanceFactor / 10.0;

    if (this.debug) { console.log('KKMatchReverse: teaming...'); console.time('KKMatchReverse'); }

    this.numberOfTeams = this.determineNumberOfTeams();
    while (!this.canBeDistributed(
      this.numberOfTeams,
      this.minimumStudentsPerTeam,
      this.maximumStudentsPerTeam,
    )) {
      this.numberOfTeams = this.incremementN(this.numberOfTeams);
    }

    this.initializeSets();
    while (this.hasUnvisitedStudents()) {
      this.runGreedyInsertionStep();
    }

    while (this.hasTeamsInSetBUnderMinimum()) {
      this.greedilyFixTeams();
    }


    if (this.debug) { console.log('KKMatchReverse: done...'); console.timeEnd('KKMatchReverse'); }

    if (this.debug) { console.log('KKMatchReverse: generating teams from match...'); }
    return this.pathToTeams(this.theTeams);
  }

  pathToTeams(solutionPath) {
    const teams = [];
    for (let i = 0; i < solutionPath.length; i += 1) {
      const team = solutionPath[i].map(j => this.students[j]);
      teams.push(new Team(team));
    }

    return teams;
  }


  // private
  removeDuplicatesStudents() {
    const dupArray = {};
    this.getStudents().forEach((student) => {
      dupArray[student.hash] = student;
    });

    this.students = Object.values(dupArray);
  }

  scoreTeam(team) {
    // pairwise team all members against each other
    if (team.length <= 1) {
      return 0;
    }

    let score = 0;
    let numPerms = 0;
    for (let i = 0; i < team.length; i += 1) {
      for (let j = i + 1; j < team.length; j += 1) {
        score += this.scoreStudents(team[i], team[j]);
        numPerms += 1;
      }
    }

    return score / numPerms;
  }

  scoreStudents(a, b) {
    return this.scoredAgainst[a][b];
  }

  initializeAdjacencyMatrix() {
    this.scoredAgainst = [];
    for (let i = 0; i < this.students.length; i += 1) {
      this.scoredAgainst.push([]);
      for (let j = 0; j < this.students.length; j += 1) {
        if (i !== j) {
          this.scoredAgainst[i].push(this.students[i].scoreAgainst(this.students[j]));
        } else {
          this.scoredAgainst[i].push(-1);
        }
      }
    }
  }
}
