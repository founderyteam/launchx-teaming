import {
  StudentPairSet, StudentSet, SortedStudentSet, default as Student,
} from './student';
import Team from './team';

const greedyPairing = (students, studentsPerTeam, minPerTeam, maxStudentsPerTeam) => {
  const studentArray = Array.from(students);
  let mutableStudents = studentArray.map(student => student.p_hash);
  const studentHashes = studentArray.reduce((prev, curr) => {
    prev[curr.p_hash] = curr;
    return prev;
  }, {});

  // if we have say... 10 students and 3 per team then there is one left over. Lob onto the previous
  // if we have say... 11 students and 3 per team then there is two left over, distribute to the previous
  // if we have say... 11 students and 4 per team then there is 2 teams and three left over so make a team
  const totalStudents = mutableStudents.length;
  const leftOver = totalStudents % studentsPerTeam;
  console.log('Left Over ', leftOver);
  let teamSize = Math.floor(totalStudents / studentsPerTeam);
  if (leftOver >= minPerTeam) {
    console.log('spillover');
    teamSize = Math.floor(totalStudents / studentsPerTeam) + 1;
  } else if (leftOver + studentsPerTeam > maxStudentsPerTeam) {
    teamSize += 1;
  }

  console.log('Number of teams ', teamSize);

  const teams = mutableStudents.slice(0, teamSize).map(student => [student]);
  mutableStudents = mutableStudents.slice(teamSize);

  for (let i = studentsPerTeam - 1; i > 0; i--) {
    for (let teamId = 0, totalTeams = teams.length; teamId < totalTeams; teamId++) {
      const team = teams[teamId];
      let bestScore = -1;
      let bestStudentId = null;
      let bestTeam = null;

      for (let studentId = 0; studentId < Math.floor(mutableStudents.length / i); studentId++) {
        const student = mutableStudents[studentId];
        const newTeam = team.concat([student]);
        const mappedNewTeam = newTeam.map(studentHash => studentHashes[studentHash]);
        let score = Team.score(mappedNewTeam);
        score = score < 0 ? 0 : score;

        if (score > bestScore) {
          bestScore = score;
          bestTeam = newTeam;
          bestStudentId = studentId;
        }
      }

      if (bestTeam !== null) {
        teams[teamId] = Array.from(bestTeam);
      }
      if (bestStudentId !== null) {
        const pre = mutableStudents.length;
        mutableStudents = mutableStudents.slice(0, bestStudentId).concat(mutableStudents.slice(bestStudentId + 1));
      }
    }
  }

  if (mutableStudents.length > 0) {
    // distribute
    for (let studentId = 0; studentId < mutableStudents.length; studentId++) {
      const student = mutableStudents[studentId];
      let bestTeam = null;
      let bestTeamId = -1;
      let bestScore = -1;

      for (let teamId = 0, totalTeams = teams.length; teamId < totalTeams; teamId++) {
        const team = teams[teamId];

        const newTeam = team.concat([student]);
        const mappedNewTeam = newTeam.map(studentHash => studentHashes[studentHash]);
        let score = Team.score(mappedNewTeam);
        score = score < 0 ? 0 : score;

        if (score > bestScore) {
          bestScore = score;
          bestTeam = newTeam;
          bestTeamId = teamId;
        }
      }

      if (bestTeam !== null) {
        teams[bestTeamId] = Array.from(bestTeam);
      }
    }
  }

  return teams.map(team => new Team(team.map(studentHash => studentHashes[studentHash])));
};

const pairIdeasWithTeams = (teams) => {
  for (let teamId = 0, r = teams.length; teamId < r; teamId++) {
    const team = teams[teamId];
    const ideas = team.students.reduce((old, curr) => old.concat(curr.ideas), []);

    let bestIdeaScore = 0;
    let bestIdeaId = null;

    for (let ideaId = 0, totIdeas = ideas.length; ideaId < totIdeas; ideaId++) {
      const idea = ideas[ideaId];
      let score = -1;
      team.students.forEach((student) => {
        const compat = Team.ideaCompatability(student, idea);
        if (!compat) score -= 1000;
        score += compat;
      });

      if (score > bestIdeaScore) {
        bestIdeaScore = score;
        bestIdeaId = ideaId;
      }
    }

    if (bestIdeaId !== null) teams[teamId].idea = ideas[bestIdeaId];
  }

  return teams;
};

export default class Match {
  constructor(students, teamingConfig) {
    this._students = new Set(students.map(student => new Student(student, teamingConfig)));
  }

  team(timing = false, studentsPerTeam = 3, minPerTeam = 3, maxStudentsPerTeam = 4) {
    if (timing === true) console.time('team');

    const teams = greedyPairing(this._students, studentsPerTeam, minPerTeam, maxStudentsPerTeam);

    if (timing === true) console.timeEnd('team');

    return teams;
  }

  static test(teams) {
    const students = [];
    for (const team of teams) {
      for (const student of team._students) {
        students.push(student);
      }
    }

    let faultCount = 0;
    for (const student1 of students) {
      for (const student2 of students) {
        if (student1.p_hash === student2.p_hash) {
          console.log('duplicate: ', student1.p_json.name);
          faultCount += 1;
        }
      }
    }
    faultCount -= students.length;
    return faultCount;
  }
}

function randomVoteForIdea(idea) {
  const newIdea = Object.assign({}, idea);
  newIdea.vote = ['strong_yes', 'yes', 'no', 'strong_no'][Math.round(Math.random() * 3)];
  newIdea.strength = ['strong_yes', 'yes', 'no', 'strong_no'].indexOf(newIdea.vote);
  return newIdea;
}

export function generateRandomStudents(numStudents, ideas, names) {
  const students = [];
  for (let i = 0; i < numStudents; i += 1) {
    const ids = ideas.map(idea => randomVoteForIdea(idea));
    const studentJson = {
      learningStyle: ['abcd', 'cdef'][Math.round(Math.random())],
      workStyle: ['abcd', 'cdef', 'abcd/cdef', 'abcd/efg'][Math.round(Math.random() * 3)],
      name: names[i],
      interests: [
        'abcdefg'[Math.round(Math.random() * 6)],
        'abcdefg'[Math.round(Math.random() * 6)],
        'abcdefg'[Math.round(Math.random() * 6)],
        'abcdefg'[Math.round(Math.random() * 6)],
        'abcdefg'[Math.round(Math.random() * 6)],
      ],
      role: ['a', 'b', 'c', 'd'][Math.round(Math.random() * 3)],
      launchSkills: [['a', 'b', 'c', 'd'][Math.round(Math.random() * 3)]],
      skills: {
        graphicDesign: Math.round(Math.random() * 9),
        making: Math.round(Math.random() * 9),
        marketing: Math.round(Math.random() * 9),
        operations: Math.round(Math.random() * 9),
        programming: Math.round(Math.random() * 9),
        selling: Math.round(Math.random() * 9),
      },
      gender: ['Male', 'Female'][Math.round(Math.random())],
      ethnicity: ['Male', 'Female'][Math.round(Math.random())],
      entrepreneurType: 'Confidence, Independence, Risk, Knowledge',
      personalityType: ['E', 'I'][Math.round(Math.random())]
                        + ['N', 'S'][Math.round(Math.random())]
                        + ['T', 'F'][Math.round(Math.random())]
                        + ['P', 'J'][Math.round(Math.random())],
      ideas: ids,
    };
    students.push(studentJson);
  }
  return students;
}

export function generateRandomIdeas(numIdeas, emails) {
  const ideas = [];
  for (let i = 0; i < numIdeas; i++) {
    ideas.push({
      name: Math.random().toString(36).substr(2, 5),
      id: i,
      thinker: {
        email: emails[Math.round(Math.random() * emails.length)],
      },
      vote: ['strong_yes', 'yes', 'no', 'strong_no'][Math.round(Math.random() * 3)],
    });
  }

  return ideas;
}
