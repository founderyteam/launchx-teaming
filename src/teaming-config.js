// const config = {
//   studentsPerTeam: {
//     name: 'Students Per Team',
//     default: 4.0,
//   },
//   minimumStudentsPerTeam: {
//     name: 'Minimum Students Per Team',
//     default: 3.0,
//   },
//   maximumStudentsPerTeam: {
//     name: 'Maximum Students Per Team',
//     default: 4.0,
//   },
//   learningStyle: {
//     name: 'Learning Style',
//     default: 5.0,
//   },
//   skillsAbilityPoints: {
//     name: 'Skill Ability Points (Programming, Marketing, etc)',
//     default: 3.0,
//   },
//   interestPoints: {
//     name: 'Interest Overlap Points',
//     default: 5.0,
//   },
//   interestPointsMax: {
//     name: 'Interest Overlap Points Limit',
//     default: 15.0,
//   },
//   commonIdeaPoints: {
//     name: 'Common Idea Points',
//     default: 1.0,
//   },
//   workStyle: {
//     name: 'Work Style Complement Points',
//     default: 5.0,
//   },
//   EP10Type: {
//     name: 'EP10 Complement Points',
//     default: 5.0,
//   },
//   personalityType: {
//     name: 'Personality Complement Points',
//     default: 5.0,
//   },
//   entrepreneurPoints: {
//     name: 'Entrepreneurial Skills Points Per Complement',
//     default: 5.0,
//   },
//   skillsPoints: {
//     name: 'Skills Points Per Complement',
//     default: 5.0,
//   },
// };

const config = {
  studentsPerTeam: {
    name: 'Maximum Students Per Team',
    default: 4.0,
  },
  minimumStudentsPerTeam: {
    name: 'Minimum Students Per Team',
    default: 3.0,
  },
  balanceFactor: {
    name: 'Balance (Between 0 and 10) 0 means bigger teams',
    default: 3,
  },
  rolePoints: {
    name: 'Points For Role Complement',
    default: 15.0,
  },
  entrepreneurTypePoints: {
    name: 'Points for Entrepreneur Type Complement',
    default: 5.0,
  },
  skillsPoints: {
    name: 'Points for Skills Complement',
    default: 10.0,
  },
  learningStylePoints: {
    name: 'Learning Style Overlap',
    default: 5.0,
  },
  workStylePoints: {
    name: 'Points for Work Style Complement',
    default: 5.0,
  },
  personalityPoints: {
    name: 'Points for Personality Complement',
    default: 8.0,
  },
  genderPoints: {
    name: 'Points for Gender Complement',
    default: 10.0,
  },
  ethnicityPoints: {
    name: 'Points for Ethnicity Complement',
    default: 10.0,
  },
  interestPoints: {
    name: 'Points for Interests Overlap',
    default: 12.0,
  },
  ideaPoints: {
    name: 'Points for Idea Overlap',
    default: 40.0,
  },
};

export default config;
